<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ include file="/WEB-INF/views/include/lib.jsp"%>
<spring:message code="device" var="device"></spring:message>
<spring:message code="cycle" var="cycle"></spring:message>
<spring:message code="ophour" var="ophour"></spring:message>
<spring:message code="confirm" var="confirm"></spring:message>

<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<meta http-equiv="X-UA-Compatible" content="chrome=1">
<meta http-equiv="cache-control" content="max-age=0" />
<meta http-equiv="cache-control" content="no-cache" />
<meta http-equiv="expires" content="0" />
<meta http-equiv="expires" content="Tue, 01 Jan 1980 1:00:00 GMT" />
<meta http-equiv="pragma" content="no-cache" />
<link rel="shortcut icon" href="${ctxPath }/images/dulink_favicon.ico">
<script type="text/javascript" src="${ctxPath }/js/jqMap.js"></script>
<title>Dash Board</title>
<script type="text/javascript">
	var ctxPath = "${ctxPath}";
	var targetWidth = 3840;
	var targetHeight = 2160;

	var originWidth = window.innerWidth;
	var originHeight = window.innerHeight;

	var contentWidth = originWidth;
	var contentHeight = targetHeight / (targetWidth / originWidth);

	var screen_ratio = getElSize(240);

	if (originHeight / screen_ratio < 9) {
		contentWidth = targetWidth / (targetHeight / originHeight)
		contentHeight = originHeight;
	};

	function getElSize(n) {
		return contentWidth / (targetWidth / n);
	};

	function setElSize(n) {
		return targetWidth / (contentWidth / n);
	};
	
	var marginWidth = (originWidth-contentWidth)/2;
	var marginHeight = (originHeight-contentHeight)/2;
</script>
<style type="text/css">
	.selected_span{
	background-color :white;
	color : #008900;
}
	
</style>
<script src="${ctxPath }/js/jquery-1.11.2.min.js"></script>
<script src="${ctxPath }/js/jquery-ui.js"></script>
<script src="${ctxPath}/js/highstock.js"></script>
<link rel="stylesheet" href="${ctxPath }/css/default.css">
<style type="text/css">
	.disabledIcon{
		filter: grayscale(100%);
		-webkit-filter: grayscale(100%);
	}
	
	.tr_table_fix_header
{
 position: relative;
 top: expression(this.offsetParent.scrollTop);
 z-index: 20;
}
</style>
<script type="text/javascript">
var shopId = 1;
var save_type = "init"
function undoLimit(){
	for(var key in initMap.map){
		if (initMap.map.hasOwnProperty(key)) {
			$("#" + key).val(initMap.map[key])
		}
		$("#save, #undo").addClass("disabledIcon");
	}
};

var changedLimit = [];
function saveLimit(){
	changedLimit = [];
	for(var key in initMap.map){
		if (initMap.map.hasOwnProperty(key)) {
			if(initMap.map[key]!=currentInitMap.map[key]){
				var array = [key, currentInitMap.map[key]]
				changedLimit.push(array)
			}
		}
	}
	
	setLimit();
};

var limitIdx = 0;
function setLimit(){
	var limit = changedLimit[limitIdx];
	var dvcId = $("#dvcId").val();
	var item = limit[0].substr(0, limit[0].lastIndexOf("__"));
	var portNo = limit[0].substr(limit[0].lastIndexOf("__")+2);
	var value = limit[1];
	
	if(item=="rn_tm_lmt_sec"){
		value *= 3600;
	}
	
	var url = "${ctxPath}/chart/setLimit.do";
	var param = "dvcId=" + dvcId + 
				"&item=" + item + 
				"&portNo=" + portNo + 
				"&value=" + value;
	
	
	$.ajax({
		url : url,
		data : param,
		type : "post",
		success : function(){
			limitIdx++;
			if(limitIdx<changedLimit.length){
				setLimit();	
			}else{
				limitIdx = 0;
				$("#save, #undo").addClass("disabledIcon");
				getToolList();
				$("#saveDiv").css("z-index", -10);
			}
		}
	});
};

function bindEvt(){
	$("#table input").focus(function(){
		preEl = $(this);
		preVal = $(this).val();
		$(this).val("")
	});
	
	$("#table input").blur(function(){
		if($(this).val()=="") preEl.val(preVal)
 	});
	
};

function setCalcTy(){
	var url = "${ctxPath}/chart/setCalcTy.do";
	var isCheck = $("#onlySpdLoad").prop("checked");
	if(isCheck){
		isCheck = 1;
	}else{
		isCheck = 0;
	}
	
	var param = "dvcId=" + $("#dvcId").val() + 
				"&calcTy=" + $("input[name='lifeCalcStandard']:checked").val() + 
				"&isSpd=" + isCheck;
	
	console.log(param)
	$.ajax({
		url : url,
		data : param,
		type : "post",
		dataType : "text",
		success : function(data){
			if(data=="success"){
				$("#calcStandards").css("display", "none")
			}
		}		
	});
};

var preVal = "";
var preEl = "";
$(function(){
	bindEvt();
	$("#undo").click(undoLimit);
	$("#save").click(chkSave);
	$(".excel").click(csvSend);
	setEl();
	setDate();
	
	$("#menu_btn").click(function(){
		location.href = "${ctxPath}/chart/index.do"
	});
	
	$(".menu").click(goReport);
	$("#panel_table td").addClass("unSelected_menu");
	$("#menu6").removeClass("unSelected_menu");
	$("#menu6").addClass("selected_menu");
	
	//getMousePos();
	getDvcList();
	$(".date").change(getToolList);
	$("#dvcId").change(getToolList);
});

var shopId = 1;
function getDvcList(){
	var url = "${ctxPath}/chart/getDvcNameList.do";
	var param = "shopId=" + shopId + 
				"&line=ALL" + 
				"&fromDashboard=true" + 
				"&sDate=" + $("#sDate").val() + 
				"&eDate=" + $("#sDate").val() + " 23:59:59";
	
	
	$.ajax({
		url : url,
		data : param,
		type : "post",
		dataType : "json",
		success : function(data){
			var options = "";
			var json = data.dataList;
			
			$(json).each(function(idx, data){
				options += "<option value='" + data.id + "'>" + decodeURIComponent(data.name).replace(/\+/gi," ") + "</option>" 
			});		
			
			$("#dvcId").html(options);
			getToolList();
		}, error : function(e1,e2,e3){
			console.log(e1)
		}
	});
	
}; 

function getCalcTy(){
	var url = "${ctxPath}/chart/getCalcTy.do";
	var param = "dvcId=" + $("#dvcId").val();
	
	$.ajax({
		url : url,
		data : param,
		type : "post",
		dataType : "json",
		success : function(data){
			if(data.isSpd=="0" || data.isSpd==null){
				$("#onlySpdLoad").prop("checked", false)
			}else{
				$("#onlySpdLoad").prop("checked", true)
			}
			
			if(data.calcTy==1 || data.calcTy==0){
				$("input[name='lifeCalcStandard']:nth(0)").prop("checked", true)
			}else{
				$("input[name='lifeCalcStandard']:nth(1)").prop("checked", true)
			}
		}
	});
};

function getSpdLoadInfo(){
	$("#dvcName").html($("#dvcId option:selected").text());

	var url = "${ctxPath}/chart/getSpdLoadInfo.do";
	var param = "dvcId=" + $("#dvcId").val();
	
	$.ajax({
		url : url,
		data : param,
		type : "post",
		dataType : "json",
		success : function(data){
			$("#0To50").val(data.zto50);
			$("#50To100").val(data.fto100);
			$("#over100").val(data.over100);
			
			if(data.zto50==null) $("#0To50").val(0);
			if(data.fto100==null) $("#50To100").val(0);
			if(data.over100==null) $("#over100").val(0);
		}
	});
};

function getProgInfo(){
	var url = "${ctxPath}/chart/getProgInfo.do";
	var param = "dvcId=" + $("#dvcId").val();
	
	$.ajax({
		url : url,
		data : param,
		type : "post",
		dataType : "json",
		success : function(data){
			var json = data.dataList;
			
			var tr = "<Tr>" +
						"<td colspan='2'>" + $("#dvcId option:selected").html() + "</td>" +
						"</Tr>" +
						"<Tr>" +
							"<Td>프로그램</Td><td>비율(%)</td>" +
					"</Tr>";
		
			
			$(json).each(function(idx, data){
				tr += "<tr>" + 
							"<td>" + data.prgCd +"</td>" +
							"<td><input type='text' size='5' value=" + data.ratio + "></td>" + 
						"</tr>";
			});
			
			$("#setProg table").html(tr);
			setEl();
		}
	});
};


var className = "";
var classFlag = true;
function getToolList(){
	getSpdLoadInfo();
	getCalcTy();
	getProgInfo();
	
	var url = "${ctxPath}/chart/getToolList.do";
	var sDate = $("#sDate").val();
	var eDate = $("#eDate").val()
	
	var param = "sDate=" + sDate +
				"&eDate=" + eDate + " 23:59:59" + 
				"&dvcId=" + $("#dvcId").val();
	
	$.ajax({
		url : url,
		data : param,
		dataType : "json",
		type : "post",
		success : function(data){
			var json = data.dataList;
			
			var tr = "<thead><Tr class='tr_table_fix_header' style=\"font-weight: bolder;background-color: rgb(34,34,34)\" class=\"thead\">" + 
						"<td rowspan=\"2\">Tool No</td>" + 
						"<td rowspan=\"2\" width=\"5%\">Spec</td>" + 
						"<td rowspan=\"2\">Port No</td>" + 
						"<td rowspan=\"2\">Status</td>" +
						"<td colspan=\"4\">Tool Cycle Count</td>" +
						"<td colspan=\"4\">RunTime (h)</td>" + 
						"<td colspan=\"3\">Offset D</td>" + 
						"<td colspan=\"3\">Offset H</td>" +
						"<td rowspan=\"2\">Reset Date</td>" + 
					"</Tr>" + 
					"<tr  class='tr_table_fix_header' style=\"font-weight: bolder;background-color: rgb(34,34,34)\" class=\"thead\">" +
						"<td>LMT</td>" + 
						"<td>Current</td>" + 
						"<td>Remain</td>" + 
						"<td>Used rate(%)</td>" +
						"<td>LMT</td>" + 
						"<td>Current</td>" + 
						"<td>Remain</td>" + 
						"<td>Used rate(%)</td>" + 
						"<td>LMT</td>" + 
						"<td>Init</td>" + 
						"<td>Current</td>" + 
						"<td>LMT</td>" + 
						"<td>Init</td>" + 
						"<td>Current</td>" + 
					"</tr></thead>";
					
			
			csvOutput = "Tool No,Spec, Port No, Cycle Limited, Cycle Current , Cycle Remain, Cycle Used rate(%),RunTime Limited, RunTime Current , RunTime Remain, RunTime Used rate(%), Offset D Limited, Offset D Initial, Offset D Current, 	Offset H Limited, Offset H Initial, Offset H CurrentLINE";

			initMap = new JqMap();
			initArray = [];
			$("#save, #undo").addClass("disabledIcon");
			$(json).each(function(idx, data){
				var status = "green";
				
				if(Number(data.runTimeUsedRate)>=90){
					status = "yellow";
				}
				
				if(Number(data.cycleUsedRate)>=90){
					status = "yellow";
				}
				
				if((Number(data.offsetDCurrent)/(Number(data.offsetDLimit) - Number(data.offsetDInit)))*100>=50){
					status = "yellow";
				}
				
				if((Number(data.offsetHCurrent)/(Number(data.offsetHLimit) - Number(data.offsetHInit)))*100>=50){
					status = "yellow";
				}
				
				if(Number(data.offsetDLimit) < (Number(data.offsetDInit) + Number(data.offsetDCurrent))){
					status = "red";
				};
				
				if(Number(data.offsetHLimit) < (Number(data.offsetHInit) + Number(data.offsetHCurrent))){
					status = "red";
				};
				
				if(data.prdCntLmt=="-" && data.runTimeLimit == "-" && Number(data.offsetDLimit) == 0 && Number(data.offsetHLimit) == 0){
					status = "gray";
				};
	
				if(data.cycleRemain<0 || data.runTimeRemain<0){
					status = "red";
				}
				
				initMap.put("spec__" + data.portNo, data.spec);
				initMap.put("prd_cnt_lmt__" + data.portNo,data.prdCntLmt);
				initMap.put("rn_tm_lmt_sec__" + data.portNo,data.runTimeLimit);
				initMap.put("of_d_lmt__" + data.portNo,data.offsetDLimit);
				initMap.put("of_h_lmt__" + data.portNo,data.offsetHLimit);
				
				initArray.push(data.spec);
				initArray.push(data.prdCntLmt);
				initArray.push(data.runTimeLimit);
				initArray.push(data.offsetDLimit);
				initArray.push(data.offsetHLimit);
				
				if(classFlag){
					className = "row2"
				}else{
					className = "row1"
				};
				classFlag = !classFlag;
				
				tr += "<tbody><tr class='" + className + "'>" + 
							"<td>" + data.toolNm + "</td>" +
							"<td><input type='text' size='5' value='" + data.spec + "'  id='spec__" + data.portNo +"'></td>" +
							"<td>" + data.portNo + "</td>" +
							"<td align='center'>" + "<div onclick = chkReset('" + data.portNo + "','" + data.date + "'); style='border-radius:50%; cursor : pointer; background-color : " + status + "; width: " + getElSize(80) + "px; height:" + getElSize(80) + "px'></div>" + "</td>" +
							"<td><input type='text' value='" + data.prdCntLmt + "' size='5' id='prd_cnt_lmt__" + data.portNo +"'></td>" +
							"<td>" + data.prdCntCrt + "</td>" +
							"<td>" + data.cycleRemain + "</td>" +
							"<td>" + data.cycleUsedRate + "</td>" +
							"<td><input type='text' value='" + data.runTimeLimit + "' id='rn_tm_lmt_sec__" + data.portNo + "'size='5'></td>" +
							"<td>" + data.runTimeCurrent + "</td>" +
							"<td>" + data.runTimeRemain + "</td>" +
							"<td>" + data.runTimeUsedRate + "</td>" +
							"<td><input type='text' value='" + data.offsetDLimit + "' id='of_d_lmt__" + data.portNo + "' size='5'></td>" +
							"<td>" + data.offsetDInit + "</td>" +
							"<td>" + data.offsetDCurrent + "</td>" +
							"<td><input type='text' value='" + data.offsetHLimit + "' id='of_h_lmt__" + data.portNo + "' size='5'></td>" +
							"<td>" + data.offsetHInit + "</td>" +
							"<td>" + data.offsetHCurrent + "</td>" +
							"<td>" + data.resetDt  + "</td>" +
					"</tr></tbody>";
					
				csvOutput += data.toolNm + "," + 
							data.spec + "," + 
							data.portNo + "," + 
							data.prdCntLmt + "," +
							data.prdCntCrt + "," + 
							data.cycleRemain + "," +
							data.cycleUsedRate + "," +
							data.runTimeLimit + "," +
							data.runTimeCurrent + "," + 
							data.runTimeRemain + "," +
							data.runTimeUsedRate + "," + 
							data.offsetDLimit + "," + 
							data.offsetDInit + "," + 
							data.offsetDCurrent + "," + 
							data.offsetHLimit + "," + 
							data.offsetHInit + "," + 
							data.offsetHCurrent + "," + 
							data.resetDt  +"LINE";
			});
			
			$("#table").html(tr);
			
			$(".row1").not(".tr_table_fix_header").css({
				"background-color" : "#222222"
			});

			$(".row2").not(".tr_table_fix_header").css({
				"background-color": "#323232"
			});

			
			
			$("#table td").css({
				"font-size" : getElSize(50),
				"border" : getElSize(7) + "px solid black"
			});
			
			$("#table input[type=text]").keyup(function(){
				currentInitArray = [];
				currentInitMap = new JqMap();
				$("#table input[type=text]").each(function(idx, data){
					var id = data.id;
					var val = $(data).val();
					
					currentInitMap.put(id, val);
					currentInitArray.push(val)
					
				});
				
				if(currentInitArray.toString()==initArray.toString()){
					$("#save, #undo").addClass("disabledIcon");
				}else{
					$("#save, #undo").removeClass("disabledIcon");					
				}
			});  
			
			//bindEvt();
			
			$("#table input").focus(function(){
				preEl = $(this);
				preVal = $(this).val();
				$(this).val("")
			});
			
			$("#table input").blur(function(){
				if($(this).val()=="") preEl.val(preVal)
		 	});
			
			$("#container div:last").remove();
			scrolify($('#table'), getElSize(1600));
			$("#container div:last").css("overflow", "auto")
	
			if($(".tmpTable").length>2){
				$("table:last").remove();
			}
		}	
	}); 
};

var initMap = new JqMap();
var currentInitMap = new JqMap();
var initArray = [];
var currentInitArray = [];
var selected_prtNo, selected_date;

function chkReset(portNo, date){
	$("#resetDiv").css("z-index",9);
	
	selected_prtNo = portNo;
	selected_date = date;
};

function chkSave(){
	$("#saveDiv").css("z-index",9);
};

function noReset(){
	$("#resetDiv, #saveDiv, #resetCurrentDiv").css("z-index",-1);
};

var cycleCntCurrent;
var runTimeCurrent;

function showSetCurrentVal(){
	$("#resetDiv, #saveDiv").css("z-index",-1);
	$("#resetCurrentDiv").css("z-index",9);
};

function okReset(){
	var url = ctxPath + "/chart/resetTool.do";
	
	var date = new Date();
	var year = date.getFullYear();
	var month = addZero(String(date.getMonth()+1));
	var day = addZero(String(date.getDate()));
	
	var today = year + "-" + month + "-" + day;
	
	var param = "portNo=" + selected_prtNo + 
				"&dvcId=" + $("#dvcId").val() + 
				"&sDate=" + today + 
				"&prdCntCrt=" + $("#resetCycleCntCrnt").val() + 
				"&rnTmCrtSec=" + $("#rnTmCrnt").val();
	
	
	/* if(selected_date!=today){
		$("#resetDiv").css("z-index",-1);
		$("#denyResetDiv").css("z-index",9);
		return;
	} */
	
	$.ajax({
		url : url,
		data : param,
		type : "post",
		dataType : "text",
		success : function(data){
			noReset();
			getToolList();
		}
	});
}

var menu = false;
function getMousePos(){
	$(document).on("mousemove", function(e){
		var target = $("#menu_btn").width();

		if((e.pageX <= target && e.pageY <= target) || panel){
			if(!menu){
				$("#menu_btn").animate({
					"left" : 0,
					"top" : getElSize(20),
				});
				menu = true;
			};
		}else{
			if(menu){
				$("#menu_btn").animate({
					"left" : -getElSize(100),
				});	
				menu = false;
			};
		};
	});
};

function goReport(){
	var type = this.id;
	var url;
	if(type=="menu0"){
		url = "${ctxPath}/chart/main.do";
		location.href = url;
	}else if(type=="menu1"){
		url = "${ctxPath}/chart/performanceReport.do";
		location.href = url;
	}else if(type=="menu2"){
		url = "${ctxPath}/chart/alarmReport.do";
		location.href = url;
	}else if(type=="menu3"){
		url = "${ctxPath}/chart/main3.do";
		location.href = url;
	}else if(type=="menu9"){
		url = "${ctxPath}/chart/singleChartStatus2.do";
		window.localStorage.setItem("dvcId", 1)
		location.href = url;
	}else if(type=="menu4"){
		url = ctxPath + "/chart/rotateChart.do";
		location.href = url;
	}else if(type=="menu5"){
		url = "${ctxPath}/chart/traceManager.do";
		location.href = url;
	}else if(type=="menu6"){
		url = "${ctxPath}/chart/toolManager.do";
		location.href = url;
	}else if(type=="menu8"){
		url = "${ctxPath}/chart/jigGraph.do";
		location.href = url;
	}else if(type=="menu9"){
		url = "${ctxPath}/chart/wcGraph.do";
		location.href = url;
	}else if(type=="menu10"){
		url = "${ctxPath}/chart/addTarget.do";
		location.href = url;
	}else if(type=="menu7"){
		url = "${ctxPath}/chart/singleChartStatus.do";
		location.href = url;
	}else if(type=="menu11"){
		url = "${ctxPath}/chart/prdStatus.do";
		location.href = url;
	}else if(type=="menu99"){
		$("#bannerDiv").css({
			"z-index" : 9999
		});
		getBanner();
		closePanel();
		panel = false;
	}else if(type=="menu12"){
		url = "${ctxPath}/chart/performanceAgainstGoal.do";
		location.href = url;
	}else if(type=="menu100"){
		url = "${ctxPath}/chart/stockStatus.do";
		location.href = url;
	}else if(type=="menu101"){
		url = "${ctxPath}/chart/programManager.do";
		location.href = url;
	}else if(type=="menu102"){
		url = "${ctxPath}/chart/leadTime.do";
		location.href = url;
	}else if(type=="menu103"){
		url = "${ctxPath}/chart/faulty.do";
		location.href = url;
	}else if(type=="menu104"){
		url = "${ctxPath}/chart/addFaulty.do";
		location.href = url;
	}
};

var panel = false;
function showPanel(){
	$("#panel").animate({
		"left" : 0
	});
	
	$("#menu_btn").animate({
		"left" : contentWidth * 0.2 + getElSize(50)	
	});
	
	$("#corver").css({
		"z-index" : 9,
		"opacity" : 0.7
	});
};

function closePanel(){
	$("#panel").animate({
		"left" : -contentWidth * 0.2 - getElSize(40)
	});
	
	$("#menu_btn").animate({
		"left" : 0
	});
	
	$("#corver").css({
		"z-index" : -1,
		"opacity" : 0.7
	});
};


function addZero(str){
	if(str.length==1){
		str = "0" + str;
	};
	return str;
};

function setDate(){
	var date = new Date();
	var year = date.getFullYear();
	var month = addZero(String(date.getMonth()+1));
	var day = addZero(String(date.getDate()));
	
	$(".date").val(year + "-" + month + "-" + day);
};


function clearMenu(){
	$("#corver").css({
		"z-index" : -1,
		"opacity" : 0
		
	});
	
};

function setEl() {
	$(".container").css({
		"width": contentWidth,
		"height" : contentHeight,
	});
	
	$(".container").css({
		"margin-top" : originHeight/2 - ($(".container").height()/2),
		"margin-left" : originWidth/2 - ($(".container").width()/2),
	});
	
	$(".layoutTd").css({
		height : contentHeight * 0.85
	});

	$(".page").css({
		"height" : originHeight
	});

	$("#part1").css({
		"top" : 0
	});

	// 페이지 위치 조정
	for (var i = 2; i <= 5; i++) {
		$("#part" + i).css({
			"top" : $("#part" + (i - 1)).height() + originHeight
		});
	};

	$(".mainTable").css({
		"border-spacing" : getElSize(40)
	});

	$(".mainTable td").css({
		"font-size" : getElSize(30)
	});
	
	$(".title").css({
		"padding" : getElSize(40),
		"font-size" : getElSize(100)
	});

	$(".subTitle").css({
		"padding" : getElSize(40),
		"font-size" : getElSize(50)
	});

	$(".tr2").css({
		"height" : contentHeight * 0.35
	})

	$("#chartTr").css({
		"height" : contentHeight * 0.4
	});

	$("#pie").css({
		"height" : contentHeight * 0.32
	});

	$(".neon1, .neon2, .neon3").css({
		"font-size" : getElSize(500)
	});

	$(".neon1, .neon2, .neon3").css({
		"margin-top" : $(".tr2").height() / 2 - $(".neon1").height()
						/ 2 - $(".subTitle").height(),
	});

	$("#machine_name").css({
		"color" : "white",
		"font-weight" : "bolder",
		"font-size" : getElSize(170),
	});

	$("#machine_name_td").css({
		"height" : getElSize(500)
	});

	$("#machine_name").css({
		"margin-top" : $("#machine_name_td").height() / 2
						- $("#machine_name").height() / 2
						- $(".subTitle").height()
	});

	$(".title_left").css({
		"float" : "left",
		"width" : contentWidth * 0.1
	});

	$(".title_right").css({
		"float" : "right",
	});

	$("#menu_btn").css({
		"position" : "absolute",
		"width" : getElSize(130),
		"left" : 0,
		"top" : getElSize(20),
		"cursor" : "pointer",
		"z-index" : 13
	});
	
	$("#panel").css({
		"background-color" : "rgb(34,34,34)",
		"border" : getElSize(20) + "px solid rgb(50,50,50)",
		"position" : "absolute",
		"width" : contentWidth * 0.2,
		"top" : 0,
		"left" : -contentWidth * 0.2 - getElSize(40),
		"z-index" : 99999
	});

	$("#corver").css({
		"width" : originWidth,
		"height" : originHeight,
		"position" : "absolute",
		"z-index" : -1,
		"background-color" : "black",
		"opacity" : 0
	});

	$("#panel_table").css({
		"color" : "white",
		"font-size" : getElSize(40)
	});

	$("#panel_table td").css({
		"padding" : getElSize(30),
		"cursor" : "pointer"
	});

	$(".dvcName_p1").css({
		"font-weight" : "bolder",
		"font-size" : getElSize(40)
	});

	$("#dvcId, #stTlLf").css({
		"font-size" : getElSize(40)	
	});
	
	$(".wrap").css({
		"width" : contentWidth * 0.2,
		"padding" : getElSize(10),
		"border-radius" : getElSize(30),
		"padding-bottom" : getElSize(10),
		//"border": getElSize(10) + "px outset white",
	});
	
	$(".waitCnt, .alarmCnt, .noConCnt").css({
		"font-size" : getElSize(20),
		"margin" :getElSize(20)
	});
	
	$(".waitCnt").css({
		"color" : "yellow"
	});
	
	$(".alarmCnt").css({
		"color" : "red"
	});
	
	$(".noConCnt").css({
		"color" : "gray"
	});
	
	$(".card_status").css({
		"height" : getElSize(60)
	});
	
	$(".card_table").css({
		"border-spacing" : getElSize(10)
	});
	
	$(".opRatio").css({
		"font-size" :getElSize(100)
	});
	
	$(".comName").css({
		"font-size" :getElSize(60)
	});
	
	$(".machine_cam").css({
		"width" : contentWidth * 0.07,
		"height" : contentHeight * 0.08,
		//"border" : getElSize(10) + "px solid rgb(50,50,50)" 
	});
	
	$(".frame").css({
		"width" : contentWidth * 0.07,
		"height" : contentHeight * 0.15
	});
	
	$(".statusCnt").css({
		"height" : getElSize(70),
		"vertical-align" : "bottom",
		"box-shadow ": "inset 0 5px 0 #ddd"
	});
	
	$(".opTime").css({
		"font-weight" : "bolder",
		"top" : getElSize(400),
		"right" : getElSize(10)
	});
	
	$(".logo").css({
		"height" : contentWidth * 0.01
	});
	
	
	$("#title_left").css({
		"position" : "absolute",
		"width" : contentWidth * 0.07,
		"z-index" : 2,
		"left" : getElSize(100),
		"top" : getElSize(60)
	});

	$("#title_right").css({
		"position" : "absolute",
		"z-index" : 2,
		"right" : getElSize(100),
		"top" : getElSize(40),
		"color" : "white",
		"font-weight" : "bolder",
		"font-size" : getElSize(40)
	});
	
	$("#jigSelector, .goGraph, .excel, .label").css({
		"margin-right": getElSize(10),
		"margin-left": getElSize(10),
		"border-radius" : getElSize(10),
		"padding" : getElSize(10),
		"cursor": "pointer",
		"font-size": getElSize(50)
	});
	
	$(".date").css({
		"height": getElSize(70),
		"font-size": getElSize(50)
	});
	
	$(".thead").css({
		"font-size" : getElSize(60)
	});
	
	$(".tmpTable, .tmpTable tr, .tmpTable td").css({
		"border": getElSize(5) + "px solid rgb(50,50,50)"
	});
	
	$(".tmpTable td").css({
		"padding" : getElSize(10),
		"height": getElSize(100),
	});
	
	$(".contentTr").css({
		"font-size" : getElSize(60)
	});
	
	$(".machineListForTarget").css({
		"position" : "absolute",
		"width" : getElSize(1200),
		"height" : getElSize(1200),
		"overflow" : "auto",
		//"top" : getElSize(50),
		//"background-color" : "rgb(34,34,34)",
		"background-color" : "green",
		"color" : "white",
		"font-size" : getElSize(50),
		"padding" : getElSize(50),
		"overflow" : "auto",
		"border-radius" : getElSize(50),
		"border" : getElSize(10) + "px solid white",
	});
	
	$(".machineListForTarget").css({
		"left" : (originWidth/2) - ($(".machineListForTarget").width()/2),
	});
	
	$("#resetDiv, #denyResetDiv, #saveDiv").css({
		"position" : "absolute",
		"width" : getElSize(700),
		"height" :getElSize(200),
		"background-color" : "#444444",
		"color" : "white"
	});
	
	$("#resetCurrentDiv").css({
		"position" : "absolute",
		"width" : getElSize(900),
		"height" :getElSize(280),
		"background-color" : "#444444",
		"color" : "white"
	});
	
	$("#resetCurrentDiv table td").css({
		"padding" :getElSize(20),
		"color" : "white",
		"font-size" : getElSize(40)
	});
	
	$("#resetCurrentDiv").css({
		"top" : (window.innerHeight/2) - ($("#resetCurrentDiv").height()/2),
		"left" : (window.innerWidth/2) - ($("#resetCurrentDiv").width()/2),
		"z-index" : -9,
		"border-radius" : getElSize(10),
		"padding" : getElSize(20)
	});
	
	$("#resetDiv, #denyResetDiv, #saveDiv").css({
		"top" : (window.innerHeight/2) - ($("#resetDiv").height()/2),
		"left" : (window.innerWidth/2) - ($("#resetDiv").width()/2),
		"z-index" : -1,
		"border-radius" : getElSize(10),
		"padding" : getElSize(20)
	});
	
	
	$("#resetDiv button, #resetDiv span, #saveDiv button, #saveDiv span, #denyResetDiv button, #denyResetDiv span").css({
		"font-size" :getElSize(40)
	});
	
	$("#statusInfo").css({
		"font-size" : getElSize(40),
		"color" : "white"
	});
	
	$("#save, #undo").css({
		"width" : getElSize(100),
		"float" : "left",
		"cursor" : "pointer"
	});
	
	$("#save, #undo").addClass("disabledIcon");
	
	$("#content").css({
		"overflow" : "hidden",
		"height" : window.innerHeight - $("#table").offset().top - getElSize(100),
		"width" : "98%"
	});
	
	$("#content").css({
		"margin-left" : (contentWidth/2) -($("#content").width()/2)
	});
	
	$("#dvcDiv").css({
		"margin-left" : $("#table").offset().left - marginWidth,
		"width" : $("#table").width(),
	});
	
	$("#bannerDiv").css({
		"position" : "absolute",
		"width" : getElSize(1500),
		"height" : getElSize(200),
		"border-radius" : getElSize(20),
		"padding" : getElSize(50),
		"background-color" : "lightgray",
		"z-index" : -9
	});

	
	$("#bannerDiv").css({
		"left" : (originWidth/2) - ($("#bannerDiv").width()/2),
		"top" : (originHeight/2) - ($("#bannerDiv").height()/2)
	});
	
	$("#bannerDiv input[type='text']").css({
		"width" : getElSize(1200),
		"font-size" : getElSize(50)
	});
	
	$("#bannerDiv button").css({
		"margin" : getElSize(50),
		"font-size" : getElSize(50)
	});
	
	$("#intro").css({
		/* "width" : contentWidth, */
		"position" : "absolute",
		"bottom" : 0 + marginHeight,
		"font-size" : getElSize(100),
		"font-weight" : "bolder",
		"z-index" : 9999
	});
	
	$("#intro_back").css({
		"width" : originWidth,
		"display" : "none",
		"height" : getElSize(130),
		"opacity" : 0.3,
		"position" : "absolute",
		"background-color" : "white",
		"bottom" : 0 + marginHeight,
		"z-index" : 9999
	});
	
	$("#setProg").css({
		"width" : getElSize(1000),
		"position" :"absolute",
		"z-index" : 9,
		"background-color" : "#444444",
		"padding" : getElSize(20),
		"display" : "none"
	});
	
	$("#setSpdLoad").css({
		"width" : getElSize(1000),
		"position" :"absolute",
		"z-index" : 9,
		"background-color" : "#444444",
		"padding" : getElSize(20),
		"display" : "none"
	});
	
	$("#calcStandards").css({
		"width" : getElSize(1000),
		"position" :"absolute",
		"z-index" : 9,
		"background-color" : "#444444",
		"padding" : getElSize(20),
		"display" : "none"
	});
	
	$("#calcStandards, #setSpdLoad, #setProg").css({
		"top" : (window.innerHeight/2) - ($("#calcStandards").height()/2),
		"left" : (window.innerWidth/2) - ($("#calcStandards").width()/2),
	});
	
	$("#calcStandards table td, #setSpdLoad table td,  #setProg table td").css({
		"color" : "white",
		"font-size" : getElSize(50),
		"text-align" : "Center",
		"border" : getElSize(2) + "px solid white"
	});
	
	$("#calcStandards table td button").css({
		"font-size" : getElSize(50),
		"width" : getElSize(700)
	});
	
	$("#close_btn").css({
		"width" : getElSize(80),
		"position" :"absolute",
		"top" : 0,
		"right" : 0,
		"cursor" : "pointer"
	}).click(function(){
		$("#calcStandards").css("display", "none")
	});
	
	
	chkBanner();
};

function getBanner(){
	var url = ctxPath + "/chart/getBanner.do";
	var param = "shopId=" + shopId;
	

	$.ajax({
		url : url,
		data : param,
		type : "post",
		dataType : "json",
		success : function(data){
			if(window.localStorage.getItem("banner")=="true"){
				$("#intro").html(data.msg).css({
					"color" : data.rgb,
					"right" :0
				})	
			}
			
			bannerWidth = $("#intro").width() + getElSize(10); 
			$("#banner").val(data.msg);
			$("#color").val(data.rgb);
			
			bannerAnim();
		}
	});		
}

var bannerWidth;
function bannerAnim(){
	$("#intro").width(bannerWidth);
	$("#intro").animate({
		"right" : window.innerWidth
	},15000, function(){
		$("#intro").css({
			"right" : -$("#intro").width()
		});
		bannerAnim();
	});
};
function chkBanner(){
	if(window.localStorage.getItem("banner")=="true"){
		$("#intro_back").css("display","block");
		getBanner();
	}else{
		$("#intro").html("");
		$("#intro_back").css("display","none");
	}
};

function cancelBanner(){
	window.localStorage.setItem("banner", "false");
	$("#bannerDiv").css("z-index",-9);
	chkBanner();
};

function addBanner(){
	var url =  ctxPath + "/chart/addBanner.do";
	var param = "msg=" + $("#banner").val() + 
				"&rgb=" + $("#color").val() + 
				"&shopId=" + shopId;
	
	$.ajax({
		url : url,
		data : param,
		type : "post",
		success : function(data){
			window.localStorage.setItem("banner", "true");
			$("#bannerDiv").css("z-index",-9);
			chkBanner();
		}
	});
};

function setToolLife(){
	$("#calcStandards").css("display","block")
};


function closeBox(){
	$("#denyResetDiv").css("z-index",-1)
}
var csvOutput;
function csvSend(){
	var sDate, eDate;
	
	sDate = $("#sDate").val();
	eDate = $("#eDate").val();
	
	csvOutput = csvOutput.replace(/\n/gi,"");
	
	f.csv.value=encodeURIComponent(csvOutput);
	f.startDate.value = sDate;
	f.endDate.value = eDate;
	f.submit(); 
};

function showSpdLoadSet(){
	$("#calcStandards").css("display", "none");
	$("#setSpdLoad").css("display", "block");
}

function showPrgSet(){
	$("#calcStandards").css("display", "none");
	$("#setProg").css("display", "block");
}

function closeSetDiv(){
	$("#setSpdLoad, #setProg").css("display", "none");
};

function setSpdLoad(){
	var url = "${ctxPath}/chart/setSpdLoad.do";
	var param = "dvcId=" + $("#dvcId").val() + 
				"&zTo50=" + $("#0To50").val() + 
				"&fTo100=" + $("#50To100").val() + 
				"&over100=" + $("#over100").val();
	
	$.ajax({
		url : url,
		data : param,
		type : "post",
		dataType : "text",
		success : function(data){
			if(data=="success") closeSetDiv();
		}
	});
}

var prgArray = [];

function setPrgSet(){
	var url = "${ctxPath}/chart/setPrgSet.do";
	var length = $("#setProg table tr").length;

	prgArray = [];
	
	for(var i = 2; i < length; i++){
		var obj = new Object();
		obj.prgCd = $("#setProg table tr:nth(" + i + ") td:nth(0)").html();
		obj.ratio = $("#setProg table tr:nth(" + i + ") td:nth(1) input").val();
		obj.dvcId = $("#dvcId").val();
		
		prgArray.push(obj)
	}
	
	var obj = new Object();
	obj.val = prgArray;
	
	var param = JSON.stringify(obj);
	
	$.ajax({
		url : url,
		data : "val=" + param,
		type : "post",
		dataType : "text",
		success : function(data){
			if(data=="success"){
				closeSetDiv();
			}
		}
	})
};

</script>
</head>

<body >
<div id="setProg">
	<table style="width: 100%">
	
	</table>
	<center>
		<button onclick="setPrgSet();">저장</button> <button onclick="closeSetDiv()">취소</button>
	</center>		
</div>

<div id="setSpdLoad">
	<table style="width: 100%">
		<Tr>
			<td id="dvcName" colspan="3"></td>
		</Tr>
		<Tr>
			<Td colspan="2">Spindle Load 범위</Td><td rowspan="2">비율(%)</td>
		</Tr>
		<Tr>		
			<Td>이상</Td>
			<Td>미만</Td>
		</Tr>
		<tr>
			<td>0</td> <td>50</td> <td><input type="text" id='0To50' size="5">  </td> 
		</tr>
		<tr>
			<td>50</td> <td>100</td><Td><input type="text" id='50To100'size="5">  </td>
		</tr>
		<tr>
			<td>100</td> <td>9999</td><td><input type="text" id='over100' size="5">  </td>
		</tr>
	</table>
	<center>
		<button onclick="setSpdLoad()">저장</button> <button onclick="closeSetDiv()">취소</button>
	</center>		
</div>
<div id="calcStandards">
	<img alt="" src="${ctxPath }/images/close_btn.png" id="close_btn">
	<table style="width: 100%">
		<tr>
			<td align="right" ><!-- <input type="radio" name="lifeCalcStandard" value="1" > --><input type="checkbox" id="onlySpdLoad"> </td><td> Spindle Load 있는 것만 적산</td>
		</tr>
		<tr>
			<Td  align="right"><input type="radio" name="lifeCalcStandard" value="1"></td><td><button onclick="showSpdLoadSet()">Spindle Load 별 차별 적산</button> </Td>
		</tr>
		<Tr>
			<Td align="right"><input type="radio" name="lifeCalcStandard" value="2"></td><td><button onclick="showPrgSet();">프로그램 별 차별 적산</button> </Td>
		</Tr>
	</table>	
	<center><button onclick="setCalcTy()">확인</button></center>
</div>

<div id="bannerDiv">
		<Center>
			<input type="text" id="banner" size="100"><input type="color" id="color"><br>
			<button onclick="addBanner();">적용</button>
			<button onclick="cancelBanner();">미적용</button>
		</Center>
	</div>
	<div id="intro_back"></div>
	<span id="intro"></span>
	
<div id="denyResetDiv">
		<Center>
			<span>과거 데이터는 초기화할 수 없습니다.</span><Br><br>
			<button  onclick="closeBox();"><spring:message code="confirm"></spring:message></button>
		</Center>
	</div>
	
	<div id="saveDiv">
		<Center>
			<span><spring:message code="check_save"></spring:message> </span><Br><br>
			<button id="resetOk" onclick="saveLimit();"><spring:message code="confirm"></spring:message></button>&nbsp;&nbsp;
			<button id="resetNo" onclick="noReset();"><spring:message code="cancel"></spring:message></button>
		</Center>
	</div>
	<div id="resetDiv">
		<Center>
			<span><spring:message code="chkReset"></spring:message></span><Br><br>
			<button id="resetOk" onclick="showSetCurrentVal();"><spring:message code="confirm"></spring:message> </button>&nbsp;&nbsp;
			<button id="resetNo" onclick="noReset();"><spring:message code="cancel"></spring:message></button>
		</Center>
	</div>
	
	<div id="resetCurrentDiv">
		<Center>
			<table style="border-collapse: collapse; width: 100%" border="1">
				<Tr>
					<Td align="center" width="50%">Tool Cycle Count</Td>
					<Td align="center">RunTime (h)	</Td>
				</Tr>
				<Tr>
					<td align="center"><input type="text" id="resetCycleCntCrnt" size="5" value="0">  </td>
					<td align="center"><input type="text" id="rnTmCrnt" size="5" value="0"> </td>
				</Tr>			
				<tr>
					<td colspan="2" align="center"><button onclick="okReset()">확인</button> </td>
				</tr>
			</table>
		</Center>
	</div>
	
	<form action='${ctxPath}/csv/csv.do' id="f"  method="post">
		<input type="hidden" name="csv" value="">
		<input type="hidden" name="startDate" value="">
		<input type="hidden" name="endDate" value="">
	</form>
	<%-- <img src="${ctxPath }/images/DashBoard/title_right.svg" id="title_right"> --%>
	<div id="title_right" class="title"> </div>	
	
	<div id="panel">
		<table id="panel_table" width="100%">
				<tr>
					<td id="menu0" class="menu"><spring:message code="layout"></spring:message> </td>
				</tr>
				<tr>
					<td id="menu7" class="menu"><spring:message code="devicestatus"></spring:message> </td>
				</tr>
				<tr>
					<td id="menu4" class="menu"><spring:message code="dailydevicestatus"></spring:message> </td>
				</tr>
				<tr>
					<td id="menu1" class="menu"><spring:message code="analsysperformance"></spring:message> </td> <!-- 장비별 가동실적분석 -->
				</tr>
				<tr>
					<td id="menu8" class="menu"><spring:message code="performancegraph1"></spring:message></td>
				</tr>
				<tr>
					<td id="menu9" class="menu"><spring:message code="performancegraph2"></spring:message> </td>
				</tr>
				<tr>
					<td id="menu12" class="menu">일생산 실적 분석 (표)</td> <!--알람 내역 -->
				</tr>
				<tr>
					<td id="menu6" class="menu"><spring:message code="toolmanagement"></spring:message> </td><!-- 무인장비 가동현황 -->
				</tr> 
				<tr>
					<td id="menu11" class="menu"><spring:message code="prdctboard"></spring:message> </td>
				</tr>
				<tr>
					<td id="menu10" class="menu"><spring:message code="addprdctgll"></spring:message> </td>
				</tr>
				<tr>
					<td id="menu5" class="menu"><spring:message code="tracemanagement"></spring:message> </td><!-- 무인장비 가동현황 -->
				</tr>
				<tr>
					<td id="menu3" class="menu"><spring:message code="24barchart"></spring:message></td><!-- 무인장비 가동현황 -->
				</tr>
				<tr>
					<td id="menu2" class="menu"><spring:message code="alarmhistory"></spring:message> </td> <!--알람 내역 -->
				</tr>
				<tr>
					<td id="menu100" class="menu">재고 현황</td> 
				</tr>
				<tr>
					<td id="menu99" class="menu">Catch Phrase 관리</td> 
				</tr>
				<tr>
					<td id="menu101" class="menu">프로그램별 가공이상 분석</td> 
				</tr>
				<tr>
					<td id="menu102" class="menu">제조 리드타임</td> 
				</tr>
				<tr>
					<td id="menu103" class="menu">고객불량율 / 공정뷸량율</td> 
				</tr>
				<tr>
					<td id="menu104" class="menu">불량등록</td> 
				</tr>
			</table>
	</div>
	<img src="${ctxPath }/images/home.png" id="menu_btn" >
					
	<div id="corver"></div>

	<!-- Part1 -->
	<div id="part1" class="page">
		<div class="container">
				<table class="mainTable">
					<tr>
						<Td align="center" style=" color:white; font-weight: bolder;" class="title" >
								<spring:message code="toolmanagement"></spring:message>
						</Td>
					</tr>
				</table>
				
				<div  id="dvcDiv">
					<table id="icons" style="float: right;">
						<tr>
							<Td>
								<button onclick="setToolLife()" id="stTlLf">Tool 수명 적산 기준 등록</button>
							</Td>
							<Td>
								<img src="${ctxPath }/images/save.png" id="save"> 
								<img src="${ctxPath }/images/undo.png" id="undo">
							</Td>
							<td>
								<span style="background-color: white; float:right; color:black; font-weight: bolder; padding: 3px;" class="excel"  ><spring:message code="excel"></spring:message> </span>							
							</td>
						</tr>
					</table>
					<select id="dvcId" ></select> <input type="date" class="date" id="sDate" style="display: none"> <input type="date" class="date" id="eDate" style="display: none">
					<br>
					<span id="statusInfo"><spring:message code="status_icon_click_label"></spring:message></span>
				</div>
				<div id="content">
					<table style="width: 100%; color: white; text-align: center; border-collapse: collapse;" class="tmpTable" border="1" id="table">
					</table>
				</div>
		</div>
	</div>
</body>
</html>