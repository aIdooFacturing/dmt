<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ include file="/WEB-INF/views/include/lib.jsp"%>
<!DOCTYPE html>
<html> 
<head>
<meta charset="UTF-8">
<link rel="shortcut icon" href="${ctxPath }/images/dulink_favicon.ico">
<title>Machine Detail Info.</title>


<script type="text/javascript">
	var ctxPath = "${ctxPath}";
</script>
<script type="text/javascript" src="${ctxPath }/js/jquery-1.11.2.min.js"></script>
<script src="http://code.highcharts.com/highcharts.js"></script>
<script src="http://code.highcharts.com/highcharts-3d.js"></script>
<script src="http://code.highcharts.com/highcharts-more.js"></script>
<script src="http://code.highcharts.com/modules/solid-gauge.js"></script>
<script src="http://code.highcharts.com/modules/exporting.js"></script>
<script type="text/javascript">
var spindle1, spindle2;
var feed1, feed2;
var operation1, operation2;
var jeolboon1, jeolboon2;
var pie1,pie2; 

var statusBar1;
var statusBar2;

var options2_1;
var optoins2_2;	
var barArray2_1 = new Array();
var barArray2_2 = new Array();

$(function(){
	

	$("#title_1").css({
		"left" : window.innerWidth/2 - $("#title_1").width()/2
	})
	
	setElement();
	statusChart3("status1");
	statusChart3("status2");
	
	//pieChart("pie1");
	//pieChart("pie2");
	
	drawGaugeChart2("gauge1", "gauge2", "Operating Time Ratio", "Cutting Time Ratio","%");
	drawGaugeChart("gauge3", "gauge4", "Spindle Load", "Feed Override","");
	
	drawGaugeChart2("gauge5", "gauge6", "Operating Time Ratio", "Cutting Time Ratio","%");
	drawGaugeChart("gauge7", "gauge8", "Spindle Load", "Feed Override","");
	
	setInterval(time3, 1000);
	
	operation1 = $("#gauge1").highcharts();
	jeolboon1 = $("#gauge2").highcharts();
	spindle1 = $("#gauge3").highcharts();
	feed1 = $("#gauge4").highcharts();
	
	operation2 = $("#gauge5").highcharts();
	jeolboon2 = $("#gauge6").highcharts();
	spindle2 = $("#gauge7").highcharts();
	feed2 = $("#gauge8").highcharts();
	
	pie1 = $("#pie1").highcharts();
	pie2 = $("#pie2").highcharts(); 
	
	statusBar1 = $("#status1").highcharts();
	statusBar2 = $("#status2").highcharts();
	
	options2_1 = statusBar1.options;
	options2_2 = statusBar2.options;
	//getAllDvcId();
	
	getDvcStatus(44,0);
	getDvcStatus(45,1);
	
	
	/* getDvcStatus(9,0);
	getDvcStatus(10,1); */
	
	//getDvcStatus(dvcList[dvcIdx]);
	//console.log(dvcList[dvcIdx])
	//getDvcStatus(dvcList[(dvcIdx+1)]); 
	//dvcIdx++;
	
	/* setInterval(function(){
		for(var i=0; i<=2; i++){
			pie1.series[0].data[i].update(0);
			pie2.series[0].data[i].update(0);
		};
		
		for(var i = 0; i < statusBar1.series.length; i++){
			statusBar1.series[0].remove(true);
		};
		
		for(var i = 0; i < statusBar2.series.length; i++){
			statusBar2.series[0].remove(true);
		};
		
		getDvcStatus(dvcList[dvcIdx],0);
		getDvcStatus(dvcList[(dvcIdx+1)],1);
		dvcIdx++;
		if(dvcList==3){
			dvcList = 0;
		};
	}, 1000* 60 * 50); */
	
	init();
});


var dvcIdx = 0;
var dvcList = [44,45,46];
function init(){
	var url = "${ctxPath}/chart/initPiePolling.do";
	
	$.ajax({
		url : url,
		type : "post",
		success : function(){
			polling1();
			polling2();
		}
	});
};

var preChart1=0;
var preChart2=0;
var flag1 = true;
var flag2 = true;

function polling1(){
	var url = "${ctxPath}/chart/piePolling1.do";
	
	$.ajax({
		url : url,
		dataType : "text",
		type  : "post",
		success : function(data){
			if(!flag1 && data.trim()==0){
				location.reload();
			};
			if(preChart1!=data.trim()){
				var id = data.trim();
				/* $("#loader1").css("opacity",1);
				
				statusChart("status1");
				pieChart("pie1");
				
				preChart1=id;
				flag1 = false;
				
				drawGaugeChart("gauge1", "gauge2", "가동율", "절분율","%");
				drawGaugeChart("gauge3", "gauge4", "Spindle Load", "Feed Override",""); */
				
				/* for(var i = 0; i < statusBar1.series.length; i++){
					statusBar1.series[0].remove(true);
				};
				 */
				 machineArray[0][26].series = [];
				 machineArray[0][26].title = null;
				//machine[26].exporting = false;
				
					
				machineArray[0][0] = new Highcharts.Chart(machineArray[0][26]);
				
				 barArray2_1 = new Array();
				 machineArray = new Array();
				//getDvcStatus(id, 0);
				flag1 = false;
				preChart1=id;
				
			};
			setTimeout(polling1, 1000);	
		}
	});
};

function polling2(){
	var url = "${ctxPath}/chart/piePolling2.do";
	
	$.ajax({
		url : url,
		dataType : "text",
		type  : "post",
		success : function(data){
			if(!flag2 && data.trim()==0){
				location.reload();
			};
			if(preChart2!=data.trim()){
				var id = data.trim();
				/* $("#loader1").css("opacity",1);
				
				statusChart("status1");
				pieChart("pie1");
				
				preChart1=id;
				flag1 = false;
				
				drawGaugeChart("gauge1", "gauge2", "가동율", "절분율","%");
				drawGaugeChart("gauge3", "gauge4", "Spindle Load", "Feed Override",""); */
				machineArray = new Array();
				barArray2_2 = new Array();
				
				
				getDvcStatus(id, 1);
				flag2 = false;
				preChart2=id;
				
			};
			setTimeout(polling2, 1000);	
		}
	});
};

var machineArray = new Array();
var index = 0;

function getAllDvcId(){
	$("#mainTable3").css("opacity","0");
	var url = ctxPath + "/chart/getAdapterId.do";
	
	$.ajax({
		url : url,
		dataType : "json",
		type : "post",
		success : function(json){
			var obj = json.dvcId;
			$(obj).each(function(i, data){
				dvcId.push(data.dvcId);
				index = i;						
			});
							
			for(var i = 0; i <2; i++){
				getDvcStatus(dvcId[i]);
			};
			
			setInterval(resetArray,15000);
		}
	});
};

function resetArray(){
	chartLoop = false;
	setTimeout(function(){
		$("#mainTable").css("opacity","0");
		$("#loader").css("opacity","1");
		
		var length1 = statusBar1.series.length;
		var length2 = statusBar2.series.length;
		
		for(var i= 0; i < length1; i++){
			statusBar1.series[0].remove(true);
		};
		
		for(var i= 0; i < length2; i++){
			statusBar2.series[0].remove(true);
		};
		
		arrayIndex = 0;
		machineArray = new Array();
		getDvcStatus(dvcId[dvcIdx], 0);
		getDvcStatus(dvcId[dvcIdx+1], 1);
		chartLoop = true;
		dvcIdx += 2;
		if(dvcIdx>=dvcId.length){
			dvcIdx = 0;
		};
	}, 5000);
};

var dvcIdx = 2;
var dvcId = new Array();
function isItemInArray(array, item) {
	for (var i = 0; i < array.length; i++) {
		for (var j = 0; j < array[i].length; j++) {
			if (array[i][j] == item) {
				return true;   // Found it
	        };
        };
    };
	return false;   // Not found
};

function removeSpace(str){
	return str = str.replace(/ /gi, "");
};

var options1, options2;
var barArray1, barArray2;

var first = true;
var arrayIndex = 0;
var spdTime1 = 0;
var spdTime2 = 0;
var waitTime1 = 0;
var waitTime2 = 0;
var alarmTime1 = 0;
var alarmTime2 = 0;
var offTime1 = 0;
var offTime2 = 0;

function getDvcStatus(dvcId, idx){
	var url = ctxPath + "/chart/getStatusData.do";
	var param = "dvcId=" + dvcId;
	
	$.ajax({
		url : url,
		data : param,
		dataType : "json",
		type : "post",
		success : function(data){
			var json = data.status;
			if(json[0]=="null"){
				console.log("null data");
				getFirstChartData(dvcId, idx);	
			}else{
				$(json).each(function(i, data){
					if(idx==1)console.log(data.chartStatus + " -> " + data.startDateTime, data.endDateTime);
					$("#loader").css("opacity","0");
					$("#mainTable").css("opacity","1");
					var splitter = (":");
					var chartStatus = data.chartStatus;
					var bar = data.bar;
					var name = data.name;
					var type = data.type
					var opTime = Number(data.operationTime);
					var stopTime = Number(data.stopTime);
					var startH = addZero(String(data.startDateTime).substr(0,String(data.startDateTime).lastIndexOf(splitter)));
					var startM = addZero(String(data.startDateTime).substr(String(data.startDateTime).lastIndexOf(splitter)+1));
					var endH = addZero(String(data.endDateTime).substr(0,String(data.endDateTime).lastIndexOf(splitter)))
					var endM = addZero(String(data.endDateTime).substr(String(data.endDateTime).lastIndexOf(splitter)+1));
					var spdRatio = data.spdTime;
					var _alarmTime = data.alarmTime;
					var _waitTime = data.waitTime;
					var _offTime = data.offTime;
					var inCycleTime = Number(data.inCycleTime);
					
				//	console.log("incycle Time : " + inCycleTime);
					if(typeof(_alarmTime)=="undfined"){
						_alarmTime = 0;
					}else if(typeof(_waitTime)=="undfined"){
						_waitTime = 0;
					}else if(typeof(_offTime)=="undfined"){
						_offTime = 0;
					};
					
				//	if(idx==1)console.log(opTime, _waitTime, _alarmTime , _offTime)
					
					if(typeof(data.stopTime)=="undefined"){
						stopTime = 0;
					};
					
					var options;
					var barArray;
					var jeolboon_;
					var spd;
					var feed;
					var statusBar;
					var pieChart;
					var spdTime;
					var waitTime;
					var alarmTime;
					var offTime;
					
					
 					if(idx==1){
 						options = options2_1;
 						barArray = barArray2_1;
 						spd = spindle1;
 						feed = feed1;
 						pieChart = pie1;
 						jeolboon_ = jeolboon1;
 						operation = operation1;
 						spdTime += data.spdTime;
 						spdTime = spdTime1;
 						
 						waitTime1 += data.waitTime;
 						
 						alarmTime1 += alarmTime1;
 						offTime1 += offTime1;
 						
 					}else{
 						options = options2_2;
 						barArray = barArray2_2;
 						spd = spindle2;
 						feed = feed2;
 						pieChart = pie2;
 						jeolboon_ = jeolboon2;
 						operation = operation2;
 						spdTime += data.spdTime;
 						
 						spdTime = spdTime2;
 						waitTime2 += waitTime2;
 						alarmTime2 += alarmTime2;
 						offTime2 += offTime2;
 					};
 					
					if(!isItemInArray(machineArray, name)){
						var chart = new Array();
						chart.push(statusBar);
						chart.push(pieChart);
						chart.push(dvcId);
						chart.push(name);
						chart.push(spdTime);
						chart.push(waitTime);
						chart.push(alarmTime);
						chart.push(offTime);
						chart.push(0);
						chart.push(0);
						chart.push(0);
						chart.push("");
						chart.push(true);
						chart.push(true);
						chart.push(0);
						chart.push("-0:0");
						chart.push(0);
						chart.push(0);
						chart.push(0);
						chart.push(false);
						chart.push(spd);
						chart.push(feed);
						chart.push(operation);
						chart.push(jeolboon_);
						chart.push("");
						chart.push(0);
						chart.push(options);
						chart.push(barArray);
						
						
						machineArray.push(chart);
					};
					
					/* machineArray[idx][5] = Number(_waitTime);
					machineArray[idx][6] = Number(_alarmTime);
					machineArray[idx][7] = Number(_offTime);
					 */
					 //console.log(machineArray[idx])
					machineArray[idx][4] = Number(spdRatio);
					machineArray[idx][5] = inCycleTime;
					var chip = machineArray[idx][23].series[0].points[0];

			      	if(Number(machineArray[idx][4])==0 && Number(inCycleTime)==0){
			      		chip.update(0);
			      	}else{
			      		chip.update(Number(Number(machineArray[idx][4]/(inCycleTime)*100).toFixed(1)));
			      	}; 
			      	
					//if(idx==1)console.log("(" + type + ") " + chartStatus + " -> " + data.startDateTime + "~" + data.endDateTime)
					var color = "";
					if(chartStatus.toLowerCase()=="in-cycle"){
						color = colors[0];
					}else if(chartStatus.toLowerCase()=="wait"){
						color = colors[1];
					}else if(chartStatus.toLowerCase()=="alarm"){
						color = colors[2];
					}else if(chartStatus.toLowerCase()=="no-connection"){
						color = colors[3];
					};
					
					if(machineArray[idx][13]){
						var firstBar = "";
						//20시 ~ 시작 시간
						if(Number(startH)>=20){
							firstBar = Number(startH) + (Number(String(startM).substr(0,1))*block) - 20;
						}else{
							firstBar = Number(startH) + (Number(String(startM).substr(0,1))*block) + 4;
						};
						
						addBar(firstBar, colors[3], machineArray[idx] );
						machineArray[idx][13] = false;
					};
					
					
					
				/* 	machineArray[idx][1].series[0].data[0].update(opTime);
					machineArray[idx][1].series[0].data[1].update(Number(machineArray[idx][5]));
					machineArray[idx][1].series[0].data[2].update(Number(machineArray[idx][6]));
					machineArray[idx][1].series[0].data[3].update(Number(machineArray[idx][7])); */
					
					addBar(bar, color, machineArray[idx] );
				
					if(idx==0){
						$("#dvcName2").html("Machine Name : " + name);
					}else{
						$("#dvcName1").html("Machine Name : " + name);
					} 
				//	$("#dvcName" + (idx+1)).html("Machine Name : " + name);
				});	
			};
			
			var date = new Date();
			var hour =Number(date.getHours());
			var minute = Number(date.getMinutes());
			getCurrentDvcStatus(machineArray[idx], idx);
			resetBar2(machineArray[idx]);	
			
			
			machineArray[idx][10] = hour*60 + minute;
		}
	});
};

function getFirstChartData(dvcId, idx){
	
	var url = ctxPath + "/chart/getCurrentDvcData.do";
	var param = "dvcId=" + dvcId;
	$.ajax({
		url : url,
		data : param,
		dataType : "json",
		type : "post",
		success : function(data){
			if(data.chartStatus=="null"){
				console.log("null data");
				$("#loader").css("opacity","0");
				$("#mainTable").css("opacity","1");
			}else{
				$("#loader").css("opacity","0");
				$("#mainTable").css("opacity","1");
				var splitter = (":");
				var chartStatus = data.chartStatus;
				var bar = data.bar;
				var name = data.name;
				var type = data.type
				var opTime = Number(data.operationTime);
				var stopTime = Number(data.stopTime);
				var startH = addZero(String(data.startDateTime).substr(0,String(data.startDateTime).lastIndexOf(splitter)));
				var startM = addZero(String(data.startDateTime).substr(String(data.startDateTime).lastIndexOf(splitter)+1));
				var endH = addZero(String(data.endDateTime).substr(0,String(data.endDateTime).lastIndexOf(splitter)))
				var endM = addZero(String(data.endDateTime).substr(String(data.endDateTime).lastIndexOf(splitter)+1));
				var spdRatio = data.spdTime;
				var _alarmTime = data.alarmTime;
				var _waitTime = data.waitTime;
				var _offTime = data.offTime;
				var inCycleTime = Number(data.inCycleTime);
				
				
				if(idx==1){
						options = options2_1;
						barArray = barArray2_1;
						spd = spindle1;
						feed = feed1;
						pieChart = pie1;
						jeolboon_ = jeolboon1;
						operation = operation1;
						spdTime += data.spdTime;
						spdTime = spdTime1;
						
						waitTime1 += data.waitTime;
						
						alarmTime1 += alarmTime1;
						offTime1 += offTime1;
						
					}else{
						options = options2_2;
						barArray = barArray2_2;
						spd = spindle2;
						feed = feed2;
						pieChart = pie2;
						jeolboon_ = jeolboon2;
						operation = operation2;
						spdTime += data.spdTime;
						
						spdTime = spdTime2;
						waitTime2 += waitTime2;
						alarmTime2 += alarmTime2;
						offTime2 += offTime2;
					};
					
				if(!isItemInArray(machineArray, name)){
					var chart = new Array();
					chart.push(statusBar);
					chart.push(pieChart);
					chart.push(dvcId);
					chart.push(name);
					chart.push(spdTime);
					chart.push(waitTime);
					chart.push(alarmTime);
					chart.push(offTime);
					chart.push(0);
					chart.push(0);
					chart.push(0);
					chart.push("");
					chart.push(true);
					chart.push(true);
					chart.push(0);
					chart.push("-0:0");
					chart.push(0);
					chart.push(0);
					chart.push(0);
					chart.push(false);
					chart.push(spd);
					chart.push(feed);
					chart.push(operation);
					chart.push(jeolboon_);
					chart.push("");
					chart.push(0);
					chart.push(options);
					chart.push(barArray);
					
					
					machineArray.push(chart);
				};
				
				//machineArray[arrayIndex][17] = spdLoad;
				
	      	
				machineArray[idx][4] = Number(spdRatio);
					machineArray[idx][5] = inCycleTime;
					var chip = machineArray[idx][23].series[0].points[0];

			      	if(Number(machineArray[idx][4])==0 && Number(inCycleTime)==0){
			      		chip.update(0);
			      	}else{
			      		chip.update(Number(Number(machineArray[idx][4]/(inCycleTime)*100).toFixed(1)));
			      	}; 
			      	
					//if(idx==1)console.log("(" + type + ") " + chartStatus + " -> " + data.startDateTime + "~" + data.endDateTime)
					var color = "";
					if(chartStatus.toLowerCase()=="in-cycle"){
						color = colors[0];
					}else if(chartStatus.toLowerCase()=="wait"){
						color = colors[1];
					}else if(chartStatus.toLowerCase()=="alarm"){
						color = colors[2];
					}else if(chartStatus.toLowerCase()=="no-connection"){
						color = colors[3];
					};
					
					if(machineArray[idx][13]){
						var firstBar = "";
						//20시 ~ 시작 시간
						if(Number(startH)>=20){
							firstBar = Number(startH) + (Number(String(startM).substr(0,1))*block) - 20;
						}else{
							firstBar = Number(startH) + (Number(String(startM).substr(0,1))*block) + 4;
						};
						
						addBar(firstBar, colors[3], machineArray[idx] );
						machineArray[idx][13] = false;
					};
					
					
					
				/* 	machineArray[idx][1].series[0].data[0].update(opTime);
					machineArray[idx][1].series[0].data[1].update(Number(machineArray[idx][5]));
					machineArray[idx][1].series[0].data[2].update(Number(machineArray[idx][6]));
					machineArray[idx][1].series[0].data[3].update(Number(machineArray[idx][7])); */
					
					addBar(bar, color, machineArray[idx] );
				
					if(idx==0){
						$("#dvcName2").html("Machine Name : " + name);
					}else{
						$("#dvcName1").html("Machine Name : " + name);
					} 
				//	$("#dvcName" + (idx+1)).html("Machine Name : " + name);
				};	
	
	
				
				var date = new Date();
				var hour =Number(date.getHours());
				var minute = Number(date.getMinutes());
				getCurrentDvcStatus(machineArray[idx], idx);
				resetBar2(machineArray[idx]);	
				
				
				machineArray[idx][10] = hour*60 + minute;
			}
		});
};

var operationTime = 0;
var chartInterval = true;
var over20 = true;
function getCurrentDvcStatus(machine, idx){
	$("#alarmCode0_0").html("");
	$("#alarmMsg0_0").html("");
	$("#alarmCode0_1").html("");
	$("#alarmMsg0_1").html("");
	
	$("#alarmCode1_0").html("");
	$("#alarmMsg1_0").html("");
	$("#alarmCode1_1").html("");
	$("#alarmMsg1_1").html("");
	
	var url = ctxPath + "/chart/getCurrentDvcData.do";
	var param = "dvcId=" + machine[2];
	$.ajax({
		url : url,
		data : param,
		dataType : "json",
		type : "post",
		success : function(data){
			var startTime = removeHypen(data.startDateTime).substr(9,8);
			var endTime = removeHypen(data.endDateTime).substr(9,8);
			
			var chartStatus = removeSpace(data.chartStatus);
			var alarm = data.alarm;
			var spdLoad = data.spdLoad;
			var feedOverride = data.feedOverride;
			var date = data.date;
			var operationTime = data.operationTime;
			var name = data.name;
			var spdTime = data.spdTime;
			//var inCycleTime = Number(data.inCycleTime);
			
			if(chartStatus.toLowerCase()=="in-cycle" && (spdLoad!="0" || spdLoad!="0.0")){
				machine[4]+=3;
				machine[5]+=3;
			}else if(chartStatus.toLowerCase()=="in-cycle"){
				operationTime+=3;
			};
			
			$("#statusLamp" + idx).attr("src", ctxPath + "/images/DashBoard/" + chartStatus +".png");
			
			var operationRatio = Number((operationTime/(hour + minute))*100).toFixed(1);
			
			if(idx==0){
				$("#dvcName2").html("Machine Name : " + name);
			}else{
				$("#dvcName1").html("Machine Name : " + name);
			} 
			
	      	
			var date = new Date();
			var hour = date.getHours();
			var minute = addZero(String(date.getMinutes()));
			
			if(hour==20 && over20){
				machineArray[0][26].series = [];
				machineArray[0][26].title = null;
				machineArray[0][0] = new Highcharts.Chart(machineArray[0][26]);		
				
				machineArray[1][26].series = [];
				machineArray[1][26].title = null;
				machineArray[1][0] = new Highcharts.Chart(machineArray[0][26]);	
				over20 = false;
			};
			
			if(hour>=20){
				hour = date.getHours()-20;
			}else{
				hour = date.getHours()+4;
			};
			
			var targetTime = (hour) + (Number(String(minute).substr(0,1))*block);
			var chartTime = 0;
			
			for(var i = 0; i < machine[27].length;i++){
				chartTime += (Number(machine[27][i][0]));
			};
			
			var feed = machine[21].series[0].points[0];
	      	feed.update(Number(feedOverride));
	      	
	      	var spd = machine[20].series[0].points[0];
	      	spd.update(Number(spdLoad));
	      	
	      	var opt = machine[22].series[0].points[0];
	      	opt.update(Number(Number((operationTime/(hour*60))*100).toFixed(1)));
	      	
	      	var chip = machine[23].series[0].points[0];

	      //	if(machine[3]=='PUMA_GT2100')console.log(inCycleTime,  machine[4])
	      
	      	if(Number(machine[4])==0 && Number(machine[5])==0){
	      		chip.update(0);
	      	}else{
	      		chip.update(Number(Number(machine[4]/(machine[5])*100).toFixed(1)));
	      	};  
	      	
			var addTime = targetTime-chartTime;				
			var color = "";
			if(chartStatus.toLowerCase()=="in-cycle"){
				color = colors[0];
			}else if(chartStatus.toLowerCase()=="wait"){
				color = colors[1];
			}else if(chartStatus.toLowerCase()=="alarm"){
				color = colors[2];
			}else if(chartStatus.toLowerCase()=="no-connection"){
				color = colors[3];
			};
			
			var _hour = date.getHours()
			var _moinute = date.getMinutes();
			
			//if(idx==1)console.log("addTime : "  + addTime);
			
			addBar(addTime, color, machine);
			machine[10] = hour*60 + minute;
			
			resetBar2(machine);
			
			setTimeout(function(){getCurrentDvcStatus(machine,idx);}, 3000);
		}
	});
};

var chartLoop = true;

function removeHypen(str){
	str = str.replace(/-/gi,"");
	return str;
};

var block = 1/6;
var stopTime = 0;

function addBar(bar, color, machine){
	var _bar = new Array();
	_bar.push(bar);
	_bar.push(color);
	
	
	//26 options, 27 bar array
	machine[27].push(_bar);
	
};


function resetBar2(machine){
	machine[26].series = [];
	machine[26].title = null;
	//machine[26].exporting = false;
	for (var i = 0; i < machine[27].length; i++){
		machine[26].series.push({
	        data: [machine[27][i][0]],
	        color : machine[27][i][1]
	    });	
	};
	
	machine[0] = new Highcharts.Chart(machine[26]);
};


/* function drawPieData(chartStatus, bar, machine){
	//statusChart, pieChart, dvcId, name, totalTime, operationTime, stopSpdTime, pieInCycle, pieWait, pieAlarm, preRecordedTime, preStatus, over20, first, barIndex, waitTime, alarmTime, spdLoad, feed, statusChange, spdChart, feedChart, operation, jeolboon
		if(chartStatus=="IN-CYCLE"){
			machine[7] += bar;
			machine[1].series[0].data[0].update(machine[7]);				
		}else if(chartStatus=="WAIT"){
			machine[8] += bar;
			machine[1].series[0].data[1].update(machine[8]);
		}else if(chartStatus=="ALARM"){
			machine[9] += bar;
			machine[1].series[0].data[2].update(machine[9]);
		}else if(chartStatus=="POWEROFF"){
			machine[1].series[0].data[3].update(bar);
		};
}; */

function drawGaugeChart(el1, el2, title1, title2, unit){
	var gaugeOptions = {
			chart: {
				type: 'solidgauge',
		     	backgroundColor : 'rgba(255, 255, 255, 0)',
		  	},
		  	title : false,
			credits : false,
		   	exporting : false, 
		 	pane: {
		 		center: ['50%', '80%'],
		      	size: '100%',
         		startAngle: -90,
         		endAngle: 90,
         		background: {
             	backgroundColor: (Highcharts.theme && Highcharts.theme.background2) || '#EEE',
             	innerRadius: '60%',
             	outerRadius: '100%',
             	shape: 'arc'
         		}
		 	},
    		tooltip: {
    			enabled: false
		  	},

		  	// the value axis
		 	yAxis: {
		 		stops: [
		 		        [1, '#13E000'], // green
		            	],
		            	lineWidth: 0,
		            	minorTickInterval: null,
		            	tickPixelInterval: 400,
		            	tickWidth: 0,
		            	title: {
		                y: -70
		            	},
		            labels: {
		                y: 16
		            	}
		 	},
		 	plotOptions: {
		            solidgauge: {
		                dataLabels: {
		                    y: 5,
		                    borderWidth: 0,
		                    useHTML: true
		                }
		            }
		        }
		    };

		    // The speed gauge
		    $('#' + el1).highcharts(Highcharts.merge(gaugeOptions, {
		        yAxis: {
		            min: 0,
		            max: 200,
		            title: {
		                text: title1,
		               y: -130,
		               style:{
		            	   color : "black",
		            	   fontSize : '20px',
							fontWeight:"bold"
		            	   
		               }
		    		},
		        },

		        credits: {
		            enabled: false
		        },

		        series: [{
		            name: '가동률',
		            data: [0],
		            dataLabels: {
		                format: '<div style="text-align:center"><span style="font-size:40px;color:white">{y}</span>' +
		                       '<span style="font-size:20px;color:white">' + unit + '</span></div>'
		            },
		            tooltip: {
		                valueSuffix: ' %'
		            }
		        }]

		    }));

		    // The RPM gauge
		    $('#' + el2).highcharts(Highcharts.merge(gaugeOptions, {
		        yAxis: {
		            min: 0,
		            max: 200,
		            title: {
		                text: title2,
		               y: -130,
		               style:{
		            	   color : "black",
		            	   fontSize : '20px',
							fontWeight:"bold"
		               }
							
		            },
		        },

		        series: [{
		            name: '절분률',
		            data: [0],
		            dataLabels: {
		                format: '<div style="text-align:center"><span style="font-size:50px;color:white">{y}</span>' +
		                       '<span style="font-size:30px;color:white">' + unit + '</span></div>'
		            },
		            tooltip: {
		                valueSuffix: '%'
		            }
		        }]

		    }));
};

function drawGaugeChart2(el1, el2, title1, title2, unit){
	var gaugeOptions = {
			chart: {
				type: 'solidgauge',
		     	backgroundColor : 'rgba(255, 255, 255, 0)'
		  	},
		  	title : false,
			credits : false,
		   	exporting : false, 
		 	pane: {
		 		center: ['50%', '80%'],
		      	size: '100%',
         		startAngle: -90,
         		endAngle: 90,
         		background: {
             	backgroundColor: (Highcharts.theme && Highcharts.theme.background2) || '#EEE',
             	innerRadius: '60%',
             	outerRadius: '100%',
             	shape: 'arc'
         		}
		 	},
    		tooltip: {
    			enabled: false
		  	},

		  	// the value axis
		 	yAxis: {
		 		stops: [
		 		        [1, '#13E000'], // green
		            	],
		            	lineWidth: 0,
		            	minorTickInterval: null,
		            	tickPixelInterval: 400,
		            	tickWidth: 0,
		            	title: {
			               		y: -70
			            	},
		            labels: {
		                y: 16
		            	}
		 	},
		 	plotOptions: {
		            solidgauge: {
		                dataLabels: {
		                    y: 5,
		                    borderWidth: 0,
		                    useHTML: true
		                }
		            }
		        }
		    };

		    // The speed gauge
		    $('#' + el1).highcharts(Highcharts.merge(gaugeOptions, {
		        yAxis: {
		            min: 0,
		            max: 100,
		            title: {
		                text: title1,
		               y: -130,
		               style:{
		            	   color : "black",
		            	   fontSize : '20px',
							fontWeight:"bold"
		            	   
		               }
		    		},
		        },

		        credits: {
		            enabled: false
		        },

		        series: [{
		            name: '가동률',
		            data: [0],
		            dataLabels: {
		                format: '<div style="text-align:center"><span style="font-size:40px;color:white">{y}</span>' +
		                       '<span style="font-size:20px;color:white">' + unit + '</span></div>'
		            },
		            tooltip: {
		                valueSuffix: ' %'
		            }
		        }]

		    }));

		    // The RPM gauge
		    $('#' + el2).highcharts(Highcharts.merge(gaugeOptions, {
		        yAxis: {
		            min: 0,
		            max: 100,
		            title: {
		                text: title2,
		               y: -130,
		               style:{
		            	   color : "black",
		            	   fontSize : '20px',
							fontWeight:"bold"
		               }
							
		            },
		        },

		        series: [{
		            name: '절분률',
		            data: [0],
		            dataLabels: {
		                format: '<div style="text-align:center"><span style="font-size:50px;color:white">{y}</span>' +
		                       '<span style="font-size:30px;color:white">' + unit + '</span></div>'
		            },
		            tooltip: {
		                valueSuffix: '%'
		            }
		        }]

		    }));
};


function time3(){
	var date = new Date();
	var month = date.getMonth()+1;
	var date_ = addZero(String(date.getDate()));
	var day = date.getDay();
	var hour = addZero(String(date.getHours()));
	var minute = addZero(String(date.getMinutes()));
	var second = addZero(String(date.getSeconds()));
	
	if(day==1){
		day = "Mon";
	}else if(day==2){
		day = "Tue";
	}else if(day==3){
		day = "Wed";
	}else if(day==4){
		day = "Thu";
	}else if(day==5){
		day = "Fri";
	}else if(day==6){
		day = "Sat";
	}else if(day==0){
		day = "Sun";
	};
	
	$("#date").html(month + " / " + date_ + " (" + day + ")");
	$("#time").html(hour + " : " + minute + " : " + second);
};

function addZero(n){
	if(n.length=="1"){
		n = "0" + n;
	};
	return n;
};

/* function pieChart(id){
	Highcharts.setOptions({
		//green yellow red black
		   colors: ['#148F01', '#C7C402', '#ff0000', '#8C9089']
	    });
	
    
    // Radialize the colors
    Highcharts.getOptions().colors = Highcharts.map(Highcharts.getOptions().colors, function (color) {
        return {
            radialGradient: { cx: 0.5, cy: 0.5, r: 0.7 },
            stops: [
                [0, Highcharts.Color(color).brighten(0.5).get('rgb')], // darken
                [1, color]
                
            ]
        };
    });
    
	$('#' + id)
	.highcharts(
			{
				chart : {
					plotBackgroundColor : null,
					plotBorderWidth : null,
					plotShadow : false,
					backgroundColor : 'rgba(255, 255, 255, 0)',
					type: 'pie',
		            options3d: {
		                enabled: true,
		                alpha: 45
		            }
				},
				credits : false,
				title : {
					text : false
				},
				tooltip : {
					pointFormat : '{series.name}: <b>{point.percentage:.1f}%</b>',
					enabled: true,
				},
				plotOptions : {
					pie : {
						innerSize: 130,
			      		depth: 45,
						size:'100%',
						allowPointSelect : true,
						cursor : 'pointer',
						dataLabels : {
							enabled : false,
							format : '<b>{point.name}</b>: {point.percentage:.1f} %',
							style : {
								color : (Highcharts.theme && Highcharts.theme.contrastTextColor)
										|| 'black'
							}
						}
					}
				},
				 exporting: false,
				series : [ {
					type : 'pie',
					name : 'status ratio',
					data : [ [ 'in-cycle', 1 ], 
					         [ 'wait', 1 ],
					         [ 'alarm', 1 ],
					         [ 'no-connection', 1 ],
							]
				} ]
			});
}; */

function parsingAlarm(idx,alarm){
	var json = $.parseJSON(alarm);
	
	$(json).each(function (i,data){
		$("#alarmCode" + idx + "_" + i).html(data.alarmCode + " - ");
		$("#alarmMsg" + idx + "_" + i).html(data.alarmMsg);
	});
};

var colors;
function statusChart3(id){
	var perShapeGradient = {
            x1: 0,
            y1: 0,
            x2: 1,
            y2: 0
        };
        colors = Highcharts.getOptions().colors;
        colors = [
                  {
			          linearGradient: perShapeGradient,
			            stops: [
			                [0, '#148F01'],
			                [1, '#9AFC8A']
			                ]
			     	}, 		            
			     	{
			            linearGradient: perShapeGradient,
			            stops: [
			                [0, '#C7C402'],
			                [1, '#FEFE82']
			                ]
		            }, 
		            {
			            linearGradient: perShapeGradient,
			            stops: [
			                [0, '#ff0000'],
			                [1, '#FC9999']
			                ]
		           },
		           {
			            linearGradient: perShapeGradient,
			            stops: [
			                [0, '#8c9089'],
			                [1, '#ffffff']
	                	]
		           }
        ]
        
	$('#' + id).highcharts({
		chart : {
			type : 'bar',
			backgroundColor : 'rgba(255, 255, 255, 0)',
			height: 100
		},
		credits : false,
		//exporting: false, 
		title : false,
		xAxis : {
			categories : [ "" ],
			labels : {
				style : {
					fontSize : '25px',
					fontWeight:"bold"
				}
			}
		},
		tooltip : {
			headerFormat : "",
			style : {
				fontSize : '20px'
			},
			enabled : false 
		},
		yAxis : {
			min : 0,
			max : 24,
			tickInterval:1,
			reversedStacks: false,
			title : {
				text : false
			},
			labels : {
				style : {
					fontSize : '20px'
				},
				enabled : false
			}
		},
		legend : {
			enabled : false
		},
		plotOptions : {
			series : {
				stacking : 'normal',
				 pointWidth:70,
				 borderWidth: 0,
				 animation : false
			}
		},
		series : []
	});
};
	
	
	function setElement(){
		var width = window.innerWidth;
		var height = window.innerHeight;
		
		$("#title_main").css({
			"left" : width/2 - $("#title_main").width()/2
		});
		
		$("hr").css({
			"left" : width/2 - $("hr").width()/2
		});
		
		$("#mainTable").css({
			"left" : width/2 - $("table").width()/2
		});
		
		$(".status").css({
			"width" : width*0.4
		});
		
		$("#loader").css({
			"left" : width/2 - $("#loader").width()/2,
			"top" : height/2 - $("#loader").height()/2
		});
	};
	
	function addZero(n){
		if(n.length=="1"){
			n = "0" + n;
		};
		return n;
	};
</script>
<style> 
body{
	background: url("../images/DashBoard/Background.png");
	background-size : 100% 100%;
}

.title{
	top : 50px;
	position: absolute;
}

#title_main{
	width: 1000px;
	z-index: 999;
}
#title_left{
	left : 50px;
	width: 300px;
}

#title_right{
	right: 50px;
	width: 300px;
}

#pieChart1{
	position: absolute;
	left : 0px;
	z-index: 99;
	top: 800px;
	height: 500px;
}

#pieChart2{
	position: absolute;
	right : 0px;
	z-index: 99;
	top: 800px;
	height: 500px;
} 

#time{
	position: absolute;
	top: 170px;
	right: 50px;	
	font-size : 30px;
	color: white;
} 
#date{
	position: absolute;
	top: 170px;
	right: 250px;	
	font-size : 30px;
	color: white;
} 

hr{
	position: absolute;
	top: 250px;
	z-index: 99;
	width: 90%;
	border: 2px solid white;
}

#mainTable{
	width: 90%;
	position: absolute;
	top: 270px;
	z-index: 99;
	opacity : 0;
}

#mainTable tr:last-child td:first-child {
    -moz-border-radius-bottomleft:10px;
    -webkit-border-bottom-left-radius:10px;
    border-bottom-left-radius:10px;
    
    -moz-border-radius-topleft:10px;
    -webkit-border-top-left-radius:10px;
    border-top-left-radius:10px
}

#mainTable tr:last-child td:last-child {
    -moz-border-radius-bottomright:10px;
    -webkit-border-bottom-right-radius:10px;
    border-bottom-right-radius:10px;
    
    -moz-border-radius-topright:10px;
    -webkit-border-top-right-radius:10px;
    border-top-right-radius:10px
}

.statusIcon{
	width: 80px;
	height: 80px;
}

.pie{
	width: 95%;
	height: 800px;
	margin: 0 auto;
}

.tr{
  background: linear-gradient(white, gray); /* Standard syntax */
  opacity : 0.7;
  
}
#mainTable{
	opacity : 0;
}
#loader{
	position: absolute;
	width: 500px;
	-webkit-filter: brightness(10);
	/* background-color: white;
	-webkit-border-radius: 50em;
	-moz-border-radius: 50em;
	border-radius: 50em; */
}
#title_2{
	right : 50px;
	position : absolute;
	top : 50px;
}
#title_1{
	top : 50px;
	position: absolute;
	z-index: 9999;
}

</style>
<script type="text/javascript">
	
</script>

</head>


<body>
	<div id="title_2" style="color: white; font-size: 45px; font-weight: bolder;">Doosan Infracore<br>	 Machine Tools</div>
	<%-- <img src="${ctxPath }/images/DashBoard/title2.svg" id="title_main" class="title"> --%>
	
	<div id="title_1" style="color: white; font-size: 130px; font-weight: bolder;">Machine Operation Status</div>
	<img src="${ctxPath }/images/DashBoard/title_left.svg" id="title_left" class="title">
	
	<hr>

	<font id="date"></font>
	<font id="time"></font>
	
	<br>
	<br>
	<img alt="" src="${ctxPath }/images/DashBoard/loader.png" id="loader">
	<table id="mainTable">
		<tr>
			<td style="border-right: 2px solid white; width: 50%; padding: 0 50 0 50">
				<font style="font-weight: bold; color: white; font-size: 50px" id="dvcName1"></font><br>
				<font style="font-weight: bold; color: white; font-size: 50px"></font><br>
				<img id="statusLamp0" class="statusIcon" src="" style="position: absolute;top: 5; left: 3	0px; top: 70PX;">
				<br>
				<br>
				<br>
				
				<div id="status1" class="status"></div>
				<div style="width: 1550px; margin-left: 0px;">
					<font style="font-weight: bold; color:white; font-size: 28px">20</font>
					<font style="font-weight: bold; color:white; font-size: 28px; margin-left: 20px">21</font>
					<font style="font-weight: bold; color:white; font-size: 28px; margin-left: 28px">22</font>
					<font style="font-weight: bold; color:white; font-size: 28px; margin-left: 28px">23</font>
					<font style="font-weight: bold; color:white; font-size: 28px; margin-left: 28px">24</font>
					<font style="font-weight: bold; color:white; font-size: 28px; margin-left: 28px">01</font>
					<font style="font-weight: bold; color:white; font-size: 28px; margin-left: 28px">02</font>
					<font style="font-weight: bold; color:white; font-size: 28px; margin-left: 24px">03</font>
					<font style="font-weight: bold; color:white; font-size: 28px; margin-left: 24px">04</font>
					<font style="font-weight: bold; color:white; font-size: 28px; margin-left: 25px">05</font>
					<font style="font-weight: bold; color:white; font-size: 28px; margin-left: 24px">06</font>
					<font style="font-weight: bold; color:white; font-size: 28px; margin-left: 25px">07</font>
					<font style="font-weight: bold; color:white; font-size: 28px; margin-left: 25px">08</font>
					<font style="font-weight: bold; color:white; font-size: 28px; margin-left: 25px">09</font>
					<font style="font-weight: bold; color:white; font-size: 28px; margin-left: 26px">10</font>
					<font style="font-weight: bold; color:white; font-size: 28px; margin-left: 32px">11</font>
					<font style="font-weight: bold; color:white; font-size: 28px; margin-left: 33px">12</font>
					<font style="font-weight: bold; color:white; font-size: 28px; margin-left: 33px">13</font>
					<font style="font-weight: bold; color:white; font-size: 28px; margin-left: 33px">14</font>
					<font style="font-weight: bold; color:white; font-size: 28px; margin-left: 33px">15</font>
					<font style="font-weight: bold; color:white; font-size: 28px; margin-left: 32px">16</font>
					<font style="font-weight: bold; color:white; font-size: 28px; margin-left: 32px">17</font>
					<font style="font-weight: bold; color:white; font-size: 28px; margin-left: 32px">18</font>
					<font style="font-weight: bold; color:white; font-size: 28px; margin-left: 30px">19</font>
					<font style="font-weight: bold; color:white; font-size: 28px; margin-left: 28px">20</font>
				</div>
				
				<br>
				<br>
				<br>
				
				<br>
				
				<table class="subTable" style="width:100%"> 
					<tr style="color: black; font-size: 40px; background-color: #fff8dc; font-weight: bold;" >
						<td width="50%" align="center" style="padding: 20px;"  colspan="2">
							Current Status
						</td>
					</tr>
					<tr class="tr">
						<td width="50%">
							<div class="pie" id="pie1"></div>
						</td>
						<td >
							<div style="width: 1300px; position: absolute; left: 220px; top: 430px">
								<div id="gauge1" style="float: left; width: 50%"></div>
								<div id="gauge2" style="float: right; width: 50%"></div>
							</div>
							<div style="width: 1300px; position: absolute; left:220; top: 830px">
								<div id="gauge3" style="float: left; width: 50%"></div>
								<div id="gauge4" style="float: right; width: 50%"></div>
							</div>
							<!-- <div style="position:absolute; left: 920px; top: 1280px; text-align: left; line-height: 50px">
								<font style="font-size: 30px; font-weight: bold; color: red;" >Alarm</font><br>
								<font style="font-size: 30px; font-weight: bold; color: black;" id="alarmCode1_0"></font>
								<font style="font-size: 30px; font-weight: bold; color: black;" id="alarmMsg1_0"></font><br>
								<font style="font-size: 30px; font-weight: bold; color: black;" id="alarmCode1_1"></font>
								<font style="font-size: 30px; font-weight: bold; color: black;" id="alarmMsg1_1"></font><br>
							</div> -->
						</td>
					</tr>
				</table>
			</td>
			<td style="border-right: 2px solid white; width: 50%; padding: 0 50 0 50">
				<font style="font-weight: bold; color: white; font-size: 50px" id="dvcName2"></font><br>
				<font style="font-weight: bold; color: white; font-size: 50px"></font><br>
				<img  class="statusIcon" src="" id="statusLamp1" style="position: absolute;top: 70; left: 1790	;">
				<br>
				<br>
				<br>
				
				<div id="status2" class="status"></div>
				<div style="width: 1550px; margin-left: 0px;">
					<font style="font-weight: bold; color:white; font-size: 28px">20</font>
					<font style="font-weight: bold; color:white; font-size: 28px; margin-left: 20px">21</font>
					<font style="font-weight: bold; color:white; font-size: 28px; margin-left: 28px">22</font>
					<font style="font-weight: bold; color:white; font-size: 28px; margin-left: 28px">23</font>
					<font style="font-weight: bold; color:white; font-size: 28px; margin-left: 28px">24</font>
					<font style="font-weight: bold; color:white; font-size: 28px; margin-left: 28px">01</font>
					<font style="font-weight: bold; color:white; font-size: 28px; margin-left: 28px">02</font>
					<font style="font-weight: bold; color:white; font-size: 28px; margin-left: 24px">03</font>
					<font style="font-weight: bold; color:white; font-size: 28px; margin-left: 24px">04</font>
					<font style="font-weight: bold; color:white; font-size: 28px; margin-left: 25px">05</font>
					<font style="font-weight: bold; color:white; font-size: 28px; margin-left: 24px">06</font>
					<font style="font-weight: bold; color:white; font-size: 28px; margin-left: 25px">07</font>
					<font style="font-weight: bold; color:white; font-size: 28px; margin-left: 25px">08</font>
					<font style="font-weight: bold; color:white; font-size: 28px; margin-left: 25px">09</font>
					<font style="font-weight: bold; color:white; font-size: 28px; margin-left: 26px">10</font>
					<font style="font-weight: bold; color:white; font-size: 28px; margin-left: 32px">11</font>
					<font style="font-weight: bold; color:white; font-size: 28px; margin-left: 33px">12</font>
					<font style="font-weight: bold; color:white; font-size: 28px; margin-left: 33px">13</font>
					<font style="font-weight: bold; color:white; font-size: 28px; margin-left: 33px">14</font>
					<font style="font-weight: bold; color:white; font-size: 28px; margin-left: 33px">15</font>
					<font style="font-weight: bold; color:white; font-size: 28px; margin-left: 32px">16</font>
					<font style="font-weight: bold; color:white; font-size: 28px; margin-left: 32px">17</font>
					<font style="font-weight: bold; color:white; font-size: 28px; margin-left: 32px">18</font>
					<font style="font-weight: bold; color:white; font-size: 28px; margin-left: 30px">19</font>
					<font style="font-weight: bold; color:white; font-size: 28px; margin-left: 28px">20</font>
				</div>
				<br>
				<br>
				<br>
				<br>
				<table class="subTable" style="width:100%"> 
					<tr style="color: black; font-size: 40px; background-color: #fff8dc; font-weight: bold;" >
						<td width="50%" align="center" style="padding: 20px;"  colspan="2">
							Current Status
						</td>
					</tr>
					<tr class="tr">
						<td width="50%">
							<div class="pie" id="pie2"></div>
						</td>
						<td >
							<div style="width: 1300px; position: absolute; left: 1940px; top: 430px">
								<div id="gauge5" style="float: left; width: 50%"></div>
								<div id="gauge6" style="float: right; width: 50%"></div>
							</div>
							<div style="width: 1300px; position: absolute; left: 1940PX; top: 830px">
								<div id="gauge7" style="float: left; width: 50%"></div>
								<div id="gauge8" style="float: right; width: 50%"></div>
							</div>
						<!-- 	<div style="position:absolute; left: 2640px; top: 1280px; text-align: left; line-height: 50px">
								<font style="font-size: 30px; font-weight: bold; color: red;" >Alarm</font><br>
								<font style="font-size: 30px; font-weight: bold; color: black;" id="alarmCode1_0"></font>
								<font style="font-size: 30px; font-weight: bold; color: black;" id="alarmMsg1_0"></font><br>
								<font style="font-size: 30px; font-weight: bold; color: black;" id="alarmCode1_1"></font>
								<font style="font-size: 30px; font-weight: bold; color: black;" id="alarmMsg1_1"></font><br>
							</div> -->
						</td>
					</tr>
				</table>
			</td>
		</tr>
	</table>
</body>
</html>