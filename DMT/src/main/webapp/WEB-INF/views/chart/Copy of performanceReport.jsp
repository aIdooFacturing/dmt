<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ include file="/WEB-INF/views/include/lib.jsp"%>


<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<meta http-equiv="X-UA-Compatible" content="chrome=1">
<meta http-equiv="cache-control" content="max-age=0" />
<meta http-equiv="cache-control" content="no-cache" />
<meta http-equiv="expires" content="0" />
<meta http-equiv="expires" content="Tue, 01 Jan 1980 1:00:00 GMT" />
<meta http-equiv="pragma" content="no-cache" />
<link rel="shortcut icon" href="${ctxPath }/images/dulink_favicon.ico">
<script type="text/javascript" src="${ctxPath }/js/jqMap.js"></script>
<title><spring:message code="operation_chart"></spring:message></title>
<style type="text/css">
	.selected_span{
	background-color :white;
	color : #008900;
}
	
</style>
<script type="text/javascript">
	var ctxPath = "${ctxPath}";
	var targetWidth = 3840;
	var targetHeight = 2160;
	var wcData;
	var jigData;
	
	var originWidth = window.innerWidth;
	var originHeight = window.innerHeight;

	var contentWidth = originWidth;
	var contentHeight = targetHeight / (targetWidth / originWidth);

	var screen_ratio = getElSize(240);

	if (originHeight / screen_ratio < 9) {
		contentWidth = targetWidth / (targetHeight / originHeight)
		contentHeight = originHeight;
	};

	function getElSize(n) {
		return contentWidth / (targetWidth / n);
	};

	function setElSize(n) {
		return targetWidth / (contentWidth / n);
	};
	
	var marginWidth = (originWidth-contentWidth)/2;
	var marginHeight = (originHeight-contentHeight)/2;
	
	function csvSend(){
		var id = this.id;
		var sDate, eDate;
		var csvOutput;
		
		if(id=="jigExcel"){
			sDate = $("#jig_sdate").val();
			eDate = $("#jig_edate").val();
			csvOutput = jigData;
		}else{
			sDate = $("#wc_sdate").val();
			eDate = $("#wc_edate").val();
			csvOutput = wcData;
		};
		
		csvOutput = csvOutput.replace(/\n/gi,"");
		
		f.csv.value=encodeURIComponent(csvOutput);
		f.startDate.value = sDate;
		f.endDate.value = eDate;
		f.submit(); 
	};
</script>
<script src="${ctxPath }/js/jquery-1.11.2.min.js"></script>
<script src="${ctxPath }/js/jquery-ui.js"></script>
<link rel="stylesheet" href="${ctxPath }/css/default.css">
<script type="text/javascript">

var shopId = 1;

window.onhashchange = function(e) {
	  var oldURL = e.oldURL.split('#')[1];
	  var newURL = e.newURL.split('#')[1];
	
	  if(oldURL!=newURL){
		  clearMenu();
	  }
}
	
var save_type = "init"

	function getBanner(){
	var url = "${ctxPath}/chart/getBanner.do";
	var param = "shopId=" + shopId;
	

	$.ajax({
		url : url,
		data : param,
		type : "post",
		dataType : "json",
		success : function(data){
			if(window.localStorage.getItem("banner")=="true"){
				$("#intro").html(data.msg).css({
					"color" : data.rgb,
					"right" : - window.innerWidth
				})
				
				$("#intro").html(data.msg).css({
					"right" : - $("#intro").width()
				})
			}
			
			bannerWidth = $("#intro").width() + getElSize(100); 
			$("#intro").width(bannerWidth);
			$("#banner").val(data.msg);
			$("#color").val(data.rgb);
			
			twinkle();
			bannerAnim();
		}
	});		
}

var twinkle_opct = true;
function twinkle(){
	var opacity;
	if(twinkle_opct){
		opacity = 0;
	}else{
		opacity = 1;
	}
	$("#intro").css("opacity",opacity);
	
	twinkle_opct = !twinkle_opct;
	setTimeout(twinkle, 100)
};

var bannerWidth;
function bannerAnim(){
	$("#intro").width(bannerWidth - getElSize(10));
	$("#intro").animate({
		"right" : window.innerWidth  - getElSize(100)
	},15000, function(){
		$("#intro").css("right" , - $("#intro").width())
		//$(this).remove();
		bannerAnim();
	});
};

function chkBanner(){
	if(window.localStorage.getItem("banner")=="true"){
		getBanner();
		$("#intro_back").css("display","block");
	}else{
		$("#intro").html("");
		$("#intro_back").css("display","none");
	}
};

function cancelBanner(){
	window.localStorage.setItem("banner", "false");
	$("#bannerDiv").css("z-index",-9);
	chkBanner();
};

function addBanner(){
	var url = "${ctxPath}/chart/addBanner.do";
	var param = "msg=" + encodeURIComponent($("#banner").val()) + 
				"&rgb=" + $("#color").val() + 
				"&shopId=" + shopId;
	
	$.ajax({
		url : url,
		data : param,
		type : "post",
		success : function(data){
			window.localStorage.setItem("banner", "true");
			location.reload();
			/* $("#bannerDiv").css("z-index",-9);
			chkBanner(); */
		}
	});
};

function getGroup(){
	var url = "${ctxPath}/chart/getGroup.do";
	var param = "shopId=" + shopId;
	
	$.ajax({
		url : url,
		data : param,
		type : "post",
		dataType : "json",
		success : function(data){
			json = data.dataList;
			
			var option = "<option value='all'>전체</option>";
			
			$(json).each(function(idx, data){
				option += "<option value='" + data.group + "'>" + data.group + "</option>"; 
			});
			
			$("#group").html(option);
			
			getTableData("jig");
		}
	});
};



$(function(){
	//location.href = ctxPath + "/chart/performanceReport.do#chart";
	chkBanner()
	getGroup();
	setDate();			
	
	//$(".date").change(getTableData);
	/* $("#group").change(function(){
		getTableData("jig");
	}); */
	
	$(".excel").click(csvSend);
	setEl();
	setEvt();
	//getMousePos();
	$("#menu_btn").click(function(){
			location.href = "${ctxPath}/chart/index.do"
	});
	
	$(".menu").click(goReport);
	
	$("#panel_table td").addClass("unSelected_menu");
	$("#menu1").removeClass("unSelected_menu");
	$("#menu1").addClass("selected_menu");
	
	$(".goGraph").click(goGraphPage);
	
	$("#dlgTable").css({
		"width" : "95%",
		"display" : "none",
		"background-color" : "#505050",
		"border-radius" : getElSize(30)
	});
	
	$("#dlgTable").css({
		"position" : "absolute",
		"z-index" : -1,
		"left" : originWidth/2 - ($("#dlgTable").width()/2),
		"top" : originHeight/2 - ($("#dlgTable").height()/2)
	});
	
	$("#corver").css({
		"width" : originWidth,
		"height" : originHeight,
		"position" : "absolute",
		"z-index" : -1,
		"background-color" : "black",
		"opacity" : 0,
		"top" : 0
	});
	

});

var jigCsv;
var wcCsv;
var selected_dvc;
var className = "";
var classFlag = true;
var preLine;
var sum_target_op_time = 0;
	sum_total_op_time = 0;
	sum_op_date = 0;
	sum_incycle = 0;
	sum_wait = 0;
	sum_alarm = 0;
	sum_noConn = 0;
function getTableData(el){
	var id = this.id;

	var sDate;
	var eDate;
	var ty;
	var url = "${ctxPath}/chart/getTableData.do";
	if(id=="jig_sdate" || id=="jig_edate" || el=="jig"){
		sDate = $("#jig_sdate").val();
		eDate = $("#jig_edate").val();
		
		window.localStorage.setItem("jig_sDate", sDate);
		window.localStorage.setItem("jig_eDate", eDate);
		
	}else{
		sDate = $("#wc_sdate").val();
		eDate = $("#wc_edate").val();
		
		showWcData(selected_dvc,sDate, eDate);
		
		window.localStorage.setItem("wc_sDate", sDate);
		window.localStorage.setItem("wc_eDate", eDate);
		
		return;
	};
	
	var param = "sDate=" + sDate + 
				"&eDate=" + eDate + 
				"&ty=" +ty +
				"&shopId=" + shopId + 
				"&group=" + $("#group").val();
	
	$.ajax({
		url : url,
		data : param,
		dataType : "json",
		type : "post",
		success : function(data){
			var json = data.tableData;
			
			var start = new Date(sDate);
			var end = new Date(eDate);
			var n = (end - start)/(24 * 3600 * 1000)+1;

			$(".contentTr").remove();
			var tr = "<tbody>";
			jigData = "${device},${target_op_time},${total_op_time},${number_of_op_day},${incycle},${wait},${alarm},${noconnection},${opratio_against_target}LINE";
			
			var sum_target_op_time = 0;
			var sum_total_op_time = 0;
			var sum_op_date = 0;
			var sum_incycle = 0;
			var sum_wait = 0;
			var sum_alarm = 0;
			var sum_noConn = 0;
			var sum_opRatio = 0;
			var dvcCnt = 0;
			$(json).each(function(idx, data){
				if(preLine != data.line && idx!=0){
					tr += "<tr>" + 
							"<td>" + preLine + " (소계)</td>" + 
							"<td>" + Math.round(sum_target_op_time) + "</td>" +
							"<td>" + Math.round(sum_total_op_time) + "</td>" + 
							"<td>" + Math.round(sum_op_date) + "</td>" + 
							"<td>" + Math.round(sum_incycle) + "</td>" +
							"<td>" + Math.round(sum_wait) + "</td>" + 
							"<td>" + Math.round(sum_alarm) + "</td>" +
							"<td>" + Math.round(sum_noConn) + "</td>" + 
							"<td>" + (sum_opRatio/dvcCnt).toFixed(1) + "%</td>" + 
						"</tr>";
						
					sum_target_op_time = 0;
					sum_total_op_time = 0;
					sum_op_date = 0;
					sum_incycle = 0;
					sum_wait = 0;
					sum_alarm = 0;
					sum_noConn = 0;
					sum_opRatio = 0;
					
					console.log(dvcCnt)
					dvcCnt = 0;
				}
				preLine = data.line;
				dvcCnt++;
				
				if(classFlag){
					className = "row2"
				}else{
					className = "row1"
				};
				classFlag = !classFlag;
				
				var incycle = Number(Number(data.inCycle_time/60/60/n).toFixed(1));
				var wait = Number(Number(data.wait_time/60/60/n).toFixed(1));
				var alarm = Number(Number(data.alarm_time/60/60/n).toFixed(1));
				var noconn = Number(24 - (incycle + wait + alarm)).toFixed(1);
				
				tr += "<tr class='contentTr " + className + "' ondblclick='showWcData(\"" + data.name + "\","  + "\"" + sDate + "\"," +  "\"" + eDate + "\"" + ",\"jig\")'>" +
							"<td>" + decodeURIComponent(data.name).replace(/\+/gi, " ") + "</td>" +
							"<td>" + Number(data.target_time/60/60).toFixed(1) + "</td>" + 
							"<td>" + Number(data.inCycle_time/60/60).toFixed(1) + "</td>" + 
							"<td>" + n + "</td>" + 
							"<td>" + incycle + "</td>" +
							"<td>" + wait + "</td>" + 
							"<td>" + alarm + "</td>" +
							"<td>" + noconn + "</td>" + 
							"<td>" + Number((data.inCycle_time/(data.target_time)) * 100).toFixed(1) + "%</td>" + 
					 "</tr>";
					 
				jigData += data.name + "," + 
						Number(data.target_time/60/60).toFixed(1) + "," +  
						Number(data.inCycle_time/60/60).toFixed(1) + "," + 
						n + "," + 
						incycle + "," + 
						wait + "," + 
						alarm + "," + 
						noconn + "," + 
						Number((data.inCycle_time/data.target_time) * 100).toFixed(1) + "%LINE";
				
				sum_target_op_time += Number(Number(data.target_time/60/60).toFixed(1));
				sum_total_op_time += Number(Number(data.inCycle_time/60/60).toFixed(1));
				sum_op_date += Number(n);
				sum_incycle += Number(incycle);
				sum_wait += Number(wait);
				sum_alarm += Number(alarm);
				sum_noConn += Number(noconn);	
				sum_opRatio += Number(Number((data.inCycle_time/(data.target_time)) * 100).toFixed(1));
				console.log((Number((data.inCycle_time/(data.target_time)) * 100).toFixed(1)))
			});
			console.log(sum_opRatio,dvcCnt)
			tr+= "<tr>" + 
					"<td>" + preLine + " (소계)</td>" + 
					"<td>" + Math.round(sum_target_op_time) + "</td>" +
					"<td>" + Math.round(sum_total_op_time) + "</td>" + 
					"<td>" + Math.round(sum_op_date) + "</td>" + 
					"<td>" + Math.round(sum_incycle) + "</td>" +
					"<td>" + Math.round(sum_wait) + "</td>" + 
					"<td>" + Math.round(sum_alarm) + "</td>" +
					"<td>" + Math.round(sum_noConn) + "</td>" + 
					"<td>" + (sum_opRatio/dvcCnt).toFixed(1) + "%</td>" + 
				"</tr></tbody>";
			
			$("#jigTable").append(tr);
			
			$(".row1").not(".tr_table_fix_header").css({
				"background-color" : "#222222"
			});

			$(".row2").not(".tr_table_fix_header").css({
				"background-color": "#323232"
			});
			
			setEl();
			
			$(".tableContainer div:last").remove()
			
			scrolify($('#jigTable'), getElSize(1500));
			
			$("*").not("#dvcDiv").css({
				"overflow-x" : "hidden",
				"overflow-y" : "auto"
			})
		}
	});
};

var dvc;

function replaceHash(str){
	return str.replace(/#/gi,"-");
};

function showWcData(name,sDate, eDate, ty){
	window.location.hash = "pop"
	selected_dvc = name;
	dvc = replaceHash(name);
	var url = "${ctxPath}/chart/getWcData.do";
	var param = "&sDate=" + sDate + 
				"&eDate=" + eDate + 
				"&name=" + name;;

	
	if(ty=="jig"){
		$("#wc_sdate").val($("#jig_sdate").val());
		$("#wc_edate").val($("#jig_edate").val());		
	};
	
	$.ajax({
		url : url,
		data : param,
		dataType : "json",
		type : "post",
		success : function(data){
			var json = data.wcList;
			
			$(".contentTr2").remove();
			var tr = "<tbody>";
			wcData = "${device},${target_op_time},${date},${incycle},${wait},${alarm},${noconnection},${opratio_against_target}LINE";
			$(json).each(function(idx, data){
				if(classFlag){
					className = "row2"
				}else{
					className = "row1"
				};
				classFlag = !classFlag;
				
				var incycle = Number(Number(data.inCycle_time/60/60).toFixed(1));
				var wait = Number(Number(data.wait_time/60/60).toFixed(1));
				var alarm = Number(Number(data.alarm_time/60/60).toFixed(1));
				var noconn = Number(24 - (incycle + wait + alarm)).toFixed(1);
				
				tr += "<tr class='contentTr2 " + className + "')'>" +
							"<td>" + decodeURIComponent(data.name).replace(/\+/gi, " ") + "</td>" +
							"<td>" + Number(data.target_time/60/60).toFixed(1) + "</td>" + 
							"<td>" + data.workDate + "</td>" + 
							"<td>" + incycle + "</td>" +
							"<td>" + wait + "</td>" + 
							"<td>" + alarm + "</td>" +
							"<td>" + noconn + "</td>" + 
							"<td>" + Number((data.inCycle_time/data.target_time) * 100).toFixed(1) + "%</td>" + 
					 "</tr></tbody>";
					 
				
				wcData += data.name + "," + 
						Number(data.target_time/60/60).toFixed(1) + "," + 
						data.workDate + "," +
						incycle + "," + 
						wait + "," +
						alarm + "," +
						noconn + "," +
						Number((data.inCycle_time/data.target_time) * 100).toFixed(1) + "%LINE";
			});
			tr += "</tbody>";
			$("#wcTable").append(tr);
			setEl();
			//getDvcList(name);
			
			$("#corver").click(function(){
				//clearMenu();
				//location.href = "${ctxPath}/chart/performanceReport.do";
			});
			
			$("#corver").css({
				"z-index" : 9,
				"opacity" : 0.7
			});
			
			$("#dlgTable").css({
				"z-index":10,
				"display" : "block"
			});
			
			$(".row1").not(".tr_table_fix_header").css({
				"background-color" : "#222222"
			});

			$(".row2").not(".tr_table_fix_header").css({
				"background-color": "#323232"
			});
			
			$(".tableContainer2 div:last").remove();
			scrolify($('#wcTable'), getElSize(1300));
			
			$("*").css({
				"overflow-x" : "hidden",
				"overflow-y" : "auto"
			})
			
			$("#dvcDiv2").css({
				"margin-left" : getElSize(100),
				"width" : $(".tableContainer2").width(),
				"margin-bottom" : getElSize(50)
			})
		}
	});
};

function showWcDatabyDvc(dvc, sDate, eDate, ty){
	var url = "${ctxPath}/chart/getWcDataByDvc.do";
	var param = "name=" + dvc + 
				"&sDate=" + sDate + 
				"&eDate=" + eDate + 
				"&shopId=" + shopId;

	//$("#wcSelector").html(wc);
	
	$.ajax({
		url : url,
		data : param,
		dataType : "json",
		type : "post",
		success : function(data){
			var json = data.wcList;
			
			$(".contentTr2").remove();
			var tr = "";
			wcData = "职务,WC,WC GROUP,目标运转时间,日子,运转,待机,中断,断电,相比目标运转率LINE";
			$(json).each(function(idx, data){
				var incycle = Number(Number(data.inCycle_time/60/60).toFixed(1));
				var wait = Number(Number(data.wait_time/60/60).toFixed(1));
				var alarm = Number(Number(data.alarm_time/60/60).toFixed(1));
				var noconn = Number(24 - (incycle + wait + alarm)).toFixed(1);
				
				tr += "<tr class='contentTr2')'>" +  
							"<td>" + Number(data.target_time/60/60).toFixed(1) + "</td>" + 
							"<td>" + data.workDate + "</td>" + 
							"<td>" + incycle.toFixed(1) + "</td>" +
							"<td>" + wait + "</td>" + 
							"<td>" + alarm + "</td>" +
							"<td>" + noconn + "</td>" + 
							"<td>" + Number((data.inCycle_time/data.target_time) * 100).toFixed(1) + "%</td>" + 
					 "</tr>";
					 
				
				wcData += 
						Number(data.target_time/60/60).toFixed(1) + "," + 
						data.workDate + "," +
						Number(data.inCycle_time/60/60).toFixed(1) + "," + 
						Number(data.wait_time/60/60).toFixed(1) + "," +
						Number(data.alarm_time/60/6).toFixed(1) + "," +
						Number(data.noConnTime/60/60).toFixed(1) + "," +
						Number((data.inCycle_time/data.target_time) * 100).toFixed(1) + "%LINE";
			});
			
			$("#wcTable").append(tr);
			setEl();
		}
	});
};

function getDvcList(dvc){
	var url = "${ctxPath}/chart/getJigList4Report.do";
	var wc = $("#wcSelector").html();
	var param = "WC=" + wc + 
				"&shopId=" + shopId;
	
	$.ajax({
		url : url,
		data : param,
		dataType : "json",
		type : "post",
		success : function(json){
			var json = json.dvcList;
			
			var tr = "";
			$(json).each(function(idx, data){
				tr += "<tr class='dvcTr'>" + 
							"<td >" + decodeURIComponent(data.name) + "</td>" + 
					"</tr>"
			});
			
			$("#dvcList").html(tr);
			$("#dvcList td").css({
				"border" : "1px solid white"
			});
		

			if(typeof(name)=="undefined") dvc = $("#dvcList tr:nth(0) td").html()
		}
	});
};

function getJigList(){
	var url = "${ctxPath}/chart/getJigList.do";
	var param = "shopId=" + shopId;
	
	$.ajax({
		url : url,
		data : param,
		dataType : "json",
		type : "post",
		success : function(json){
			var json = json.jigList;
			
			var tr = "";
			$(json).each(function(idx, data){
				tr += "<tr class='jigTr'>" + 
							"<td >" + decodeURIComponent(data.jig) + "</td>" + 
					"</tr>"
			});
			
			$("#jigList").html(tr);
			$("#jigList td").css({
				"border" : "1px solid white"
			});
		
		}
	});
};


function replaceHyphen(str){
	return str.replace(/#/gi,"-");	
};

function goGraphPage(){
	var type = this.id;
	var url;
	if(type=="jig"){
		url = "${ctxPath}/chart/jigGraph.do?group=" + $("#group").val();
	}else{
		var sDate = $("#wc_sdate").val();
		var eDate = $("#wc_edate").val();
		url = "${ctxPath}/chart/dailyChart.do?sDate=" + sDate + "&eDate=" + eDate + "&name=" + dvc;
	};
	location.href = url;
};

var menu = false;
function getMousePos(){
	$(document).on("mousemove", function(e){
		var target = $("#menu_btn").width();

		if((e.pageX <= target && e.pageY <= target) || panel){
			if(!menu){
				$("#menu_btn").animate({
					"left" : 0,
					"top" : getElSize(20),
				});
				menu = true;
			};
		}else{
			if(menu){
				$("#menu_btn").animate({
					"left" : -getElSize(100),
				});	
				menu = false;
			};
		};
	});
};

function goReport(){
	var type = this.id;
	var url;
	if(type=="menu0"){
		url = "${ctxPath}/chart/main.do";
		location.href = url;
	}else if(type=="menu1"){
		closePanel();
		panel = false;
	}else if(type=="menu2"){
		url = "${ctxPath}/chart/alarmReport.do";
		location.href = url;
	}else if(type=="menu3"){
		url = "${ctxPath}/chart/main3.do";
		location.href = url;
	}else if(type=="menu9"){
		url = "${ctxPath}/chart/singleChartStatus2.do";
		window.localStorage.setItem("dvcId", 1)
		location.href = url;
	}else if(type=="menu4"){
		url = ctxPath + "/chart/rotateChart.do";
		location.href = url;
	}else if(type=="menu5"){
		url = "${ctxPath}/chart/traceManager.do";
		location.href = url;
	}else if(type=="menu6"){
		url = "${ctxPath}/chart/toolManager.do";
		location.href = url;
	}else if(type=="menu8"){
		url = "${ctxPath}/chart/jigGraph.do";
		location.href = url;
	}else if(type=="menu9"){
		url = "${ctxPath}/chart/wcGraph.do";
		location.href = url;
	}else if(type=="menu10"){
		url = "${ctxPath}/chart/addTarget.do";
		location.href = url;
	}else if(type=="menu7"){
		url = "${ctxPath}/chart/singleChartStatus.do";
		location.href = url;
	}else if(type=="menu11"){
		url = "${ctxPath}/chart/prdStatus.do";
		location.href = url;
	}else if(type=="menu99"){
		$("#bannerDiv").css({
			"z-index" : 9999
		});
		getBanner();
		closePanel();
		panel = false;
	}else if(type=="menu12"){
		url = "${ctxPath}/chart/performanceAgainstGoal.do";
		location.href = url;
	}else if(type=="menu100"){
		url = "${ctxPath}/chart/stockStatus.do";
		location.href = url;
	}else if(type=="menu101"){
		url = ctxPath + "/chart/programManager.do";
		location.href = url;
	}else if(type=="menu102"){
		url = "${ctxPath}/chart/leadTime.do";
		location.href = url;
	}else if(type=="menu103"){
		url = "${ctxPath}/chart/faulty.do";
		location.href = url;
	}else if(type=="menu104"){
		url = "${ctxPath}/chart/addFaulty.do";
		location.href = url;
	}
};

var panel = false;
function showPanel(){
	$("#panel").animate({
		"left" : 0
	});
	
	$("#menu_btn").animate({
		"left" : contentWidth * 0.2 + getElSize(50)	
	});
	
	$("#corver").css({
		"z-index" : 9,
		"opacity" : 0.7
	});
};

function closePanel(){
	$("#panel").animate({
		"left" : -contentWidth * 0.2 - getElSize(40)
	});
	
	$("#menu_btn").animate({
		"left" : 0
	});
	
	$("#corver").css({
		"z-index" : -1,
		"opacity" : 0.7
	});
};

function addZero(str){
	if(str.length==1){
		str = "0" + str;
	};
	return str;
};

function caldate(day){
	 
	 var caledmonth, caledday, caledYear;
	 var loadDt = new Date();
	 var v = new Date(Date.parse(loadDt) - day*1000*60*60*24);
	 
	 caledYear = v.getFullYear();
	 
	 if( v.getMonth() < 9 ){
	  caledmonth = '0'+(v.getMonth()+1);
	 }else{
	  caledmonth = v.getMonth()+1;
	 }
	 if( v.getDate() < 9 ){
	  caledday = '0'+v.getDate();
	 }else{
	  caledday = v.getDate();
	 }
	 return caledYear + "-" + caledmonth+ '-' + caledday;
	}


function setDate(){
	var date = new Date();
	var year = date.getFullYear();
	var month = addZero(String(date.getMonth()+1));
	var day = addZero(String(date.getDate()));
	
	$("#jig_sdate").val(caldate(7));
	$("#jig_edate").val(year + "-" + month + "-" + day);
};

function setEvt(){
	
	
};

function showJigList(){
	$("#corver").css({
		"z-index" : 9,
		"opacity" : 0.7
	});
	
	$("#jigList").toggle();	
};

function dvcList(){
	$("#corver").css({
		"z-index" : 9,
		"opacity" : 0.7
	});
	
	$("#dvcList").toggle();	
};


function clearMenu(){
	$("#corver").css({
		"z-index" : -1,
		"opacity" : 0
		
	});
	
	$("#dlgTable").css({
		"z-index":-1,
		"display" : "none"
	});
	$("#dvcList").css("display","none");
};

function setEl() {
	$("#intro_back").css({
		"width" : originWidth,
		"display" : "none",
		"height" : getElSize(180),
		"opacity" : 0.3,
		"position" : "absolute",
		"background-color" : "white",
		"bottom" : 0 + marginHeight,
		"z-index" : 9999
	});
	
	$(".container").css({
		"width": contentWidth,
		"height" : contentHeight,
		//"margin-top" : originHeight/2 - ($(".container").height()/2)
	});
	
	$(".container").css({
		"margin-top" : originHeight/2 - ($(".container").height()/2),
		"margin-left" : originWidth/2 - ($(".container").width()/2),
	});
	
	$(".layoutTd").css({
		height : contentHeight * 0.85
	});

	$(".page").css({
		"height" : originHeight
	});

	$("#part1").css({
		"top" : 0
	});

	// 페이지 위치 조정
	for (var i = 2; i <= 5; i++) {
		$("#part" + i).css({
			"top" : $("#part" + (i - 1)).height() + originHeight
		});
	};

	$(".mainTable").css({
		"border-spacing" : getElSize(30)
	});

	$(".title").css({
		"padding" : getElSize(40),
		"font-size" : getElSize(100)
	});

	$(".subTitle").css({
		"padding" : getElSize(40),
		"font-size" : getElSize(50)
	});

	$(".tr2").css({
		"height" : contentHeight * 0.35
	})

	$("#chartTr").css({
		"height" : contentHeight * 0.4
	});

	$("#pie").css({
		"height" : contentHeight * 0.32
	});

	$(".neon1, .neon2, .neon3").css({
		"font-size" : getElSize(500)
	});

	$(".neon1, .neon2, .neon3").css({
		"margin-top" : $(".tr2").height() / 2 - $(".neon1").height()
						/ 2 - $(".subTitle").height(),
	});

	$("#machine_name").css({
		"color" : "white",
		"font-weight" : "bolder",
		"font-size" : getElSize(170),
	});

	$("#machine_name_td").css({
		"height" : getElSize(500)
	});

	$("#machine_name").css({
		"margin-top" : $("#machine_name_td").height() / 2
						- $("#machine_name").height() / 2
						- $(".subTitle").height()
	});

	$("#title_left").css({
		"position" : "absolute",
		"width" : contentWidth * 0.07,
		"z-index" : 2,
		"left" : getElSize(100),
		"top" : getElSize(60)
	});

	$("#title_right").css({
		"position" : "absolute",
		"z-index" : 2,
		"right" : getElSize(100),
		"top" : getElSize(40),
		"color" : "white",
		"font-weight" : "bolder",
		"font-size" : getElSize(40)
	});

	
	$("#menu_btn").css({
		"position" : "absolute",
		"width" : getElSize(130),
		"left" : 0,
		"top" : getElSize(20),
		"cursor" : "pointer",
		"z-index" : 13
	});

	$("#panel").css({
		"background-color" : "rgb(34,34,34)",
		"border" : getElSize(20) + "px solid rgb(50,50,50)",
		"position" : "absolute",
		"width" : contentWidth * 0.2,
		"top" : 0,
		"left" : -contentWidth * 0.2 - getElSize(40),
		"z-index" : 99999
	});
	
	$("#panel_table").css({
		"color" : "white",
		"font-size" : getElSize(40)
	});

	$("#panel_table td").css({
		"padding" : getElSize(30),
		"cursor" : "pointer"
	});

	$(".dvcName_p1").css({
		"font-weight" : "bolder",
		"font-size" : getElSize(40)
	});

	$(".wrap").css({
		"width" : contentWidth * 0.2,
		"padding" : getElSize(10),
		"border-radius" : getElSize(30),
		"padding-bottom" : getElSize(10),
		//"border": getElSize(10) + "px outset white",
	});
	
	$(".waitCnt, .alarmCnt, .noConCnt").css({
		"font-size" : getElSize(20),
		"margin" :getElSize(20)
	});
	
	$(".waitCnt").css({
		"color" : "yellow"
	});
	
	$(".alarmCnt").css({
		"color" : "red"
	});
	
	$(".noConCnt").css({
		"color" : "gray"
	});
	
	$(".card_status").css({
		"height" : getElSize(60)
	});
	
	$(".card_table").css({
		"border-spacing" : getElSize(10)
	});
	
	$(".opRatio").css({
		"font-size" :getElSize(100)
	});
	
	$(".comName").css({
		"font-size" :getElSize(60)
	});
	
	$(".machine_cam").css({
		"width" : contentWidth * 0.07,
		"height" : contentHeight * 0.08,
		//"border" : getElSize(10) + "px solid rgb(50,50,50)" 
	});
	
	$(".frame").css({
		"width" : contentWidth * 0.07,
		"height" : contentHeight * 0.15
	});
	
	$(".statusCnt").css({
		"height" : getElSize(70),
		"vertical-align" : "bottom",
		"box-shadow ": "inset 0 5px 0 #ddd"
	});
	
	$(".opTime").css({
		"font-weight" : "bolder",
		"top" : getElSize(400),
		"right" : getElSize(10)
	});
	
	$(".logo").css({
		"height" : contentWidth * 0.01
	});
	
	
	$(".tableContainer").css({
		"height" : getElSize(1750),
		"overflow" : "hidden",
		"width" : "95%"
	});
	
	$(".tableContainer2").css({
		"height" : getElSize(1650),
		"overflow" : "hidden",
		"width" : "95%"
	});
	
	$(".tableContainer, .tableContainer2").css({
		"margin-left" : (contentWidth/2) -($(".tableContainer").width()/2)		
	});
	
	$("#dvcDiv").css({
		"margin-left" : $(".tableContainer").offset().left - marginWidth,
		"width" : $(".tableContainer").width(),
		"margin-bottom" : getElSize(50)
	})
	
	$(".goGraph, .excel, .label, #wcSelector").css({
		"margin-right": getElSize(10),
		"margin-left": getElSize(10),
		"border-radius" : getElSize(10),
		"padding" : getElSize(10),
		"cursor": "pointer",
		"font-size": getElSize(50)
	});
	
	$("#group").css({
		"font-size": getElSize(50)
	});
	
	$(".date").css({
		"height": getElSize(70),
		"font-size": getElSize(50)
	});
	
	$(".thead").css({
		"font-size" : getElSize(60)
	});
	
	$(".tmpTable, .tmpTable tr, .tmpTable td").css({
		"border": getElSize(5) + "px solid rgb(50,50,50)"
	});
	
	$(".tmpTable td").css({
		"padding" : getElSize(10),
		"height": getElSize(100)
	});
	
	$(".contentTr, .contentTr2").css({
		"font-size" : getElSize(40)
	});
	
	$("#bannerDiv").css({
		"position" : "absolute",
		"width" : getElSize(1500),
		"height" : getElSize(200),
		"border-radius" : getElSize(20),
		"padding" : getElSize(50),
		"background-color" : "lightgray",
		"z-index" : -9
	});

	
	$("#bannerDiv").css({
		"left" : (originWidth/2) - ($("#bannerDiv").width()/2),
		"top" : (originHeight/2) - ($("#bannerDiv").height()/2)
	});
	
	$("#bannerDiv input[type='text']").css({
		"width" : getElSize(1200),
		"font-size" : getElSize(50)
	});
	
	$("#bannerDiv button").css({
		"margin" : getElSize(50),
		"font-size" : getElSize(50)
	});
	
	$("#intro").css({
		/* "width" : contentWidth, */
		"position" : "absolute",
		"bottom" : 0 + marginHeight,
		"font-size" : getElSize(140),
		"font-weight" : "bolder",
		"z-index" : 9999
	});
	
};

</script>
</head>

<body >
	<div id="bannerDiv">
		<Center>
			<input type="text" id="banner" size="100"><input type="color" id="color"><br>
			<button onclick="addBanner();">적용</button>
			<button onclick="cancelBanner();">미적용</button>
		</Center>
	</div>
	<div id="intro_back"></div>
	<span id="intro"></span>
	
	<form action='${ctxPath}/csv/csv.do' id="f"  method="post">
		<input type="hidden" name="csv" value="">
		<input type="hidden" name="startDate" value="">
		<input type="hidden" name="endDate" value="">
	</form>
		
	<div id="title_right" class="title"> </div>	
	
	<img src="${ctxPath }/images/home.png" id="menu_btn" >
	
	
	<table id="dvcList" style="color:white; border-collapse: collapse;"></table>
			
	<div id="corver"></div>

	<div id="dlgTable">
		<div class="label" id="dvcDiv2">
			<spring:message code="op_period"></spring:message> <input type="date" class="date" id="wc_sdate"> ~ <input type="date" class="date" id="wc_edate">
			<span style="background-color: white; color:black; font-weight: bolder; padding: 3px;" class="goGraph" id="graph"><spring:message code="graph"></spring:message></span>
			<span style="background-color: white; color:black; font-weight: bolder; padding: 3px;" class="excel" id="wcExcel"><spring:message code="excel"></spring:message></span> 
		</div>
		
		<div class="tableContainer2" style="width: 95%">
				<table style="color: white; width: 100%; text-align: center; border-collapse: collapse; " class="tmpTable" id="wcTable">
					<thead>
						<tr style="font-weight: bolder;background-color: rgb(34,34,34)" class="thead"> 
							<td rowspan="2"><spring:message code="device"></spring:message> </td>
							<td rowspan="2"><spring:message code="target_op_time"></spring:message></td>
							<td rowspan="2"><spring:message code="date"></spring:message></td>
							<td colspan="4"><spring:message code="avrg_of_day"></spring:message></td>
							<td rowspan="2"><spring:message code="opratio_against_target"></spring:message></td>
						</tr>
						<tr style="font-weight: bolder;background-color: rgb(34,34,34)" class="thead">
							<td><spring:message code="incycle"></spring:message></td>
							<td><spring:message code="wait"></spring:message></td>
							<td><spring:message code="stop"></spring:message></td>
							<td><spring:message code="noconnection"></spring:message></td>
						</tr>
					</thead>
				</table>
		</div>
	</div>
				
	<!-- Part1 -->
	<div id="part1" class="page">
		<div class="container">
				<table class="mainTable">
					<tr>
						<Td align="center" style=" color:white; font-weight: bolder;" class="title" >
							<spring:message code="operation_chart"></spring:message>
						</Td>
					</tr>
				</table>
				
				<div id="dvcDiv" style="overflow: hidden;">
					<select id="group"></select>
					<span class="label"><spring:message code="op_period"></spring:message> </span> 
					<input type="date" class="date" id="jig_sdate"> ~ <input type="date" class="date" id="jig_edate">
					<span style="background-color: white; color:black; font-weight: bolder; padding: 3px;" class="goGraph" id="jig"><spring:message code="graph"></spring:message></span>
					<span style="background-color: white; color:black; font-weight: bolder; padding: 3px;" class="excel" id="jigExcel"><spring:message code="excel"></spring:message></span>
					<span style="background-color: white; color:black; font-weight: bolder; padding: 3px;" class="excel" onclick="getTableData('jig')">조회</span>
				</div>

				<div class="tableContainer" style="width: 95%" >
					<table style="color: white; width: 100%; text-align: center; border-collapse: collapse; " class="tmpTable" id="jigTable">
						<thead>
							<tr style="font-weight: bolder; background-color: rgb(34,34,34)" class="thead">
								<td rowspan="2"><spring:message code="device"></spring:message> </td> 
								<td rowspan="2"><spring:message code="target_op_time"></spring:message></td>
								<td rowspan="2"><spring:message code="total_op_time"></spring:message> </td>
								<td rowspan="2"><spring:message code="number_of_op_day"></spring:message></td>
								<td colspan="4"><spring:message code="avrg_of_day"></spring:message></td>
								<td rowspan="2"><spring:message code="opratio_against_target"></spring:message></td>
							</tr>
							<tr style="font-weight: bolder; background-color: rgb(34,34,34)" class="thead">
								<td><spring:message code="incycle"></spring:message> </td>
								<td><spring:message code="wait"></spring:message></td>
								<td><spring:message code="stop"></spring:message></td>
								<td><spring:message code="noconnection"></spring:message></td>
							</tr>
						</thead>
					</table>
				</div>
		</div>
	</div>
</body>
</html>