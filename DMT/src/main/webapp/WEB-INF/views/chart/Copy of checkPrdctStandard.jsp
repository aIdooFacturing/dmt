<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ include file="/WEB-INF/views/include/lib.jsp"%>
<spring:message code="device" var="device"></spring:message>
<spring:message code="cycle" var="cycle"></spring:message>
<spring:message code="ophour" var="ophour"></spring:message>
<spring:message code="confirm" var="confirm"></spring:message>

<spring:message code="start_time" var="start_time"></spring:message>
<spring:message code="end_time" var="end_time"></spring:message>
<spring:message code="fixed_time" var="fixed_time"></spring:message>
<spring:message code="alarm" var="alarm"></spring:message>
<spring:message code="content" var="content"></spring:message>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<meta http-equiv="X-UA-Compatible" content="chrome=1">
<meta http-equiv="cache-control" content="max-age=0" />
<meta http-equiv="cache-control" content="no-cache" />
<meta http-equiv="expires" content="0" />
<meta http-equiv="expires" content="Tue, 01 Jan 1980 1:00:00 GMT" />
<meta http-equiv="pragma" content="no-cache" />
<link rel="shortcut icon" href="${ctxPath }/images/dulink_favicon.ico">
<script type="text/javascript" src="${ctxPath }/js/jqMap.js"></script>
<style type="text/css">
	.selected_span{
	background-color :white;
	color : #008900;
}
	
</style>
<title>Dash Board</title>
<script type="text/javascript">
	var ctxPath = "${ctxPath}";
	var targetWidth = 3840;
	var targetHeight = 2160;

	var originWidth = window.innerWidth;
	var originHeight = window.innerHeight;

	var contentWidth = originWidth;
	var contentHeight = targetHeight / (targetWidth / originWidth);

	var screen_ratio = getElSize(240);

	if (originHeight / screen_ratio < 9) {
		contentWidth = targetWidth / (targetHeight / originHeight)
		contentHeight = originHeight;
	};

	function getElSize(n) {
		return contentWidth / (targetWidth / n);
	};

	function setElSize(n) {
		return targetWidth / (contentWidth / n);
	};
	
	var marginWidth = (originWidth-contentWidth)/2;
	var marginHeight = (originHeight-contentHeight)/2;
</script>
<script src="${ctxPath }/js/jquery-1.11.2.min.js"></script>
<script src="${ctxPath }/js/jquery-ui.js"></script>
<script src="${ctxPath}/js/highstock.js"></script>
<link rel="stylesheet" href="${ctxPath }/css/default.css">
<style type="text/css">
	body{
		overflow :hidden;
	}
</style>
<script type="text/javascript">
var shopId = 1;

function getGroup(){
	var url = "${ctxPath}/chart/getPrdNoList.do";
	var param = "shopId=" + shopId;
	
	$.ajax({
		url : url,
		data : param,
		type : "post",
		dataType : "json",
		success : function(data){
			var json = data.dataList;
			
			var option;
			
			$(json).each(function(idx, data){
				option += "<option value='" + data.prdNo + "'>" + data.prdNo + "</option>"; 
			});
			
			$("#prdNo").html(option).val(json[0].prdNo).change(getDvcListByPrdNo);
			
			//getLeadTime();
			getDvcListByPrdNo();
		}
	});
};

function decode(str){
	return decodeURIComponent(str).replace(/\+/gi, " ")
};


function getDvcListByPrdNo(){
	var url = "${ctxPath}/chart/getDvcListByPrdNo.do";
	var param = "prdNo=" + $("#prdNo").val();
	
	$.ajax({
		url : url,
		data : param,
		type : "post",
		dataType : "json",
		success : function(data){
			var json = data.dataList;
			
			var option;
			
			$(json).each(function(idx, data){
				option += "<option value='" + data.dvcId + "'>" + decode(data.name) + "</option>"; 
			});
			
			$("#dvcId").html(option).val(json[0].dvcId);
			getChkStandardList();
		}
	});
};

function getCheckType(){
	var url = "${ctxPath}/chart/getCheckType.do";
	var param = "codeGroup=INSPPNT";
	
	var option = "";
	
	$.ajax({
		url : url,
		data : param,
		async : false,
		type : "post",
		dataType : "json",
		success : function(data){
			var json = data.dataList;
			
			$(json).each(function(idx, data){
				option += "<option value='" + data.id + "' >" + decode(data.codeName) + "</option>"; 
			});
			
			chkTy = "<select>" + option + "</select>";
		}
	});
	
	return option;
};

var valid = true;
var valueArray = [];
function saveRow(){
	valid = true;
	valueArray = [];
	$(".contentTr").each(function(idx, data){
		if(valid){
			var obj = new Object();
			
			obj.id = data.id.substr(2);
			obj.prdNo = $("#prdNo").val();
			obj.dvcId = $("#dvcId").val();
			obj.chkTy = $(data).children("td:nth(2)").children("select").val();
			obj.attrTy = $(data).children("td:nth(3)").children("select").val();
			obj.attrCd = $(data).children("td:nth(4)").children("span").html() + $(data).children("td:nth(4)").children("input").val();
			obj.attrNameKo = $(data).children("td:nth(5)").children("input").val();
			obj.dp = $(data).children("td:nth(6)").children("input").val();
			obj.unit = $(data).children("td:nth(7)").children("input").val();
			obj.spec = $(data).children("td:nth(8)").children("input").val();
			obj.target = $(data).children("td:nth(9)").children("input").val();
			obj.low = $(data).children("td:nth(10)").children("input").val();
			obj.up = $(data).children("td:nth(11)").children("input").val();
			obj.measurer = $(data).children("td:nth(12)").children("input").val();
			obj.jsGroupCd = $(data).children("td:nth(13)").children("select").val();
			obj.attrNameOthr = $(data).children("td:nth(14)").children("input").val();
			
			valueArray.push(obj);
			
			var target = $(data).children("td:nth(9)").children("input").val();
			var min = $(data).children("td:nth(10)").children("input").val();
			var max = $(data).children("td:nth(11)").children("input").val();
			var dp = Number($(data).children("td:nth(6)").children("input").val());
			
			
			if(obj.attrTy==2){
				if(min.lastIndexOf(".")==-1){
					alert("소수점 자리가 맞지 않습니다.");
					$(data).children("td:nth(10)").children("input").focus();
					valid = false;
					return;
				};
				
				var min_dp_length = min.substr(min.lastIndexOf(".")+1).length;
				if(dp!=min_dp_length){
					alert("소수점 자리가 맞지 않습니다.");
					$(data).children("td:nth(10)").children("input").focus();
					valid = false;
					return;
				}
				
				if(max.lastIndexOf(".")==-1){
					alert("소수점 자리가 맞지 않습니다.");
					$(data).children("td:nth(11)").children("input").focus();
					valid = false;
					return;
				};
				
				var max_dp_length = max.substr(max.lastIndexOf(".")+1).length;
				if(dp!=max_dp_length){
					alert("소수점 자리가 맞지 않습니다.");
					$(data).children("td:nth(11)").children("input").focus();
					valid = false;
					return;
				};
				
				if(target.lastIndexOf(".")==-1){
					alert("소수점 자리가 맞지 않습니다.");
					$(data).children("td:nth(9)").children("input").focus();
					valid = false;
					return;
				};
				
				var target_dp_length = target.substr(target.lastIndexOf(".")+1).length;
				if(dp!=target_dp_length){
					alert("소수점 자리가 맞지 않습니다.");
					$(data).children("td:nth(9)").children("input").focus();
					valid = false;
					return;
				};
				
				if(min==""){
					alert("하한 값이 비어있습니다.");
					$(data).children("td:nth(10)").children("input").focus();
					valid = false;
					return;
				};
				
				if(max==""){
					alert("상한 값이 비어있습니다.");
					$(data).children("td:nth(11)").children("input").focus();
					valid = false;
					return;
				};
				
				if(target==""){
					alert("목표 값이 비어있습니다.");
					$(data).children("td:nth(9)").children("input").focus();
					valid = false;
					return;
				};
				
				if(Number(target) > Number(max)){
					alert("목표 값이 상한 값보다 큽니다.");
					$(data).children("td:nth(9)").children("input").focus();
					valid = false;
					return;
				}else if(Number(target) < Number(min)){
					alert("목표 값이 하한 값보다 작습니다.");
					$(data).children("td:nth(9)").children("input").focus();
					valid = false;
					return;
				}else if(Number(max) < Number(min)){
					alert("상한 값이 하한 값보다 작습니다.");
					$(data).children("td:nth(11)").children("input").focus();
					valid = false;
					return;
				}	
				return;
			}			
		}
	});
	
	if(!valid) return;
	
	var obj = new Object();
	obj.val = valueArray;
	
	var url = "${ctxPath}/chart/addCheckStandard.do";
	var param = "val=" + JSON.stringify(obj);
	
	$.ajax({
		url : url,
		data : param,
		type : "post",
		dataType :"text",
		success : function(data){
			if(data=="success") {
				alert ("저장 되었습니다.");
				getChkStandardList();
			}
		}
	}); 
};

$(function(){
	getGroup();
	getGroupCode();
	$("#chkTy").html("<option value='ALL'>전체</option>" + getCheckType());
	$(".excel").click(csvSend);
	setEl();
	setDate();
	
	$("#menu_btn").click(function(){
		location.href = "${ctxPath}/chart/index.do"
	});
	
	$(".menu").click(goReport);
	$("#panel_table td").addClass("unSelected_menu");
	$("#menu102").removeClass("unSelected_menu");
	$("#menu102").addClass("selected_menu");
	
	/* $("#prdNo").change(getChkStandardList);
	$("#dvcId").change(getChkStandardList);
	$("#chkTy").change(getChkStandardList); */
});

function getChkStandardList(){
	var url = "${ctxPath}/chart/getChkStandardList.do";
	var param = "prdNo=" + $("#prdNo").val() + 
				"&dvcId=" + $("#dvcId").val() + 
				"&chkTy=" + $("#chkTy").val()

	$.ajax({
		url : url,
		data : param,
		type : "post",
		dataType : "json",
		success : function(data){
			var json = data.dataList;
	
			$(".contentTr").remove();
			var tr = "<tbody>";
			$(json).each(function(idx, data){
				if(classFlag){
					className = "row2"
				}else{
					className = "row1"
				};
				classFlag = !classFlag;
				
				tr = "<tr class='" + className + " contentTr' id='tr" + data.id + "'>" + 
							"<td>" + data.prdNo + "</td>" +
							"<td>" + decodeURIComponent(data.name).replace(/\+/gi, " ") + "</td>" + 
							"<td id='chkTy" + data.id + "'><select style='font-size : " + getElSize(40) + "'>" + getCheckType() + "</select></td>" + 
							"<td id='attrTy" + data.id + "'>" + attrTy + "</td>" + 
							"<td><span>" + data.attrCd.substr(0,1) + "</span><input type='text' value='" + data.attrCd.substr(1) + "' size='6'></td>" + 
							"<td><input type='text' value='" + decodeURIComponent(data.attrNameKo).replace(/\+/gi, " ") + "'></td>" + 
							"<td><input id='dp" + data.id + "' type='text' value='" + data.dp + "' size='2'></td>" + 
							"<td><input id='unit" + data.id + "' type='text' value='" + data.unit + "' size='2'></td>" + 
							"<td><input type='text' value='" + decodeURIComponent(data.spec).replace(/\+/gi, " ") + "'></td>" + 
							"<td><input id='target" + data.id + "' type='text' value='" + data.target + "' size='6'></td>" + 
							"<td><input id='low" + data.id + "' type='text' value='" + data.low + "' size='6'></td>" + 
							"<td><input id='up" + data.id + "' type='text' value='" + data.up + "' size='6'></td>" + 
							"<td><input type='text' value='" + decodeURIComponent(data.measurer).replace(/\+/gi, " ") + "' size='8'></td>" + 
							"<td><select id='jsGroupCd" + data.id + "' style='font-size : " + getElSize(40) + "'>" + getGroupCode() + "</select></td>" + 
							"<td><input type='text' value='" + data.attrNameOthr + "'></td>" + 
							"<td><button onclick='chkDel(this)'>삭제</button></td>" + 
					"</tr>";
					
				$("#table").append(tr);	
				$("#chkTy" + data.id + " select option[value=" + data.chkTy + "]").attr('selected','selected');
				$("#attrTy" + data.id + " select option[value=" + data.attrTy + "]").attr('selected','selected');
				
				if(data.attrTy==1){
					$("#dp" + data.id).val("").attr("disabled", true).css("background-color", "#929292");
					$("#unit" + data.id).val("").attr("disabled", true).css("background-color", "#929292");
					
					$("#target" + data.id).val("").attr("disabled", true).css("background-color", "#929292");
					$("#up" + data.id).val("").attr("disabled", true).css("background-color", "#929292");
					$("#low" + data.id).val("").attr("disabled", true).css("background-color", "#929292");
					$("#jsGroupCd" + data.id).attr("disabled", false).css("background-color", "#ffffff");
				}else{
					$("#jsGroupCd" + data.id).attr("disabled", true).css("background-color", "#929292");
				}
			});
			
			$("#table").append("</tbody>");
			
			$(".row1").not(".tr_table_fix_header").css({
				"background-color" : "#222222"
			});

			$(".row2").not(".tr_table_fix_header").css({
				"background-color": "#323232"
			});
			
			setEl();
		}
	});
};

var className = "";
var classFlag = true;

function goReport(){
	var type = this.id;
	var url;
	if(type=="menu0"){
		url = "${ctxPath}/chart/main.do";
		location.href = url;
	}else if(type=="menu1"){
		url = "${ctxPath}/chart/performanceReport.do";
		location.href = url;
	}else if(type=="menu2"){
		url = "${ctxPath}/chart/alarmReport.do";
		location.href = url;
	}else if(type=="menu3"){
		url = "${ctxPath}/chart/main3.do";
		location.href = url;
	}else if(type=="menu4"){
		url = ctxPath + "/chart/rotateChart.do";
		location.href = url;
	}else if(type=="menu6"){
		url = "${ctxPath}/chart/toolManager.do";
		location.href = url;
	}else if(type=="menu5"){
		url = "${ctxPath}/chart/traceManager.do";
		location.href = url;
	}else if(type=="menu8"){
		url = "${ctxPath}/chart/jigGraph.do";
		location.href = url;
	}else if(type=="menu9"){
		url = "${ctxPath}/chart/wcGraph.do";
		location.href = url;
	}else if(type=="menu10"){
		url = "${ctxPath}/chart/addTarget.do";
		location.href = url;
	}else if(type=="menu7"){
		url = "${ctxPath}/chart/singleChartStatus.do";
		location.href = url;
	}else if(type=="menu11"){
		url = "${ctxPath}/chart/prdStatus.do";
		location.href = url;
	}else if(type=="menu99"){
		$("#bannerDiv").css({
			"z-index" : 9999
		});
		getBanner();
		closePanel();
		panel = false;
	}else if(type=="menu12"){
		url = "${ctxPath}/chart/performanceAgainstGoal.do";
		location.href = url;
	}else if(type=="menu100"){
		url = "${ctxPath}/chart/stockStatus.do";
		location.href = url;
	}else if(type=="menu101"){
		url = "${ctxPath}/chart/programManager.do";
		location.href = url;
	}else if(type=="menu102"){
		url = "${ctxPath}/chart/leadTime.do";
		location.href = url;
	}else if(type=="menu103"){
		url = "${ctxPath}/chart/faulty.do";
		location.href = url;
	}else if(type=="menu104"){
		url = "${ctxPath}/chart/addFaulty.do";
		location.href = url;
	}
};

function getBanner(){
	var url = "${ctxPath}/chart/getBanner.do";
	var param = "shopId=" + shopId;
	

	$.ajax({
		url : url,
		data : param,
		type : "post",
		dataType : "json",
		success : function(data){
			if(window.localStorage.getItem("banner")=="true"){
				$("#intro").html(data.msg).css({
					"color" : data.rgb,
					"right" : - window.innerWidth
				})
				
				$("#intro").html(data.msg).css({
					"right" : - $("#intro").width()
				})
			}
			
			bannerWidth = $("#intro").width() + getElSize(100);
			$("#intro").width(bannerWidth);
			$("#banner").val(data.msg);
			$("#color").val(data.rgb);
			
			twinkle();
			bannerAnim();
		}
	});		
}

var twinkle_opct = true;
function twinkle(){
	var opacity;
	if(twinkle_opct){
		opacity = 0;
	}else{
		opacity = 1;
	}
	$("#intro").css("opacity",opacity);
	
	twinkle_opct = !twinkle_opct;
	setTimeout(twinkle, 100)
};

var bannerWidth;
function bannerAnim(){
	$("#intro").width(bannerWidth - getElSize(10));
	$("#intro").animate({
		"right" : window.innerWidth  - getElSize(100)
	},15000, function(){
		$("#intro").css("right" , - $("#intro").width())
		//$(this).remove();
		bannerAnim();
	});
};
function chkBanner(){
	if(window.localStorage.getItem("banner")=="true"){
		getBanner();
		$("#intro_back").css("display","block");
	}else{
		$("#intro").html("");
		$("#intro_back").css("display","none");
	}
};

function cancelBanner(){
	window.localStorage.setItem("banner", "false");
	$("#bannerDiv").css("z-index",-9);
	chkBanner();
};

function addBanner(){
	var url = "${ctxPath}/chart/addBanner.do";
	var param = "msg=" + $("#banner").val() + 
				"&rgb=" + $("#color").val() + 
				"&shopId=" + shopId;
	
	$.ajax({
		url : url,
		data : param,
		type : "post",
		success : function(data){
			window.localStorage.setItem("banner", "true");
			$("#bannerDiv").css("z-index",-9);
			location.reload();
			//chkBanner();
		}
	});
};

var panel = false;
function showPanel(){
	$("#panel").animate({
		"left" : 0
	});
	
	$("#menu_btn").animate({
		"left" : contentWidth * 0.2 + getElSize(50)	
	});
	
	$("#corver").css({
		"z-index" : 9,
		"opacity" : 0.7
	});
};

function closePanel(){
	$("#panel").animate({
		"left" : -contentWidth * 0.2 - getElSize(40)
	});
	
	$("#menu_btn").animate({
		"left" : 0
	});
	
	$("#corver").css({
		"z-index" : -1,
		"opacity" : 0.7
	});
};


function addZero(str){
	if(str.length==1){
		str = "0" + str;
	};
	return str;
};

function setDate(){
	var date = new Date();
	var year = date.getFullYear();
	var month = addZero(String(date.getMonth()+1));
	var day = addZero(String(date.getDate()));
	
	$(".date").val(year + "-" + month + "-" + day);
};


function clearMenu(){
	$("#corver").css({
		"z-index" : -1,
		"opacity" : 0
	});
};

function setEl() {
	$(".container").css({
		"width": contentWidth,
		"height" : contentHeight, 
		"margin-top" : originHeight/2 - ($(".container").height()/2)
	});
	
	$("#prdNo, #dvcId, #chkTy, button").css({
		"font-size" : getElSize(40)
	});
	
	$(".container").css({
		"margin-top" : originHeight/2 - ($(".container").height()/2),
		"margin-left" : originWidth/2 - ($(".container").width()/2),
	});
	
	$(".layoutTd").css({
		height : contentHeight * 0.85
	});

	$(".page").css({
		"height" : originHeight
	});

	$("#part1").css({
		"top" : 0
	});

	// 페이지 위치 조정
	for (var i = 2; i <= 5; i++) {
		$("#part" + i).css({
			"top" : $("#part" + (i - 1)).height() + originHeight
		});
	};

	$(".mainTable").css({
		"border-spacing" : getElSize(40)
	});

	$(".title").css({
		"padding" : getElSize(40),
		"font-size" : getElSize(100)
	});

	$(".subTitle").css({
		"padding" : getElSize(40),
		"font-size" : getElSize(50)
	});

	$(".tr2").css({
		"height" : contentHeight * 0.35
	})

	$("#chartTr").css({
		"height" : contentHeight * 0.4
	});

	$("#pie").css({
		"height" : contentHeight * 0.32
	});

	$(".neon1, .neon2, .neon3").css({
		"font-size" : getElSize(500)
	});

	$(".neon1, .neon2, .neon3").css({
		"margin-top" : $(".tr2").height() / 2 - $(".neon1").height()
						/ 2 - $(".subTitle").height(),
	});

	$("#machine_name").css({
		"color" : "white",
		"font-weight" : "bolder",
		"font-size" : getElSize(170),
	});

	$("#machine_name_td").css({
		"height" : getElSize(500)
	});

	$("#machine_name").css({
		"margin-top" : $("#machine_name_td").height() / 2
						- $("#machine_name").height() / 2
						- $(".subTitle").height()
	});

	$(".title_left").css({
		"float" : "left",
		"width" : contentWidth * 0.1
	});

	$(".title_right").css({
		"float" : "right",
	});

	$("#menu_btn").css({
		"position" : "absolute",
		"width" : getElSize(130),
		"left" : 0,
		"top" : getElSize(20),
		"cursor" : "pointer",
		"z-index" : 13
	});
	

	$("#panel").css({
		"background-color" : "rgb(34,34,34)",
		"border" : getElSize(20) + "px solid rgb(50,50,50)",
		"position" : "absolute",
		"width" : contentWidth * 0.2,
		"top" : 0,
		"left" : -contentWidth * 0.2 - getElSize(40),
		"z-index" : 99999
	});

	$("#corver").css({
		"width" : originWidth,
		"height" : originHeight,
		"position" : "absolute",
		"z-index" : -1,
		"background-color" : "black",
		"opacity" : 0
	});

	$("#panel_table").css({
		"color" : "white",
		"font-size" : getElSize(40)
	});

	$("#panel_table td").css({
		"padding" : getElSize(30),
		"cursor" : "pointer"
	});

	$(".dvcName_p1").css({
		"font-weight" : "bolder",
		"font-size" : getElSize(40)
	});

	$(".wrap").css({
		"width" : contentWidth * 0.2,
		"padding" : getElSize(10),
		"border-radius" : getElSize(30),
		"padding-bottom" : getElSize(10),
		//"border": getElSize(10) + "px outset white",
	});
	
	$(".waitCnt, .alarmCnt, .noConCnt").css({
		"font-size" : getElSize(20),
		"margin" :getElSize(20)
	});
	
	$(".waitCnt").css({
		"color" : "yellow"
	});
	
	$(".alarmCnt").css({
		"color" : "red"
	});
	
	$(".noConCnt").css({
		"color" : "gray"
	});
	
	$(".card_status").css({
		"height" : getElSize(60)
	});
	
	$(".card_table").css({
		"border-spacing" : getElSize(10)
	});
	
	$(".opRatio").css({
		"font-size" :getElSize(100)
	});
	
	$(".comName").css({
		"font-size" :getElSize(60)
	});
	
	$(".machine_cam").css({
		"width" : contentWidth * 0.07,
		"height" : contentHeight * 0.08,
		//"border" : getElSize(10) + "px solid rgb(50,50,50)" 
	});
	
	$(".frame").css({
		"width" : contentWidth * 0.07,
		"height" : contentHeight * 0.15
	});
	
	$(".statusCnt").css({
		"height" : getElSize(70),
		"vertical-align" : "bottom",
		"box-shadow ": "inset 0 5px 0 #ddd"
	});
	
	$(".opTime").css({
		"font-weight" : "bolder",
		"top" : getElSize(400),
		"right" : getElSize(10)
	});
	
	$(".logo").css({
		"height" : contentWidth * 0.01
	});
	
	
	$("#title_left").css({
		"position" : "absolute",
		"width" : contentWidth * 0.07,
		"z-index" : 2,
		"left" : getElSize(100),
		"top" : getElSize(60)
	});

	$("#title_right").css({
		"position" : "absolute",
		"z-index" : 2,
		"right" : getElSize(100),
		"top" : getElSize(40),
		"color" : "white",
		"font-weight" : "bolder",
		"font-size" : getElSize(40)
	});
	
	$("#jigSelector, .goGraph, .excel, .label").css({
		"margin-right": getElSize(10),
		"margin-left": getElSize(10),
		"border-radius" : getElSize(10),
		"padding" : getElSize(10),
		"cursor": "pointer",
		"font-size": getElSize(50)
	});
	
	$(".date").css({
		"height": getElSize(70),
		"font-size": getElSize(50)
	});
	
	$(".thead").css({
		"font-size" : getElSize(60)
	});
	
	$(".alarmTable, .alarmTable tr, .alarmTable td").css({
		"border": getElSize(5) + "px solid rgb(50,50,50)"
	});
	
	$(".alarmTable td").css({
		"padding" : getElSize(10),
		"height": getElSize(100),
	});
	
	$(".contentTr").css({
		"font-size" : getElSize(60)
	});
	
	$(".machineListForTarget").css({
		"position" : "absolute",
		"width" : getElSize(1200),
		"height" : getElSize(1200),
		"overflow" : "auto",
		//"top" : getElSize(50),
		//"background-color" : "rgb(34,34,34)",
		"background-color" : "green",
		"color" : "white",
		"font-size" : getElSize(50),
		"padding" : getElSize(50),
		"overflow" : "auto",
		"border-radius" : getElSize(50),
		"border" : getElSize(10) + "px solid white",
	});
	
	$(".machineListForTarget").css({
		"left" : (originWidth/2) - ($(".machineListForTarget").width()/2),
	});
	
	$("#bannerDiv").css({
		"position" : "absolute",
		"width" : getElSize(1500),
		"height" : getElSize(200),
		"border-radius" : getElSize(20),
		"padding" : getElSize(50),
		"background-color" : "lightgray",
		"z-index" : -9
	});

	
	$("#bannerDiv").css({
		"left" : (originWidth/2) - ($("#bannerDiv").width()/2),
		"top" : (originHeight/2) - ($("#bannerDiv").height()/2)
	});
	
	$("#bannerDiv input[type='text']").css({
		"width" : getElSize(1200),
		"font-size" : getElSize(50)
	});
	
	$("#bannerDiv button").css({
		"margin" : getElSize(50),
		"font-size" : getElSize(50)
	});
	
	$("#intro").css({
		/* "width" : contentWidth, */
		"position" : "absolute",
		"bottom" : 0 + marginHeight,
		"font-size" : getElSize(140),
		"font-weight" : "bolder",
		"z-index" : 9999
	});
	
	$("#intro_back").css({
		"width" : originWidth,
		"display" : "none",
		"height" : getElSize(180),
		"opacity" : 0.3,
		"position" : "absolute",
		"background-color" : "white",
		"bottom" : 0 + marginHeight,
		"z-index" : 9999
	});
	
	$("#table td").css({
		"padding" : getElSize(20),
		"font-size": getElSize(40),
		"border": getElSize(5) + "px solid black"
	});
	
	$("#delDiv").css({
		"position" : "absolute",
		"width" : getElSize(700),
		"height" :getElSize(200),
		"background-color" : "#444444",
		"color" : "white"
	});
	
	$("#delDiv").css({
		"top" : (window.innerHeight/2) - ($("#delDiv").height()/2),
		"left" : (window.innerWidth/2) - ($("#delDiv").width()/2),
		"z-index" : -1,
		"border-radius" : getElSize(10),
		"padding" : getElSize(20)
	});
	
	
	$("#delDiv button").css({
		"font-size" :getElSize(40)
	});
	
	chkBanner();
};

var csvOutput;
function csvSend(){
	var sDate, eDate;
	
	sDate = $("#sDate").val();
	eDate = $("#eDate").val();
	
	csvOutput = csvOutput.replace(/\n/gi,"");
	
	f.csv.value=encodeURIComponent(csvOutput);
	f.startDate.value = sDate;
	f.endDate.value = eDate;
	f.submit(); 
};

var row;
function changeAttrTy(el){
	//1 정성검사 
	//2 정량검사	
	var ty = el.value;
	$(el).parent("td").parent("tr").children("td").children("input").attr("disabled", false).css("background-color", "#ffffff");
	
	if(ty==1){
		$(el).parent("td").parent("tr").children("td:nth(4)").children("span").html("L")
		$(el).parent("td").parent("tr").children("td:nth(6)").children("input").val("").attr("disabled", true).css("background-color", "#929292");
		$(el).parent("td").parent("tr").children("td:nth(7)").children("input").val("").attr("disabled", true).css("background-color", "#929292");
		$(el).parent("td").parent("tr").children("td:nth(9)").children("input").val("").attr("disabled", true).css("background-color", "#929292");
		$(el).parent("td").parent("tr").children("td:nth(10)").children("input").val("").attr("disabled", true).css("background-color", "#929292");
		$(el).parent("td").parent("tr").children("td:nth(11)").children("input").val("").attr("disabled", true).css("background-color", "#929292");
		$(el).parent("td").parent("tr").children("td:nth(13)").children("select").attr("disabled", false).css("background-color", "#ffffff");
	}else{
		$(el).parent("td").parent("tr").children("td:nth(4)").children("span").html("N")
		$(el).parent("td").parent("tr").children("td:nth(13)").children("select").attr("disabled", true).css("background-color", "#929292");
	}
};

function delRow(el){
	$(el).parent("td").parent("tr").remove();
};

var chkTy;

var attrTy = "<select onchange='changeAttrTy(this)' style='font-size : " + getElSize(40) + "'>" + 
				"<option value='1'>정성검사</option>" +
				"<option value='2'>정량검사</option>" + 
			"</select>";

var JSGroupCode;

function getGroupCode(){
	var url = "${ctxPath}/chart/getJSGroupCode.do";
	
	var options = "";
	
	$.ajax({
		url : url,
		async : false,
		type : "post",
		dataType : "json",
		success : function(data){
			
			console.log(data.dataList)
			$(data.dataList).each(function(idx,data){
				options += "<option value='" + data.id + "'>" + decode(data.name) + "</option>";
			});
			
			JSGroupCode = "<select>" + options + "</select>";
		}
	});
	
	return options;
};

function addRow(){
	var row = "<tr class='ContentTr' id='tr0'>" + 
				"<td>" + $("#prdNo").val() + "</td>" +
				"<td>" + $("#dvcId option:selected").html() + "</td>" +
				"<td>" + chkTy + "</td>" + //검사유형
				"<td>" + attrTy + "</td>" + //특성유형 
				"<td><span>L</span><input type='text' size='6'></td>" + //특성코드
				"<td>" + "<input type='text'>" + "</td>" + //특성명(한글)
				"<td>" + "<input type='text' size='2' disabled='disable' style='background-color:#929292'> "+"</td>" + //소수점 
				"<td>" + "<input type='text' size='2' disabled='disable' style='background-color:#929292'> "+"</td>" +  //단위 
				"<td>" + "<input type='text'>" + "</td>" + //도면 
				"<td>" + "<input type='text' size='6' disabled='disable' style='background-color:#929292'> "+"</td>" + //목표 값 
				"<td>" + "<input type='text' size='6' disabled='disable' style='background-color:#929292'> "+"</td>" + //하한 값
				"<td>" + "<input type='text' size='6' disabled='disable' style='background-color:#929292'> "+"</td>" + //상한 값
				"<td>" + "<input type='text' size='8'> "+"</td>" + //계측기 
				"<td>" + JSGroupCode + "</td>" + //정성그룹코드
				"<td>" + "<input type='text'>" + "</td>" + //특성명(기타언어)
				"<td><button onclick='chkDel(this)'>삭제</button></td>" +
			"</tr>";
			
	$("#table").append(row);
	
	$("#table td").css({
		"font-size" :getElSize(40),
		"padding" : getElSize(20)		
	});
};

var delId;
var delEl;

function chkDel(obj){
	$("#delDiv").css("z-index",9);
	
	delId = $(obj).parent("td").parent("tr").attr("id").substr(2);
	delEl = $(obj);
};

function noDel(){
	$("#delDiv").css("z-index",-1);
};

function okDel(){
	if(delId==0){
		delRow(delEl);
		noDel();
		return;
	}
	
	var url = ctxPath + "/chart/delChkStandard.do";
	var param = "id=" + delId;
	
	$.ajax({
		url : url,
		data : param,
		type : "post",
		dataType : "text",
		success : function(data){
			if(data=="success"){
				noDel();
				delRow(delEl);
			}
		}
	}); 
};


</script>
</head>

<body >
	<div id="delDiv">
		<Center>
			<span>삭제하시겠습니까?</span><Br><br>
			<button id="resetOk" onclick="okDel();">삭제</button>&nbsp;&nbsp;
			<button id="resetNo" onclick="noDel();">취소</button>
		</Center>
	</div>

	<div id="bannerDiv">
		<Center>
			<input type="text" id="banner" size="100"><input type="color" id="color"><br>
			<button onclick="addBanner();">적용</button>
			<button onclick="cancelBanner();">미적용</button>
		</Center>
	</div>
	<div id="intro_back"></div>
	<span id="intro"></span>
	
	<form action='${ctxPath}/csv/csv.do' id="f"  method="post">
		<input type="hidden" name="csv" value="">
		<input type="hidden" name="startDate" value="">
		<input type="hidden" name="endDate" value="">
	</form>
	<%-- <img src="${ctxPath }/images/DashBoard/title_right.svg" id="title_right"> --%>
	<div id="title_right" class="title"> </div>	
	
	<div id="panel">
		<table id="panel_table" width="100%">
				<tr>
					<td id="menu0" class="menu"><spring:message code="layout"></spring:message> </td>
				</tr>
				<tr>
					<td id="menu7" class="menu"><spring:message code="devicestatus"></spring:message> </td>
				</tr>
				<tr>
					<td id="menu4" class="menu"><spring:message code="dailydevicestatus"></spring:message> </td>
				</tr>
				<tr>
					<td id="menu1" class="menu"><spring:message code="analsysperformance"></spring:message> </td> <!-- 장비별 가동실적분석 -->
				</tr>
				<tr>
					<td id="menu8" class="menu"><spring:message code="performancegraph1"></spring:message></td>
				</tr>
				<tr>
					<td id="menu9" class="menu"><spring:message code="performancegraph2"></spring:message> </td>
				</tr>
				<tr>
					<td id="menu12" class="menu">일생산 실적 분석 (표)</td> <!--알람 내역 -->
				</tr>
				<tr>
					<td id="menu6" class="menu"><spring:message code="toolmanagement"></spring:message> </td><!-- 무인장비 가동현황 -->
				</tr> 
				<tr>
					<td id="menu11" class="menu"><spring:message code="prdctboard"></spring:message> </td>
				</tr>
				<tr>
					<td id="menu10" class="menu"><spring:message code="addprdctgll"></spring:message> </td>
				</tr>
				<tr>
					<td id="menu5" class="menu"><spring:message code="tracemanagement"></spring:message> </td><!-- 무인장비 가동현황 -->
				</tr>
				<tr>
					<td id="menu3" class="menu"><spring:message code="24barchart"></spring:message></td><!-- 무인장비 가동현황 -->
				</tr>
				<tr>
					<td id="menu2" class="menu"><spring:message code="alarmhistory"></spring:message> </td> <!--알람 내역 -->
				</tr>
				<tr>
					<td id="menu100" class="menu">재고 현황</td> 
				</tr>
				<tr>
					<td id="menu99" class="menu">Catch Phrase 관리</td> 
				</tr>
				<tr>
					<td id="menu101" class="menu">프로그램별 가공이상 분석</td> 
				</tr>
				<tr>
					<td id="menu102" class="menu">제조 리드타임</td> 
				</tr>
				<tr>
					<td id="menu103" class="menu">고객불량율 / 공정뷸량율</td> 
				</tr>
				<tr>
					<td id="menu104" class="menu">불량등록</td> 
				</tr>
			</table>
	</div>
	<img src="${ctxPath }/images/home.png" id="menu_btn" >
	
					
	<div id="corver"></div>

	<!-- Part1 -->
	<div id="part1" class="page">
		<div class="container">
				<table class="mainTable">
					<tr>
						<Td align="center" style=" color:white; font-weight: bolder;" class="title" >
							초.중.종물 검사기준 관리
						</Td>
					</tr>
				</table>
	
				<Center>				
				<table style="color: white">
							<Td>품번</Td><Td><select id="prdNo"></select></Td>
							<Td>장비</Td><Td><select id="dvcId"></select></Td>
							<Td>검사유형</Td><Td><select  id="chkTy"></select></Td>
						</tr>
					</table>
				</Center>
				
				
				
				<button style="float: right;" onclick="saveRow()">저장</button>	
				<button style="float: right;" onclick="addRow()">추가</button>
				<button style="float: right;" onclick="getChkStandardList()">조회</button>

				<div id="wrapper">
					<table style="width: 100%; color: white; text-align: center; border-collapse: collapse;" class="alarmTable" border="1" id="table">
						<thead>
							<tr style="background-color: #222222">
								<Td>품번</Td>
								<Td>장비</Td>
								<Td>검사유형</Td>
								<Td>특성유형</Td>
								<Td>특성코드</Td>
								<Td>특성명(한글)</Td>
								<Td>소수점</Td>
								<Td>단위</Td>
								<Td>도면Spec</Td>
								<Td>목표 값</Td>
								<Td>하한 값</Td>
								<Td>상한 값</Td>
								<Td>계측기</Td>
								<Td>정성그룹코드</Td>
								<Td style="width: 10%">특성명(기타언어)</Td>
								<Td>삭제</Td>
							</tr>
						</thead>
					</table>
				</div>
		</div>
	</div>
</body>
</html>