<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ include file="/WEB-INF/views/include/lib.jsp"%>
<!DOCTYPE html>
<html> 
<head>
<meta charset="UTF-8">
<link rel="shortcut icon" href="${ctxPath }/images/dulink_favicon.ico">
<title>DIMF Remote Controller.</title>
<script type="text/javascript">
	var ctxPath = "${ctxPath}";
</script>
<script type="text/javascript" src="${ctxPath }/js/jquery-1.11.2.min.js"></script>
<script type="text/javascript" src="${ctxPath }/js/chart/drag.js"></script>
<script src="//code.jquery.com/ui/1.11.4/jquery-ui.js"></script>
<style>
body{
	padding: 0px;
	margin: 0px;
}
</style> 
<script type="text/javascript">
	$(function(){
		init();
		setElPos();
		
		var machine1 = new webkit_draggable('44');
		var machine2 = new webkit_draggable('45');
		var machine3 = new webkit_draggable('46');
		
		var width = window.innerWidth;
		var height = window.innerHeight;
		var left_stack = new Array();
		var right_stack = new Array();
		
		webkit_drop.add('left', {
			onOver : function(){
				$("#left").css({
					"background-color" : "red"
				});
			},
			onDrop : function(el){
				$(el).remove();
				toChartPage(el);
				var img = "<img src='" + el.src + "' id=" + el.id + " width=" + width*0.1 +">";
				$("#left").append(img);
				
				left_stack.push(el.id);
				var machine = new webkit_draggable(el.id);
			} ,
			onOut : function(){
				$("#left").css({
					"background-color" : "yellow"
				});
			}
		});
		
		webkit_drop.add('right', {
			onOver : function(){
				$("#right").css({
					"background-color" : "red"
				});
			},
			onDrop : function(el){
				$(el).remove();
				toVideoPage(el);
				var img = "<img src='" + el.src + "' id=" + el.id + " width=" + width*0.1 +">";
				$("#right").append(img);
				
				right_stack.push(el.id);
				var machine = new webkit_draggable(el.id);
				
				
			},
			onOut : function(){
				$("#right").css({
					"background-color" : "yellow"
				});
			}
		});
		
		webkit_drop.add('center', {
			onDrop : function(el){
				$(el).remove();

				var img = "<img src='" + el.src + "' id=" + el.id + " height=" + height*0.27 +">";
				$("#center").append(img);
				
				var machine = new webkit_draggable(el.id);
				
				if(isItemInArray(left_stack, el.id)){
					delVideoMachine(el.id)	
				};
				
				if(isItemInArray(right_stack, el.id)){
					delChartMachine(el.id)
				};
			},
		});
		
		
		$("img").draggable({
			drag :function(){
				var left = $(this).offset().left;
				var right = $(this).offset().left + $(this).width();
				
				if(left<$("#left").offset().left + $("#left").width()){
					$("#left").css({
						"background-color" : "red"
					});
				}else{
					$("#left").css({
						"background-color" : "yellow"
					});
				};
				
				if(right>$("#right").offset().left){
					$("#right").css({
						"background-color" : "red"
					});
				}else{
					$("#right").css({
						"background-color" : "yellow"
					});
				};
			},
			stop : function (){
				var left = $(this).offset().left;
				var right = $(this).offset().left + $(this).width();
				
				if(left<$("#left").offset().left + $("#left").width()){
					toChartPage($(this));
				};
				
				if(right>$("#right").offset().left){
					
					toVideoPage($(this));
				};
			}
		});
	});
	 
	function delVideoMachine(id){
		var url = "${ctxPath}/chart/delVideoMachine.do";
		var param = "id=" + id;
		
		$.ajax({
			url : url,
			data : param,
			type : "post",
			success : function(data){}
		});
	};
	
	function delChartMachine(id){
		var url = "${ctxPath}/chart/delChartMachine.do";
		var param = "id=" + id;
		
		$.ajax({
			url : url,
			data : param,
			type : "post",
			success : function(data){}
		});
	};
	
	function isItemInArray(array, item) {
		for (var i = 0; i < array.length; i++) {
			if (array[i] == item) {
				return true;   // Found it
	        };
	    };
		return false;   // Not found
	};
	
	function init(){
		var url = "${ctxPath}/chart/initVideoPolling.do";
		$.ajax({
			url : url,
			type : "post",
			success :function(){
			}
		});
		
	var url = "${ctxPath}/chart/initPiePolling.do";
		
		$.ajax({
			url : url,
			type : "post",
			success : function(){
			}
		});
	};
	var chartIdx = 1;
	function toChartPage(obj){
		var url;
		if(chartIdx==1){
			url = "${ctxPath}/chart/setChart1.do"
		}else if(chartIdx==2){
			url = "${ctxPath}/chart/setChart2.do"
		};
		var param = "id=" + obj.id;	

		$.ajax({
			url : url,
			data : param,
			type : "post",
			dataType :"text",
			success : function(data){
				if(data=="success"){};
				chartIdx++;
			}
		});	
	};
	function toVideoPage(obj){
		var url = "${ctxPath}/chart/setVideo.do"
		var param = "id=" + obj.id;
	
		$.ajax({
			url : url,
			data : param,
			type : "post",
			dataType :"text",
			success : function(data){
				if(data=="success"){};
			}
		});
	};
	
	function setElPos(){
		var width = window.innerWidth;
		var height = window.innerHeight;
		
		$("#table").css({
			"height" : height	
		});
		
		$("#center").css({
			width : width*0.8	
		});
		
		$("#left, #right").css({
			width : width*0.1	
		});
		
		$("img").css({
			height : height*0.27
		})
	};
</script>
</head>
<body>
	<table id="table">
		<Tr>
			<td align="center"  style="background-color: yellow; font-size: 30px;  font-weight: bold; line-height: 1" id="left" ></td>
			
			<td align="center" id="center" valign="middle" id="center">
				<img src="${ctxPath }/images/pad/Lynx220A.jpg" id="44"><br>
				<img src="${ctxPath }/images/pad/GT2100.jpg" id="45"><br>
				<img src="${ctxPath }/images/pad/NHM6300.jpg" id="46"><br>
			</td>
			 
			<td align="center" style="background-color: yellow; font-size: 30px;  font-weight: bold; line-height: 1" id="right" ></td>
		</Tr>	
	</table>
</body>
</html>