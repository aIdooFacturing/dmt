<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ include file="/WEB-INF/views/include/lib_mobile.jsp"%>
<!DOCTYPE html>
<html> 
<head>
<meta charset="UTF-8">
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<link rel="shortcut icon" href="${ctxPath }/images/dulink_favicon.ico">
<title>Machine List</title>
<style>
.fontBtnsSelected {
	width : 48%;
	background : #3995CF;
	font-color : #FFFFFF;
	border-style : solid;
	border-width : 1px;
}
		
.fontBtnsUnselected {
	width : 48%;
	background : #FFFFFF;
	font-color : #000000;
	border-style : solid;
	border-width : 1px;
}

</style>
<script type="text/javascript">
	var ctxPath = "${ctxPath}";
	var fromDashboard = "${fromDashBoard}";
	var targetWidth = 3840;
	var targetHeight = 2160;

	var originWidth = window.innerWidth;
	var originHeight = window.innerHeight;
	
	var contentWidth = originWidth;
	var contentHeight = targetHeight/(targetWidth/originWidth);
	
	var screen_ratio = getElSize(240);
	
	if(originHeight/screen_ratio<9){
		contentWidth = targetWidth/(targetHeight/originHeight)
		contentHeight = originHeight; 
	};
	
	function getElSize(n){
		return contentWidth/(targetWidth/n);
	};
	
	function setElSize(n) {
		return Math.floor(targetWidth / (contentWidth / n));
	};
	
	var marginWidth = (originWidth-contentWidth)/2;
	var marginHeight = (originHeight-contentHeight)/2;
</script>
<script type="text/javascript" src="${ctxPath }/js/chart/highcharts.js"></script>
<script type="text/javascript" src="${ctxPath }/js/chart/highcharts-3d.js"></script>
<script type="text/javascript" src="${ctxPath }/js/chart/highcharts-more.js"></script>
<script type="text/javascript" src="${ctxPath }/js/jqMap.js"></script>
<link rel="stylesheet" href="${ctxPath }/css/jquery.loading.min.css">
<script type="text/javascript" src="${ctxPath }/js/jquery.loading.min.js"></script>
<script type="text/javascript" src="${ctxPath }/js/chart/moment.js"></script>

<script type="text/javascript">
	var shopId = 1;
	
	var broswerInfo = navigator.userAgent;
	
	$(function(){
		setElement();
	});

	
	function setElement(){
		var width = window.innerWidth;
		var height = window.innerHeight;
		
		$(".btn").css({
			"font-size" : getElSize(150),
			"background" : "white",
			"border" : getElSize(10)+"px solid black",
			"border-radius" : getElSize(20),
			"width" : getElSize(2200),
			"padding" : getElSize(20),
			"margin-bottom" : getElSize(20),
			"margin-top" : getElSize(20)
		})
		
		$(".save-setting").css({
			"background" : "#4DA63A",
			"color" : "white",
			"margin-left" : getElSize(50),
			"padding" : getElSize(100),
			"width" : getElSize(3640),
			"margin-top" : getElSize(200)
		})
		
		$(".close-setting").css({
			"background" : "#F1F1F1",
			"color" : "black",
			"padding" : getElSize(100),
			"margin-left" : getElSize(50),
			"padding" : getElSize(100),
			"width" : getElSize(3640),
			"margin-top" : getElSize(200)
		})
		
		$(".inputs").css({
			"font-size" : getElSize(150),
			"margin": getElSize(20),
			"height" : getElSize(300),
			"width" : "100%",
			"border-style" : "groove",
			"border-width" : "1px",
			"text-align" : "center",
		})
		
		$(".defaultFont").css({
			"font-size" : getElSize(200),
			"padding-top" : getElSize(50),
			"padding-bottom" : getElSize(50),
		})
		
		$(".buttons").css({
			"position" : "fixed",
			"left"	:	"0px",
			"margin-left" : getElSize(50),
			"margin-right" : getElSize(50),
			"bottom" :	getElSize(200),
		})
		
		$(".outer").css({
			"display" : "table",
			"position" : "absolute",
			"top" : "0",
			"left" : "0",
			"height" : "70%",
			"width" : "100%",
		})

		$(".middle").css({
  			"display" : "table-cell",
  			"vertical-align" : "middle",
		})

		$(".inner").css({
  			"margin-left" : "auto",
  			"margin-right" : "auto",
  			"width" : getElSize(3300),
		})
	};
	
	
	function saveBtn(){
		if(window.sessionStorage.getItem("login")=="success"){
			
			var r = confirm("변경하시겠습니까?");
			
			if(r==true){
				$('body').loading({
					message: '기다려 주세요',
					theme: 'dark'
				});
				
				id=user_id;
				console.log(id)
				name=$("#name").val();
				console.log(name)
				password=$("#password").val();
				console.log(password)
				
				
				var url = ctxPath + "/chart/setUserInfo.do";
				var param = "id=" + id + 
							"&name=" + name + 
							"&pwd=" + password;
				
				console.log(url)
				console.log(param)
				/*
				$.ajax({
					url : url,
					data : param,
					type : "post",
					success : function(data){
						$('body').loading('stop');
						alert("변경을 완료하였습니다.");
					},error :function(e1,e2,e3){
						$('body').loading('stop');
						alert("변경을 실패하였습니다.");
						console.log(e1,e2,e3)
					}
				})*/
			}else{
				
			}
		} else {
			
		}
		
		
	}
	
	function closeBtn(){
		history.go(-1);
		//window.history.back();
	}
	
	function changeFont(val){
		if (val == 'big'){
			$('#fontBigBtn').removeClass('fontBtnsUnselected');
			$('#fontBigBtn').addClass('fontBtnsSelected');
			
			$('#fontNormalBtn').removeClass('fontBtnsSelected');
			$('#fontNormalBtn').addClass('fontBtnsUnselected');
		} else {
			$('#fontBigBtn').removeClass('fontBtnsSelected');
			$('#fontBigBtn').addClass('fontBtnsUnselected');
			
			$('#fontNormalBtn').removeClass('fontBtnsUnselected');
			$('#fontNormalBtn').addClass('fontBtnsSelected');
		}
	}
</script>
</head>
<body>
	<div class="buttons">
		<button class="btn save-setting" onclick="saveBtn();">저장</button>
		<button class="btn close-setting" onclick="closeBtn();">닫기</button>
	</div>
	<div class="outer">
  		<div class="middle">
			<div class="inner">
				<table style="width:100%;">
					<colgroup>
						<col width="40%">
						<col width="60%">
					</colgroup>
					<tr>
						<td class="defaultFont tds">글자크기</td>
						<td class="tds" align="center">
							<button id="fontNormalBtn" class="defaultFont fontBtnsSelected" onclick="changeFont('normal')">보통</button>
							<button id="fontBigBtn" class="defaultFont fontBtnsUnselected" onclick="changeFont('big')">크게</button>
						</td>
					</tr>
					<tr>
						<td class="tds"></td>
						<td class="tds"></td>
					</tr>
					<tr>
						<td class="defaultFont tds">언어 설정</td>
						<td class="tds" align="center">
							<select class="defaultFont" style="width:100%;text-align:center;">
								<option>한글</option>
								<option>中文</option>
								<option>English</option>
								<option>German</option>
							</select>
						</td>
					</tr>
					<tr>
						<td class="tds"></td>
						<td class="tds"></td>
					</tr>
					<tr>
						<td class="defaultFont tds">배경 설정</td>
						<td class="tds"></td>
					</tr>
				</table>
				<div>
					<input type="checkbox"> 백그라운드 그림 출력
				</div>
			</div>
		</div>
	</div>	
</body>
</html>