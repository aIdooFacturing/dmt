<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<%@ include file="/WEB-INF/views/include/lib.jsp"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1">
<title>CNC Information</title>
<script src="${ctxPath }/js/jq.js" ></script>
<script src="${ctxPath }/js/RGraph/RGraph.common.core.js" ></script>
<script src="${ctxPath }/js/RGraph/RGraph.common.dynamic.js" ></script>
<script src="${ctxPath }/js/RGraph/RGraph.common.effects.js" ></script>
<script src="${ctxPath }/js/RGraph/RGraph.gauge.js" ></script>
<script src="${ctxPath }/js/RGraph/RGraph.gantt.js" ></script>
<script src="${ctxPath }/js/RGraph/RGraph.hbar.js" ></script>
<script src="${ctxPath }/js/RGraph/RGraph.hprogress.js" ></script>
<script type="text/javascript">
	var width = window.innerWidth;
	var height = window.innerHeight;
	var myInterval;
	var intervalValue = 2000;
	var ip = "${param.ip}";
	var port = "${param.port}";
	var status;
	var pStatus;
	var sHour;
	var maxRPM = 0;
	var timeline_data;
	var time_line;
	var spindle_load_chart;
	var feed_override_chart;
	var status_chart;
	var spindle;
	var feed;
	var sTime;
	
	var date = new Date();
	var year = date.getFullYear();
	var month = addZero(String(date.getMonth()+1));
	var day = addZero(String(date.getDate()));
	var today = "" + year + month + day;
	
	function init_status_chart(){
		status_chart = new RGraph.HProgress({
            id: 'status_chart',
            min: 0,
            max: 24,
            value:0,
            options: {
                tickmarks: 100,
                numticks: 20,
                gutter: {
                	right: 13,
                   left : 10,
                   top:10,
                  	bottom : 20
               	 	},
                margin: 5,
                text:{
   	        	  		size:7
   	          		},
	   	         	background:{
	            		grid:{
	            			autofit:{
	            				numvlines : 24
	            			}
	            		}
	            	}
            	}
        }).draw();
	};
	
	$(function(){
		init_status_chart();
		$("#interval").val(intervalValue/1000);
		
	   spindle_load_chart = new RGraph.HProgress({
            id: 'spindle_chart',
            min: 0,
            max: 160,
            value:0,
            options: {
                tickmarks: 100,
                numticks: 20,
                gutter: {
                	right: 13,
                   left : 10,
                   top:10,
                  	bottom : 20
               	 	},
                margin: 5,
                units: {
                   post: '%'
                	},
                text:{
   	        	  		size:7
   	          		},
            	}
        }).draw();
	  	
	   feed_override_chart = new RGraph.HProgress({
           id: 'feed_override_chart',
           min: 0,
           max: 200,
           value:0,
           options: {
               tickmarks: 100,
               numticks: 20,
               gutter: {
	               	right: 13,
                   	left : 10,
                   	top : 10,
                   	bottom :20
               	  },
               units: {
                   post: '%'
              	  },
               margin: 5,
            	  text:{
 	        	  		size:7
 	          	  },
           		}
       }).draw();
	   
	  	getDataLoop();
	  	
	  	/* setInterval(function(){
	  		var random = parseInt(Math.random()*10);
	  		var statusColor;
	  		if(random>=6){
	  			statusColor = "red";
	  		}else if(random>=3){
	  			statusColor = "yellow";
	  		}else{
	  			statusColor = "green";
	  		};
	  		
	  		var date = new Date();
			var hour = date.getHours();
			var minute = date.getMinutes();
			var time = hour + 0.16 * addZero(String(minute)).substring(0,1);
		  	time_loop(statusColor, time, minute);
	  	},1000) */
	  	
	});
	"<iframe src=test>iframe</iframe>\n<iframe src=test2>iframe</iframe>";
	function getDataLoop(){
		myInterval = setInterval(getData,intervalValue);
	};
	
	var flag = true;
	var pre_msgArray = new Array();
	var progName;
	var feedOverride;
	var progName;
	var spindleValues;
	var spindleLoad;
	var progress;
	function getData(){
		var date = new Date();
		var hour = date.getHours();
		var minute = date.getMinutes();
		var time = hour + 0.16 * addZero(String(minute)).substring(0,1);
		var second = date.getSeconds();
		var url = "${ctxPath}/nfc/getData.do";
		var param = "ip=" + ip + 
						"&port=" + port;

		$.ajax({
			url : url,
			data : param,
			type : "post",
			success : function(data){
				if(data=="fail"){
					$("#currentTime").html("noConnection");
					return;
				}
				sTime = hour;
				//$("#currentTime").html(addZero(String(hour)) + " : " + addZero(String(minute)) + " : " + addZero(String(second)));
				var index = data.split("/");
				
				$("#currentTime").html(""+index[10]);
				recodedData = true;
				var statusTime = index[0];
				var statusColors = index[1];
				status_time = new Array();
				status_colors = new Array();
				
				RGraph.clear(document.getElementById("status_chart"));
				//init_status_chart();
				var arrayTimeIdx = statusTime.split(",");
				var arrayColorsIdx = statusColors.split(",");
				
				for(var i =0 ; i < arrayTimeIdx.length; i++){
					status_time.push(Number(arrayTimeIdx[i]));
				};
				
				for(var i =0 ; i < arrayColorsIdx.length; i++){
					status_colors.push(arrayColorsIdx[i]);
				};
				
				status_chart.value=status_time;
				status_chart.set("colors", status_colors);
				status_chart.draw();	
				
				feedOverride = index[3];
				$("#main_prog_name").html("<center><h2><b>" + index[4] +"</b><h2></center>");
				alarmNum = index[5];
				status = index[6];
				
				//feedOverride = 100;				
				spindleValues = index[2];
				
				//var spindleIdx = spindleValues.split("|");
				//var startIdx = spindleIdx[2];
				//var endIdx = startIdx.indexOf("%");
				//spindleLoad = startIdx.substring(0,endIdx);
				spindleLoad = index[2];
				
				//spindleLoad = spindleValues.substring(19,spindleIdx);
				var alarmMsg = removePlus(decodeURIComponent(index[7]));
				if(status == "STRT" || status == "START"){
					stateColor = "green";
				}else if( !(status == "STRT" || status == "START")){
					stateColor = "yellow";
				}
				
				var arrayIndex = alarmMsg.split("\n");
				var alarmMsgArray = new Array();
				alarmMsgArray.push(arrayIndex[0]);
				alarmMsgArray.push(arrayIndex[1]);
				alarmMsgArray.push(arrayIndex[2]);
				
				if(alarmMsg!=""){
					for(var i = 0; i < alarmMsgArray.length-1; i++){
						var idx = alarmMsgArray[i].indexOf(":");
						var lidx = alarmMsgArray[i].lastIndexOf(":");
						var alarmTy = alarmMsgArray[i].substring(lidx+1) + "_";
						if(alarmTy=="0_"){
							alarmTy = "";
						};
						$("#alarmCd" + (i+1)).html(alarmTy + alarmMsgArray[i].substring(0,idx));
						$("#alarmMsg" + (i + 1) + "-1").html(alarmMsgArray[i].substring(idx+2,lidx));
					};
					
					time_loop("red", time, addZero(String(minute)));
				}else{
					$(".alarmCd").html("<font style='color: white'>.</font>");
					$(".alarmMsg").html("<font style='color: white'>.</font>");
					time_loop(stateColor, time, addZero(String(minute)));
				};

				
				spindle_loop();
				feed_loop();
				
			},
			error : function(){
				time_loop("black",time, addZero(String(minute)));
			}
		});
	};
	
	function spindle_loop(){
		//var spindleLoad=0;
		//while(spindleLoad<75 && spindleLoad>85){
		//	spindleLoad = parseInt(Math.random()*100);
		//};
		/* var rnd = parseInt(Math.random()*100);
		rnd = 75+ (rnd % 10);
		spindleLoad = rnd;
*/
		var color = "";
		if(spindleLoad>=120){
			color = "red";
		}else if(spindleLoad>=80){
			color = "yellow";	
		}else{
			color = "green";
		}; 
		
		spindle_load_chart.value=Number(spindleLoad);
		spindle_load_chart.set('colors', [color]);
		spindle_load_chart.grow();
		
		$("#spindle_value").html(spindleLoad + "%");
	};
	
	function feed_loop(){
		var color = "";
		if(feedOverride>=150){
			color = "red";
		}else if(feedOverride>=100){
			color = "yellow";	
		}else{
			color = "green";
		};
		feed_override_chart.value=Number(feedOverride);
		feed_override_chart.set('colors', [color]);
		feed_override_chart.grow();

		$("#feed_override_value").html(feedOverride + "%");
	};
	
	function removePlus(str){
		return str = str.replace(/\+/gi," ");
	};
	
	function setChartInterval(){
		var interval = $("#interval").val()*1000;
		intervalValue = interval;
		
		clearInterval(myInterval);
		getDataLoop();	
	};
	
	function resetIntervalVal(){
		$("#interval").val("");
	};
	
	function sendURI(){
		var url = window.location.protocol + "//" + window.location.host + window.location.pathname;
		location.href="ditalk://talkurl?confirm=" + url;
	};
	
	var pHour;
	var pStatus;
	var first = true;
	var pMinute;
	var arrayIdx = 2;
	var status_time = new Array();
	var status_colors = new Array();
	var idx=0;
	
	function time_loop(statusColor, time, minute){
		$("#currentStatus").css("background-color",statusColor);
		$("#currentStatusFont").css("color",statusColor);
		minute = String(minute).substring(0, 1);
		//getStatusData();
		/* var status_ty;
		if(statusColor=="green"){
			status_ty = "In-cycle";
		}else if(statusColor=="yellow"){
			status_ty = "Wait";
		}else if(statusColor=="red"){
			status_ty = "Alarm";
		}else{
			status_ty = "black";
		};
		
		if(first && recodedData==false){
			status_time.push(time-0.16);
			status_chart.value=status_time;
			status_colors.push("white");
			status_chart.set("colors", status_colors);
			status_chart.draw();
			first = false;
		};

		if(pMinute!=minute){
			RGraph.clear(document.getElementById("status_chart"));
			//init_status_chart();
			status_time.push(0.16);
			status_colors.push(statusColor);	
			
			status_chart.value=status_time;
			status_chart.set("colors", status_colors);
			status_chart.draw();	
			
			pMinute=minute;
			idx++;
		};
		
		if(statusColor=="red" || (pStatus=="green" && statusColor=="yellow" && status_colors[idx]!="red")){
			RGraph.clear(document.getElementById("status_chart"));
			status_colors[idx]=statusColor;
			
			status_chart.value=status_time;
			status_chart.set("colors", status_colors);
			status_chart.draw();	
			
			pMinute=minute;
		};
 
		pStatus = statusColor; */
	};
	
	var recodedData = false;
	function getStatusData(){
		var url = "${ctxPath}/nfc/getData.do";
		var param = "today_=" + today;
		$.ajax({
			url : url,
			data : param,
			dataType : "text",
			type : "post",
			success : function(data){
				if(data!="/"){
					recodedData = true;
					var index = data.split("/");
					var statusTime = index[0];
					var statusColors = index[1];
					
					status_time = new Array();
					status_colors = new Array();
					
					RGraph.clear(document.getElementById("status_chart"));
					//init_status_chart();
					var arrayTimeIdx = statusTime.split(",");
					var arrayColorsIdx = statusColors.split(",");
					
					for(var i =0 ; i < arrayTimeIdx.length; i++){
						status_time.push(Number(arrayTimeIdx[i]));
					};
					
					for(var i =0 ; i < arrayColorsIdx.length; i++){
						status_colors.push(arrayColorsIdx[i]);
					};
					
					status_chart.value=status_time;
					status_chart.set("colors", status_colors);
					status_chart.draw();	
				};
			}
		});
	};
	
	function addZero(str){
		if(str.length=="1"){
			str = "0" + str;
		};
		return str;
	};
</script>
<style type="text/css">
	.circle{
		height: 10px;
		padding: 10px 5px 10px 5px;
		display: none;
		border-radius: 100%;
	   -o-border-radius: 100%;
	   -webkit-border-radius: 100%;
	   -moz-border-radius: 100%;
	}
	.label{
		font-size: 13px;
		margin-left: 3px;
	}
</style>
</head>
<body>
	<Table width="100%" style="margin: 0px; ">
		<tr >
			<td colspan="2">갱신주기 : 
				<input id="interval" type="tel" size="2" onfocus="resetIntervalVal();">(초)
				<button onclick="setChartInterval();" >설정</button>
				<button onclick="sendURI()">공유</button>
				<span id="currentTime" style="margin-left: 10px"></span>
			</td>
		</tr>
		<tr>
			<td colspan="2">
				<span id="main_prog_name"></span>
			</td>
		</tr>
		<tr>
			<td colspan="2">
				<span style="background-color: green; padding: 0.2px;">　</span><font class="label" >In-cycle</font>
				<span style="background-color: yellow; padding: 0.2px;">　</span><font class="label">Wait</font>
				<span style="background-color: red; padding: 0.2px;">　</span><font class="label">Alarm</font>
				<span style="background-color: black; padding: 0.2px;">　</span><font class="label">No Connection</font>　
			</td>
		</tr>
		<tr>
			<td colspan="2">
				<canvas id="status_chart" width="360" height="100"></canvas>
				<!-- <div id="status_chart"></div> -->
				<div>Current Status : <span  id="currentStatus" ><font id="currentStatusFont" style="color: white">....</font></span></div>
			</td>
		</tr>
		<tr>
			<Td colspan="2">
				<table width="100%" border="1" style="border-collapse: collapse;">
					<tr>
						<td colspan="3">
							<b>Alarm</b>
						</td>
					</tr>
					<tr>
						<Td  width="20%" id="alarmCd1" align="right" class="alarmCd"><font style="color: white">.</font></Td>
						<td id="alarmMsg1-1" class="alarmMsg">　</td>							
					</tr>
					<tr>
						<Td  width="20%" id="alarmCd2" align="right" class="alarmCd"><font style="color: white">.</font></Td>
						<td id="alarmMsg2-1" class="alarmMsg">　</td>							
					</tr>
				</table>
			</Td>
		</tr>
		<tr>
			<td colspan="2" >
				Spindle Load : <span id="spindle_value"></span>
				<canvas id="spindle_chart" width="350" height="80"></canvas>
				<%-- <canvas id="spindle_chart"  height="80"></canvas> --%>
			</td>
		</tr>
		<tr>
			<td colspan="2" >
				Feed Override : <span id="feed_override_value"></span>
				<canvas id="feed_override_chart" width="350" height="80"></canvas> 
				<%-- <canvas id="feed_override_chart"  height="80"></canvas> --%>
			</td>
		</tr>
	</Table>
</body>
</html>