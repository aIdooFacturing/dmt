<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ include file="/WEB-INF/views/include/lib_mobile.jsp"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<link rel="shortcut icon" href="${ctxPath }/images/dulink_favicon.ico">
<title>Machine List</title>
<style>
</style>
<script type="text/javascript">
	var ctxPath = "${ctxPath}";
	var fromDashboard = "${fromDashBoard}";
	var targetWidth = 3840;
	var targetHeight = 2160;

	var originWidth = window.innerWidth;
	var originHeight = window.innerHeight;
	
	var contentWidth = originWidth;
	var contentHeight = targetHeight/(targetWidth/originWidth);
	
	var screen_ratio = getElSize(240);
	
	if(originHeight/screen_ratio<9){
		contentWidth = targetWidth/(targetHeight/originHeight)
		contentHeight = originHeight; 
	};
	
	function getElSize(n){
		return contentWidth/(targetWidth/n);
	};
	
	function setElSize(n) {
		return Math.floor(targetWidth / (contentWidth / n));
	};
	
	var marginWidth = (originWidth-contentWidth)/2;
	var marginHeight = (originHeight-contentHeight)/2;
</script>
<script type="text/javascript" src="${ctxPath }/js/chart/highcharts.js"></script>
<script type="text/javascript"
	src="${ctxPath }/js/chart/highcharts-3d.js"></script>
<script type="text/javascript"
	src="${ctxPath }/js/chart/highcharts-more.js"></script>
<script type="text/javascript" src="${ctxPath }/js/jqMap.js"></script>
<link rel="stylesheet" href="${ctxPath }/css/jquery.loading.min.css">
<script type="text/javascript"
	src="${ctxPath }/js/jquery.loading.min.js"></script>
<script type="text/javascript" src="${ctxPath }/js/chart/moment.js"></script>

<script type="text/javascript">
	var shopId = 1;
	
	var broswerInfo = navigator.userAgent;
	
	$(function(){
		
		getCheckedList()
		setElement()
	});

	function decode(str){
		return decodeURIComponent(str).replace(/\+/gi, " ")
	};
	
	function setElement(){
		var width = window.innerWidth;
		var height = window.innerHeight;
		
		$(".btn").css({
			"font-size" : getElSize(150),
			"background" : "white",
			"border" : getElSize(10)+"px solid black",
			"border-radius" : getElSize(20),
			"width" : getElSize(1500),
			"padding" : getElSize(100)
		})
		
		$(".save-setting").css({
			"background" : "#4DA63A",
			"color" : "white",
			"margin-left" : getElSize(50),
			"padding" : getElSize(100),
			"width" : getElSize(3000),
			"margin-top" : getElSize(200)
		})
		
		$(".close-setting").css({
			"background" : "#F1F1F1",
			"color" : "black",
			"padding" : getElSize(100),
			"margin-left" : getElSize(50),
			"width" : getElSize(3000),
			"margin-top" : getElSize(200)
		})
		
	};
	
	function getCheckedList(){
		
		
		let userId = window.sessionStorage.getItem("user_id");
		let uuid = window.sessionStorage.getItem("uuid");
		
		var url = ctxPath + "/chart/getCheckedList.do";
		var param = "id=" + userId + 
					"&shopId=" + shopId + 
					"&uuid=" + uuid;
		$.ajax({
			url : url,
			data : param,
			type : "post",
			dataType : "json",
			success : function(data){
				var json = data.dataList;
				
				kendodata = new kendo.data.DataSource({
					schema: {
			           model: {
			               id: "dvcId",
			               fields: {
			            	   name : {},
			            	   jig : {},
			            	   checked : {},
			            	   dvcId : {}
			               }
			           }
					},
					group: { field: "jig" }
				})
				
				for(var i=0;i<json.length;i++){
					json[i].jig=decode(json[i].jig)
					kendodata.add(json[i])
				}
				
				$("#setting-grid").css({
					"width" : getElSize(3300)
				})
				
				$("#setting-grid").kendoGrid({
					dataSource : kendodata,
		            persistSelection: true,
					height : getElSize(4000),
					columns : [
						{ 
							selectable: true, 
							width: getElSize(100), 
							headerAttributes: {
							      style: "font-size : " + getElSize(150)
					    	},	
							attributes: {
							      style: "font-size : " + getElSize(150)
					    	}	
						},
						{ 
							field :"name",
							width: getElSize(300),
							headerTemplate : "장비명",
							headerAttributes: {
							      style: "font-size : " + getElSize(150)
					    	},
							attributes: {
						      style: "font-size : " + getElSize(150)
					    	}
						},
						{ 
							field :"jig",
							width: getElSize(200),
							headerTemplate : "직",
							groupHeaderTemplate: "#=value#",
							headerAttributes: {
							      style: "font-size : " + getElSize(150)
					    	},
							attributes: {
						      style: "font-size : " + getElSize(150)
					    	}
						},
					],
					change :function(arg){
						console.log("Checked")
						console.log(this.selectedKeyNames())
						
						saveList = this.selectedKeyNames()
					},
					dataBinding : function(){
						$("#setting-grid").css({
							"top" : "0"
						})
					},
					dataBound : function(){
						$(".k-no-text").css({
							"-webkit-transform": "scale(3)",
							"margin-bottom" : getElSize(-25),
							"margin-left" : getElSize(30)
						})
						
						$(".k-reset").css({
							"font-size" : getElSize(150)
						})
						
						$(".k-checkbox-label").first().css({
							"margin-bottom" : getElSize(70),
						})
						
						$(".btn").css({
							"font-size" : getElSize(150),
							"background" : "white",
							"border" : getElSize(10)+"px solid black",
							"border-radius" : getElSize(20),
							"width" : getElSize(2200),
							"padding" : getElSize(20),
							"margin-bottom" : getElSize(20),
							"margin-top" : getElSize(20)
						})
						
						$("#save-setting").css({
							"background" : "#4DA63A",
							"color" : "white",
							"margin-left" : getElSize(50),
							"padding" : getElSize(100),
							"width" : getElSize(3000),
							"margin-top" : getElSize(200)
						})
						
						$("#close-setting").css({
							"background" : "#F1F1F1",
							"color" : "black",
							"padding" : getElSize(100),
							"margin-left" : getElSize(50),
							"width" : getElSize(3000),
							"margin-top" : getElSize(200)
						})
						
						
						var grid = this;
						
						grid.items().each(function(){
							
							var data = grid.dataItem(this);
							
							if(data.checked==1){
								grid.select(this)
							}
														
						})
					}
				})
			}
		})
	}
	
	function settingDialogClose(){
		history.go(-1);
	}
	
	
	
</script>
</head>
<body>

	<div>
		<div id="">
			알림설정 <input type="checkbox">
		</div>
		<div>
			<button>월</button>
			<button>화</button>
			<button>수</button>
			<button>목</button>
			<button>금</button>
			<button>토</button>
			<button>일</button>
			<input type="checkbox">공휴일 제외
		</div>
	</div>

	<div>
		<div>알림금지 시간설정</div>
		<input type="checkbox">야간 제외 <input type="checkbox">직접
		설정 <input type="time"> ~ <input type="time">
	</div>

	<div>
		<div>장비 설정</div>
		<div id="setting-grid"></div>
	</div>

	<div>
		<button class="btn save-setting" onclick="pushSave()"> 저장 </button>
		<br>
		<button class="btn close-setting" onclick="settingDialogClose()"> 닫기 </button>
		<!-- <button>저장</button>
		<button>닫기</button> -->
	</div>

</body>
</html>