<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ include file="/WEB-INF/views/include/lib.jsp"%>

<spring:message code="division" var="division"></spring:message>
<spring:message code="ophour" var="ophour"></spring:message>
<spring:message code="wait" var="wait"></spring:message>
<spring:message code="stop" var="stop"></spring:message>
<spring:message code="noconnection" var="noconnection"></spring:message>
<spring:message code="avrg" var="avrg"></spring:message>


<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<meta http-equiv="X-UA-Compatible" content="chrome=1">
<meta http-equiv="cache-control" content="max-age=0" />
<meta http-equiv="cache-control" content="no-cache" />
<meta http-equiv="expires" content="0" />
<meta http-equiv="expires" content="Tue, 01 Jan 1980 1:00:00 GMT" />
<meta http-equiv="pragma" content="no-cache" />
<link rel="shortcut icon" href="${ctxPath }/images/dulink_favicon.ico">

<title><spring:message code="operation_graph_daily"></spring:message></title>
<style type="text/css">
	.selected_span{
	background-color :white;
	color : #008900;
}
	
</style>
<style type="text/css">
.unSelected_menu{
	background-color :gray;
	color : white
}

.selected_menu{
	background-color :rgb(33,128,250);
	color : white
}
</style>
<script type="text/javascript">
	var ctxPath = "${ctxPath}";
	var targetWidth = 3840;
	var targetHeight = 2160;

	var originWidth = window.innerWidth;
	var originHeight = window.innerHeight;

	var contentWidth = originWidth;
	var contentHeight = targetHeight / (targetWidth / originWidth);

	var screen_ratio = getElSize(240);

	if (originHeight / screen_ratio < 9) {
		contentWidth = targetWidth / (targetHeight / originHeight)
		contentHeight = originHeight;
	};

	function getElSize(n) {
		return contentWidth / (targetWidth / n);
	};

	function setElSize(n) {
		return targetWidth / (contentWidth / n);
	};
	
	var marginWidth = (originWidth-contentWidth)/2;
	var marginHeight = (originHeight-contentHeight)/2;
</script>
<script src="${ctxPath }/js/jquery-1.11.2.min.js"></script>
<script src="${ctxPath }/js/jquery-ui.js"></script>
<script src="${ctxPath }/js/chart/highstock_data.js"></script>
<script src="${ctxPath}/js/highstock.js"></script>
<script src="${ctxPath }/js/multicolor_series.js"></script>
<script src="${ctxPath }/js/jqMap.js"></script>
<link rel="stylesheet" href="${ctxPath }/css/default.css">
<script type="text/javascript">

var shopId = 1;
var jig = window.localStorage.getItem("jig");

var dvc = replaceHyphen("${dvcName}");
/* var sDate = "${sDate}";
var eDate = "${eDate}"; */


var date = new Date();
var year = date.getFullYear();
var month = addZero(String(date.getMonth()+1));
var day = addZero(String(date.getDate()));

var eDate = year + "-" + month + "-" + day;

var myDate = new Date(month + "/" + day +"/" + year)
myDate.setDate (myDate.getDate() - 10);

var year = myDate.getFullYear();
var month = addZero(String(myDate.getMonth()+1));
var day = addZero(String(myDate.getDate()));

var sDate = year + "-" + month + "-" + day;



function replaceHyphen(str){
	return str.replace(/-/gi,"#");
};

var targetMap = new JqMap();
var targetMap2 = new JqMap();
var targetMap3 = new JqMap();
var targetMap_night = new JqMap();
var targetMap2_night = new JqMap();
var targetMap3_night = new JqMap();
function getTargetData(ty){
	var url = "${ctxPath}/chart/getTargetData.do";
	var type;
	if(ty=="day"){
		type = 2;
	}else{
		type = 1;
	};
	
	var param = "shopId=" + shopId + 
				"&type=" + type;
	
	$.ajax({
		url : url,
		data : param,
		dataType : "json",
		type : "post",
		success : function(data){
			var  json = data.dataList;
			
			var tr ="<tr>" + 
						"<td colspan='4' align='center'>" + 
							"<span class='daynight_span day' id='day'>주간</span>&nbsp;&nbsp;&nbsp;" + 
							"<span class='daynight_span night' id='night'>야간</span>" +
						"</td>" +   
						
					"</tr>" + 
					"<tr>" + 
						"<td>${device}</td><td style='text-align: center;'>${cycle}</td><td style='text-align: center;'>${ophour} (h)</td><td style='text-align: center;'>사이클당 생산량</td>" + 
					"</tr>";
			var class_name = "";
			$(json).each(function(idx, data){
					class_name = " ";
					if(ty=="day"){
						targetMap.put("t" + data.dvcId, data.tgCnt);
						targetMap2.put("c" + data.dvcId, data.tgRunTime);
						targetMap3.put("p" + data.dvcId, data.cntPerCyl);
					}else{
						targetMap_night.put("t" + data.dvcId, data.tgCnt);
						targetMap2_night.put("c" + data.dvcId, data.tgRunTime);
						targetMap3_night.put("p" + data.dvcId, data.cntPerCyl)
					}
					
				var name = decodeURIComponent(data.name).replace(/\+/gi, " ");
				var trs;
				if(ty=="day"){
					trs = "<td style='text-align: center;'> <input type='text' id=d_t" + data.dvcId +" class='targetCnt t" + class_name + "' value=" + data.tgCnt + "><span id=s" + data.dvcId + " class='span'></td>" + 
					"<td style='text-align: center;'> <input type='text' id=d_c" + data.dvcId +" class='targetCnt t" + class_name + "' value=" + data.tgRunTime/3600 + "><span id=s2" + data.dvcId + " class='span'></td>" + 
					"<td style='text-align: center;'> <input type='text' id=d_p" + data.dvcId +" class='targetCnt t" + class_name + "' value=" + data.cntPerCyl + "><span id=s2" + data.dvcId + " class='span'></td>";
				}else{
					trs = "<td style='text-align: center;'> <input type='text' id=n_t" + data.dvcId +" class='targetCnt t" + class_name + "' value=" + data.tgCnt + "><span id=s" + data.dvcId + " class='span'></td>" + 
					"<td style='text-align: center;'> <input type='text' id=n_c" + data.dvcId +" class='targetCnt t" + class_name + "' value=" + data.tgRunTime/3600 + "><span id=s2" + data.dvcId + " class='span'></td>" + 
					"<td style='text-align: center;'> <input type='text' id=d_p" + data.dvcId +" class='targetCnt t" + class_name + "' value=" + data.cntPerCyl + "><span id=s2" + data.dvcId + " class='span'></td>";
				};
				tr += "<tr>" + 
							"<td>" + name + "</td>" +
							trs + 
					  "</tr>";
				
			});
			
			
			tr += "<tr>" + 
						"<td colspan='5' style='text-align:center'><span style='cursor : pointer' id='save_btn' class='save_btn'>${confirm}</span></td>" + 
					"</tr>";
			
			$(".machineListForTarget").css({
				"opacity" :0,
				"z-index" : -999
			});
					
			$("#machineListForTarget_" + ty + " #machineListTable").html(tr);
			
			$(".daynight_span").css({
				"padding" : getElSize(10),
				"border-radius" : getElSize(10),
				"cursor" : "pointer"
			});
			
			$(".daynight_span").removeClass("selected_span")
			if(ty=="day"){
				$(".day").addClass("selected_span");	
			}else{ 
				$(".night").addClass("selected_span");
			};
			
			$("#machineListForTarget_" + ty).animate({
				"opacity" : 1
			},10, function(){
				$(this).css("z-index", 99999)
			});
			
			$(".save_btn").click(function(){
				var img = document.createElement("img");
				img.setAttribute("id", "loading_img");
				img.setAttribute("src", "${ctxPath}/images/load.gif");
				img.style.cssText = "width : " + getElSize(500) + "; " + 
									"position : absolute;" + 
									"z-index : 99999999;" + 
									"border-radius : 50%;"
									
				$("body").prepend(img);
				$("#loading_img").css({
					"top" : (window.innerHeight/2) - ($("#loading_img").height()/2),
					"left" : (window.innerWidth/2) - ($("#loading_img").width()/2)
				});
				
				save_type = "day"
				addTarget("day")
			});
			
			$(".targetCnt").css({
				"font-size" : getElSize(40),
				"width" : getElSize(250),
				"outline" : "none",
				"border" : "none"
			});
			
			$(".span").css({
				"font-size" : getElSize(40),
				"color" : "red",
				"margin-left" : getElSize(10),
				"font-weight" : "bolder"
			});
			
			$(".save_btn").css({
				"background-color" : "white",
				"color" : "black",
				"border-radius" : getElSize(10),
				"font-weight" : "bolder",
				"padding" : getElSize(10)
			});
			
			$(".daynight_span").click(function(){
				getTargetData(this.id)	
			});
			
			$(".tdisable").each(function(idx, data){
				this.disabled = true;
				this.value = "";
			});
			
			$(".tdisable").css({
				"background-color" : "rgba(	4,	238,	91,0.5)"
			});
			
			$("#machineListTable td").css({
				"color" : "white",
				"font-size" : getElSize(50),
			});
			
			$(".machineListForTarget").css({
				"width" : getElSize(1300),
				"z-index" : -1
			});
			
			$(".machineListForTarget").css({
				"top" : (originHeight/2) - ($(".machineListForTarget").height()/2)
			});
		}
	});
};

var tgArray = new Array();
var tgArray2 = new Array();
var tgArray3 = new Array();
var tgArray_night = new Array();
var tgArray2_night = new Array();
var tgArray3_night = new Array();

var target_i = 0;
function addTarget(ty){
	var date = new Date();
	var year = date.getFullYear();
	var month = addZero(String(date.getMonth()+1));
	var day = addZero(String(date.getDate()));
	
	today = year + "-" + month + "-" + day;
	var type;
	var cnt;
	var cntPerCyl;
	var time;
	var dvcId;
	if(ty=="day"){
		tgArray = targetMap.keys();
		tgArray2 = targetMap2.keys();
		tgArray3 = targetMap3.keys();
		
		tgArray_night = targetMap_night.keys();
		tgArray2_night = targetMap2_night.keys();
		tgArray3_night = targetMap3_night.keys();
		
		cnt = $("#d_" + tgArray[target_i]).val();
		time = $("#d_" + tgArray2[target_i]).val();
		cntPerCyl = $("#d_" + tgArray3[target_i]).val();
		
		dvcId = tgArray[target_i].substr(1);
		type = 2;
	}else{
		cnt = $("#d_" + tgArray[target_i]).val();
		time = $("#d_" + tgArray2[target_i]).val();
		cntPerCyl = $("#d_" + tgArray3[target_i]).val();
		dvcId = tgArray[target_i].substr(1);
		
		/* cnt = $("#n_" + tgArray_night[target_i]).val();
		time = $("#n_" + tgArray2_night[target_i]).val();
		dvcId = tgArray_night[target_i].substr(1); */
		type = 1;
	}
	
	var url = "${ctxPath}/chart/addTargetCnt.do";
	
	
	if(cnt==null || cnt == "") cnt = 0;
	if(time==null || time == "") time = 0;
	if(cntPerCyl==null || cntPerCyl == "") cntPerCyl = 0;
	
	var param = "dvcId=" + dvcId + 
				"&tgCnt=" + cnt +
				"&tgRunTime=" + (time*3600) +  
				"&tgDate=" + today + 
				"&type=" + type +
				"&cntPerCyl=" + cntPerCyl;
	
	$.ajax({
		url : url,
		data : param,
		dataType : "text",
		type : "post",
		success : function(data){
			if(data=="success") {
				if (target_i<(tgArray.length-1) && save_type == "day"){
					target_i++;
					addTarget("day");
					if (target_i==(tgArray.length-1)) {
						target_i = -1;
						save_type = "init";
					}
				/* }else if(target_i<(tgArray_night.length-1) && save_type == "night"){ */
				}else if(target_i<(tgArray.length-1) && save_type == "night"){
					target_i++;
					addTarget("night");
					if (target_i==(tgArray.length-1)) {
						target_i = 0;
						save_type = "init";
					}
				}
				else{
					$("#loading_img").remove();
					target_i = 0;
					$(".machineListForTarget, #close_btn").animate({
						"opacity" :0
					}, function(){
						$(".machineListForTarget").css("z-index",0);
						$("#close_btn").css("z-index",0	);
					});
				}
				
			}else{
				//i
			}
		}
	});
};


var save_type = "init"

$(function(){
	chkBanner();
	if(sDate==""){
		sDate = window.localStorage.getItem("jig_sDate");
		eDate = window.localStorage.getItem("jig_eDate");
	};
	
	//$(".date").change(getDvcList);
	
	setEl();
	
	setDate();
	//getMousePos();
	
	$("#menu_btn").click(function(){
		location.href = "${ctxPath}/chart/index.do"
	});
	
	$("#dvcSelector").click(dvcList);
	
	//getWcDataList();
	getDvcList();
	
	chartWidth = $("#chart").width()
	$("#table").click(function(){
		location.href = "${ctxPath}/chart/performanceReport.do";
	});
});

function dvcList(){
	$("#corver").css({
		"z-index" : 9,
		"opacity" : 0.7
	});
	
	$("#dvcList").toggle();	
};

function getDvcList(){
	var url = "${ctxPath}/chart/getJigList4Report.do";
	var param = "shopId=" + shopId;
	
	console.log(param)
	$.ajax({
		url : url,
		data : param,
		dataType : "json",
		type : "post",
		success : function(json){
			var json = json.dvcList;
			
			var tr = "";
			var options = "";
			$(json).each(function(idx, data){
				tr += "<tr class='dvcTr'>" + 
							"<td >" + decodeURIComponent(data.name).replace(/\+/gi," ") + "</td>" + 
					"</tr>"
					
				options += "<option value='" + decodeURIComponent(data.name).replace(/\+/gi,' ') + "'>" + decodeURIComponent(data.name).replace(/\+/gi," ") + "</option>";  
			});
			
			//$("#dvcList").html(tr);
			$("#dvcList").html(options);
			
		
			if(!first_load){
				$("#dvcList").val(dvc);
			};

			first_load = false;	
			$("#dvcList").val(dvc);
			
			
			if(dvc=="") {
				$("#dvcList").val($("#dvcList option:nth(0)").html());
				dvc = $("#dvcList option:nth(0)").html();
			}
			
			showWcDatabyDvc();
			/* $("#dvcList").change(function(){
				showWcDatabyDvc();
			}); */
			
		}
	});
};

var first_load = true;
function showWcDatabyDvc(dvc, sDate, eDate, ty){
	var dvc = $("#dvcList").val();
	var sDate = $("#sDate").val();
	var eDate = $("#eDate").val();
	
	var url = "${ctxPath}/chart/getWcDataByDvc.do";
	var param = "name=" + dvc + 
				"&sDate=" + sDate + 
				"&eDate=" + eDate + 
				"&shopId=" + shopId;

	
	if(ty=="jig"){
		$("#wc_sdate").val($("#jig_sdate").val());
		$("#wc_edate").val($("#jig_edate").val());		
	};
	
	$.ajax({
		url : url,
		data : param,
		dataType : "json",
		type : "post",
		success : function(data){
			var json = data.wcList;
			
			var start = new Date(sDate);
			var end = new Date(eDate);
			var n = (end - start)/(24 * 3600 * 1000)+1;

			var tr = "";

			wcList = new Array();
			var wc = new Array();
			
			wc.push("${division}");
			wc.push("${ophour}");
			wc.push("${wait}");
			wc.push("${stop}");
			wc.push("${noconnection}");
			
			wcList.push(wc);
			
			dateList = new Array();
			inCycleBar = new Array();
			waitBar = new Array();
			alarmBar = new Array();
			noConnBar = new Array();
			$(json).each(function(idx, data){
				dateList.push(data.workDate.substr(5).replace("-","/"));
				tmpArray = dateList;
				
				var wc = new Array();
				wc.push(data.workDate.substr(5).replace("-","/"));
				wc.push(data.inCycle_time);
				wc.push(data.wait_time);
				wc.push(data.alarm_time);
				wc.push(Number(Number(24 * 60 * 60 - (data.inCycle_time + data.wait_time + data.alarm_time )).toFixed(1)));
				
				wcList.push(wc);
				tmpWcList = wcList;
				
				var incycle = Number(Number(data.inCycle_time/60/60).toFixed(1));
				var wait = Number(Number(data.wait_time/60/60).toFixed(1));
				var alarm = Number(Number(data.alarm_time/60/60).toFixed(1));
				var noconn = Number(24 - (incycle + wait + alarm)).toFixed(1);
				
				inCycleBar.push(incycle);
				waitBar.push(wait);
				alarmBar.push(alarm);
				noConnBar.push(noconn);
				
				tmpInCycleBar = inCycleBar;
				tmpWaitBar = waitBar;
				tmpAlarmBar = alarmBar;
				tmpNoConnBar = noConnBar;
			});
		
			var blank = maxBar - json.length
			maxPage = json.length - maxBar; 
				
			for(var i = 0; i < blank; i++){
				dateList.push("");
				var wc = new Array();
				wc.push("____");
				wc.push("");
				wc.push("");
				wc.push("");
				wc.push("");
				
				wcList.push(wc);
				
				inCycleBar.push(0);
				waitBar.push(0);
				alarmBar.push(0);
				noConnBar.push(0);
			};
			
			if(json.length > maxBar){
				overBar = true;
				reArrangeArray();
			};
			
			$(".chartTable").remove();
			var table = "<table style='width: 100%; text-align: center; color: white;' class='chartTable'>";
			
			for(var i = 0; i < wcList[0].length; i++){
				table += "<tr class='contentTr'>";
				for(var j = 0; j < wcList.length; j++){
					if(j==0){
						table += "<td style='font-weight: bolder; background-color: rgb(34,34,34); width:" + getElSize(250) + "'>" + wcList[j][i] + "</td>";
					}else if(j==0 || i==0){
						table += "<td style='font-weight: bolder; background-color: rgb(34,34,34); width:" + chartWidth/(maxBar-1) + "' onclick='toDailyChart(\"" + wcList[j][i] + "\")'>" + wcList[j][i] + "</td>";
					}else {
						var n;
						if(typeof(wcList[j][i])=="number"){
							n = Number(wcList[j][i]/60/60).toFixed(1)
						}else{
							n = "";
						};
						table += "<td>" + n + "</td>";
					};
				};
				table += "</tr>";
			};
			
			table += "</table>";
			$("#tableContainer").append(table)
			
			setEl();
			chart("chart");
			addSeries();
			
			//$("#tableContainer").css("margin-top",$("#chart").offset().top + $("#chart").height() - $(".mainTable").height() + getElSize(50))
		}
	});
};

var chartWidth;
var maxBar = 10;
var tmpArray = new Array();
var tmpInCycleBar = new Array();
var tmpWaitBar = new Array();
var tmpAlarmBar = new Array();
var tmpNoConnBar = new Array();
var tmpWcName = new Array();
var tmpWcList = new Array();

function reArrangeArray(){
	dateList = new Array();
	inCycleBar = new Array();
	waitBar = new Array();
	alarmBar = new Array();
	noConnBar = new Array();
	wcName = new Array();
	wcList = new Array();
	
	for(var i = cBarPoint; i < (maxBar+cBarPoint); i++){
		dateList[i-cBarPoint] = tmpArray[i];
		inCycleBar[i-cBarPoint] = tmpInCycleBar[i];
		waitBar[i-cBarPoint] = tmpWaitBar[i];
		alarmBar[i-cBarPoint] = tmpAlarmBar[i];
		noConnBar[i-cBarPoint] = tmpNoConnBar[i];
		wcName[i-cBarPoint] = tmpWcName[i];
	};
	
	for(var i = cBarPoint; i < (maxBar+cBarPoint+1); i++){
		wcList[i-cBarPoint] = tmpWcList[i];	
	};
	var wc = new Array();
	wc.push("${division}");
	wc.push("${ophour}");
	wc.push("${wait}");
	wc.push("${stop}");
	wc.push("${noconnection}");
	
	//wcList.push(wc);
	wcList[0] = wc;
};

function nextBarArray(){
	if(cBarPoint>=maxPage || maxPage<=0){
		alert("${end_of_chart}");
		return;
	};
	cBarPoint++;
	reArrangeArray();
	
	$(".chartTable").remove();
	var table = "<table style='width: 100%; text-align: center; color: white;' class='chartTable'>";
	
	for(var i = 0; i < wcList[0].length; i++){
		table += "<tr class='contentTr'>";
		for(var j = 0; j < wcList.length; j++){
			if(j==0){
				table += "<td style='font-weight: bolder; background-color: rgb(34,34,34); width:" + getElSize(250) + "'>" + wcList[j][i] + "</td>";
			}else if(j==0 || i==0){
				table += "<td style='font-weight: bolder; background-color: rgb(34,34,34); width:" + $("#chart").width()/(maxBar-1) + "' onclick='toDailyChart(\"" + wcList[j][i] + "\")'>" + wcList[j][i] + "</td>";
			}else {
				var n;
				if(typeof(wcList[j][i])=="number"){
					n = Number(wcList[j][i]/60/60).toFixed(1)
				}else{
					n = "";
				};
				table += "<td>" + n + "</td>";
			};
		};
		table += "</tr>";
	};
	
	table += "</table>";
	$("#tableContainer").append(table)
	
	setEl();
	chart("chart");
	addSeries();
};

function prevBarArray(){
	if(cBarPoint<=0){
		alert("${first_of_chart}");
		return;
	};
	cBarPoint--;
	reArrangeArray();
	
	$(".chartTable").remove();
	var table = "<table style='width: 100%; text-align: center; color: white;' class='chartTable'>";
	
	for(var i = 0; i < wcList[0].length; i++){
		table += "<tr class='contentTr'>";
		for(var j = 0; j < wcList.length; j++){
			if(j==0){
				table += "<td style='font-weight: bolder; background-color: rgb(34,34,34); width:" + getElSize(250) + "'>" + wcList[j][i] + "</td>";
			}else if(j==0 || i==0){
				table += "<td style='font-weight: bolder; background-color: rgb(34,34,34); width:" + $("#chart").width()/(maxBar-1) + "' onclick='toDailyChart(\"" + wcList[j][i] + "\")'>" + wcList[j][i] + "</td>";
			}else {
				var n;
				if(typeof(wcList[j][i])=="number"){
					n = Number(wcList[j][i]/60/60).toFixed(1)
				}else{
					n = "";
				};
				table += "<td>" + n + "</td>";
			};
		};
		table += "</tr>";
	};
	
	table += "</table>";
	$("#tableContainer").append(table)
	
	setEl();
	chart("chart");
	addSeries();
};
var maxPage;
var cBarPoint = 0;
var overBar = false;
function getWcList(){
	var url = "${ctxPath}/chart/getWcList.do";
	var param = "jig=" + jig +
				"&shopId=" + shopId;
	
	$.ajax({
		url : url,
		data : param,
		dataType : "json",
		type : "post",
		success : function(json){
			var json = json.wcList;
			
			var tr = "";
			$(json).each(function(idx, data){
				tr += "<tr>" + 
							"<td>" + decodeURIComponent(data.WC) + "</td>" + 
					"</tr>"
			});
			
			$("#wcList").html(tr);
			$("#wcList td").css({
				"border" : "1px solid white"
			});
			
		}
	});
};
var panel = false;
var menu = false;
function getMousePos(){
	$(document).on("mousemove", function(e){
		var target = $("#menu_btn").width();

		if((e.pageX <= target && e.pageY <= target) || panel){
			if(!menu){
				$("#menu_btn").animate({
					"left" : 0,
					"top" : getElSize(20),
				});
				menu = true;
			};
		}else{
			if(menu){
				$("#menu_btn").animate({
					"left" : -getElSize(100),
				});	
				menu = false;
			};
		};
	});
};

function addZero(str){
	if(str.length==1){
		str = "0" + str;
	};
	return str;
};

function setDate(){
	var date = new Date();
	var year = date.getFullYear();
	var month = addZero(String(date.getMonth()+1));
	var day = addZero(String(date.getDate()));
	
	//$(".date").val(year + "-" + month + "-" + day);
	$("#sDate").val(sDate);
	$("#eDate").val(eDate);
};

function showwcList(){
	$("#corver").css({
		"z-index" : 9,
		"opacity" : 0.7
		
	});
	
	$("#wcList").toggle();	
};

function clearMenu(){
	$("#corver").css({
		"z-index" : -1,
		"opacity" : 0
		
	});
	
	$("#wcList").css("display","none");
	$("#dvcList").css("display","none");
};

function getDateDiff(date1,date2){
    var arrDate1 = date1.split("-");
    var getDate1 = new Date(parseInt(arrDate1[0]),parseInt(arrDate1[1])-1,parseInt(arrDate1[2]));
    var arrDate2 = date2.split("-");
    var getDate2 = new Date(parseInt(arrDate2[0]),parseInt(arrDate2[1])-1,parseInt(arrDate2[2]));
    
    var getDiffTime = getDate1.getTime() - getDate2.getTime();
    
    return Math.floor(getDiffTime / (1000 * 60 * 60 * 24));
};

var xAxis = new Array();
var barChart;
function chart(id){
	var sDate = window.localStorage.getItem("sDate");
	var eDate = window.localStorage.getItem("eDate");
	
	var date = new Date(sDate);
	
	var margin = (originWidth*0.05)/2;
	$("#" + id).css({
		//"position" : "absolute",
		"margin-left" : $("#tableContainer table td:nth(1)").offset().left - margin - getElSize(80),
		"width" : $("#tableContainer table").width() - $("#tableContainer table td:nth(0)").width()-getElSize(20) + getElSize(80),
		"top" : $(".label").offset().top + $(".label").height() + getElSize(50)
	})
	
	
	$('#' + id).highcharts({
        chart: {
            type: 'column',
            backgroundColor : 'rgb(50, 50, 50)',
            height : contentHeight * 0.5,
            marginLeft:getElSize(80),
            marginRight:0
        },
        title: {
            text:false
        },
        xAxis: {
            categories: dateList,
            labels : {
            	style : {
            		"color" : "white",
            		"font-size" : getElSize(30)
            	}
            }
        },
        yAxis: {
            min: 0,
            max : 24,
            title: {
                text: false
            },
            labels : {
            	style : {
            		"color" : "white",
            		"font-size" : getElSize(30)
            	},
            	enabled:true
            },
            //reversed:true 
        },
        tooltip: {
            enabled : false
        },
        plotOptions: {
            column: {
                stacking: 'normal',
                dataLabels: {
                    enabled: true,
                    color: (Highcharts.theme && Highcharts.theme.dataLabelsColor) || 'black',
                    style: {
                        //textShadow: '0 0 3px black',
                    }
                }
            },
            series : {
            	pointWidth:getElSize(100)
            }
        },
        credits : false,
        exporting : false,
        legend:{
        	enabled : false
        },
        series: []
    });
	
	barChart = $("#" + id).highcharts();
}
function setEl() {
	$(".container").css({
		"width": originWidth,
		"height" : originHeight,
		"margin-top" : originHeight/2 - ($(".container").height()/2)
	});
	
	$(".container").css({
		"margin-top" : originHeight/2 - ($(".container").height()/2),
		"margin-left" : originWidth/2 - ($(".container").width()/2),
	});
	
	$(".layoutTd").css({
		height : contentHeight * 0.85
	});

	$(".page").css({
		"height" : originHeight
	});

	$("#part1").css({
		"top" : 0
	});

	// 페이지 위치 조정
	for (var i = 2; i <= 5; i++) {
		$("#part" + i).css({
			"top" : $("#part" + (i - 1)).height() + originHeight
		});
	};

	$(".mainTable").css({
		"border-spacing" : getElSize(40)
	});

	$(".title").css({
		"padding" : getElSize(40),
		"font-size" : getElSize(100)
	});

	$(".subTitle").css({
		"padding" : getElSize(40),
		"font-size" : getElSize(50)
	});

	$(".tr2").css({
		"height" : contentHeight * 0.35
	})

	$("#chartTr").css({
		"height" : contentHeight * 0.4
	});

	$("#pie").css({
		"height" : contentHeight * 0.32
	});

	$(".neon1, .neon2, .neon3").css({
		"font-size" : getElSize(500)
	});

	$(".neon1, .neon2, .neon3").css({
		"margin-top" : $(".tr2").height() / 2 - $(".neon1").height()
						/ 2 - $(".subTitle").height(),
	});

	$("#machine_name").css({
		"color" : "white",
		"font-weight" : "bolder",
		"font-size" : getElSize(170),
	});

	$("#machine_name_td").css({
		"height" : getElSize(500)
	});

	$("#machine_name").css({
		"margin-top" : $("#machine_name_td").height() / 2
						- $("#machine_name").height() / 2
						- $(".subTitle").height()
	});

	$(".title_left").css({
		"float" : "left",
		"width" : contentWidth * 0.1
	});

	$(".title_right").css({
		"float" : "right",
	});



	$("#corver").css({
		"width" : originWidth,
		"height" : originHeight,
		"position" : "absolute",
		"z-index" : -1,
		"background-color" : "black",
		"opacity" : 0
	});
 
	$("#panel_table").css({
		"color" : "white",
		"font-size" : getElSize(40)
	});

	$("#panel_table td").css({
		"padding" : getElSize(30),
		"cursor" : "pointer"
	});
	
	$("#panel_table td").addClass("unSelected_menu");

	$(".dvcName_p1").css({
		"font-weight" : "bolder",
		"font-size" : getElSize(40)
	});

	$(".wrap").css({
		"width" : contentWidth * 0.2,
		"padding" : getElSize(10),
		"border-radius" : getElSize(30),
		"padding-bottom" : getElSize(10),
		//"border": getElSize(10) + "px outset white",
	});
	
	$(".waitCnt, .alarmCnt, .noConCnt").css({
		"font-size" : getElSize(20),
		"margin" :getElSize(20)
	});
	
	$(".waitCnt").css({
		"color" : "yellow"
	});
	
	$(".alarmCnt").css({
		"color" : "red"
	});
	
	$(".noConCnt").css({
		"color" : "gray"
	});
	
	$(".card_status").css({
		"height" : getElSize(60)
	});
	
	$(".card_table").css({
		"border-spacing" : getElSize(10)
	});
	
	$(".opRatio").css({
		"font-size" :getElSize(100)
	});
	
	$(".comName").css({
		"font-size" :getElSize(60)
	});
	
	$(".machine_cam").css({
		"width" : contentWidth * 0.07,
		"height" : contentHeight * 0.08,
		//"border" : getElSize(10) + "px solid rgb(50,50,50)" 
	});
	
	$(".frame").css({
		"width" : contentWidth * 0.07,
		"height" : contentHeight * 0.15
	});
	
	$(".statusCnt").css({
		"height" : getElSize(70),
		"vertical-align" : "bottom",
		"box-shadow ": "inset 0 5px 0 #ddd"
	});
	
	$(".opTime").css({
		"font-weight" : "bolder",
		"top" : getElSize(400),
		"right" : getElSize(10)
	});
	
	$(".logo").css({
		"height" : contentWidth * 0.01
	});
	
	$("#wcList").css({
		"position" : "absolute",
		"left" : originWidth/2 + $("#wcList").width()/2,
		"top" : originHeight/2 - $("#wcList").height()/2,
		"z-index" : 9999999,
		"display" : "none"
	}); 
	
	$("#title_left").css({
		"position" : "absolute",
		"width" : contentWidth * 0.07,
		"z-index" : 2,
		"left" : getElSize(100),
		"top" : getElSize(60)
	});

	$("#title_right").css({
		"position" : "absolute",
		"z-index" : 2,
		"right" : getElSize(100),
		"top" : getElSize(40),
		"color" : "white",
		"font-weight" : "bolder",
		"font-size" : getElSize(40)
	});
	
	$("#menu_btn").css({
		"position" : "absolute",
		"width" : getElSize(100),
		"left" : getElSize(20),
		"top" : getElSize(20),
		"cursor" : "pointer",
		"z-index" : 13
	});

	$("#panel").css({
		"background-color" : "rgb(34,34,34)",
		"border" : getElSize(20) + "px solid rgb(50,50,50)",
		"position" : "absolute",
		"width" : contentWidth * 0.2,
		"top" : 0,
		"left" : -contentWidth * 0.2 - getElSize(40),
		"z-index" : 99999
	});


	$("#panel_table td").addClass("unSelected_menu");
	
	$(".goGraph, .excel, .label, #dvcSelector").css({
		"margin-right": getElSize(10),
		"margin-left": getElSize(10),
		"border-radius" : getElSize(10),
		"padding" : getElSize(10),
		"cursor": "pointer",
		"font-size": getElSize(50)
	});
	
	$(".date").css({
		"height": getElSize(70),
		"font-size": getElSize(50)
	});
	
	$(".thead").css({
		"font-size" : getElSize(60)
	});
	
	$(".tmpTable, .tmpTable tr, .tmpTable td").css({
		"border": getElSize(5) + "px solid rgb(50,50,50)"
		//"border": getElSize(5) + "px solid gray"
	});
	
	$(".tmpTable td").css({
		"padding" : getElSize(10),
		"height": getElSize(100)
	});
	
	$(".contentTr").css({
		"font-size" : getElSize(40)
	});
	
	$(".chartTable").css({
		"margin-top" : getElSize(60),
		"font-size" : getElSize(50),
		//"padding" : getElSize(20),
		"border-collapse" : "collapse"
	});
	
	$(".chartTable td").css({
		"border" : "solid 1px gray"
	});
	
	
	$("#dvcList, button").css({
		"font-size": getElSize(60)
	}); 
	
	$("#chart").css({
		"margin-bottom" : getElSize(20)
	});
	
	$("#arrow_left, #arrow_right").css({
		"width" : getElSize(100),
		"margin-left" : getElSize(50),
		"margin-right" : getElSize(50),
		"cursor" : "pointer"
	});
	
	$("#menu9").addClass("selected_menu");
	$("#menu9").removeClass("unSelected_menu");
	
	$(".machineListForTarget").css({
		"position" : "absolute",
		"width" : getElSize(1200),
		"height" : getElSize(1200),
		"overflow" : "auto",
		//"top" : getElSize(50),
		//"background-color" : "rgb(34,34,34)",
		"background-color" : "green",
		"color" : "white",
		"font-size" : getElSize(50),
		"padding" : getElSize(50),
		"overflow" : "auto",
		"border-radius" : getElSize(50),
		"border" : getElSize(10) + "px solid white",
	});
	
	$(".machineListForTarget").css({
		"left" : (originWidth/2) - ($(".machineListForTarget").width()/2),
	});
	
	$("#bannerDiv").css({
		"position" : "absolute",
		"width" : getElSize(1500),
		"height" : getElSize(200),
		"border-radius" : getElSize(20),
		"padding" : getElSize(50),
		"background-color" : "lightgray",
		"z-index" : -9
	});

	
	$("#bannerDiv").css({
		"left" : (originWidth/2) - ($("#bannerDiv").width()/2),
		"top" : (originHeight/2) - ($("#bannerDiv").height()/2)
	});
	
	$("#bannerDiv input[type='text']").css({
		"width" : getElSize(1200),
		"font-size" : getElSize(50)
	});
	
	$("#bannerDiv button").css({
		"margin" : getElSize(50),
		"font-size" : getElSize(50)
	});
	
	$("#intro").css({
		/* "width" : contentWidth, */
		"position" : "absolute",
		"bottom" : 0 + marginHeight,
		"font-size" : getElSize(140),
		"font-weight" : "bolder",
		"z-index" : 9999
	});
	
	$("#intro_back").css({
		"width" : originWidth,
		"display" : "none",
		"height" : getElSize(180),
		"opacity" : 0.3,
		"position" : "absolute",
		"background-color" : "white",
		"bottom" : 0 + marginHeight,
		"z-index" : 9999
	});
	
};

function getBanner(){
	var url = "${ctxPath}/chart/getBanner.do";
	var param = "shopId=" + shopId;
	

	$.ajax({
		url : url,
		data : param,
		type : "post",
		dataType : "json",
		success : function(data){
			if(window.localStorage.getItem("banner")=="true"){
				$("#intro").html(data.msg).css({
					"color" : data.rgb,
					"right" : - window.innerWidth
				})
				
				$("#intro").html(data.msg).css({
					"right" : - $("#intro").width()
				})
			}
			
			bannerWidth = $("#intro").width() + getElSize(100);
			$("#intro").width(bannerWidth);
			$("#banner").val(data.msg);
			$("#color").val(data.rgb);
			
			twinkle();
			bannerAnim();
		}
	});		
}

var twinkle_opct = true;
function twinkle(){
	var opacity;
	if(twinkle_opct){
		opacity = 0;
	}else{
		opacity = 1;
	}
	$("#intro").css("opacity",opacity);
	
	twinkle_opct = !twinkle_opct;
	setTimeout(twinkle, 100)
};

var bannerWidth;
function bannerAnim(){
	$("#intro").width(bannerWidth - getElSize(10));
	$("#intro").animate({
		"right" : window.innerWidth  - getElSize(100)
	},15000, function(){
		$("#intro").css("right" , - $("#intro").width())
		//$(this).remove();
		bannerAnim();
	});
};

function chkBanner(){
	if(window.localStorage.getItem("banner")=="true"){
		$("#intro_back").css("display","block");
		getBanner();
	}else{
		$("#intro").html("");
		$("#intro_back").css("display","none");
	}
};

function cancelBanner(){
	window.localStorage.setItem("banner", "false");
	$("#bannerDiv").css("z-index",-9);
	chkBanner();
};

function addBanner(){
	var url =  ctxPath + "/chart/addBanner.do";
	var param = "msg=" + $("#banner").val() + 
				"&rgb=" + $("#color").val() + 
				"&shopId=" + shopId;
	
	$.ajax({
		url : url,
		data : param,
		type : "post",
		success : function(data){
			window.localStorage.setItem("banner", "true");
			$("#bannerDiv").css("z-index",-9);
			//chkBanner();
			location.reload();
		}
	});
};
function showPanel(){
	$("#panel").animate({
		"left" : 0
	});
	
	$("#menu_btn").animate({
		"left" : contentWidth * 0.2 + getElSize(50)	
	});
	
	$("#corver").css({
		"z-index" : 9,
		"opacity" : 0.7
	});
};

var inCycleBar = new Array();
var waitBar = new Array();
var alarmBar = new Array();
var noConnBar = new Array();
var dateList = new Array();

function removeHypen(str){
	return str.replace(/-/gi,"");
};

function addSeries(){
	/* barChart.addSeries({
		color : "gray",
		data : noConnBar
	}, true); */

	barChart.addSeries({
		color : "red",
		data : alarmBar
	}, true);
	
	barChart.addSeries({
		color : "yellow",
		data : waitBar
	}, true);
	
	barChart.addSeries({
		color : "green",
		data : inCycleBar
	}, true);
};

function closePanel(){
	$("#panel").animate({
		"left" : -contentWidth * 0.2 - getElSize(40)
	});
	
	$("#menu_btn").animate({
		"left" : 0
	});
	
	$("#corver").css({
		"z-index" : -1,
		"opacity" : 0.7
	});
};

</script>
</head>

<body >
	<div id="bannerDiv">
		<Center>
			<input type="text" id="banner" size="100"><input type="color" id="color"><br>
			<button onclick="addBanner();">적용</button>
			<button onclick="cancelBanner();">미적용</button>
		</Center>
	</div>
	<div id="intro_back"></div>
	<span id="intro"></span>
	
	<div id="title_right" class="title"></div>	
	
	<img src="${ctxPath }/images/home.png" id="menu_btn" >
	
	<table id="wcList" style="color:white; border-collapse: collapse;"></table>
	<!-- <table id="dvcList" style="color:white; border-collapse: collapse;"></table> -->
					
	<div id="corver"></div>

	<!-- Part1 -->
	<div id="part1" class="page">
		<div class="container">
			<center>
				<table class="mainTable">
					<tr>
						<Td align="center" style=" color:white; font-weight: bolder;" class="title" >
								<spring:message code="operation_graph_daily"></spring:message>
						</Td>
					</tr>
				</table>
				
				<div style="width: 95%" class="label">
						<spring:message code="device"></spring:message><select id="dvcList"></select>  
						<spring:message code="op_period"></spring:message> <input type="date" class="date" id="sDate"> ~ <input type="date" class="date" id="eDate"> 
						<button id="table"><spring:message code="table"></spring:message></button>
						<button onclick="showWcDatabyDvc()"><spring:message code="check"></spring:message></button>
				</div>
				
				<div id="chartContainer" style="width: 95%">
					<div id="chart" style="width: 100%">
						
					</div>
					<img alt="" src="${ctxPath }/images/arrow_left.png" id="arrow_left"  onclick="prevBarArray();">
					<img alt="" src="${ctxPath }/images/arrow_right.png" id="arrow_right" onclick="nextBarArray();">
					<div id="tableContainer" style="width: 100%"></div>
				</div>				
			</center>
		</div>
	</div>
	
</body>
</html>