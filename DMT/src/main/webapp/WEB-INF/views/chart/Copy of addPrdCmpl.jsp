<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ include file="/WEB-INF/views/include/lib.jsp"%>
<spring:message code="device" var="device"></spring:message>
<spring:message code="cycle" var="cycle"></spring:message>
<spring:message code="ophour" var="ophour"></spring:message>
<spring:message code="confirm" var="confirm"></spring:message>

<spring:message code="start_time" var="start_time"></spring:message>
<spring:message code="end_time" var="end_time"></spring:message>
<spring:message code="fixed_time" var="fixed_time"></spring:message>
<spring:message code="alarm" var="alarm"></spring:message>
<spring:message code="content" var="content"></spring:message>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<meta http-equiv="X-UA-Compatible" content="chrome=1">
<meta http-equiv="cache-control" content="max-age=0" />
<meta http-equiv="cache-control" content="no-cache" />
<meta http-equiv="expires" content="0" />
<meta http-equiv="expires" content="Tue, 01 Jan 1980 1:00:00 GMT" />
<meta http-equiv="pragma" content="no-cache" />
<link rel="shortcut icon" href="${ctxPath }/images/dulink_favicon.ico">
<script type="text/javascript" src="${ctxPath }/js/jqMap.js"></script>
<style type="text/css">
	.selected_span{
	background-color :white;
	color : #008900;
}

.table_title{
	background-color : #222222;
	border-color: black;
}
	
</style>
<title>Dash Board</title>
<script type="text/javascript">
	var ctxPath = "${ctxPath}";
	var targetWidth = 3840;
	var targetHeight = 2160;

	var originWidth = window.innerWidth;
	var originHeight = window.innerHeight;

	var contentWidth = originWidth;
	var contentHeight = targetHeight / (targetWidth / originWidth);

	var screen_ratio = getElSize(240);

	if (originHeight / screen_ratio < 9) {
		contentWidth = targetWidth / (targetHeight / originHeight)
		contentHeight = originHeight;
	};

	function getElSize(n) {
		return contentWidth / (targetWidth / n);
	};

	function setElSize(n) {
		return targetWidth / (contentWidth / n);
	};
	
	var marginWidth = (originWidth-contentWidth)/2;
	var marginHeight = (originHeight-contentHeight)/2;
</script>
<script src="${ctxPath }/js/jquery-1.11.2.min.js"></script>
<script src="${ctxPath }/js/jquery-ui.js"></script>
<script src="${ctxPath}/js/highstock.js"></script>
<link rel="stylesheet" href="${ctxPath }/css/default.css">
<style type="text/css">
	body{
		overflow :hidden;
	}
</style>
<script type="text/javascript">
var shopId = 1;

var save_type = "init"

function getPrdNo(){
	var url = "${ctxPath}/chart/getMatInfo.do";
	var param = "shopId=" + shopId;
	
	$.ajax({
		url : url,
		data : param,
		type : "post",
		dataType : "json",
		success : function(data){
			json = data.dataList;
			
			var option = "";
			
			$(json).each(function(idx, data){
				option += "<option value='" + decode(data.prdNo) + "'>" + decode(data.prdNo) + "</option>";
			});
			
			$("#group").html(option);
			$("#prdNo_form").html(option).change(getDvcList);
			
		}
	});
};

function getDvcList(){
	var url = ctxPath + "/chart/getDevieList.do"
	var param = "shopId=" + shopId + 
				"&prdNo=" + $("#prdNo_form").val(); 	
	
	$.ajax({
		url : url,
		data : param,
		dataType : "json",
		type : "post",
		success : function(data){
			var json = data.dataList;
			
			var options = "<option value='0'>선택</option>";	
			
			if(json.length==0) options += "<option>장비 없음</option>";
			
			$(json).each(function(idx, data){
				options += "<option value='" + data.id + "'>" + decodeURIComponent(data.name).replace(/\+/gi, " ") + "</option>";
			});
			
			$("#dvcId_form").html(options);
		}
	});	
};


function showCorver(){
	$("#corver").css("z-index", 999);	
};

function hideCorver(){
	$("#corver").css("z-index", -999);
};

function bindEvt(){
	$("#insertForm input[type='text']").keyup(calcTotalCnt)
};

function calcTotalCnt(){
	var cnt1 = Number($("#cnt1").val());
	var cnt2 = Number($("#cnt2").val());
	var cnt3 = Number($("#cnt3").val());
	var cnt4 = Number($("#cnt4").val());
	var cnt5 = Number($("#cnt5").val());
	
	var sum = cnt1 + cnt2 + cnt3 + cnt4 +cnt5;
	$("#totalCnt").val(sum);
};

function closeInsertForm(){
	hideCorver();
	$("#insertForm, #updateForm").css("z-index",-9999);
	return false;
};

function addPrdCmpl(){
	var cnt1 = $("#cnt1").val();
	var cnt2 = $("#cnt2").val();
	var cnt3 = $("#cnt3").val();
	var cnt4 = $("#cnt4").val();
	var cnt5 = $("#cnt5").val();
	
	if(cnt1==""){
		cnt1 = "0";
	};
	
	if(cnt2==""){
		cnt2 = "0";
	};
	
	if(cnt3==""){
		cnt3 = "0";
	};
	
	if(cnt4==""){
		cnt4 = "0";
	};
	
	if(cnt5==""){
		cnt5 = "0";
	};
	
	var workTime = Number(($("#workTimeH").val() * 60)) + Number($("#workTimeM").val()); 
	var url = "${ctxPath}/chart/addPrdCmplCnt.do";
	var param = "prdNo=" + $("#prdNo_form").val() + 
				"&dvcId=" + $("#dvcId_form").val() + 
				"&worker=" + $("#worker_form").val() + 
				"&date=" + $("#date_form").val() + 
				"&lot1=" + $("#lot1").val() + 
				"&lot2=" + $("#lot2").val() +
				"&lot3=" + $("#lot3").val() +
				"&lot4=" + $("#lot4").val() +
				"&lot5=" + $("#lot5").val() +
				"&cnt1=" + cnt1 +
				"&cnt2=" + cnt2 +
				"&cnt3=" + cnt3 +
				"&cnt4=" + cnt4 +
				"&cnt5=" + cnt5 + 
				"&totalCnt=" + $("#totalCnt").val() + 
				"&ty=" + $("#ty").val() + 
				"&workTime=" + workTime;
	
	console.log(param)
	if($("#dvcId_form").val()==0){
		alert("장비를 선택하세요.");
		$("#dvcId_form").focus();
		
		return false;
	};
	
	
	$.ajax({
		url : url,
		data : param,
		type : "post",
		dataType : "text",
		success : function(data){
			if(data=="success"){
				alert("저장 되었습니다.")
			}	
		}
	});
	
	return false;
};

function calcAQL(){
	var lotCnt = $("#insert #lotCnt").val();
	var notiCnt = $("#insert #notiCnt").val();
	
	$("#insert #smplCnt").html("5");
	if(notiCnt>0){
		$("#insert #rcvCnt").html("0").css("color","red");	
	}else{
		$("#insert #rcvCnt").html(lotCnt).css("color","white");
	}
};

function calcAQL_update(){
	var lotCnt = $("#update #lotCnt").val();
	var notiCnt = $("#update #notiCnt").val();
	
	$("#update #smplCnt").html("5");
	if(notiCnt>0){
		$("#update #rcvCnt").html("0").css("color","red");	
	}else{
		$("#update #rcvCnt").html(lotCnt).css("color","white");
	}
};


function showInsertForm(){
	var date = new Date();
	var year = date.getFullYear();
	var month = addZero(String(date.getMonth()+1));
	var day= addZero(String(date.getDate()));
	var hour = addZero(String(date.getHours()));
	var minute = addZero(String(date.getMinutes()));
	
	$("input[type='time']").val(hour + ":" + minute).change(function(){
		$("#time_form").html(calcTimeDiff($("#startTime_form").val(), $("#endTime_form").val()))	
	});
	
	$("#date_form").val(year + "-" + month + "-" + day);
	//showCorver();
	$("#insertForm").css("z-index",9999);
	
	return false;
};

function showUpdateForm(id){
	showCorver();
	nonId = id;
	getNonOpData(id);
};


function getNonOpData(id){
	var url = "${ctxPath}/chart/getNonOpData.do";
	var param = "dvcId=" + nonId + 
				"&date=" + $("#sDate").val();

	$.ajax({
		url : url,
		data : param,
		type : "post",
		dataType : "json",
		success : function(data){
			$("#update #prdNo").val(decodeURIComponent(data.prdNo).replace(/\+/gi, " "));
			$("#update #dvcId").val(data.dvcId);
			$("#update #date").val(data.date);
			$("#update #ty").val(data.ty);
			$("#update #worker").val(data.worker);
			$("#update #totalTime").html(data.totalTime);
			$("#update #nonOpTime").html(data.nonOpTime);
			$("#update #workerOpTime").html(data.totalTime - data.nonOpTime)
			$("#update #workerOpRatio").html(Math.round((data.totalTime - data.nonOpTime) / data.totalTime * 100) + "%");
			$("#update #nonOpTy").val(decodeURIComponent(data.nonOpTy).replace(/\+/gi, " "));
			$("#update #startTime").val(data.startTime).change(function(){
				$("#update #time").html(calcTimeDiff($("#update #startTime").val(), $("#update #endTime").val()));
			});
			$("#update #endTime").val(data.endTime).change(function(){
				$("#update #time").html(calcTimeDiff($("#update #startTime").val(), $("#update #endTime").val()));
			});
			
			$("#update #time").html(calcTimeDiff(data.startTime, data.endTime));
			
			/* $("#update #id").html(data.id);
			$("#update #comName").html(data.comName);
			$("#update #prdNo").html(data.prdNo);
			$("#update #spec").html(data.spec);
			$("#update #lotNo").val(data.lotNo);
			$("#update #smplCnt").html(data.smplCnt);
			$("#update #rcvCnt").html(data.rcvCnt);
			$("#update #lotCnt").val(data.lotCnt);
			$("#update #notiCnt").val(data.notiCnt);
			$("#update input[type='time']").val(data.sdate.substr(11,5));
			
			if(data.rcvCnt==0) $("#update #rcvCnt").html("0").css("color","red");
			
 */			$("#updateForm").css("z-index",9999);	
		}
	});
};

function getWorkerList(){
	var url = "${ctxPath}/chart/getWorkerList.do";
	
	$.ajax({
		url : url,
		dataType : "json",
		type : "post",
		success : function(data){
			var json = data.dataList;
			
			var option = "";
			$(json).each(function(idx, data){
				option += "<option value='" + data.id + "'>" + decode(data.name) + "</option>";
			});
			
			$("#worker_form").html(option);
		}
	});
};
	
	
$(function(){
	getWorkerList();
	getNonOpTy();
	getDvcList();
	bindEvt();
	getPrdNo();
	$(".excel").click(csvSend);
	setEl();
	setDate();
	
	$("#menu_btn").click(function(){
		location.href = "${ctxPath}/chart/index.do"
	});
	
	
	showInsertForm();
});
var className = "";
var classFlag = true;

function getNonOpTy(){
	var url = ctxPath + "/chart/getNonOpTy.do"
	
	$.ajax({
		url : url,
		dataType : "json",
		type : "post",
		success : function(data){
			var json = data.dataList;
			
			var options = "<option value='0'>선택</option>";	
			
			$(json).each(function(idx, data){
				options += "<option value='" + data.id + "'>" + decodeURIComponent(data.nonOpTy).replace(/\+/gi, " ") + "</option>";
			});
			
			$("#nonOpTy_form").html(options);
		}
	});		
};


function calcTimeDiff(start, end){
	var startHour = Number(start.substr(0,2));
	var startMinute = Number(start.substr(3,2));
	var endHour = Number(end.substr(0,2));
	var endMinute = Number(end.substr(3,2));
	
	return ((endHour*60) + endMinute) - ((startHour*60) + startMinute);
}

var nonId;
function chkDel(id){
	$("#delDiv").css("z-index",9);
	
	nonId = id;
};

function noDel(){
	$("#delDiv").css("z-index",-1);
};

function okDel(){
	var url = ctxPath + "/chart/okDelNonOp.do";
	var param = "id=" + nonId;
	
	$.ajax({
		url : url,
		data : param,
		type : "post",
		dataType : "text",
		success : function(data){
			noDel();
		}
	});
};

function getBanner(){
	var url = "${ctxPath}/chart/getBanner.do";
	var param = "shopId=" + shopId;
	

	$.ajax({
		url : url,
		data : param,
		type : "post",
		dataType : "json",
		success : function(data){
			if(window.localStorage.getItem("banner")=="true"){
				$("#intro").html(data.msg).css({
					"color" : data.rgb,
					"right" : - window.innerWidth
				})
				
				$("#intro").html(data.msg).css({
					"right" : - $("#intro").width()
				})
			}
			
			bannerWidth = $("#intro").width() + getElSize(100);
			$("#intro").width(bannerWidth);
			$("#banner").val(data.msg);
			$("#color").val(data.rgb);
			
			twinkle();
			bannerAnim();
		}
	});		
}

var twinkle_opct = true;
function twinkle(){
	var opacity;
	if(twinkle_opct){
		opacity = 0;
	}else{
		opacity = 1;
	}
	$("#intro").css("opacity",opacity);
	
	twinkle_opct = !twinkle_opct;
	setTimeout(twinkle, 100)
};

var bannerWidth;
function bannerAnim(){
	$("#intro").width(bannerWidth - getElSize(10));
	$("#intro").animate({
		"right" : window.innerWidth  - getElSize(100)
	},15000, function(){
		$("#intro").css("right" , - $("#intro").width())
		//$(this).remove();
		bannerAnim();
	});
};
function chkBanner(){
	if(window.localStorage.getItem("banner")=="true"){
		getBanner();
		$("#intro_back").css("display","block");
	}else{
		$("#intro").html("");
		$("#intro_back").css("display","none");
	}
};

function cancelBanner(){
	window.localStorage.setItem("banner", "false");
	$("#bannerDiv").css("z-index",-9);
	chkBanner();
};

function addBanner(){
	var url = "${ctxPath}/chart/addBanner.do";
	var param = "msg=" + $("#banner").val() + 
				"&rgb=" + $("#color").val() + 
				"&shopId=" + shopId;
	
	$.ajax({
		url : url,
		data : param,
		type : "post",
		success : function(data){
			window.localStorage.setItem("banner", "true");
			$("#bannerDiv").css("z-index",-9);
			location.reload();
			//chkBanner();
		}
	});
};

var panel = false;
function showPanel(){
	$("#panel").animate({
		"left" : 0
	});
	
	$("#menu_btn").animate({
		"left" : contentWidth * 0.2 + getElSize(50)	
	});
	
	$("#corver").css({
		"z-index" : 9,
		"opacity" : 0.7
	});
};

function closePanel(){
	$("#panel").animate({
		"left" : -contentWidth * 0.2 - getElSize(40)
	});
	
	$("#menu_btn").animate({
		"left" : 0
	});
	
	$("#corver").css({
		"z-index" : -1,
		"opacity" : 0.7
	});
};


function addZero(str){
	if(str.length==1){
		str = "0" + str;
	};
	return str;
};

function setDate(){
	var date = new Date();
	var year = date.getFullYear();
	var month = addZero(String(date.getMonth()+1));
	var day = addZero(String(date.getDate()));
	
	$(".date").val(year + "-" + month + "-" + day);
};


function clearMenu(){
	$("#corver").css({
		"z-index" : -1,
		"opacity" : 0
		
	});
	
};

function setEl() {
	$(".container").css({
		"width": contentWidth,
		"height" : contentHeight, 
		"margin-top" : originHeight/2 - ($(".container").height()/2)
	});
	
	$(".container").css({
		"margin-top" : originHeight/2 - ($(".container").height()/2),
		"margin-left" : originWidth/2 - ($(".container").width()/2),
	});
	
	$("#group, #addBtn").css({
		"font-size" :getElSize(50)	
	});
	
	$(".layoutTd").css({
		height : contentHeight * 0.85
	});

	$(".page").css({
		"height" : originHeight
	});

	$("#part1").css({
		"top" : 0
	});

	// 페이지 위치 조정
	for (var i = 2; i <= 5; i++) {
		$("#part" + i).css({
			"top" : $("#part" + (i - 1)).height() + originHeight
		});
	};

	$(".mainTable").css({
		"border-spacing" : getElSize(40)
	});

	$(".title").css({
		"padding" : getElSize(40),
		"font-size" : getElSize(100)
	});

	$(".subTitle").css({
		"padding" : getElSize(40),
		"font-size" : getElSize(50)
	});

	$(".tr2").css({
		"height" : contentHeight * 0.35
	})

	$("#chartTr").css({
		"height" : contentHeight * 0.4
	});

	$("#pie").css({
		"height" : contentHeight * 0.32
	});

	$(".neon1, .neon2, .neon3").css({
		"font-size" : getElSize(500)
	});

	$(".neon1, .neon2, .neon3").css({
		"margin-top" : $(".tr2").height() / 2 - $(".neon1").height()
						/ 2 - $(".subTitle").height(),
	});

	$("#machine_name").css({
		"color" : "white",
		"font-weight" : "bolder",
		"font-size" : getElSize(170),
	});

	$("#machine_name_td").css({
		"height" : getElSize(500)
	});

	$("#machine_name").css({
		"margin-top" : $("#machine_name_td").height() / 2
						- $("#machine_name").height() / 2
						- $(".subTitle").height()
	});

	$(".title_left").css({
		"float" : "left",
		"width" : contentWidth * 0.1
	});

	$(".title_right").css({
		"float" : "right",
	});

	$("#menu_btn").css({
		"position" : "absolute",
		"width" : getElSize(130),
		"left" : 0,
		"top" : getElSize(20),
		"cursor" : "pointer",
		"z-index" : 13
	});
	

	$("#panel").css({
		"background-color" : "rgb(34,34,34)",
		"border" : getElSize(20) + "px solid rgb(50,50,50)",
		"position" : "absolute",
		"width" : contentWidth * 0.2,
		"top" : 0,
		"left" : -contentWidth * 0.2 - getElSize(40),
		"z-index" : 99999
	});

	$("#corver").css({
		"width" : originWidth,
		"height" : originHeight,
		"position" : "absolute",
		"z-index" : -1,
		"background-color" : "rgba(0,0,0,0.7)",
	});

	$("#panel_table").css({
		"color" : "white",
		"font-size" : getElSize(40)
	});

	$("#panel_table td").css({
		"padding" : getElSize(30),
		"cursor" : "pointer"
	});

	$(".dvcName_p1").css({
		"font-weight" : "bolder",
		"font-size" : getElSize(40)
	});

	$(".wrap").css({
		"width" : contentWidth * 0.2,
		"padding" : getElSize(10),
		"border-radius" : getElSize(30),
		"padding-bottom" : getElSize(10),
		//"border": getElSize(10) + "px outset white",
	});
	
	$(".waitCnt, .alarmCnt, .noConCnt").css({
		"font-size" : getElSize(20),
		"margin" :getElSize(20)
	});
	
	$(".waitCnt").css({
		"color" : "yellow"
	});
	
	$(".alarmCnt").css({
		"color" : "red"
	});
	
	$(".noConCnt").css({
		"color" : "gray"
	});
	
	$(".card_status").css({
		"height" : getElSize(60)
	});
	
	$(".card_table").css({
		"border-spacing" : getElSize(10)
	});
	
	$(".opRatio").css({
		"font-size" :getElSize(100)
	});
	
	$(".comName").css({
		"font-size" :getElSize(60)
	});
	
	$(".machine_cam").css({
		"width" : contentWidth * 0.07,
		"height" : contentHeight * 0.08,
		//"border" : getElSize(10) + "px solid rgb(50,50,50)" 
	});
	
	$(".frame").css({
		"width" : contentWidth * 0.07,
		"height" : contentHeight * 0.15
	});
	
	$(".statusCnt").css({
		"height" : getElSize(70),
		"vertical-align" : "bottom",
		"box-shadow ": "inset 0 5px 0 #ddd"
	});
	
	$(".opTime").css({
		"font-weight" : "bolder",
		"top" : getElSize(400),
		"right" : getElSize(10)
	});
	
	$(".logo").css({
		"height" : contentWidth * 0.01
	});
	
	
	$("#title_left").css({
		"position" : "absolute",
		"width" : contentWidth * 0.07,
		"z-index" : 2,
		"left" : getElSize(100),
		"top" : getElSize(60)
	});

	$("#title_right").css({
		"position" : "absolute",
		"z-index" : 2,
		"right" : getElSize(100),
		"top" : getElSize(40),
		"color" : "white",
		"font-weight" : "bolder",
		"font-size" : getElSize(40)
	});
	
	$("#jigSelector, .goGraph, .excel, .label").css({
		"margin-right": getElSize(10),
		"margin-left": getElSize(10),
		"border-radius" : getElSize(10),
		"padding" : getElSize(10),
		"cursor": "pointer",
		"font-size": getElSize(50),
		"margin-bottom" : 0
	});
	
	$(".label").css({
		"margin-top" : 0
	});
	
	$(".date").css({
		"height": getElSize(70),
		"font-size": getElSize(50)
	});
	
	$(".thead").css({
		"font-size" : getElSize(60)
	});
	
	$(".alarmTable, .alarmTable tr, .alarmTable td").css({
		"border": getElSize(5) + "px solid rgb(50,50,50)"
	});
	
	$(".alarmTable td").css({
		"padding" : getElSize(10),
		"height": getElSize(100),
	});
	
	$(".contentTr").css({
		"font-size" : getElSize(60)
	});
	
	$(".machineListForTarget").css({
		"position" : "absolute",
		"width" : getElSize(1200),
		"height" : getElSize(1200),
		"overflow" : "auto",
		//"top" : getElSize(50),
		//"background-color" : "rgb(34,34,34)",
		"background-color" : "green",
		"color" : "white",
		"font-size" : getElSize(50),
		"padding" : getElSize(50),
		"overflow" : "auto",
		"border-radius" : getElSize(50),
		"border" : getElSize(10) + "px solid white",
	});
	
	$(".machineListForTarget").css({
		"left" : (originWidth/2) - ($(".machineListForTarget").width()/2),
	});
	
	$("#bannerDiv").css({
		"position" : "absolute",
		"width" : getElSize(1500),
		"height" : getElSize(200),
		"border-radius" : getElSize(20),
		"padding" : getElSize(50),
		"background-color" : "lightgray",
		"z-index" : -9,
		'opacity' : 0
	});

	
	$("#bannerDiv").css({
		"left" : (originWidth/2) - ($("#bannerDiv").width()/2),
		"top" : (originHeight/2) - ($("#bannerDiv").height()/2)
	});
	
	$("#bannerDiv input[type='text']").css({
		"width" : getElSize(1200),
		"font-size" : getElSize(50)
	});
	
	$("#bannerDiv button").css({
		"margin" : getElSize(50),
		"font-size" : getElSize(50)
	});
	
	$("#intro").css({
		/* "width" : contentWidth, */
		"position" : "absolute",
		"bottom" : 0 + marginHeight,
		"font-size" : getElSize(140),
		"font-weight" : "bolder",
		"z-index" : 9999
	});
	
	$("#intro_back").css({
		"width" : originWidth,
		"display" : "none",
		"height" : getElSize(180),
		"opacity" : 0.3,
		"position" : "absolute",
		"background-color" : "white",
		"bottom" : 0 + marginHeight,
		"z-index" : 9999
	});
	
	$("#addBtn").css({
		"margin-top" : getElSize(50)	
	}).click(addRow);
	
	$("#insertForm, #updateForm").css({
		"width" : "95%",
		"position" : "absolute",
		"z-index" : -999,
	});
	
	$("#insertForm, #updateForm").css({
		"left" : (originWidth/2) - ($("#insertForm").width()/2),
		"top" : (originHeight/2) - ($("#insertForm").height()/2)
	});
	
	$("#insert table td, #update table td").css({
		"color" : "white",
		"font-size" : getElSize(50),
		"padding" : getElSize(20),
		//"border" : getElSize(2) + "px solid black",
		"text-align" : "Center"
	});
	
	$("#delDiv").css({
		"position" : "absolute",
		"width" : getElSize(700),
		"height" :getElSize(200),
		"background-color" : "#444444",
		"color" : "white"
	});
	
	$("#delDiv").css({
		"top" : (window.innerHeight/2) - ($("#delDiv").height()/2),
		"left" : (window.innerWidth/2) - ($("#delDiv").width()/2),
		"z-index" : -1,
		"border-radius" : getElSize(10),
		"padding" : getElSize(20)
	});
	
	$("#insertForm").css({
		"position" : "absolute",
		"z-index" : -999,
		"width" : getElSize(3300),
	});
	 
	$("#insertForm table td").css({
		"font-size" : getElSize(100),
		"padding" : getElSize(30),
		"background-color" : "#323232"
	});
	
	$("#insertForm button, #insertForm select, #insertForm input").css({
		"font-size" : getElSize(100),
		"height" : getElSize(180),
		"margin" : getElSize(20)
	});
	
	$("#insertForm").css({
		"top" :(window.innerHeight	/2) - ($("#insertForm").height()/2),
		"left" : (window.innerWidth	/2) - ($("#insertForm").width()/2)
	});
	
	$(".table_title").css({
		"background-color" : "#222222",
		"color" : "white"
	});
	
	chkBanner();
};

function addRow(){

};

var csvOutput;
function csvSend(){
	var sDate, eDate;
	
	sDate = $("#sDate").val();
	eDate = $("#eDate").val();
	
	csvOutput = csvOutput.replace(/\n/gi,"");
	
	f.csv.value=encodeURIComponent(csvOutput);
	f.startDate.value = sDate;
	f.endDate.value = eDate;
	f.submit(); 
};

</script>
</head>

<body >
	<div id="insertForm">
		<table style="width: 100%">
			<Tr> 
				<Td class='table_title'  style="width: 20%" > 생산차종 </Td> <Td width="30%" colspan="2"><select id="prdNo_form"></select> </Td> <Td class='table_title'  width="20%"  > 장비 </Td> <Td colspan="2"><select id="dvcId_form"></select><select id='ty'><option value="2">주간</option><option value="1">야간</option> </select> </Td>
			</Tr>
			<Tr> 
				<Td class='table_title' >작업자</Td> <Td colspan="2"><select id="worker_form"></select>  </Td> <Td class='table_title'  > 일자 </Td> <Td colspan="2"><input type="date" id="date_form"> </Td>
			</Tr>
			<Tr> 
				<Td class='table_title'  > LOT </Td> <Td style="text-align: center;" ><input type="text" size="4" id='lot1'> </Td> <Td style="text-align: center;"><input type="text" size="4" id='lot2'> </Td> 
				<td style="text-align: center;"><input type="text" size="4" id='lot3'> </Td> <Td style="text-align: center;"><input type="text" size="4" id='lot4'> </Td> <Td style="text-align: center;"><input type="text" size="4" id='lot5'></Td>
			</Tr>
			<Tr> 
				<Td class='table_title'   > 수량 </Td> <Td style="text-align: center;" ><input type="text" size="4" id='cnt1' style="text-align: right;"> </Td> <Td style="text-align: center;"><input type="text" size="4" id='cnt2' style="text-align: right;"> </Td> 
				<td style="text-align: center;"><input type="text" size="4" id='cnt3' style="text-align: right;"> </Td> <Td style="text-align: center;"><input type="text" size="4" id='cnt4' style="text-align: right;"> </Td> <Td style="text-align: center;"><input type="text" size="4" id='cnt5' style="text-align: right;"></Td>
			</Tr>
			<Tr>
				<Td class='table_title' >합계</Td>
				<Td   style="text-align: center; color: white;" colspan="2"> <input id="totalCnt" value="0" size="4"  style="text-align: right;"> </Td>
				<Td class='table_title' >생산시간</Td> <Td colspan="2" style="color: white;"><input type="text" id="workTimeH" value="10" size="5">시간 <input type="text" id="workTimeM" value="30" size="5">분 </Td>
			</Tr>
			<Tr>
				<Td colspan="6" style="text-align: center;"><button onclick="addPrdCmpl()" >저장</button></Td>
			</Tr>
		</table> 
	</div>
		
	<div id="bannerDiv">
		<Center>
			<input type="text" id="banner" size="100"><input type="color" id="color"><br>
			<button onclick="addBanner();">적용</button>
			<button onclick="cancelBanner();">미적용</button>
		</Center>
	</div>
	<div id="intro_back"></div>
	<span id="intro"></span>
	
	<div id="delDiv">
		<Center>
			<span>삭제하시겠습니까?</span><Br><br>
			<button id="resetOk" onclick="okDel();">삭제</button>&nbsp;&nbsp;
			<button id="resetNo" onclick="noDel();">취소</button>
		</Center>
	</div>
	
	<form action='${ctxPath}/csv/csv.do' id="f"  method="post">
		<input type="hidden" name="csv" value="">
		<input type="hidden" name="startDate" value="">
		<input type="hidden" name="endDate" value="">
	</form>
	<div id="title_right" class="title"> </div>	
	
	<img src="${ctxPath }/images/home.png" id="menu_btn" >
	
					
	<div id="corver"></div>

	<!-- Part1 -->
	<div id="part1" class="page">
	</div>
</body>
</html>