<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@ include file="/WEB-INF/views/include/lib.jsp"%>

<% 
response.setHeader("cache-control", "max-age=0, must-revalidate, no-cache, no-store, private"); // HTTP 1.1
response.setHeader("pragma", "no-cache"); // HTTP 1.0
response.setDateHeader("expires", -1); // Stop proxy caching
%>
<!DOCTYPE html>
<html> 
<head>
<meta charset="UTF-8">
<meta http-equiv="X-UA-Compatible" content="chrome=1">
<meta http-equiv="cache-control" content="max-age=0" />
<meta http-equiv="cache-control" content="no-cache" />
<meta http-equiv="expires" content="0" />
<meta http-equiv="expires" content="Tue, 01 Jan 1980 1:00:00 GMT" />
<meta http-equiv="pragma" content="no-cache" />
<link rel="shortcut icon" href="${ctxPath }/images/dulink_favicon.ico">
<link rel="stylesheet" href="${ctxPath }/css/style.css">
<title>Dash Board</title>
<script type="text/javascript">
	var ctxPath = "${ctxPath}";
	var fromDashboard = "${fromDashBoard}";
	var targetWidth = 3840;
	var targetHeight = 2160;

	var originWidth = window.innerWidth;
	var originHeight = window.innerHeight;
	
	var contentWidth = originWidth;
	var contentHeight = targetHeight/(targetWidth/originWidth);
	
	var screen_ratio = getElSize(240);
	
	if(originHeight/screen_ratio<9){
		contentWidth = targetWidth/(targetHeight/originHeight)
		contentHeight = originHeight; 
	};
	
	function getElSize(n){
		return contentWidth/(targetWidth/n);
	};
	
	function setElSize(n) {
		return Math.floor(targetWidth / (contentWidth / n));
	};
	
	var marginWidth = (originWidth-contentWidth)/2;
	var marginHeight = (originHeight-contentHeight)/2;
</script>
<script type="text/javascript" src="${ctxPath }/js/smil.user.js"></script>
<script type="text/javascript" src="${ctxPath }/js/chart/highcharts.js"></script>
<script type="text/javascript" src="${ctxPath }/js/chart/highcharts-3d.js"></script>
<script type="text/javascript" src="${ctxPath }/js/chart/highcharts-more.js"></script>
<script type="text/javascript" src="${ctxPath }/js/jqMap.js"></script>
<script type="text/javascript" src="${ctxPath }/js/multicolor_series.js"></script>
<script type="text/javascript" src="${ctxPath }/js/chart/moment.js"></script>
<style>
*{
	margin: 0px;
	padding: 0px;
}
body{
	margin: 0px;
	padding: 0px;
	width: 100%;
	height: 100%;
	/* overflow : hidden; */
	background-color: black;
  	font-family:'Helvetica';
}

</style> 
<script type="text/javascript">
	function getBanner(){
		var url = "${ctxPath}/chart/getBanner.do";
		var param = "shopId=" + shopId;
		

		$.ajax({
			url : url,
			data : param,
			type : "post",
			dataType : "json",
			success : function(data){
				if(window.localStorage.getItem("banner")=="true"){
					$("#intro").html(data.msg).css({
						"color" : data.rgb,
						"right" : - window.innerWidth
					})
					
					$("#intro").html(data.msg).css({
						"right" : - $("#intro").width()
					})
				}
				
				bannerWidth = $("#intro").width() + getElSize(100); 
				$("#intro").width(bannerWidth);
				$("#banner").val(data.msg);
				$("#color").val(data.rgb);
				
				twinkle();
				bannerAnim();
			}
		});		
	}
	
	var twinkle_opct = false;
	function twinkle(){
		var opacity;
		if(twinkle_opct){
			opacity = 0;
		}else{
			opacity = 1;
		}
		$("#intro").css("opacity",opacity);
		
		twinkle_opct = !twinkle_opct;
		setTimeout(twinkle, 300)
	};
	
	var bannerWidth;
	function bannerAnim(){
		$("#intro").width(bannerWidth - getElSize(10));
		$("#intro").animate({
			"right" : window.innerWidth  - getElSize(100)
		},8000, function(){
			$("#intro").css("right" , - $("#intro").width())
			//$(this).remove();
			bannerAnim();
		});
	};
	
	function chkBanner(){
		if(window.localStorage.getItem("banner")=="true"){
			getBanner();
			$("#intro_back").css("display","block");
		}else{
			$("#intro").html("");
			$("#intro_back").css("display","none");
		}
	};
	
	function cancelBanner(){
		window.localStorage.setItem("banner", "false");
		$("#bannerDiv").css("z-index",-9);
		chkBanner();
	};
	
	function addBanner(){
		var url = "${ctxPath}/chart/addBanner.do";
		var param = "msg=" + encodeURIComponent($("#banner").val()) + 
					"&rgb=" + $("#color").val() + 
					"&shopId=" + shopId;
		
		$.ajax({
			url : url,
			data : param,
			type : "post",
			success : function(data){
				window.localStorage.setItem("banner", "true");
				location.reload();
				/* $("#bannerDiv").css("z-index",-9);
				chkBanner(); */
			}
		});
	};
	
	var comList = "<select>";

	function getComList(){
		var url = "${ctxPath}/chart/getComList.do";
		
		$.ajax({
			url :url,
			dataType :"json",
			type : "post",
			success : function(data){
				var json = data.dataList;
				
				var option = "";
				$(json).each(function(idx, data){
					option += "<option value='" + data.id + "'>" + decode(data.name) + "</option>";
				});
			
				comList += option + "</select>";
			}
		});
	};
	
	function getIncomStock(){
		classFlag = true;
		var url = "${ctxPath}/chart/getRcvInfo.do";
		var sDate = $("#sDate").val();
		var eDate = $("#eDate").val();
		
		var param = "sDate=" + sDate +
					"&eDate=" + eDate + " 23:59:59" + 
					"&prdNo=" + $("#group").val();
		
		$.ajax({
			url : url,
			data : param,
			dataType : "json",
			type : "post",
			success : function(data){
				var json = data.dataList;
				
				var tr = "<thead>" + 
				"<Tr style='background-color:#222222'>" +
					"<Td>" +
						"${income_no}" +
					"</Td>" +
					"<Td>" +
						"${com_name}" +
					"</Td>" +
					"<Td>" +
						"${prd_no}" +
					"</Td>" +
					"<Td>" +
						"${spec}" +
					"</Td>" +
					"<Td>" +
						"Lot No" +
					"</Td>" +
					"<Td>" +
						"${lot_cnt}" +
					"</Td>" +
					"<Td>" +
						"${check_cnt}" +
					"</Td>" +
					"<Td>" +
						"${faulty_cnt}" +
					"</Td>" +
					"<Td>" +
						"${income_cnt}" +
					"</Td>" +
					"<Td>" +
						"${income_date}" +
					"</Td>" +
					/* "<Td>" +
						"CHK" +
					"</Td>" + */
					/* "<Td>" +
						"수정" +
					"</Td>" + */
				"</Tr></thead><tbody>";
							
				$(json).each(function(idx, data){
					if(data.seq!="."){
						if(classFlag){
							className = "row2"
						}else{
							className = "row1"
						};
						classFlag = !classFlag;
						
						tr += "<tr class='contentTr " + className + "' id='tr" + data.id + "'>" +
									"<td >" + data.id + "</td>" +
									"<td>" + decodeURIComponent(data.comName).replace(/\+/gi, " ") + "</td>" + 
									"<td>" + decodeURIComponent(data.prdNo).replace(/\+/gi, " ") + "</td>" +
									"<td>" + data.spec + "</td>" + 
									"<td>" + data.lotNo + "</td>" + 
									"<td>" + data.lotCnt + "</td>" + 
									"<td>" + data.smplCnt + "</td>" + 
									"<td>" + data.notiCnt + "</td>" + 
									"<td>" + data.rcvCnt + "</td>" +
									"<td>" + data.inputDate + "</td>" +
									/* "<td><input type='checkbox'> </td>" + */ 
									/* "<td><button onclick='showUpdateForm(" + data.id + ")'>수정</button></td>" + */
							"</tr>";					
					}
				});
				
				tr += "</tbody>";
				
				$(".alarmTable").html(tr).css({
					"font-size": getElSize(40),
				});
				
				$(".alarmTable td").css({
					"padding" : getElSize(20),
					"font-size": getElSize(40),
					"border": getElSize(5) + "px solid black"
				});
				
				$("#wrapper").css({
					"height" :getElSize(1650),
					"overflow" : "hidden"
				});
				
				$(".row1").not(".tr_table_fix_header").css({
					"background-color" : "#222222"
				});

				$(".row2").not(".tr_table_fix_header").css({
					"background-color": "#323232"
				});
				
				$("#wrapper div:last").remove();
				
				scrolify($('.alarmTable'), getElSize(1450));
				$("#wrapper div:last").css("overflow", "auto");
				
				$("button, input[type='time'], select").css("font-size", getElSize(40));
			}
		});
	};
	
	function replaceHash(str){
		return str.replace(/#/gi,"-");
	};

	function replaceHyphen(str){
		return str.replace(/#/gi,"-");	
	};

	
	function setDate(){
		var date = new Date();
		var year = date.getFullYear();
		var month = addZero(String(date.getMonth()+1));
		var day = addZero(String(date.getDate()));
		
		$(".date").val(year + "-" + month + "-" + day);
	};
	
	function caldate(day){
		 var caledmonth, caledday, caledYear;
		 var loadDt = new Date();
		 var v = new Date(Date.parse(loadDt) - day*1000*60*60*24);
		 
		 caledYear = v.getFullYear();
		 
		 if( v.getMonth() < 9 ){
		  caledmonth = '0'+(v.getMonth()+1);
		 }else{
		  caledmonth = v.getMonth()+1;
		 }
		 if( v.getDate() < 9 ){
		  caledday = '0'+v.getDate();
		 }else{
		  caledday = v.getDate();
		 }
		 return caledYear + "-" + caledmonth+ '-' + caledday;
		}
		
	
	function getPrdNo(){
		var url = "${ctxPath}/chart/getMatInfo.do";
		var param = "shopId=" + shopId;
		
		$.ajax({
			url : url,
			data : param,
			type : "post",
			dataType : "json",
			success : function(data){
				json = data.dataList;
				
				var option = "<option value='ALL'>${total}</option>";
				
				$(json).each(function(idx, data){
					option += "<option value='" + decode(data.prdNo) + "'>" + decode(data.prdNo) + "</option>"; 
				});
				
				$("#group").html(option);
				
				getIncomStock();
			}
		});
	};
	
	var handle = 0;
	$(function(){
		createNav("inven_nav", 1);
		getPrdNo();
		
		setEl();
		setDate();	
		time();
		
		setEvt();
		$("#home").click(function(){ location.href= "${ctxPath}/chart/index.do" });
		
		window.setInterval(function(){
			var width = window.innerWidth;
			var height = window.innerHeight;

			if(width!=originWidth || height!=originHeight){
				location.reload();
			};
		},1000*10);
		
		
		chkBanner();
	});
	
	function setEvt(){
		$("#addBtn").click(addRow);
		$("#save").click(addStock);
		$("#modify").click(updateStock);
	};
	
	function addStock(){
		var url = "${ctxPath}/chart/addStock.do";
		var param = "prdNo=" + $("#insert #prdNo").html() + 
					"&sDate=" + $("#sDate").val() + " " + $("#insert input[type='time']").val() + 
					"&lotNo=" + $("#insert #lotNo").val() + 
					"&lotCnt=" + $("#insert #lotCnt").val() + 
					"&smplCnt=" + $("#insert #smplCnt").html() +
					"&notiCnt=" + $("#insert #notiCnt").val() +
					"&rcvCnt=" + $("#insert #rcvCnt").html();
		
		$.ajax({
			url : url,
			data : param,
			type : "post",
			dataType : "text",
			success : function(data){
				if(data=="success"){
					$("#insert input").not("input[type=time]").val("");
					closeInsertForm();
					getIncomStock();
				}	
			}
		});
		
		return false;
	};


	var a;
	function calcAQL(el){
		var lotCnt = $(el).parent("td").parent("tr").children("td:nth(5)").children("input").val();
		var notiCnt = $(el).parent("td").parent("tr").children("td:nth(7)").children("input").val();
		
		$(el).parent("td").parent("tr").children("td:nth(6)").html("5");
		if(notiCnt>0){
			$(el).parent("td").parent("tr").children("td:nth(8)").html("0").css("color","red");	
		}else{
			$(el).parent("td").parent("tr").children("td:nth(8)").html(lotCnt).css("color","white");
		}
	};

	var preRCV_QTY = 0;
	var preNOTI_QTY = 0;

	function updateStock(){
		var url = "${ctxPath}/chart/updateStock.do";
		var param = "id=" + matId + 
					"&lotNo=" + $("#update #lotNo").val() + 
					"&lotCnt=" + $("#update #lotCnt").val() +
					"&notiCnt=" + $("#update #notiCnt").val() +
					"&rcvCnt=" + $("#update #rcvCnt").html() +
					"&prdNo=" + $("#update #prdNo").html() + 
					"&updatedNotiCnt=" + ($("#update #notiCnt").val() - preNOTI_QTY) +
					"&updatedRcvCnt=" + ($("#update #rcvCnt").html() - preRCV_QTY) +
					"&sDate=" + $("#sDate").val() + " " + $("#update input[type='time']").val();
		
		$.ajax({
			url : url,
			data : param,
			type : "post",
			dataType : "text",
			success :function(data){
				if(data=="success"){
					$("#insert input").not("input[type=time]").val("");
					closeInsertForm();
					getIncomStock();
				}
			}
		});
		
		return false;
	};
	
	function getTime(){
		var date = new Date();
		var hour = addZero(String(date.getHours()));
		var minute = addZero(String(date.getMinutes()));
		var second = addZero(String(date.getSeconds()));
		
		return hour + ":" + minute;
	};
	
	function calcAQL_update(){
		var lotCnt = $("#update #lotCnt").val();
		var notiCnt = $("#update #notiCnt").val();
		
		$("#update #smplCnt").html("5");
		if(notiCnt>0){
			$("#update #rcvCnt").html("0").css("color","red");	
		}else{
			$("#update #rcvCnt").html(lotCnt).css("color","white");
		}
	};
	
	function addRow(){
		var tr = "<tr class='contentTr2'>" + 
					"<td></td>" +
					"<td>" + comList + "</td>" +
					"<td>" + $("#group option:selected").html() + "</td>" +
					"<td></td>" +
					"<td><input type='text' size='5' ></td>" +
					"<td><input type='text' size='5' value='0' onkeyup='calcAQL(this)'></td>" +
					"<td>0</td>" +
					"<td><input type='text' size='5' value='0' onkeyup='calcAQL(this)'></td>" +
					"<td>0</td>" +
					"<td><button onclick='addFaulty(this)'>${add_faulty}</button></td>" +
					"<td><input type='time' value='" + getTime() + "'></td>" +
					"<td><button onclick='chkDel(\"0\", this)'>${del}</button></td>" +
				"</tr>";
		 
		if($(".contentTr").length==0){
			$("#table2").append(tr);
		}else{
			$(".alarmTable").last().append(tr);
		}
		
		$("button, input[type='time'], select").css("font-size", getElSize(50));
	};

	var valueArray = [];
	function saveRow(ty){
		valueArray=[];
		
		$(".contentTr2").each(function(idx, data){
			var obj = new Object();
			obj.id = $(data).children("td:nth(0)").html();
			obj.prdNo = $("#group").val();
			obj.date = $("#sDate").val() + " " + $(data).children("td:nth(10)").children("input").val();
			obj.lotNo = $(data).children("td:nth(4)").children("input").val();
			obj.lotCnt = $(data).children("td:nth(5)").children("input").val();
			obj.smplCnt = $(data).children("td:nth(6)").html();
			obj.notiCnt = $(data).children("td:nth(7)").children("input").val();
			obj.rcvCnt = $(data).children("td:nth(8)").html();
			obj.vndNo =  $(data).children("td:nth(1)").children("select").val();
			valueArray.push(obj);
		});
		
		var obj = new Object();
		obj.val = valueArray;
		
		var url = "${ctxPath}/chart/addStock.do";
		var param = "val=" + JSON.stringify(obj);
		
		$.ajax({
			url : url,
			data : param,
			type : "post",
			dataType : "text",
			success : function(data){
				if(data=="success") {
					if(ty!="faulty"){
						alert("${save_ok}");
						getIncomStock();					
					};
				}
			}
		});
	};

	var matId;
	var delObj;
	function chkDel(id, el){
		$("#delDiv").css({
			"z-index" : 9,
			"display" : "block"
		});
		
		matId = id;
		delObj = el;
	};

	function noDel(){
		$("#delDiv").css({
			"z-index" : -1,
			"display" : "none"
		});
	};

	function okDel(){
		if(matId==0){
			$(delObj).parent("td").parent("tr").remove();
			noDel();
			return;
		}
		var url = ctxPath + "/chart/okDelStock.do";
		var param = "id=" + matId;
		
		$.ajax({
			url : url,
			data : param,
			type : "post",
			dataType : "text",
			success : function(data){
				noDel();
				getIncomStock();
			}
		});
	};

	
	function addFaulty(el){
		var prdNo = $(el).parent("td").parent("tr").children("td:nth(2)").html();
		var cnt = $(el).parent("td").parent("tr").children("td:nth(7)").html();
		if(cnt.length>10){
			cnt = $(el).parent("td").parent("tr").children("td:nth(7)").children("input").val();
		}
		
		saveRow("faulty");
		var url = "${ctxPath}/chart/addFaulty.do?addFaulty=true&prdNo=" + prdNo + "&cnt="+ cnt;
		
		location.href = url;
	};
	
	function addZero(n){
		if(n.length=="1"){
			n = "0" + n;
		};
		return n;
	};
	
	function setEl(){
		var neonColor = "#0096FF";
		
		var width = window.innerWidth;
		var height = window.innerHeight;
		
		$(".right").css({
			"height" : getElSize(120)
		});
		
		$(".left, .menu_left").css({
			"width" : getElSize(495)			
		})
		
		$("#container").css({
			"width" : contentWidth,
			"height" : contentHeight,
		});
		
		$("#container").css({
			"margin-left" : (originWidth/2) - ($("#container").width()/2),
			"margin-top" : (originHeight/2) - ($("#container").height()/2)
		})
		
		$("#bannerDiv").css({
			"position" : "absolute",
			"width" : getElSize(1500),
			"display" : "none",
			"height" : getElSize(200),
			"border-radius" : getElSize(20),
			"padding" : getElSize(50),
			"background-color" : "lightgray",
			"z-index" : -9
		});

	
		$("#bannerDiv").css({
			"left" : (originWidth/2) - ($("#bannerDiv").width()/2),
			"top" : (originHeight/2) - ($("#bannerDiv").height()/2)
		});
		
		$("#bannerDiv input[type='text']").css({
			"width" : getElSize(1200),
			"font-size" : getElSize(50)
		});
		
		$("#bannerDiv button").css({
			"margin" : getElSize(50),
			"font-size" : getElSize(50)
		});
		
		$("#intro").css({
			"position" : "absolute",
			"bottom" : 0 + marginHeight,
			"font-size" : getElSize(140),
			"font-weight" : "bolder",
			"z-index" : 9999
		});
		
		$("#intro_back").css({
			"width" : originWidth,
			"display" : "none",
			"height" : getElSize(180),
			"opacity" : 0.5,
			"position" : "absolute",
			"background-color" : "black",
			"bottom" : 0 + marginHeight,
			"z-index" : 9999
		})
		
		$("#time").css({
			"color" : "white",
			"position" : "absolute",
			"font-size" : getElSize(40),
			"top" : getElSize(25) + marginHeight,
			"right" : getElSize(30) + marginWidth
		});
		
		$("#table").css({
			"position" : "absolute",
			"width" : $("#container").width(),
			"top" : getElSize(100) + marginHeight
		});
		
		$(".right").css({
			"width" : contentWidth - $(".left").width() 
		});
		
		$(".menu_right").css({
			"width" : $(".right").width()
		})
		
		$("#home").css({
			"cursor" : "pointer"
		})
		
		$("#title_right").css({
			"position" : "absolute",
			"z-index" : 2,
			"color" : "white",
			"font-size" : getElSize(40),
			"top" : getElSize(130) + marginHeight,
			"right" : getElSize(30) + marginWidth
		});
		
		$("span").css({
			"color" : "#8D8D8D",
			"position" : "absolute",
			"font-size" : getElSize(45),
			"margin-top" : getElSize(20),
			"margin-left" : getElSize(20)
		});
		
		$("#selected").css({
			"color" : "white",
		});
		
		$("span").parent("td").css({
			"cursor" : "pointer"
		});
		
		$(".title_span").css({
			"color" : "white",
			"font-size" : getElSize(40),
			"background-color" : "#353535",
			"padding" : getElSize(15)
		});
		
		
		$("select, button, input").css({
			"font-size" : getElSize(40),
			"margin-left" : getElSize(20),
			"margin-right" : getElSize(20)
		});
		
		$("button").css({
			"padding" : getElSize(15),
		})
		
		$("#search").css({
			"cursor" : "pointer",
			"width" : getElSize(80),
		});
		
		$("#content_table td").css({
			"color" : "##BFBFBF",
			"font-size" : getElSize(50)
		});
		
		$(".tmpTable, .tmpTable tr, .tmpTable td").css({
			"border": getElSize(5) + "px solid rgb(50,50,50)"
		});
		
		$(".tmpTable td").css({
			"padding" : getElSize(10),
			"height": getElSize(100)
		});
		
		$(".contentTr").css({
			"font-size" : getElSize(60)
		});
		
		$("#delDiv").css({
			"position" : "absolute",
			"width" : getElSize(700),
			"height" :getElSize(200),
			"background-color" : "#444444",
			"color" : "white"
		});
		
		$("#delDiv").css({
			"top" : (window.innerHeight/2) - ($("#delDiv").height()/2), 
			"left" : (window.innerWidth/2) - ($("#delDiv").width()/2),
			//"z-index" : -1,
			"display" : "none",
			"border-radius" : getElSize(10),
			"padding" : getElSize(20)
		});
		
		$("#intro").css({
			"font-size" : getElSize(100)
		});
	};
	
	function time(){
		$("#time").html(getToday());
		 handle = requestAnimationFrame(time)
	};
	
	function goGraph(){
		location.href = "${ctxPath}/chart/jigGraph.do"
	};
</script>
</head>
<!-- <body oncontextmenu="return false"> -->
<body>
	<div id="delDiv">
		<Center>
			<font><spring:message code="chk_del" ></spring:message></font><Br><br>
			<button id="resetOk" onclick="okDel();"><spring:message code="del" ></spring:message></button>&nbsp;&nbsp;
			<button id="resetNo" onclick="noDel();"><spring:message code="cancel" ></spring:message></button>
		</Center>
	</div>
	
	<div id="bannerDiv">
	<Center>
		<input type="text" id="banner" size="100"><input type="color" id="color"><br>
		<button onclick="addBanner();">적용</button>
		<button onclick="cancelBanner();">미적용</button>
	</Center>
	</div>
	
	<div id="time"></div>
	<div id="title_right"></div>

	<div id="container">
		<table id="table" style="border-collapse: collapse;">
			<Tr>
				<td >
					<img alt="" src="${ctxPath }/images/gray_left.png" class='left' id="home" >
				</td>
				<td >
					<img alt="" src="${ctxPath }/images/gray_right.png" class='right'>
				</td>
			</Tr>
			<tr>
				<td >
					<img alt="" src="${ctxPath }/images/inven_left.png" class='menu_left'  >
				</td>
				<td >
					<img alt="" src="${ctxPath }/images/purple_right.png" class='menu_right'>
				</td>
			</tr>
			<Tr>
				<Td>
					<span class='nav_span'></span>
					<img alt="" src="${ctxPath }/images/selected_purple.png" class='menu_left'>
				</Td>
				<td rowspan="10" id="svg_td" style="vertical-align: top;">
					<table id="content_table" style="width: 100%"> 
						<Tr>
							<td> 
								<spring:message code="prd_no"></spring:message>
								<select id="group"></select>
								<spring:message code="income_date"></spring:message> 
								<input type="date" class="date" id="sDate"> ~ 
								<input type="date" class="date" id="eDate"> 
								<img alt="" src="${ctxPath }/images/search.png" id="search" onclick="getIncomStock()">
							</td>
						</Tr>		
						<tr>
							<td>
								<div id="wrapper">
									<table style="width: 100%; color: white; text-align: center; border-collapse: collapse;" class="alarmTable" id="table2" border="1" >
									</table>
								</div>
							</td>
						</tr>			
					</table> 
				</td>
			</Tr>
			<Tr>
				<Td>
					<span class='nav_span'></span>
					<img alt="" src="${ctxPath }/images/unselected.png" class='menu_left' >
				</Td>
			</Tr>
			<Tr>
				<Td>
					<span class="nav_span"></span>
					<img alt="" src="${ctxPath }/images/unselected.png" class='menu_left'>
				</Td>
			</Tr>
			<Tr>
				<Td>
					<span class='nav_span'></span>
					<img alt="" src="${ctxPath }/images/unselected.png" class='menu_left' >
				</Td>
			</Tr>
			<Tr>
				<Td>
					<span class='nav_span'></span>
					<img alt="" src="${ctxPath }/images/unselected.png" class='menu_left'  >
				</Td>
			</Tr>
			<Tr>
				<Td>
					<span class='nav_span'></span>
					<img alt="" src="${ctxPath }/images/unselected.png" class='menu_left'  >
				</Td>
			</Tr>
			<Tr>
				<Td>
					<span class='nav_span'></span>
					<img alt="" src="${ctxPath }/images/unselected.png" class='menu_left'  >
				</Td>
			</Tr>
			<Tr>
				<Td>
					<span class='nav_span' ></span>
					<img alt="" src="${ctxPath }/images/unselected.png" class='menu_left'  >
				</Td>
			</Tr>
			<Tr>
				<Td>
					<img alt="" src="${ctxPath }/images/unselected.png" class='menu_left'  >
				</Td>
			</Tr>
			<Tr>
				<Td>
					<img alt="" src="${ctxPath }/images/unselected.png" class='menu_left'  >
				</Td>
			</Tr>
		</table>
	 </div>
	
	<div id="intro_back"></div>
	<span id="intro"></span>
</body>
</html>	