<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@ include file="/WEB-INF/views/include/lib.jsp"%>
<spring:message code="device" var="device"></spring:message>
<spring:message code="cycle" var="cycle"></spring:message>
<spring:message code="ophour" var="ophour"></spring:message>
<spring:message code="confirm" var="confirm"></spring:message>

<spring:message code="opstatus" var="opstatus"></spring:message>
<spring:message code="opratio" var="opratio"></spring:message>

<% 
response.setHeader("cache-control", "max-age=0, must-revalidate, no-cache, no-store, private"); // HTTP 1.1
response.setHeader("pragma", "no-cache"); // HTTP 1.0
response.setDateHeader("expires", -1); // Stop proxy caching
%>
<!DOCTYPE html>
<html> 
<head>
<meta charset="UTF-8">
<meta http-equiv="X-UA-Compatible" content="chrome=1">
<meta http-equiv="cache-control" content="max-age=0" />
<meta http-equiv="cache-control" content="no-cache" />
<meta http-equiv="expires" content="0" />
<meta http-equiv="expires" content="Tue, 01 Jan 1980 1:00:00 GMT" />
<meta http-equiv="pragma" content="no-cache" />
<link rel="shortcut icon" href="${ctxPath }/images/dulink_favicon.ico">
<link rel="stylesheet" href="${ctxPath }/css/style.css">
<title>Dash Board</title>
<script type="text/javascript">
	var ctxPath = "${ctxPath}";
	var targetWidth = 3840;
	var targetHeight = 2160;

	var originWidth = window.innerWidth;
	var originHeight = window.innerHeight;
	
	var contentWidth = originWidth;
	var contentHeight = targetHeight/(targetWidth/originWidth);
	
	var screen_ratio = getElSize(240);
	
	if(originHeight/screen_ratio<9){
		contentWidth = targetWidth/(targetHeight/originHeight)
		contentHeight = originHeight; 
	};
	
	function getElSize(n){
		return contentWidth/(targetWidth/n);
	};
	
	var marginWidth = (originWidth-contentWidth)/2;
	var marginHeight = (originHeight-contentHeight)/2;
	
	var opratio_title="${opratio}"
	var opstatus_title="${opstatus}"
</script>
<script type="text/javascript" src="${ctxPath }/js/smil.user.js"></script>
<script type="text/javascript" src="${ctxPath }/js/chart/highcharts.js"></script>
<script type="text/javascript" src="${ctxPath }/js/chart/highcharts-3d.js"></script>
<script type="text/javascript" src="${ctxPath }/js/chart/highcharts-more.js"></script>
<script type="text/javascript" src="${ctxPath }/js/jqMap.js"></script>
<script type="text/javascript" src="${ctxPath }/js/chart/highchart.js"></script>
<script type="text/javascript" src="${ctxPath }/js/chart/svg_controller_test.js"></script>

<script type="text/javascript" src="${ctxPath }/js/chart/SVG_.js"></script>
<script type="text/javascript" src="${ctxPath }/js/chart/moment.js"></script>
<script type="text/javascript" src="${ctxPath }/js/chart/SVG_draggable.js"></script>
<style>
.selected_span{
	background-color :white;
	color : #008900;
}

.blur1{
	-webkit-filter: url("#blur");
	filter: url("#blur"); 
}

.blur2{
	-webkit-filter: url("#blur2");
	filter: url("#blur2"); 
}		

#chart{
	overflow: hidden;
}

#container{
	background: url("../images/DashBoard/Background.png");
	background-size : 100% 100%;
	background-color: white;
}

body{
	margin: 0px;
	padding: 0px;
	width: 100%;
	height: 100%;
	overflow: hidden;
	background-color: black;
  	font-family:'Helvetica';
}

.title{
	top : 50px;
	position: absolute;
	z-index: 99;
}


#title_left{
	left : 50px; 
	width: 300px;
}

#pieChart1{
	position: absolute;
	z-index: 99;
}

#pieChart2{
	position: absolute;
	z-index: 99;
}

#tableDiv{
	left: 20px;
	width: 600px; 
	position: absolute; 
	z-index: 999;
	top: 450px;
}

#table{
	border: 1px solid white;
	border-collapse: collapse;
	z-index: 999;
	background-color: black;
}

.tr{
	font-size: 30px;
}

.td{
	padding: 10px;
	border: 1px solid white; 
}


#time{
	position: absolute;
	top: 170px;
	font-size : 30px;
	color: white;
} 
#logo{
	position: absolute;
	top: 170px;
	right: 60px;	
}
#date{
	position: absolute;
	top: 170px;
	right: 250px;	
	font-size : 30px;
	color: white;
} 
.unSelected_menu{
	background-color :gray;
	color : white
}

.selected_menu{
	background-color :rgb(33,128,250);
	color : white
}

.menu:HOVER{
	background-color: white;
	color: gray;
}
</style> 
<script type="text/javascript">
	var loopFlag = null;
	var session = window.localStorage.getItem("auto_flag");
	if(session==null) window.localStorage.setItem("auto_flag", false);
	
	var flag = false;
	function stopLoop(){
		var flag = window.localStorage.getItem("auto_flag");
		
		if(flag=="true"){
			flag = "false"
		}else{
			flag = "true"
		}
		
		window.localStorage.setItem("auto_flag", flag);
		
		if(flag=="false"){
			clearInterval(loopFlag);	
		}else{
			startPageLoop();	
		};
		
		alert("페이지 자동 이동 : " + flag);
	};
	
	
	var menu = false;
	function getMousePos(){
		$(document).on("mousemove", function(e){
			var target = $("#menu_btn").width();

			if((e.pageX <= target && e.pageY <= target) || panel){
				if(!menu){
					$("#menu_btn").animate({
						"left" : 0,
						"top" : getElSize(20)
					});
					menu = true;
				};
			}else{
				if(menu){
					$("#menu_btn").animate({
						"left" : -getElSize(100),
					});	
					menu = false;
				};
			};
		});
	};
	
	var panel = false;
	function showPanel(){
		$("#panel").animate({
			"left" : 0
		});
		
		$("#menu_btn").animate({
			"left" : contentWidth * 0.2 + getElSize(50)	
		});
		
		$("#corver").css({
			"z-index" : 99999
		});
	};

	function closePanel(){
		$("#panel").animate({
			"left" : -contentWidth * 0.2 - getElSize(40)
		});
		
		$("#menu_btn").animate({
			"left" : 0
		});
		
		$("#corver").css({
			"z-index" : -1
		});
	};
	
	function goReport(){
		var type = this.id;
		var url;
		if(type=="menu0"){
			closePanel();
			panel = false;
		}else if(type=="menu1"){
			url = "${ctxPath}/chart/performanceReport.do";
			location.href = url;
		}else if(type=="menu2"){
			url = "${ctxPath}/chart/alarmReport.do";
			location.href = url;
		}else if(type=="menu3"){
			url = "${ctxPath}/chart/main3.do";
			location.href = url;
		}else if(type=="menu4"){
			url = ctxPath + "/chart/rotateChart.do";
			location.href = url;
		}else if(type=="menu5"){
			url = "${ctxPath}/chart/traceManager.do";
			location.href = url;
		}else if(type=="menu6"){
			url = "${ctxPath}/chart/toolManager.do";
			location.href = url;
		}else if(type=="menu8"){
			url = "${ctxPath}/chart/jigGraph.do";
			location.href = url;
		}else if(type=="menu9"){
			url = "${ctxPath}/chart/wcGraph.do";
			location.href = url;
		}else if(type=="menu10"){
			/* getTargetData("day");
			closePanel();
			panel = false; */
			url = "${ctxPath}/chart/addTarget.do";
			location.href = url;
		}else if(type=="menu7"){
			url = "${ctxPath}/chart/singleChartStatus.do";
			location.href = url;
		}else if(type=="menu11"){
			url = "${ctxPath}/chart/prdStatus.do";
			location.href = url;
		}else if(type=="menu99"){
			$("#bannerDiv").css({
				"z-index" : 9999
			});
			getBanner();
			closePanel();
			panel = false;
		}else if(type=="menu12"){
			url = "${ctxPath}/chart/performanceAgainstGoal.do";
			location.href = url;
		}else if(type=="menu100"){
			url = "${ctxPath}/chart/stockStatus.do";
			location.href = url;
		}else if(type=="menu101"){
			url = "${ctxPath}/chart/programManager.do";
			location.href = url;
		}else if(type=="menu102"){
			url = "${ctxPath}/chart/leadTime.do";
			location.href = url;
		}else if(type=="menu103"){
			url = "${ctxPath}/chart/faulty.do";
			location.href = url;
		}else if(type=="menu104"){
			url = "${ctxPath}/chart/addFaulty.do";
			location.href = url;
		}
	};
	
	var openCal = false;
	
	var table_flag = true;
	var table_8 = false;
	
	function dateUp(){
		var $date = $("#sDate").val();
		var year = $date.substr(0,4);
		var month = $date.substr(5,2);
		var day = $date.substr(8,2);
		
		var current_day = month + "/" + day + "/" + year;
		
		var date = new Date(current_day);
		date.setDate(date.getDate() + 1);
		
		var year = date.getFullYear();
		var month = addZero(String(date.getMonth()+1));
		var day = addZero(String(date.getDate()));
		
		var today = year + "-" + month + "-" + day;
		$("#sDate").val(today);
		changeDateVal();
	};
	
	function changeDateVal(){
		var date = new Date();
		var year = date.getFullYear();
		var month = addZero(String(date.getMonth()+1));
		var day = addZero(String(date.getDate()));
		var today = year + "-" + month + "-" + day;
		
		if(today==$("#sDate").val()){
			isToday = true;
		}else{
			isToday = false;
		};
		
		table_8 = false;
	}
	
	function dateDown(){
		var $date = $("#sDate").val();
		var year = $date.substr(0,4);
		var month = $date.substr(5,2);
		var day = $date.substr(8,2);
		
		var current_day = month + "/" + day + "/" + year;
		
		var date = new Date(current_day);
		date.setDate(date.getDate() - 1);
		
		var year = date.getFullYear();
		var month = addZero(String(date.getMonth()+1));
		var day = addZero(String(date.getDate()));
		
		var today = year + "-" + month + "-" + day;
		$("#sDate").val(today);
		changeDateVal();
	};
	
	function getBanner(){
		var url = "${ctxPath}/chart/getBanner.do";
		var param = "shopId=" + shopId;
		

		$.ajax({
			url : url,
			data : param,
			type : "post",
			dataType : "json",
			success : function(data){
				if(window.localStorage.getItem("banner")=="true"){
					$("#intro").html(data.msg).css({
						"color" : data.rgb,
						"right" : - window.innerWidth
					})
					
					$("#intro").html(data.msg).css({
						"right" : - $("#intro").width()
					})
				}
				
				bannerWidth = $("#intro").width() + getElSize(100); 
				$("#intro").width(bannerWidth);
				$("#banner").val(data.msg);
				$("#color").val(data.rgb);
				
				twinkle();
				bannerAnim();
			}
		});		
	}
	
	var twinkle_opct = false;
	function twinkle(){
		var opacity;
		if(twinkle_opct){
			opacity = 0;
		}else{
			opacity = 1;
		}
		$("#intro").css("opacity",opacity);
		
		twinkle_opct = !twinkle_opct;
		setTimeout(twinkle, 300)
	};
	
	var bannerWidth;
	function bannerAnim(){
		$("#intro").width(bannerWidth - getElSize(10));
		$("#intro").animate({
			"right" : window.innerWidth  - getElSize(100)
		},8000, function(){
			$("#intro").css("right" , - $("#intro").width())
			//$(this).remove();
			bannerAnim();
		});
	};
	
	function chkBanner(){
		if(window.localStorage.getItem("banner")=="true"){
			getBanner();
			$("#intro_back").css("display","block");
		}else{
			$("#intro").html("");
			$("#intro_back").css("display","none");
		}
	};
	
	function cancelBanner(){
		window.localStorage.setItem("banner", "false");
		$("#bannerDiv").css("z-index",-9);
		chkBanner();
	};
	
	function addBanner(){
		var url = "${ctxPath}/chart/addBanner.do";
		var param = "msg=" + encodeURIComponent($("#banner").val()) + 
					"&rgb=" + $("#color").val() + 
					"&shopId=" + shopId;
		
		console.log(param)
		$.ajax({
			url : url,
			data : param,
			type : "post",
			success : function(data){
				window.localStorage.setItem("banner", "true");
				location.reload();
				/* $("#bannerDiv").css("z-index",-9);
				chkBanner(); */
			}
		});
	};
	
	var $rad,deg = 0;
	
	var noconn = [];
	
	function rotate(){
		if(deg>=359){
			deg = 0;
			if($(".radar_label").length!=0){
				$(".radar_label").each(function(idx, data){
					$(data).animate({
						"opacity" : 0
					}, function(){
						$(this).remove()
					})
				});
			}
			
			for(var i = 0; i < noconn.length; i++){
				var div = document.createElement("div");
				var textNode = document.createTextNode(noconn[i]);
				div.setAttribute("id", "label" + i);
				div.setAttribute("class", "radar_label");
				div.appendChild(textNode);
				
				div.style.cssText = "position : absolute;" + 
									"width : " + getElSize(295) + "px;" +
									"padding : " + getElSize(10) + "px;" + 
									"font-weight : bolder;" + 
									"font-size : " + getElSize(35) + ";" + 
									"background-color : #898989;" + 
									"top:" + ($("#rad").offset().top + (getElSize(70) * i)) + ";" + 
									"left:" + $("#rad").offset().left + ";" +
									"opacity : 0;";
				
				$("body").prepend(div);	
			}
			
			animShowLabel(0);
		};
		
	  	$rad.css({
	 	   	transform: 'rotate('+ deg +'deg)'
	  	});
		  
	  	setTimeout(function() {
	    	//deg = ++deg%360;
	    	deg += 2;
		   rotate();
	  	}, 30);
	};
	
	function animShowLabel(n){
		$("#label" + n).animate({
			"opacity" : 1,
			"left" : ($("#innerRadar").offset().left + $("#innerRadar").width() + getElSize(250))
		}, function(){
			$(this).animate({
				"left" : $(this).offset().left - getElSize(50)
			},150)
		});
		
		if(n<noconn.length){
			setTimeout(function(){
				animShowLabel(n+1)
			},50);			
		}
	};
	
	function drawGroupLine(){
		ctx.lineWidth = getElSize(5);
		ctx.strokeStyle = "#ffffff";
		
		// YF/FRT CNC
		//ctx.rect(getElSize(120), getElSize(200), getElSize(390), getElSize(680));
		ctx.rect(getElSize(310), getElSize(200), getElSize(390), getElSize(680));
		var text = document.createTextNode("YP FRT (CNC)")
		var div = document.createElement("div");
		div.appendChild(text);
		
		div.appendChild(document.createElement("br"));
		div.appendChild(document.createTextNode("내수, 북미"));
		
		div.style.cssText = "position :absolute;" + 
							"color : white;" + 
							"font-size : " + getElSize(40) + ";" + 
							'text-align :center;' +
							"z-index : 999"; 
		
		$("#container").prepend(div);
		
		$(div).css({
			"top" : getElSize(100) + marginHeight,
			//"left" : marginWidth + getElSize(180)
			"left" : marginWidth + getElSize(380)
		});
		
		// YF/FRT CNC
		ctx.rect(getElSize(800), getElSize(200), getElSize(390), getElSize(680));
		var text = document.createTextNode("UM FRT (CNC)");
		var div = document.createElement("div");
		div.appendChild(text);
		
		div.style.cssText = "position :absolute;" + 
							"color : white;" + 
							"font-size : " + getElSize(40) + ";" + 
							"z-index : 999"; 
		
		$("#container").prepend(div);
		
		$(div).css({
			"top" : getElSize(150) + marginHeight,
			"left" : getElSize(860) + marginWidth
		});
		
		//TA/RR
		ctx.rect(getElSize(1325), getElSize(200), getElSize(390), getElSize(830));
		var text = document.createTextNode("TA RR");
		var div = document.createElement("div");
		div.appendChild(text);
		
		div.style.cssText = "position :absolute;" + 
							"color : white;" + 
							"font-size : " + getElSize(40) + ";" + 
							"z-index : 999"; 
		
		$("#container").prepend(div);
		
		$(div).css({
			"top" : getElSize(150) + marginHeight,
			"left" : getElSize(1455) + marginWidth
		});
		
		//TA/FRT
		ctx.rect(getElSize(1900), getElSize(200), getElSize(390), getElSize(830));
		var text = document.createTextNode("TA FRT");
		var div = document.createElement("div");
		div.appendChild(text);
		
		div.style.cssText = "position :absolute;" + 
							"color : white;" + 
							"font-size : " + getElSize(40) + ";" + 
							"z-index : 999";
		
		$("#container").prepend(div);
		
		$(div).css({
			"top" : getElSize(150) + marginHeight,
			"left" : getElSize(2030) + marginWidth
		});
		
		//LFA/RR
		ctx.rect(getElSize(2480), getElSize(200), getElSize(390), getElSize(830));
		var text = document.createTextNode("JC RR");
		var div = document.createElement("div");
		div.appendChild(text);
		
		div.style.cssText = "position :absolute;" + 
							"color : white;" + 
							"font-size : " + getElSize(40) + ";" + 
							"z-index : 999"; 
		
		$("#container").prepend(div);
		
		$(div).css({
			"top" : getElSize(150) + marginHeight,
			"left" : getElSize(2605) + marginWidth
		});
		
		ctx.rect(getElSize(20), getElSize(200), getElSize(190), getElSize(680));
		var text = document.createTextNode("LFA RR");
		var div = document.createElement("div");
		div.appendChild(text);
		
		div.style.cssText = "position :absolute;" + 
							"color : white;" + 
							"font-size : " + getElSize(40) + ";" + 
							"z-index : 999"; 
		
		$("#container").prepend(div);
		
		$(div).css({
			//"top" : getElSize(650) + marginHeight,
			//"left" : getElSize(2505) + marginWidth
			"left" : getElSize(40) + marginWidth,
			"top" : getElSize(150) + marginHeight,
		});
		
		//HR/FRT.PU
		ctx.rect(getElSize(3060), getElSize(200), getElSize(390), getElSize(830));
		var text = document.createTextNode("HR PU / FRT");
		var div = document.createElement("div");
		div.appendChild(text);
		
		div.style.cssText = "position :absolute;" + 
							"color : white;" + 
							"font-size : " + getElSize(40) + ";" + 
							"z-index : 999";  
		
		$("#container").prepend(div);
		
		$(div).css({
			"top" : getElSize(150) + marginHeight,
			"left" : getElSize(3150) + marginWidth
		});
		
		//YD RR
		ctx.rect(getElSize(1325), getElSize(1070), getElSize(390), getElSize(1080));
		/* var text = document.createTextNode("YD RR");
		var div = document.createElement("div");
		div.appendChild(text);
		
		div.style.cssText = "position :absolute;" + 
							"color : white;" + 
							"font-size : " + getElSize(40) + ";" + 
							"z-index : 999"; 
		
		$("#container").prepend(div);
		
		$(div).css({
			"top" : getElSize(1080) + marginHeight,
			"left" : getElSize(1490) + marginWidth
		}); */
		
		var text = document.createTextNode("FS RR");
		var div = document.createElement("div");
		div.appendChild(text);
		
		div.style.cssText = "position :absolute;" + 
							"color : white;" + 
							"font-size : " + getElSize(40) + ";" + 
							"z-index : 999"; 
		
		$("#container").prepend(div);

		$(div).css({
			"top" : getElSize(1100) + marginHeight,
			"left" : getElSize(1535) + marginWidth
		});
		
		//YP RR
		ctx.rect(getElSize(1900), getElSize(1070), getElSize(390), getElSize(1080));
		
		var text = document.createTextNode("YP RR 북미");
		var div = document.createElement("div");
		div.appendChild(text);
		
		div.style.cssText = "position :absolute;" + 
							"color : white;" + 
							"font-size : " + getElSize(40) + ";" + 
							"z-index : 999"; 
		
		$("#container").prepend(div);
		
		$(div).css({
			"top" : getElSize(1500) + marginHeight,
			"left" : getElSize(1910) + marginWidth 
		});
		
		var text = document.createTextNode("UM RR 내수");
		var div = document.createElement("div");
		div.appendChild(text);
		
		div.style.cssText = "position :absolute;" + 
							"color : white;" + 
							"font-size : " + getElSize(40) + ";" + 
							"z-index : 999"; 
		
		$("#container").prepend(div);
		
		$(div).css({
			"top" : getElSize(1220) + marginHeight,
			"left" : getElSize(2055) + marginWidth
		});
		
		//LFA RR, MCT
		ctx.rect(getElSize(2480), getElSize(1070), getElSize(390), getElSize(1080));
		var text = document.createTextNode("UM "); //RR 유럽
		var div = document.createElement("div");
		div.appendChild(text);
		
		//div.appendChild(document.createElement("br"));
		div.appendChild(document.createTextNode("RR"));
		div.appendChild(document.createElement("br"));
		div.appendChild(document.createTextNode("유럽"));
		
		div.style.cssText = "position :absolute;" + 
							"color : white;" + 
							"font-size : " + getElSize(40) + ";" + 
							"text-align : center;" +
							"z-index : 999"; 
		
		$("#container").prepend(div);
		
		$(div).css({
			/* "top" : getElSize(1750),
			"left" : getElSize(2395) + marginWidth */
			"top" : getElSize(1350),
			"left" : getElSize(2490) + marginWidth
		});
		
		var text = document.createTextNode("LFA RR")
		var div = document.createElement("div");
		div.appendChild(text);
		
		div.appendChild(document.createElement("br"));
		div.appendChild(document.createTextNode("(MCT)"));
		
		
		div.style.cssText = "position :absolute;" + 
							"color : white;" + 
							"font-size : " + getElSize(40) + ";" + 
							"z-index : 999"; 
		
		$("#container").prepend(div);
		
		$(div).css({
			//"top" : getElSize(1100),
			//"left" : getElSize(2545) + marginWidth
			"text-align" : "center",
			"left" : getElSize(2710) + marginWidth,
			"top" : getElSize(1260),
		});
		
		//YP RR
		ctx.rect(getElSize(3060), getElSize(1070), getElSize(390), getElSize(1080));
		var text = document.createTextNode("YP RR");
		var div = document.createElement("div");
		div.appendChild(text);
		
		div.appendChild(document.createElement("br"));
		div.appendChild(document.createTextNode("내수"));
		
		div.style.cssText = "position :absolute;" + 
							"color : white;" + 
							"font-size : " + getElSize(40) + ";" + 
							"text-align : center;" +
							"z-index : 999"; 
		
		$("#container").prepend(div);
		
		$(div).css({
			"top" : getElSize(1075),
			"left" : getElSize(3075) + marginWidth
		});
		
		var text = document.createTextNode("YP FRT");
		var div = document.createElement("div");
		div.appendChild(text);
		
		div.appendChild(document.createElement("br"));
		div.appendChild(document.createTextNode("(MCT)"));
		
		div.style.cssText = "position :absolute;" + 
							"color : white;" + 
							"text-align :center;"+
							"font-size : " + getElSize(40) + ";" + 
							"z-index : 999"; 
		
		$("#container").prepend(div);
		
		$(div).css({
			"top" : getElSize(1075),
			"left" : getElSize(3260) + marginWidth
		});
		//TQ FRT
		ctx.rect(getElSize(3580), getElSize(200), getElSize(220), getElSize(1960));
		var text = document.createTextNode("TQ FRT");
		var div = document.createElement("div");
		div.appendChild(text);
		
		div.style.cssText = "position :absolute;" + 
							"color : white;" + 
							"font-size : " + getElSize(40) + ";" +
							"text-align : center;" + 
							"z-index : 999"; 
		
		$("#container").prepend(div);
		
		$(div).css({
			"top" : getElSize(150),
			"left" : getElSize(3620) + marginWidth
		});
		
		var text = document.createTextNode("UM FRT");
		var div = document.createElement("div");
		div.appendChild(text);
		
		div.appendChild(document.createElement("br"));
		div.appendChild(document.createTextNode("(MCT)"));
		
		div.style.cssText = "position :absolute;" + 
							"color : white;" + 
							"font-size : " + getElSize(40) + ";" + 
							"text-align : center;" + 
							"z-index : 999"; 
		
		$("#container").prepend(div);
		
		$(div).css({
			"top" : getElSize(1250),
			"left" : getElSize(3620) + marginWidth
		});
		
		ctx.stroke();
	};
	
	var canvas;
	var ctx;
	
	$(function(){
		canvas = document.getElementById("canvas");
		ctx = canvas.getContext("2d");
		canvas.width = contentWidth;
		canvas.height = contentHeight;
		$("#canvas").css({
			"z-index" : 7,
			"position" : "absolute",
			"top" : marginHeight,
			"left" : marginWidth
		});
		
		$rad = $('#rad');
		$rad = $('#rad_canvas');
		rotate();
		
		$("#color").change(function(){
			$("#intro").css("color", this.value)
		});
		
		$("#up").click(dateUp);
		$("#down").click(dateDown);
		
		var date = new Date();
		var year = date.getFullYear();
		var month = addZero(String(date.getMonth()+1));
		var day = addZero(String(date.getDate()));
		var hour = date.getHours();
		var minute = date.getMinutes();
		
		$("#excel").click(csvSend);
		
		$(".menu").click(goReport);
		//getMousePos();
		$("#menu_btn").click(function(){
			location.href = "${ctxPath}/chart/index.do"
		});
		document.oncontextmenu = function() {stopLoop()}; 
		setDivPos();
		setInterval(time, 1000);
		
		$("#table_toggle_btn").click(function(){
			$("#tableSubTitle").html("기준 시간 : 18시 ~ 07시");
			table_8 = false;
			if(table_flag){
				$("#tableDiv").hide();				
			}else{
				$("#tableDiv").show();
			}
			table_flag=!table_flag;
		});
		
		if(hour>=7 && (hour<=8 && minute<=30)){
			$("#tableDiv").show();
			table_flag = true;
		}else{
			$("#tableDiv").hide();
			table_flag = false;
		};
		
		$("#title_main").click(function(){
			//location.href = "${ctxPath}/chart/multiVision.do";
			
			getTargetData("day");
		});
		
		$("#title_right").click(function(){
			//location.href = "${ctxPath}/chart/main3.do";
		});
		
		$("#title_left").click(function(){
			//location.href = "${ctxPath}/chart/main3.do";
		});
		
		window.setInterval(function(){
			var width = window.innerWidth;
			var height = window.innerHeight;

			if(width!=originWidth || height!=originHeight){
				location.reload();
			};
		},1000*10);
		
		
		
		$(".menu").click(goReport);
		$("#menu0").addClass("selected_menu");
		$("#menu0").removeClass("unSelected_menu");
		
		if(session=="false"){
			clearInterval(loopFlag);	
		}else{
			startPageLoop();	
		};
		
		chkBanner();
		drawGroupLine();
	});
	
	var targetMap = new JqMap();
	var targetMap2 = new JqMap();
	var targetMap3 = new JqMap();
	var targetMap_night = new JqMap();
	var targetMap2_night = new JqMap();
	var targetMap3_night = new JqMap();
	function getTargetData(ty){
		var url = ctxPath + "/chart/getTargetData.do";
		var type;
		if(ty=="day"){
			type = 2;
		}else{
			type = 1;
		};
		
		var param = "shopId=" + shopId + 
					"&type=" + type;
		
		console.log(param)
		$.ajax({
			url : url,
			data : param,
			dataType : "json",
			type : "post",
			success : function(data){
				var  json = data.dataList;
				
				var tr ="<tr>" + 
							"<td colspan='4' align='center'>" + 
								"<span class='daynight_span day' id='day'>주간</span>&nbsp;&nbsp;&nbsp;" + 
								"<span class='daynight_span night' id='night'>야간</span>" +
							"</td>" +   
							
						"</tr>" + 
						"<tr>" + 
							"<td>${device}</td><td style='text-align: center;'>${cycle}</td><td style='text-align: center;'>${ophour} (h)</td><td style='text-align: center;'>사이클당 생산량</td>" + 
						"</tr>";
				var class_name = "";
				$(json).each(function(idx, data){
						class_name = " ";
						if(ty=="day"){
							targetMap.put("t" + data.dvcId, data.tgCnt);
							targetMap2.put("c" + data.dvcId, data.tgRunTime);
							targetMap3.put("p" + data.dvcId, data.cntPerCyl);
						}else{
							targetMap_night.put("t" + data.dvcId, data.tgCnt);
							targetMap2_night.put("c" + data.dvcId, data.tgRunTime);
							targetMap3_night.put("p" + data.dvcId, data.cntPerCyl)
						}
						
					var name = decodeURIComponent(data.name).replace(/\+/gi, " ");
					var trs;
					if(ty=="day"){
						trs = "<td style='text-align: center;'> <input type='text' id=d_t" + data.dvcId +" class='targetCnt t" + class_name + "' value=" + data.tgCnt + "><span id=s" + data.dvcId + " class='span'></td>" + 
						"<td style='text-align: center;'> <input type='text' id=d_c" + data.dvcId +" class='targetCnt t" + class_name + "' value=" + data.tgRunTime/3600 + "><span id=s2" + data.dvcId + " class='span'></td>" + 
						"<td style='text-align: center;'> <input type='text' id=d_p" + data.dvcId +" class='targetCnt t" + class_name + "' value=" + data.cntPerCyl + "><span id=s2" + data.dvcId + " class='span'></td>";
					}else{
						trs = "<td style='text-align: center;'> <input type='text' id=n_t" + data.dvcId +" class='targetCnt t" + class_name + "' value=" + data.tgCnt + "><span id=s" + data.dvcId + " class='span'></td>" + 
						"<td style='text-align: center;'> <input type='text' id=n_c" + data.dvcId +" class='targetCnt t" + class_name + "' value=" + data.tgRunTime/3600 + "><span id=s2" + data.dvcId + " class='span'></td>" + 
						"<td style='text-align: center;'> <input type='text' id=d_p" + data.dvcId +" class='targetCnt t" + class_name + "' value=" + data.cntPerCyl + "><span id=s2" + data.dvcId + " class='span'></td>";
					};
					tr += "<tr>" + 
								"<td>" + name + "</td>" +
								trs + 
						  "</tr>";
					
				});
				
				
				tr += "<tr>" + 
							"<td colspan='5' style='text-align:center'><span style='cursor : pointer' id='save_btn' class='save_btn'>${confirm}</span></td>" + 
						"</tr>";
				
				$(".machineListForTarget").css({
					"opacity" :0,
					"z-index" : -999
				});
						
				$("#machineListForTarget_" + ty + " #machineListTable").html(tr);
				
				$(".daynight_span").css({
					"padding" : getElSize(10),
					"border-radius" : getElSize(10),
					"cursor" : "pointer"
				});
				
				$(".daynight_span").removeClass("selected_span")
				if(ty=="day"){
					$(".day").addClass("selected_span");	
				}else{ 
					$(".night").addClass("selected_span");
				};
				
				$("#machineListForTarget_" + ty).animate({
					"opacity" : 1
				},10, function(){
					$(this).css("z-index", 99999)
				});
				
				$(".save_btn").click(function(){
					var img = document.createElement("img");
					img.setAttribute("id", "loading_img");
					img.setAttribute("src", "${ctxPath}/images/load.gif");
					img.style.cssText = "width : " + getElSize(500) + "; " + 
										"position : absolute;" + 
										"z-index : 99999999;" + 
										"border-radius : 50%;"
										
					$("body").prepend(img);
					$("#loading_img").css({
						"top" : (window.innerHeight/2) - ($("#loading_img").height()/2),
						"left" : (window.innerWidth/2) - ($("#loading_img").width()/2)
					});
					
					save_type = "day"
					addTarget("day")
				});
				
				$(".targetCnt").css({
					"font-size" : getElSize(40),
					"width" : getElSize(250),
					"outline" : "none",
					"border" : "none"
				});
				
				$(".span").css({
					"font-size" : getElSize(40),
					"color" : "red",
					"margin-left" : getElSize(10),
					"font-weight" : "bolder"
				});
				
				$(".save_btn").css({
					"background-color" : "white",
					"color" : "black",
					"border-radius" : getElSize(10),
					"font-weight" : "bolder",
					"padding" : getElSize(10)
				});
				
				$(".daynight_span").click(function(){
					getTargetData(this.id)	
				});
				
				$(".tdisable").each(function(idx, data){
					this.disabled = true;
					this.value = "";
				});
				
				$(".tdisable").css({
					"background-color" : "rgba(	4,	238,	91,0.5)"
				});
				
				$("#machineListTable td").css({
					"color" : "white",
					"font-size" : getElSize(50),
				});
				
				$(".machineListForTarget").css({
					"width" : getElSize(1300),
					"z-index" : -1
				});
				
				$(".machineListForTarget").css({
					"top" : (originHeight/2) - ($(".machineListForTarget").height()/2)
				});
			}
		});
	};
	
	var tgArray = new Array();
	var tgArray2 = new Array();
	var tgArray3 = new Array();
	var tgArray_night = new Array();
	var tgArray2_night = new Array();
	var tgArray3_night = new Array();
	
	var target_i = 0;
	function addTarget(ty){
		var date = new Date();
		var year = date.getFullYear();
		var month = addZero(String(date.getMonth()+1));
		var day = addZero(String(date.getDate()));
		
		today = year + "-" + month + "-" + day;
		var type;
		var cnt;
		var cntPerCyl;
		var time;
		var dvcId;
		if(ty=="day"){
			tgArray = targetMap.keys();
			tgArray2 = targetMap2.keys();
			tgArray3 = targetMap3.keys();
			
			tgArray_night = targetMap_night.keys();
			tgArray2_night = targetMap2_night.keys();
			tgArray3_night = targetMap3_night.keys();
			
			cnt = $("#d_" + tgArray[target_i]).val();
			time = $("#d_" + tgArray2[target_i]).val();
			cntPerCyl = $("#d_" + tgArray3[target_i]).val();
			
			dvcId = tgArray[target_i].substr(1);
			type = 2;
		}else{
			cnt = $("#d_" + tgArray[target_i]).val();
			time = $("#d_" + tgArray2[target_i]).val();
			cntPerCyl = $("#d_" + tgArray3[target_i]).val();
			dvcId = tgArray[target_i].substr(1);
			
			/* cnt = $("#n_" + tgArray_night[target_i]).val();
			time = $("#n_" + tgArray2_night[target_i]).val();
			dvcId = tgArray_night[target_i].substr(1); */
			type = 1;
		}
		
		var url = ctxPath + "/chart/addTargetCnt.do";
		
		
		if(cnt==null || cnt == "") cnt = 0;
		if(time==null || time == "") time = 0;
		if(cntPerCyl==null || cntPerCyl == "") cntPerCyl = 0;
		
		var param = "dvcId=" + dvcId + 
					"&tgCnt=" + cnt +
					"&tgRunTime=" + (time*3600) +  
					"&tgDate=" + today + 
					"&type=" + type +
					"&cntPerCyl=" + cntPerCyl;
		
		$.ajax({
			url : url,
			data : param,
			dataType : "text",
			type : "post",
			success : function(data){
				if(data=="success") {
					if (target_i<(tgArray.length-1) && save_type == "day"){
						target_i++;
						addTarget("day");
						if (target_i==(tgArray.length-1)) {
							target_i = -1;
							save_type = "init";
						}
					/* }else if(target_i<(tgArray_night.length-1) && save_type == "night"){ */
					}else if(target_i<(tgArray.length-1) && save_type == "night"){
						target_i++;
						addTarget("night");
						if (target_i==(tgArray.length-1)) {
							target_i = 0;
							save_type = "init";
						}
					}
					else{
						$("#loading_img").remove();
						target_i = 0;
						$(".machineListForTarget, #close_btn").animate({
							"opacity" :0
						}, function(){
							$(".machineListForTarget").css("z-index",0);
							$("#close_btn").css("z-index",0	);
						});
					}
					
				}else{
					//i
				}
			}
		});
	};
	
	var save_type = "init"
	
	function startPageLoop(){
		/* loopFlag = setInterval(function(){
			location.href = ctxPath + "/chart/multiVision.do";
		},1000*60*10); */
		
		
		loopFlag = setInterval(function(){
			//location.href = ctxPath + "/chart/multiVision.do";
			
			/* window.sessionStorage.setItem("dvcId", "1");
			window.sessionStorage.setItem("name", "UM/F C#1"); */
			
			location.href=ctxPath + "/chart/singleChartStatus.do";
			
		},1000*5);
	};
	
	var isToday = true;
	
	function addZero(n){
		if(n.length=="1"){
			n = "0" + n;
		};
		return n;
	};
	
	function setDivPos(){
		var neonColor = "#0096FF";
		
		var width = window.innerWidth;
		var height = window.innerHeight;
		
		$("#container").css({
			"width": contentWidth,
			"height" : contentHeight,
			"margin-top" : height/2 - ($("#container").height()/2)
		});
		
		$("#container").css({
			"margin-top" : height/2 - ($("#container").height()/2),
			"margin-left" : width/2 - ($("#container").width()/2),
		});
		
		
		
		$("#title_main").css({
			"font-size" : getElSize(100),
			"top" : $("#container").offset().top + (getElSize(10)),
			"color" : "white",
			"font-weight" : "bolder"
		});

		$("#title_main").css({
			"left" : originWidth/2 - ($("#title_main").width()/2)
		});
		
		$("#title_left").css({
			"width" : getElSize(300),
			"top" : $("#container").offset().top + (getElSize(50))
		});
		
		$("#title_right").css({
			"top" : $("#container").offset().top + (getElSize(10))
		});
		
		$("#title_left").css({
			"left" : $("#container").offset().left + getElSize(50)
		});
		
		$("#title_right").css({
			"right" : $("#container").offset().left + getElSize(50),
			"color" : "white",
			"font-weight" : "bolder",
			"font-size" : getElSize(40)
		});
		
		$("#tableDiv").css({
			"left" : $("#container").offset().left + getElSize(20),
			"width" : getElSize(600),
			"top" : $("#container").offset().top + getElSize(450)
		});
		
		//$("#table").css("margin-top", contentHeight/(targetHeight/50));
		$("#tableTitle").css("font-size", getElSize(40));
		$("#tableSubTitle").css("font-size", getElSize(30));
		$(".tr").css("font-size", getElSize(30));
		$(".td").css("padding", getElSize(10));
		
		$("#time").css({
			"top": $("#container").offset().top + getElSize(70),
			"right": $("#container").offset().left + getElSize(50),
			"font-size" : getElSize(30)
		});
		
		$("#date").css({
			"top": $("#container").offset().top + getElSize(70),
			"right": $("#container").offset().left + getElSize(250),
			"font-size" : getElSize(30)
		});
		
		$("#Legend").css({
			"display" : "none",
			"font-size" : getElSize(45),
			"top" : marginHeight + getElSize(600),
			"left" : marginWidth + getElSize(100),
			"color" : "white"
 		});
		
		
		$("#menu_btn").css({
			"position" : "absolute",
			"width" : getElSize(130),
			"left" : 0,
			"top" : getElSize(20),
			"cursor" : "pointer",
			"z-index" : 999999
		});
		$("#panel").css({
			"background-color" : "rgb(34,34,34)",
			"border" : getElSize(20) + "px solid rgb(50,50,50)",
			"position" : "absolute",
			"width" : contentWidth * 0.2,
			"top" : 0,
			"left" : -contentWidth * 0.2 - getElSize(40),
			"z-index" : 99999
		});
		

		$("#panel_table td").css({
			"padding" : getElSize(30),
			"cursor" : "pointer"
		});
		
		$("#panel_table td").addClass("unSelected_menu");
		
		$("#corver").css({
			"width" : originWidth,
			"height" : originHeight,
			"position" : "absolute",
			"z-index" : -1,
			"background-color" : "black",
			"opacity" : 0.4
		});
		
		$("#panel_table").css({
			"color" : "white",
			"font-size" : getElSize(40)
		});
		
		
		$("#excel").css({
			"width" : getElSize(70),
			"cursor" : "pointer"
		});
		
		$("#table_toggle_btn").css({
			"position" : "absolute",
			"left" : marginWidth + getElSize(150),
			"top" : getElSize(300)
		});
		
		$(".machineListForTarget").css({
			"position" : "absolute",
			"width" : getElSize(1200),
			"height" : getElSize(1200),
			"overflow" : "auto",
			//"top" : getElSize(50),
			//"background-color" : "rgb(34,34,34)",
			"background-color" : "green",
			"color" : "white",
			"font-size" : getElSize(50),
			"padding" : getElSize(50),
			"overflow" : "auto",
			"border-radius" : getElSize(50),
			"border" : getElSize(10) + "px solid white",
		});
		
		$(".machineListForTarget").css({
			"left" : (originWidth/2) - ($(".machineListForTarget").width()/2),
		});
		
		$("#bannerDiv").css({
			"position" : "absolute",
			"width" : getElSize(1500),
			"height" : getElSize(200),
			"border-radius" : getElSize(20),
			"padding" : getElSize(50),
			"background-color" : "lightgray",
			"z-index" : -9
		});

		
		$("#bannerDiv").css({
			"left" : (originWidth/2) - ($("#bannerDiv").width()/2),
			"top" : (originHeight/2) - ($("#bannerDiv").height()/2)
		});
		
		$("#bannerDiv input[type='text']").css({
			"width" : getElSize(1200),
			"font-size" : getElSize(50)
		});
		
		$("#bannerDiv button").css({
			"margin" : getElSize(50),
			"font-size" : getElSize(50)
		});
		
		$("#intro").css({
			/* "width" : contentWidth, */
			"position" : "absolute",
			"bottom" : 0 + marginHeight,
			"font-size" : getElSize(140),
			"font-weight" : "bolder",
			"z-index" : 9999
		});
		
		$("#intro_back").css({
			"width" : contentWidth,
			"display" : "none",
			"height" : getElSize(180),
			"opacity" : 0.5,
			"position" : "absolute",
			"background-color" : "black",
			"bottom" : 0 + marginHeight,
			"z-index" : 9999
		})
		
		/* $("#radar").css({
			"top" : getElSize(890) + marginHeight,
			"left" : getElSize(130) + marginWidth,
		 	"position" : "absolute",
			"width" : getElSize(550),
			"height" : getElSize(550),
			//"background" : "#898989",
			"background" : "rgba(0,0,0,0)",
			"border-radius" : "50%",
			"overflow" : "hidden",
			"z-index":999,
			"border" : getElSize(20) + "px solid #00A700"
		});
		
		$("#radar2").css({
			"top" : getElSize(890) + marginHeight + getElSize(5),
			"left" : getElSize(130) + marginWidth + getElSize(5),
		 	"position" : "absolute",
			"width" : getElSize(550) + getElSize(10),
			"height" : getElSize(550) + getElSize(10),
			//"background" : "#898989",
			"background" : "rgba(0,0,0,0)",
			"border-radius" : "50%",
			"overflow" : "hidden",
			"z-index":999,
			"border" : getElSize(10) + "px solid #00A700"
		}); */
		
		$("#innerRadar").css({
		 	"position" : "absolute",
			"width" : getElSize(550) * 0.8,
			"height" : getElSize(550) * 0.8,
			"background" : "rgba(0,0,0,0)",
			"border-radius" : "50%",
			"overflow" : "hidden",
			"z-index":999,
			"border" : getElSize(20) + "px solid " + neonColor
		});
		
		$("#innerRadar2").css({
		 	"position" : "absolute",
			"width" : getElSize(550) * 0.8 + getElSize(10),
			"height" : getElSize(550) * 0.8 + getElSize(10),
			"background" : "rgba(0,0,0,0)",
			"border-radius" : "50%",
			"overflow" : "hidden",
			"z-index":999,
			"border" : getElSize(10) + "px solid " + neonColor
		}); 
		
		$("#innerRadar").css({
			/* "top" : $("#radar2").offset().top + ($("#radar2").height()/2) - ($("#innerRadar").height()/2) - getElSize(10),
			"left" : $("#radar2").offset().left + ($("#radar2").width()/2)  - ($("#innerRadar").width()/2) - getElSize(10) */
			"top" : getElSize(950)+ marginHeight,
			"left" : getElSize(160)+ marginWidth
		});
		
		$("#innerRadar2").css({
			/* "top" : $("#radar2").offset().top + ($("#radar2").height()/2) - ($("#innerRadar").height()/2) - getElSize(10) + getElSize(5),
			"left" : $("#radar2").offset().left + ($("#radar2").width()/2)  - ($("#innerRadar").width()/2) - getElSize(10) + getElSize(5), */
			"top" : getElSize(955)+ marginHeight,
			"left" : getElSize(165)+ marginWidth
		});
		
		$("#rad").css({
			"z-index" : 1,
		  	"position" : "absolute",
		  	"width" :  getElSize(550) + getElSize(15),
		  	"height" :  getElSize(550) + getElSize(15), 
		  	"position" :"absolute",
		  	"opacity" : 0
		});
		
		$("#rad").css({
		  	"top" : $("#innerRadar2").offset().top - getElSize(51),
		  	"left" : $("#innerRadar2").offset().left - getElSize(51) 
		});
		
		var rad_canvas = document.getElementById("rad_canvas");
		var rad_ctx = rad_canvas.getContext("2d");
		
		rad_canvas.width = $("#innerRadar2").width() + getElSize(100);
		rad_canvas.height = $("#innerRadar2").width() + getElSize(100);
		
		$("#rad_canvas").css({
			"background-color" : "rgba(0,0,0,0)",
			"position" : "absolute",
			"z-index" : 9999,
			"top" : $("#innerRadar2").offset().top - getElSize(40),
			"left" : $("#innerRadar2").offset().left - getElSize(40)
		})
		
		//rad_ctx.arc($("#rad_canvas").width()/2,$("#rad_canvas").width()/2, $("#rad_canvas").width()/2 - getElSize(10), 1.4 * Math.PI, 1.5*Math.PI);
		rad_ctx.arc($("#rad_canvas").width()/2,$("#rad_canvas").height()/2, $("#rad_canvas").width()/2 - getElSize(10), 0.8 * Math.PI,1.5*Math.PI);
		
		rad_ctx.lineCap = 'round';
		
		var grd=ctx.createLinearGradient(0,0,getElSize(195),getElSize(335));
		grd.addColorStop(0,"#B4C6FA");
		grd.addColorStop(1,"rgba(0,0,0,0)");
		rad_ctx.strokeStyle = neonColor;
		
		rad_ctx.strokeStyle = grd;
		rad_ctx.lineWidth = getElSize(20);
		rad_ctx.stroke();
		
		$("#radar span").css({
			"z-index":999,
			"position" :"absolute",
			"font-weight" : "bolder",
			"font-size" : getElSize(300),
			"color" : neonColor
		}); 
		
		$("#radar_opratio").css({
			"z-index":99999,
			"position" :"absolute",
			"font-weight" : "bolder",
			"font-size" : getElSize(300),
			"color" : neonColor
		});
		
		$("#radar_opratio2").css({
			"z-index":99999,
			"position" :"absolute",
			"font-weight" : "bolder",
			"font-size" : getElSize(300),
			"color" : neonColor
		});
		
		$("#color2").css({
			"position" : "absolute",
			"top" : getElSize(1000)
		});
		
		$("#color2").change(function(){
			$("#radar").css({
				"background-color" : this.value
			})
		});
		
		$("#color3").css({
			"position" : "absolute",
			"top" : getElSize(1100)
		});
		
		$("#color3").change(function(){
			$("#radar span").css({
				"color" : this.value
			})
		});
		
		$("svg").css({
			"position" :"absolute",
			"z-index" : 9
		});
	};
	
	var border_interval = null;
	function bodyNeonEffect(color) {
		var lineWidth = getElSize(100);
		var toggle = true;
		border_interval = setInterval(function() {
			$("#container").css("box-shadow",
					"inset 0px 0px " + lineWidth + "px " + color);

			if (toggle) {
				lineWidth -= getElSize(3);
				if (lineWidth <= 0) {
					toggle = false;
				};
			} else if (!toggle) {
				lineWidth += getElSize(3);
				if (lineWidth >= getElSize(100)) {
					toggle = true;
				};
			};
		},50);
	};
	
	function time(){
		var date = new Date();
		var month = date.getMonth()+1;
		var date_ = addZero(String(date.getDate()));
		var day = date.getDay();
		var hour = addZero(String(date.getHours()));
		var minute = addZero(String(date.getMinutes()));
		var second = addZero(String(date.getSeconds()));
		
		if(day==1){
			day = "Mon";
		}else if(day==2){
			day = "Tue";
		}else if(day==3){
			day = "Wed";
		}else if(day==4){
			day = "Thu";
		}else if(day==5){
			day = "Fri";
		}else if(day==6){
			day = "Sat";
		}else if(day==0){
			day = "Sun";
		};
		
		$("#date").html(month + " / " + date_ + " (" + day + ")");
		$("#time").html(hour + " : " + minute + " : " + second);
	};
	
	var csvData = "";
	function csvSend(){
		var sDate, eDate;
		var csvOutput;
		
		sDate = $("#sDate").val();
		eDate = "";
		csvOutput = csvData;
		
		csvOutput = csvOutput.replace(/\n/gi,"");
		
		f.csv.value=encodeURIComponent(csvOutput);
		f.startDate.value = sDate;
		f.endDate.value = eDate;
		f.submit(); 
	};
	
</script>
</head>
<body oncontextmenu="return false">
	<form action='${ctxPath}/csv/csv.do' id="f"  method="post">
		<input type="hidden" name="csv" value="">
		<input type="hidden" name="startDate" value="">
		<input type="hidden" name="endDate" value="">
	</form>
	
		<!-- <input type="color" id="color2">
		<input type="color" id="color3"> -->
	<div id="corver"></div>
	
	<div id="machineListForTarget_day" style="opacity:0" class="machineListForTarget">
		<center>
			<table id="machineListTable" style="width: 100%">
			</table>
		</center>
	</div>
	<div id="machineListForTarget_night" style="opacity:0" class="machineListForTarget">
		<center>
			<table id="machineListTable" style="width: 100%">
			</table>
		</center>
	</div>			
					
	<div id="container" >
		<div id="bannerDiv">
		<Center>
			<input type="text" id="banner" size="100"><input type="color" id="color"><br>
			<button onclick="addBanner();">적용</button>
			<button onclick="cancelBanner();">미적용</button>
		</Center>
	</div>
	
	
	<div id="radar" class="blur1">
    	<%-- <img alt="" src="${ctxPath}/images/fbgUD.png" id="rad"> --%>
  	</div>
  	<!-- <img alt="" src="http://i.stack.imgur.com/fbgUD.png" id="rad">  -->
  	<img alt="" src="${ctxPath }/images/fbgUD2.png" id="rad"> 
  	<span class="radar_opratio blur1" id="radar_opratio"></span>
  	<span class="radar_opratio blur2" id="radar_opratio2" ></span>
  	
  	<canvas id="rad_canvas"></canvas>
  	<div id="radar2" class="blur2"></div>
  	
  	<div id="innerRadar" class="blur1"></div>
  	<div id="innerRadar2" class="blur2"></div>
  	
  	<canvas id="canvas"></canvas>
  	
		<div id="panel">
			<table id="panel_table" width="100%">
				<tr>
					<td id="menu0" class="menu"><spring:message code="layout"></spring:message> </td>
				</tr>
				<tr>
					<td id="menu7" class="menu"><spring:message code="devicestatus"></spring:message> </td>
				</tr>
				<tr>
					<td id="menu4" class="menu"><spring:message code="dailydevicestatus"></spring:message> </td>
				</tr>
				<tr>
					<td id="menu1" class="menu"><spring:message code="analsysperformance"></spring:message> </td> <!-- 장비별 가동실적분석 -->
				</tr>
				<tr>
					<td id="menu8" class="menu"><spring:message code="performancegraph1"></spring:message></td>
				</tr>
				<tr>
					<td id="menu9" class="menu"><spring:message code="performancegraph2"></spring:message> </td>
				</tr>
				<tr>
					<td id="menu12" class="menu">일생산 실적 분석 (표)</td> <!--알람 내역 -->
				</tr>
				<tr>
					<td id="menu6" class="menu"><spring:message code="toolmanagement"></spring:message> </td><!-- 무인장비 가동현황 -->
				</tr> 
				<tr>
					<td id="menu11" class="menu"><spring:message code="prdctboard"></spring:message> </td>
				</tr>
				<tr>
					<td id="menu10" class="menu"><spring:message code="addprdctgll"></spring:message> </td>
				</tr>
				<tr>
					<td id="menu5" class="menu"><spring:message code="tracemanagement"></spring:message> </td><!-- 무인장비 가동현황 -->
				</tr>
				<tr>
					<td id="menu3" class="menu"><spring:message code="24barchart"></spring:message></td><!-- 무인장비 가동현황 -->
				</tr>
				<tr>
					<td id="menu2" class="menu"><spring:message code="alarmhistory"></spring:message> </td> <!--알람 내역 -->
				</tr>
				<tr>
					<td id="menu100" class="menu">재고 현황</td> 
				</tr>
				<tr>
					<td id="menu99" class="menu">Catch Phrase 관리</td> 
				</tr>
				<tr>
					<td id="menu101" class="menu">프로그램별 가공이상 분석</td> 
				</tr>
				<tr>
					<td id="menu102" class="menu">제조 리드타임</td> 
				</tr>
				<tr>
					<td id="menu103" class="menu">고객불량율 / 공정뷸량율</td> 
				</tr>
				<tr>
					<td id="menu104" class="menu">불량등록</td> 
				</tr>
			</table>
		</div>
		<img src="${ctxPath }/images/home.png" id="menu_btn" >
		<div id="svg"></div>
		<svg xmlns="http://www.w3.org/2000/svg" version="1.1" class="filters" style="width: 0px; height: 0px">
			<defs>
				<filter id="blur">
					<feGaussianBlur in="SourceGraphic" stdDeviation="4, 4" />
				</filter>
			</defs>
			<defs>
				<filter id="blur2">
					<feGaussianBlur in="SourceGraphic" stdDeviation="0, 0" />
				</filter>
			</defs>
		</svg>
		
		<div id="intro_back"></div>
		<span id="intro"></span>
	</div>
	<div id="Legend" style="position: absolute; font-size: 50px;">
		<span style="background-color: GREEN; color: green; border: 1px solid black;">범례</span> <font style="color: white"><spring:message code="incycle"/></font><br><br> 
		<span style="background-color: yellow; color: yellow; border: 1px solid black;">범례</span> <font style="color: white"><spring:message code="wait"/></font><br> <br>
		<span style="background-color: red; color: red; border: 1px solid black;">범례</span> <font style="color: white"><spring:message code="alarm"/></font> <br><br>
		<span style="background-color: rgb(206,206,206); color: rgb(206,206,206); border: 1px solid black;">범례</span> <font style="color: white"><spring:message code="noconnection"/></font>  
	</div>

	<div id="pieChart1" ></div>
	<div id="pieChart2" ></div>
	
	

	<%-- <img src="${ctxPath }/images/DashBoard/title_main.svg" id="title_main" class="title"> --%>
	<div id="title_main" class="title"><spring:message code="layout"></spring:message> </div>
	<%-- <img src="${ctxPath }/images/DashBoard/title_left.svg" id="title_left" class="title"> --%>
	<div id="title_right" class="title"><spring:message code="comname"></spring:message></div>	

	<font id="date"></font>
	<font id="time"></font>
</body>
</html>	