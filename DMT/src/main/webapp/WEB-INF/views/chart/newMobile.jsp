<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ include file="/WEB-INF/views/include/lib_mobile.jsp"%>
<!DOCTYPE html>
<html> 
<head>
<meta charset="UTF-8">
<link rel="shortcut icon" href="${ctxPath }/images/dulink_favicon.ico">
<title>Machine List</title>
<style>
body{
	background: url("../images/DashBoard/back_mobile.jpg");
	background-size : 100% 100%;
}

.title{
	top : 50px;
	position: absolute;
}

#title_main{
	z-index: 999;
}

#title_right{
	color : white;
}

#time{
	position: absolute;
	color: white;
} 
#date{
	position: absolute;
} 

hr{
	position: absolute;
	top: 250px;
	z-index: 99;
	width: 90%;
	border: 2px solid white;
}

#mainTable{
	width: 100%;
	z-index: 99;
	background: blue; /* Standard syntax */
 	opacity : 1;
 	text-align: center;
}

#header{
	width: 100%;
	z-index: -99;
	opacity : 1;
	background-color: blue;
}

#mainTable tr:last-child td:first-child {
    -moz-border-radius-bottomleft:10px;
    -webkit-border-bottom-left-radius:10px;
    border-bottom-left-radius:10px;
    
    -moz-border-radius-topleft:10px;
    -webkit-border-top-left-radius:10px;
    border-top-left-radius:10px
}

#mainTable tr:last-child td:last-child {
    -moz-border-radius-bottomright:10px;
    -webkit-border-bottom-right-radius:10px;
    border-bottom-right-radius:10px;
    
    -moz-border-radius-topright:10px;
    -webkit-border-top-right-radius:10px;
    border-top-right-radius:10px
}

#wraper{
	overflow: scroll;
	width : 100%;
}

.tr_table_fix_header
{
 position: relative;
 top: expression(this.offsetParent.scrollTop);
 z-index: 20;
}

.loading-overlay{
	z-index: 9999999999999 !important;
}

</style>
<script type="text/javascript">
	var ctxPath = "${ctxPath}";
	var fromDashboard = "${fromDashBoard}";
	var targetWidth = 3840;
	var targetHeight = 2160;

	var originWidth = window.innerWidth;
	var originHeight = window.innerHeight;
	
	var contentWidth = originWidth;
	var contentHeight = targetHeight/(targetWidth/originWidth);
	
	var screen_ratio = getElSize(240);
	
	if(originHeight/screen_ratio<9){
		contentWidth = targetWidth/(targetHeight/originHeight)
		contentHeight = originHeight; 
	};
	
	function getElSize(n){
		return contentWidth/(targetWidth/n);
	};
	
	function setElSize(n) {
		return Math.floor(targetWidth / (contentWidth / n));
	};
	
	var marginWidth = (originWidth-contentWidth)/2;
	var marginHeight = (originHeight-contentHeight)/2;
</script>
<script type="text/javascript" src="${ctxPath }/js/chart/highcharts.js"></script>
<script type="text/javascript" src="${ctxPath }/js/chart/highcharts-3d.js"></script>
<script type="text/javascript" src="${ctxPath }/js/chart/highcharts-more.js"></script>
<script type="text/javascript" src="${ctxPath }/js/jqMap.js"></script>
<link rel="stylesheet" href="${ctxPath }/css/jquery.loading.min.css">
<script type="text/javascript" src="${ctxPath }/js/jquery.loading.min.js"></script>
<script type="text/javascript" src="${ctxPath }/js/chart/moment.js"></script>

<script type="text/javascript">
	
	var hideModal = function(event) {
		//alert(event.state)
	    if (event.state == 'settingDialogBackPressed') {
	    	settingDialogClose()
	    } else if (event.state == 'settingDisplayDialogBackPressed') {
	    	settingDisplayDialogClose()
	    } else if (event.state == 'settingAccountDialogBackPressed') {
	    	settingAccountDialogClose()
	    } else if (event.state == 'openSettingBtnBackPressed'){
	    	openSettingBtn()
	    } else {

	    }
	};
	window.addEventListener('popstate', hideModal, { once: false });
	
	var shopId = 1;
	
	var broswerInfo = navigator.userAgent;
	
	$(function(){
		getComName();
		setElement();
		
		setInterval(time, 1000);
		
		getBarChartDvcId2();
		
		/*
		* Date : 19.07.10
		* Author : wilson
		* show or hide 'PMC' and 'LAMP'  
		*/
		
		$('#pmc').show();
		$('#lamp').hide();
		
		$("#pmc-btn, #lamp-btn").css({
			"font-size" : getElSize(80),
			"border" : getElSize(1) + "px solid white",
			"font-weight" : "bolder",
			"padding-right" : getElSize(70),
			"padding-left" : getElSize(70),
			"padding-top" : getElSize(30),
			"padding-bottom" : getElSize(30)
		});
		
		$("#pmc-btn").css({
			"color" : "white",
			"background" : "blue",
			"margin-right" : getElSize(20)
		});	
		
		$("#lamp-btn").css({
			"color" : "#9999FF",
			"background" : "white"
		});
		
		$("#pmc_lamp").css({
			"position" : "absolute",
			"top" : getElSize(500),
			"right" : getElSize(50)			
		});	
		
				
		setInterval(function(){
			getBarChartDvcId2()
		}, 5000)	
	});

	function isMobile() {
	    return /Android|webOS|iPhone|iPad|iPod|BlackBerry|IEMobile|Opera Mini/i.test(navigator.userAgent);
	}
	
	function getTime(){
		var url = "${ctxPath}/chart/getTime.do";
		var time = "";
		$.ajax({
			url : url,
			dataType : "text",
			type : post,
			success : function(data){
				time = data;	
			}
		});
		
		return time;
	};
	
	function getMachineOrder(array, item) {
 		var rtn;
		for (var i = 0; i < array.length; i++) {
 			for (var j = 0; j < array[i].length; j++) {
 				if (array[i][j] == item) {
 					rtn = array[i][j+1]; 
 				};
 	        };
 	    };
		return rtn;
	};
	
	function getBarChartDvcId2(){
				
		/*
		* Date : 19.05.22 
		* Author : wilson
		* 현재시간이 가동시작시간보다 크거나 같을 경우, 하루를 더해준다.
		* If the current time is greater than or equal to the start-up time, add one day.
		*/
		
		var url = "${ctxPath}/chart/getStartTime.do";
		var param = "shopId=" + shopId;
		var startHour;
		var startMinute;
		
		$.ajax({
			url : url,
			dataType :"text",
			type : "post",
			data : param,
			success : function(data){
				startHour = data.substring(0,2);
				startMinute = data.substring(3,5);
				
				//console.log("start시 : " + startHour);
				//console.log("start분 : " + startMinute);
				
				url = "${ctxPath}/chart/getAllDvcId.do";
				var date = new Date();
				var hour = date.getHours();
				var minute = date.getMinutes();
				
				//console.log("now시 : " + hour);
				//console.log("now분 : " + minute);
				
				// 가동시작시간 보다 크거나 같을 경우 
				if(hour >= startHour){
					if( (hour > startHour) || (minute >= startMinute) ){
						date.setDate(date.getDate() + 1)		
					}else{
						//console.log("17시 이상이나 30분 미만");						
					}
				}
				
				var year = date.getFullYear();
				var month = addZero(String(date.getMonth()+1));
				var day = addZero(String(date.getDate()));
				var today = year + "-" + month + "-" + day; 
				var param = "workDate=" + today + 
							"&shopId=" + shopId;
				
				//console.log("today : " + today);
				
				$.ajax({
					url : url,
					dataType : "json",
					type : "post",
					data : param,
					success : function(json){
						
						var obj = json.dvcId;
						var pmc = "";
						var lamp = "";
						$('#mainTable tr:not(:first)').remove();
						$(obj).each(function(i, data){
							if(data.chartStatus=="CUT"){
								data.chartStatus="IN-CYCLE"
							}
											
												
							/*
							* Date : 19.04.08 
							* Author : wilson
							* if 'type' is 'IOL', show data into 'LAMP' not 'PMC'
							*/
							
							/*
							* Date : 19.04.11
							* Author : wilson
							* if 'type' is 'IOL', show data into 'PMC' and 'LAMP'
							*/
							
							/*
							* Date : 19.07.10
							* Author : wilson
							* show or hide 'PMC' and 'LAMP'
							*/
							
							
							if(isPmc){								
								if(data.type == "IOL"){
									pmc = "<td style='font-size: "+getElSize(160)+"px; font-weight: bold; padding: "+getElSize(40)+"px;' valign='top' align='center'><img src=" + ctxPath + "/images/DashBoard/" + data.lampStatus +".png width='50px'></td>"
								}else{
									pmc = "<td style='font-size: "+getElSize(160)+"px; font-weight: bold; padding: "+getElSize(40)+"px;' valign='top' align='center'><img src=" + ctxPath + "/images/DashBoard/" + data.chartStatus +".png width='50px'></td>"
								}
							} else{								
								pmc = "";
							}
							
							if(isLamp){																
								lamp = "<td style='font-size: "+getElSize(160)+"px; font-weight: bold; padding: "+getElSize(40)+"px;' valign='top' align='center'><img src=" + ctxPath + "/images/DashBoard/" + data.lampStatus +".png width='50px'></td>"
							} else{
								lamp = "";
							}
							
							
							var operationTime = Number(Number(Number(data.operationTime/60/60).toFixed(1)) + Number(Number(data.cuttingTime/60/60).toFixed(1))).toFixed(1);
							var tr = "<tr onclick='goMobilePage(" + data.dvcId + ")'>" + 
											"<td style='font-size: "+getElSize(140)+"px; font-weight: bold; padding: "+getElSize(40)+"px;' valign='middle' >" + decodeURIComponent(data.jig) + "</td>" +
											"<td style='font-size: "+getElSize(140)+"px; font-weight: bold; padding: "+getElSize(40)+"px;' valign='middle' >" + decodeURIComponent(data.WC) + "</td>" +
											"<td style='font-size: "+getElSize(140)+"px; font-weight: bold; padding: "+getElSize(40)+"px;' valign='middle' >" + data.name + "</td>" +									
												pmc +	
												lamp +	
											"<td style='font-size: "+getElSize(140)+"px; font-weight: bold; padding: "+getElSize(40)+"px;' valign='middle' >" + operationTime + "</td>" +  
										"</tr>";
										
							$("#mainTable").append(tr);
							
							$("#mainTable").css({
								"height" : 500
							});
						});
					},
					error : function(e1,e2,e3){
					}
				});
			},
			error : function(e1,e2,e3){
			}
			
		});
	};
	
	function goMobilePage(dvcId){
		window.sessionStorage.setItem("dvcId", dvcId);
		location.href="${ctxPath}/chart/mobile2.do";
	};
	
	function time(){
		var date = new Date();
		var month = date.getMonth()+1;
		var date_ = addZero(String(date.getDate()));
		var day = date.getDay();
		var hour = addZero(String(date.getHours()));
		var minute = addZero(String(date.getMinutes()));
		var second = addZero(String(date.getSeconds()));
		
		if(day==1){
			day = "Mon";
		}else if(day==2){
			day = "Tue";
		}else if(day==3){
			day = "Wed";
		}else if(day==4){
			day = "Thu";
		}else if(day==5){
			day = "Fri";
		}else if(day==6){
			day = "Sat";
		}else if(day==0){
			day = "Sun";
		};
		
		$("#date").html(month + " / " + date_ + " (" + day + ")");
		$("#time").html(hour + " : " + minute + " : " + second);
	};
	
	function setElement(){
		var width = window.innerWidth;
		var height = window.innerHeight;
		
		$("#wraper").css({
			"height" : height * 0.73
		});
		
		$("#title_main").css({
			"left" : "30%",
			"width" : getElSize(1600),
			"margin-top" : getElSize(50)
		});
		
		$("#title_left").css({
			"left" : getElSize(100)
		});
		
		
		
		$("hr").css({
			"left" : width/2 - $("hr").width()/2
		});
		
		$(".status").css({
			"width" : width*0.45,
			"height" : height*0.8
		});
		
		$("#login-img").css({
			"position" : "absolute",
			"font-size" : getElSize(100),
			"top" : getElSize(50),
			"right" : getElSize(0),
			"border-radius" : getElSize(15),
			"width" : getElSize(1200),
			"height" : getElSize(400),
			"z-index" : 1000
		})
		
		
		$(".btn").css({
			"font-size" : getElSize(150),
			"background" : "white",
			"border" : getElSize(10)+"px solid black",
			"border-radius" : getElSize(20),
			"width" : getElSize(1500),
			"padding" : getElSize(100)
		})
		
		$("#setting-btn").css({
			"position" : "absolute",
			"font-size" : getElSize(200),
			"top" : getElSize(520),
			"right" : getElSize(0),
			"border-radius" : getElSize(55),
			"width" : getElSize(1200),
			"height" : getElSize(400),
			"font-weight" : "bolder",
			"line-height" : 0,
			"z-index" : "1",
			"background-color" : "white"
		})
		
		$("#setting-btn2").css({
			"position" : "absolute",
			"font-size" : getElSize(200),
			"top" : getElSize(990),
			"right" : getElSize(0),
			"border-radius" : getElSize(55),
			"width" : getElSize(1200),
			"height" : getElSize(400),
			"font-weight" : "bolder",
			"line-height" : 0,
			"z-index" : "1",
			"background-color" : "white"
		})
		
		$("#setting-btn3").css({
			"position" : "absolute",
			"font-size" : getElSize(200),
			"top" : getElSize(1460),
			"right" : getElSize(0),
			"border-radius" : getElSize(55),
			"width" : getElSize(1200),
			"height" : getElSize(400),
			"font-weight" : "bolder",
			"line-height" : 0,
			"z-index" : "1",
			"background-color" : "white"
		})
		
		$("#date").css({
			"left" : getElSize(100),
			"top" : getElSize(700),
			"color" : "white",
			"font-size" : getElSize(100),
			"font-weight" : "bolder",
		})
		
		$("#time").css({
			"left" : getElSize(900),
			"top" : getElSize(700),
			"color" : "white",
			"font-size" : getElSize(100),
			"font-weight" : "bolder"
		})
		
		$("#header").css({
			"height" : getElSize(900)
		})
		
		$("#title_right").css({
			"font-size" : getElSize(100),
			"right" : getElSize(50),
			"top" : getElSize(700)
		})
		
		$("#gear_img").css({
			"position" : "absolute",
			"right" : getElSize(0),
			"width" : getElSize(400),
			"height" : getElSize(400),
			"top" : getElSize(50),
			"transition-duration": "0.5s",
			"z-index" : "1000",
			"border-radius" : getElSize(55),
			"background-color" : "white"
			
		})
		
		$("#btn-img").css({
			"height" : getElSize(300),
			"width" : getElSize(300)
		})
		
		$("#name-div").css({
			"position" : "absolute",
			"top" : getElSize(700),
			"left" : getElSize(1800),
			"color" : "white",
			"font-size" : getElSize(100)
		})
		
		if(broswerInfo.indexOf("APP_AIDOO")>-1){
			
			$("#login-img").css({
				"display" : "none"
			})
			
			$("#setting-btn").css({
				"display" : "none"
			})
			
			$("#setting-btn2").css({
				"display" : "none"
			})
			
			$("#setting-btn3").css({
				"display" : "none"
			})
			
		}else{
			$("#login-img").css({
				"display" : "none"
			})
			
			$("#setting-btn").css({
				"display" : "none"
			})
			
			$("#setting-btn2").css({
				"display" : "none"
			})
			
			$("#setting-btn3").css({
				"display" : "none"
			})
			//wilson
			/* $("#gear_img").css({
				"display" : "none"
			}) */
		}
		
		
	};
	
	function addZero(n){
		if(n.length=="1"){
			n = "0" + n;
		};
		return n;
	};
	
	function changePage(){
		location.href="${ctxPath}/chart/mobile2.do";
	};
	
	function getComName(){
		
		$("#title_right").html(comName)
	};
	
	
</script>
</head>
<body>

	<script>
	
	function decode(str){
		return decodeURIComponent(str).replace(/\+/gi, " ")
	};
	
	var dialog;
	
	var userId;
	var settingDialog;
	var settingDialogDisplay;
	var settingDialogAccount;
	
	function pushSave(){
		
		$('body').loading({
			message: '기다려 주세요',
			theme: 'dark'
		});
		
		//console.log(saveList)
		
		var weekday = $("#weekday").is(":checked");
		var holiday = $("#holiday").is(":checked");
		var nightTime = $("#nightTime").is(":checked");
		var mobile = null;
		
		if( $("#mobile").val() == null || $("#mobile").val() == "" || $("#mobile").val() =="undefined" ){
			mobile = "";
		}else{
			mobile = $("#mobile").val().trim();
		}
		
		var url = ctxPath + "/chart/pushSave.do";
		var param = "id=" + userId + 
					"&shopId=" + shopId +
					"&list=" + saveList + 
					"&uuid=" + uuid +
					"&weekday=" + weekday +
					"&nightTime=" + nightTime +
					"&holiday=" + holiday +					
					"&mobile=" + mobile;
		
		//console.log(url+param)
		
		$.ajax({
			url : url,
			data : param,
			type : "post",
			dataType : "text",
			success : function(data){
				
				$('body').loading('stop');
				
				if(data=="success"){
					alert("저장이 완료 되었습니다.")
					settingDialog.close();
				}
			}
		})
		
	}
	
	var saveList=[];
	var settingGrid;
	var settingGridDisplay;
	var settingGridAccount;
	$(function(){
		
		settingDialog = $("#setting-dialog").kendoDialog({
			modal :true,
			minWidth :getElSize(3500),
			minHeight :getElSize(5500),
			closable :false,
			title :false,
			visible :false,
			open :function(){
				
				var url = ctxPath + "/chart/getCheckedList.do";
				var param = "id=" + userId + 
							"&shopId=" + shopId + 
							"&uuid=" + uuid;
				$.ajax({
					url : url,
					data : param,
					type : "post",
					dataType : "json",
					success : function(data){
						var json = data.dataList;
						
						kendodata = new kendo.data.DataSource({
							schema: {
					           model: {
					               id: "dvcId",
					               fields: {
					            	   name : {},
					            	   jig : {},
					            	   checked : {},
					            	   dvcId : {}
					               }
					           }
							},
							group: { field: "jig" }
						})
						
						for(var i=0;i<json.length;i++){
							json[i].jig=decode(json[i].jig)
							kendodata.add(json[i])
						}
						
						$("#setting-grid").css({
							"width" : getElSize(3300)
						})
						
						$("#setting-grid").kendoGrid({
							dataSource : kendodata,
				            persistSelection: true,
							height : getElSize(4000),
							columns : [
								{ 
									selectable: true, 
									width: getElSize(100), 
									headerAttributes: {
									      style: "font-size : " + getElSize(150)
							    	},	
									attributes: {
									      style: "font-size : " + getElSize(150)
							    	}	
								},
								{ 
									field :"name",
									width: getElSize(300),
									headerTemplate : "장비명",
									headerAttributes: {
									      style: "font-size : " + getElSize(150)
							    	},
									attributes: {
								      style: "font-size : " + getElSize(150)
							    	}
								},
								{ 
									field :"jig",
									width: getElSize(200),
									headerTemplate : "직",
									groupHeaderTemplate: "#=value#",
									headerAttributes: {
									      style: "font-size : " + getElSize(150)
							    	},
									attributes: {
								      style: "font-size : " + getElSize(150)
							    	}
								},
							],
							change :function(arg){
								console.log("Checked")
								console.log(this.selectedKeyNames())
								
								saveList = this.selectedKeyNames()
							},
							dataBinding : function(){
								$("#setting-grid").css({
									"top" : "0"
								})
								
								//wilson
								$("#mobileSetting").css({
									"font-size" : getElSize(150),
									"text-align" : "left",
									"margin-bottom" : getElSize(40)
								})
								
								$("#mobile").css({
									"font-size" : getElSize(150),
									"width" : getElSize(1180)
								})
								
								$("#alarmSetting").css({
									"font-size" : getElSize(150),
									"margin-bottom" : getElSize(50),
									"text-align" : "center"
								})
								
								$(".chkbox").css({
									"width" : getElSize(157),
									"height" : getElSize(157),
									"vertical-align" : "middle",
									"margin-bottom" : getElSize(39)
								})
								
								$("#weekday_title").css({
									//"margin-left" : getElSize(110)
								})
								
								$("#night_title").css({
									"margin-left" : getElSize(120)
								})
								
								$("#holiday_title").css({
									"margin-left" : getElSize(120)
								})
								
								
							},
							dataBound : function(){
								$(".k-no-text").css({
									"-webkit-transform": "scale(3)",
									"margin-bottom" : getElSize(-25),
									"margin-left" : getElSize(30)
								})
								
								$(".k-reset").css({
									"font-size" : getElSize(150)
								})
								
								$(".k-checkbox-label").first().css({
									"margin-bottom" : getElSize(70),
								})
								
								$(".btn").css({
									"font-size" : getElSize(150),
									"background" : "white",
									"border" : getElSize(10)+"px solid black",
									"border-radius" : getElSize(20),
									"width" : getElSize(2200),
									"padding" : getElSize(20),
									"margin-bottom" : getElSize(20),
									"margin-top" : getElSize(20)
								})
								
								$(".save-setting").css({
									"background" : "#4DA63A",
									"color" : "white",
									"margin-left" : getElSize(50),
									"padding" : getElSize(100),
									"width" : getElSize(3000),
									"margin-top" : getElSize(200)
								})
								
								$(".close-setting").css({
									"background" : "#F1F1F1",
									"color" : "black",
									"padding" : getElSize(100),
									"margin-left" : getElSize(50),
									"width" : getElSize(3000),
									"margin-top" : getElSize(200)
								})
								
								
								var grid = this;
								
								grid.items().each(function(){
									
									var data = grid.dataItem(this);
									
									if(data.checked==1){
										grid.select(this)
									}
																
								})
							}
						})
						
					}
				});
				
				url = ctxPath + "/chart/getAlarmSetting.do";
				param = "id=" + userId + 
						"&shopId=" + shopId + 
						"&uuid=" + uuid;
				
				$.ajax({
					url : url,
					data : param,
					type : "post",
					dataType : "json",
					success : function(data){
						
						//var json = data.dataList;
						
						var mobile = data.mobile;
						
						if( mobile == null || mobile == "" || mobile == "undefined" ){					
						}else{
							$("#mobile").val(mobile);
						}
						
						if(data.weekday == "true"){
							$("#weekday").prop("checked", true)
						}else{
							$("#weekday").prop("checked", false)	
						}
						
						if(data.holiday == "true"){
							$("#holiday").prop("checked", true)
						}else{
							$("#holiday").prop("checked", false)	
						}
						
						if(data.nightTime == "true"){
							$("#nightTime").prop("checked", true)
						}else{
							$("#nightTime").prop("checked", false)	
						}
					}
				});
				
			}
		}).data("kendoDialog");
	
		settingDialogDisplay = $("#setting-dialog-display").kendoDialog({
			modal :true,
			minWidth :getElSize(3500),
			minHeight :getElSize(5500),
			closable :false,
			title :false,
			visible :false,
			open :function(){
				
				var url = ctxPath + "/chart/getCheckedList.do";
				var param = "id=" + userId + 
							"&shopId=" + shopId + 
							"&uuid=" + uuid;
				$.ajax({
					url : url,
					data : param,
					type : "post",
					dataType : "json",
					success : function(data){
						var json = data.dataList;
						
						kendodata = new kendo.data.DataSource({
							schema: {
					           model: {
					               id: "dvcId",
					               fields: {
					            	   name : {},
					            	   jig : {},
					            	   checked : {},
					            	   dvcId : {}
					               }
					           }
							},
							group: { field: "jig" }
						})
						
						for(var i=0;i<json.length;i++){
							json[i].jig=decode(json[i].jig)
							kendodata.add(json[i])
						}
						
						$("#setting-grid-display").css({
							"width" : getElSize(3300)
						})
						
						$("#setting-grid-display").kendoGrid({
							dataSource : kendodata,
				            persistSelection: true,
							height : getElSize(4000),
							columns : [
								{ 
									selectable: true, 
									width: getElSize(100), 
									headerAttributes: {
									      style: "font-size : " + getElSize(150)
							    	},	
									attributes: {
									      style: "font-size : " + getElSize(150)
							    	}	
								},
								{ 
									field :"name",
									width: getElSize(300),
									headerTemplate : "장비명",
									headerAttributes: {
									      style: "font-size : " + getElSize(150)
							    	},
									attributes: {
								      style: "font-size : " + getElSize(150)
							    	}
								},
								{ 
									field :"jig",
									width: getElSize(200),
									headerTemplate : "직",
									groupHeaderTemplate: "#=value#",
									headerAttributes: {
									      style: "font-size : " + getElSize(150)
							    	},
									attributes: {
								      style: "font-size : " + getElSize(150)
							    	}
								},
							],
							change :function(arg){
								console.log("Checked")
								console.log(this.selectedKeyNames())
								
								saveList = this.selectedKeyNames()
							},
							dataBinding : function(){
								$("#setting-grid").css({
									"top" : "0"
								})
							},
							dataBound : function(){
								$(".k-no-text").css({
									"-webkit-transform": "scale(3)",
									"margin-bottom" : getElSize(-25),
									"margin-left" : getElSize(30)
								})
								
								$(".k-reset").css({
									"font-size" : getElSize(150)
								})
								
								$(".k-checkbox-label").first().css({
									"margin-bottom" : getElSize(70),
								})
								
								$(".btn").css({
									"font-size" : getElSize(150),
									"background" : "white",
									"border" : getElSize(10)+"px solid black",
									"border-radius" : getElSize(20),
									"width" : getElSize(2200),
									"padding" : getElSize(20),
									"margin-bottom" : getElSize(20),
									"margin-top" : getElSize(20)
								})
								
								$(".save-setting").css({
									"background" : "#4DA63A",
									"color" : "white",
									"margin-left" : getElSize(50),
									"padding" : getElSize(100),
									"width" : getElSize(3000),
									"margin-top" : getElSize(200)
								})
								
								$(".close-setting").css({
									"background" : "#F1F1F1",
									"color" : "black",
									"padding" : getElSize(100),
									"margin-left" : getElSize(50),
									"width" : getElSize(3000),
									"margin-top" : getElSize(200)
								})
								
								
								var grid = this;
								
								grid.items().each(function(){
									
									var data = grid.dataItem(this);
									
									if(data.checked==1){
										grid.select(this)
									}
																
								})
							}
						})
						
					}
				});
				
				
			}
		}).data("kendoDialog");
			
				
		settingDialogAccount = $("#setting-dialog-account").kendoDialog({
			modal :true,
			minWidth :getElSize(3500),
			minHeight :getElSize(5500),
			closable :false,
			title :false,
			visible :false,
			open :function(){
				
				var url = ctxPath + "/chart/getCheckedList.do";
				var param = "id=" + userId + 
							"&shopId=" + shopId + 
							"&uuid=" + uuid;
				$.ajax({
					url : url,
					data : param,
					type : "post",
					dataType : "json",
					success : function(data){
						var json = data.dataList;
						
						kendodata = new kendo.data.DataSource({
							schema: {
					           model: {
					               id: "dvcId",
					               fields: {
					            	   name : {},
					            	   jig : {},
					            	   checked : {},
					            	   dvcId : {}
					               }
					           }
							},
							group: { field: "jig" }
						})
						
						for(var i=0;i<json.length;i++){
							json[i].jig=decode(json[i].jig)
							kendodata.add(json[i])
						}
						
						$("#setting-grid-account").css({
							"width" : getElSize(3300)
						})
						
						$("#setting-grid-account").kendoGrid({
							dataSource : kendodata,
				            persistSelection: true,
							height : getElSize(4000),
							columns : [
								{ 
									selectable: true, 
									width: getElSize(100), 
									headerAttributes: {
									      style: "font-size : " + getElSize(150)
							    	},	
									attributes: {
									      style: "font-size : " + getElSize(150)
							    	}	
								},
								{ 
									field :"name",
									width: getElSize(300),
									headerTemplate : "장비명",
									headerAttributes: {
									      style: "font-size : " + getElSize(150)
							    	},
									attributes: {
								      style: "font-size : " + getElSize(150)
							    	}
								},
								{ 
									field :"jig",
									width: getElSize(200),
									headerTemplate : "직",
									groupHeaderTemplate: "#=value#",
									headerAttributes: {
									      style: "font-size : " + getElSize(150)
							    	},
									attributes: {
								      style: "font-size : " + getElSize(150)
							    	}
								},
							],
							change :function(arg){
								console.log("Checked")
								console.log(this.selectedKeyNames())
								
								saveList = this.selectedKeyNames()
							},
							dataBinding : function(){
								$("#setting-grid").css({
									"top" : "0"
								})
							},
							dataBound : function(){
								$(".k-no-text").css({
									"-webkit-transform": "scale(3)",
									"margin-bottom" : getElSize(-25),
									"margin-left" : getElSize(30)
								})
								
								$(".k-reset").css({
									"font-size" : getElSize(150)
								})
								
								$(".k-checkbox-label").first().css({
									"margin-bottom" : getElSize(70),
								})
								
								$(".btn").css({
									"font-size" : getElSize(150),
									"background" : "white",
									"border" : getElSize(10)+"px solid black",
									"border-radius" : getElSize(20),
									"width" : getElSize(2200),
									"padding" : getElSize(20),
									"margin-bottom" : getElSize(20),
									"margin-top" : getElSize(20)
								})
								
								$(".save-setting").css({
									"background" : "#4DA63A",
									"color" : "white",
									"margin-left" : getElSize(50),
									"padding" : getElSize(100),
									"width" : getElSize(3000),
									"margin-top" : getElSize(200)
								})
								
								$(".close-setting").css({
									"background" : "#F1F1F1",
									"color" : "black",
									"padding" : getElSize(100),
									"margin-left" : getElSize(50),
									"width" : getElSize(3000),
									"margin-top" : getElSize(200)
								})
								
								
								var grid = this;
								
								grid.items().each(function(){
									
									var data = grid.dataItem(this);
									
									if(data.checked==1){
										grid.select(this)
									}
																
								})
							}
						})
						
					}
				});
				
				
			}
		}).data("kendoDialog");
		
		signDialog = $("#signUp-dialog").kendoDialog({
			modal : true,
			minWidth: getElSize(3000),
			minHeight : getElSize(3500),
			closable: false,
			title: false,
			visible: false,
			close : function (){
				
				$("#sign-id-input").val("") 
				$("#sign-password-input").val("")
				$("#sign-repeat-password-input").val("")
				$("#sign-name-input").val("")
				
			},
			open : function(){
				
				$("#signUp-dialog").css({
					"height" : getElSize(3400)
				})
				
				$("#sign-login-table").css({
					"height" : "100%",
					"width" : "100%"
				})
				
				$("#id-div").css({
					"font-size" :getElSize(100)
				})
				
				$("#img-tr").css({
					"height" : "0%"
				})
				
				$("#sign-id-input, #sign-password-input,#sign-repeat-password-input, #sign-name-input").css({
					"font-size" : getElSize(300),
					"width" : getElSize(2500),
					"padding" : getElSize(50)
				})
				
				$("#sign-id-input").css({
					"border-top-left-radius" : getElSize(35),
					"border-top-right-radius" : getElSize(35),
					"border-top" : getElSize(10)+"px solid #EBEADB",
					"border-left" : getElSize(10)+"px solid #EBEADB",
					"border-right" : getElSize(10)+"px solid #EBEADB",
					"border-bottom" : getElSize(0)+"px solid #EBEADB",
				})
				
				$("#sign-repeat-password-input").css({
					"border" : getElSize(10)+"px solid #EBEADB",
					"border-bottom-left-radius" : getElSize(35),
					"border-bottom-right-radius" : getElSize(35),
					"margin-right" : getElSize(18)
				})
				
				$("#sign-password-input").css({
					"border-top" : getElSize(10)+"px solid #EBEADB",
					"border-bottom" : getElSize(0)+"px solid #EBEADB",
					"border-left" : getElSize(10)+"px solid #EBEADB",
					"border-right" : getElSize(10)+"px solid #EBEADB"
				})
				
				$("#sign-name-input").css({
					"border-top" : getElSize(10)+"px solid #EBEADB",
					"border-bottom" : getElSize(0)+"px solid #EBEADB",
					"border-left" : getElSize(10)+"px solid #EBEADB",
					"border-right" : getElSize(10)+"px solid #EBEADB"
				})
				
				$(".btn").css({
					"font-size" : getElSize(150),
					"background" : "white",
					"border" : getElSize(10)+"px solid black",
					"border-radius" : getElSize(20),
					"width" : getElSize(2500),
					"padding" : getElSize(100)
				})
				
				$("#btn-sign").css({
					"margin-top" : getElSize(50),
					"margin-right" : getElSize(22)
				})
				
				$("#close-btn").css({
					"margin-top" : getElSize(50),
					"margin-right" : getElSize(22)
				})
				
				$("#sign-btn-sign").css({
					"background" : "#4DA63A",
					"color" : "white"
				})
				
				$("#close-btn").css({
					"background" : "#3871A6",
					"color" : "white"
				})
				
			},show :function(){

				$(".k-widget").css({
					"top" : getElSize(500),
					"left" : getElSize(390)
				})
				
				
				
				$("#sign-logo").css({
					"width" : getElSize(1500)
				})
				
				$("#sign-logo2").css({
					"width" : getElSize(1000)
				})
				
			},close: function(e) {
				$(".k-widget").css({
					"top" : getElSize(500)
				})
 			}
		}).data("kendoDialog");
		
		dialog = $("#login-dialog").kendoDialog({
			modal : true,
			minWidth: getElSize(3000),
			minHeight : getElSize(3000),
			closable: false,
			title: false,
			visible: false,
			open : function(){
				
				$("#login-dialog").css({
					"height" : getElSize(2800)
				})
				
				$("#login-table").css({
					"height" : "100%",
					"width" : "100%"
				})
				
				$("#id-div").css({
					"font-size" :getElSize(100)
				})
				
				$("#img-tr").css({
					"height" : "0%"
				})
				
				$("#id-input, #password-input").css({
					"font-size" : getElSize(300),
					"width" : getElSize(2500),
					"padding" : getElSize(100)
				})
				
				$("#id-input").css({
					"border-top-left-radius" : getElSize(35),
					"border-top-right-radius" : getElSize(35),
					"border-top" : getElSize(10)+"px solid #EBEADB",
					"border-left" : getElSize(10)+"px solid #EBEADB",
					"border-right" : getElSize(10)+"px solid #EBEADB",
					"border-bottom" : getElSize(0)+"px solid #EBEADB",
				})
				
				$("#password-input").css({
					"border" : getElSize(10)+"px solid #EBEADB",
					"border-bottom-left-radius" : getElSize(35),
					"border-bottom-right-radius" : getElSize(35),
					"margin-right" : getElSize(18)
				})
				
				$(".btn").css({
					"font-size" : getElSize(150),
					"background" : "white",
					"border" : getElSize(10)+"px solid black",
					"border-radius" : getElSize(20),
					"width" : getElSize(2500),
					"padding" : getElSize(20)
				})
				
				$("#btn-sign").css({
					"margin-top" : getElSize(50),
					"margin-right" : getElSize(22)
				})
				
				$("#btn-login").css({
					"background" : "#4DA63A",
					"padding" : getElSize(100), 
					"color" : "white"
				})
				
				$("#btn-sign").css({
					"background" : "#3871A6",
					"padding" : getElSize(100), 
					"color" : "white"
				})
				
			},
			show :function(){
				
				$("#logo").css({
					"width" : getElSize(1500)
				})
				
				$("#logo2").css({
					"width" : getElSize(1000)
				})

				$(".k-widget").css({
					"top" : getElSize(500)
				})
				
			}
		}).data("kendoDialog");
		
		if( broswerInfo.toLowerCase().indexOf('android') > -1){
			window.JsBridge.checkLogin();
		}else if (broswerInfo.toLowerCase().indexOf('iphone') > -1){
			window.webkit.messageHandlers.checkLogin.postMessage("")
		}
		
	})
	
	function login(){
		
		$('body').loading({
			message: '기다려 주세요',
			theme: 'dark'
		});
		
		var url = ctxPath + "/chart/login.do";
		var $id = $("#id-input").val();
		var $pwd = $("#password-input").val();
		var param = "id=" + $id + 
					"&pwd=" + $pwd + 
					"&shopId=" + shopId;
		$.ajax({
			url : url,
			data : param,
			type : "post",
			dataType : "json",
			success : function(data){
				
				$('body').loading('stop');
				
				if(data.message!="failed"){
					
					$("#id-input").val("")
					$("#password-input").val("")
					
					window.sessionStorage.setItem("login_time", new Date().getTime());
					window.sessionStorage.setItem("login", "success");
					window.sessionStorage.setItem("user_id", $id);
					window.sessionStorage.setItem("level", data.level);
					window.sessionStorage.setItem("name", decode(data.name));
					
					userId=$id;
					
					dialog.close()
					
					console.log(data)
					
					
					$("#name-div").text("로그인 : " + decode(data.name))
					
					/* if(broswerInfo.indexOf("APP_AIDOO")>-1){
						window.JsBridge.successLogin($id, $pwd);
						window.JsBridge.getUuid();
					} else {
					} */
					
					// wilson0506
					if( broswerInfo.toLowerCase().indexOf('android') > -1){
						window.JsBridge.successLogin($id, $pwd);
						window.JsBridge.getUuid();
					}else if (broswerInfo.toLowerCase().indexOf('iphone') > -1){
						var text = '{"userid":"' + $id + '", "userpwd":"' + $pwd + '"}';
						var message = JSON.parse(text);
												
						window.webkit.messageHandlers.successLogin.postMessage(message);
						window.webkit.messageHandlers.getUuid.postMessage("");
					}
					
				}else{
					alert("계정 정보가 올바르지 않습니다. 다시 로그인 해 주세요.")
					
					if( broswerInfo.toLowerCase().indexOf('android') > -1){
						window.JsBridge.deleteCookie();
					}else if (broswerInfo.toLowerCase().indexOf('iphone') > -1){
						window.webkit.messageHandlers.deleteCookie.postMessage("");
					}
					
					dialog.open();
				}
			}
		});
	};
	
	
	function setuuid(args){
		uuid = args;
	}
	
	function notLogin(){
		dialog.open();
	}
	
	
	var userId;
	var uuid;
	function autoLogin(id,pwd,uuid){
		var url = ctxPath + "/chart/login.do";
		var param = "id=" + id + 
					"&pwd=" + pwd + 
					"&shopId=" + shopId + 
					"&uuid=" + uuid;
		$.ajax({
			url : url,
			data : param,
			type : "post",
			dataType : "json",
			success : function(data){
				
				userId = id;
				uuid = uuid;
				
				if(data.message!="failed"){
					
					window.sessionStorage.setItem("login_time", new Date().getTime());
					window.sessionStorage.setItem("login", "success");
					window.sessionStorage.setItem("user_id", id);
					window.sessionStorage.setItem("level", data.level);
					window.sessionStorage.setItem("uuid", uuid);
					
					userId=id;
					
					dialog.close()
					
					$("#name-div").text("로그인 : " + decode(data.name))
					
					/* if(broswerInfo.indexOf("APP_AIDOO")>-1){
						window.JsBridge.successLogin(id, pwd);
					} */
					
					if( broswerInfo.toLowerCase().indexOf('android') > -1){
						window.JsBridge.successLogin(id, pwd);
					}else if (broswerInfo.toLowerCase().indexOf('iphone') > -1){
						var text = '{"userid":"' + id + '", "userpwd":"' + pwd + '"}';
						var message = JSON.parse(text);
												
						window.webkit.messageHandlers.successLogin.postMessage(message);
					}
					
					
				}else{
					alert("계정 정보가 올바르지 않습니다. 다시 로그인 해 주세요.")
					
					if( broswerInfo.toLowerCase().indexOf('android') > -1){
						window.JsBridge.deleteCookie();
					}else if (broswerInfo.toLowerCase().indexOf('iphone') > -1){
						window.webkit.messageHandlers.deleteCookie.postMessage("");
					}
					
					dialog.open();
				}
			}
		});
	};
	
	function logout(){
		
		var r = confirm("로그아웃 합니까?");
		
		if(r==true){
			
			$('body').loading({
				message: '기다려 주세요',
				theme: 'dark'
			});
			
			id=userId;
			
			var url = ctxPath + "/chart/logout.do";
			var param = "id=" + id + 
						"&shopId=" + shopId + 
						"&uuid=" + uuid;
			
			console.log(url + param)
			
			$.ajax({
				url : url,
				data : param,
				type : "post",
				dataType : "json",
				success : function(data){
					
					window.sessionStorage.removeItem("login_time");
					window.sessionStorage.removeItem("login");
					window.sessionStorage.removeItem("user_id");
					window.sessionStorage.removeItem("level");
					window.sessionStorage.removeItem("uuid");
					
					userId=null;	
					
					$('body').loading('stop');
					
					
					
				},error :function(e1,e2,e3){
					$('body').loading('stop');
					console.log(e1,e2,e3)
				}
			})
			
			/* if(broswerInfo.indexOf("APP_AIDOO")>-1){
				window.JsBridge.deleteCookie();
			} */
			
			if( broswerInfo.toLowerCase().indexOf('android') > -1){
				window.JsBridge.deleteCookie();
			}else if (broswerInfo.toLowerCase().indexOf('iphone') > -1){
				window.webkit.messageHandlers.deleteCookie.postMessage("");
			}
			
			dialog.open();
		}else{
			
		}
	}
	
	function loginCheck(){
		if(window.sessionStorage.getItem("login")=="success"){
		}else{
			if(broswerInfo.indexOf("APP_AIDOO")>-1){
				dialog.open();
			}
		}
	}
	
	function signUp(){
		signDialog.open();
	}
	
	
	function setSignInfo(){
		
		if($("#sign-password-input").val()==$("#sign-repeat-password-input").val() && $("#sign-id-input").val()!=""){
			
			if($("#sign-id-input").val().length<5){
				alert("아이디는 5글자 이상이어야 합니다.")
			}else if($("#sign-password-input").val().length<6){
				alert("비밀번호는 6글자 이상이어야 합니다.")
			}else if($("#sign-name-input").val().length<3){
				alert("사용자 명은 3글자 이상이어야 합니다.")
			}else{
				var url = ctxPath + "/chart/setUser.do";
				var param = "id=" + $("#sign-id-input").val() + 
							"&pwd=" + $("#sign-password-input").val() + 
							"&shopId=" + shopId + 
							"&name=" + $("#sign-name-input").val();
				
				$('body').loading({
					message: '기다려 주세요',
					theme: 'dark'
				});
				
				$.ajax({
					url : url,
					data : param,
					type : "post",
					dataType : "text",
					success : function(data){
						
						
						$('body').loading('stop');
						
						if(data=="success"){
							
							alert("가입이 완료 되었습니다. 해당 정보로 로그인 해 주세요.");
							signDialog.close()
							
							$(".k-widget").css({
								"top" : getElSize(500)
							})
							
							$("#btn-login").css({
								"background" : "#4DA63A",
								"color" : "white"
							})
							
							$("#btn-sign").css({
								"background" : "#3871A6",
								"color" : "white"
							})
							
							
							$("#sign-id-input").val("") 
							$("#sign-password-input").val("")
							$("#sign-repeat-password-input").val("")
							$("#sign-name-input").val("")
							
							
						}else if(data=="duple"){
							alert("같은 아이디가 존재 합니다.");
						}else{
							alert("가입에 실패 하였습니다.");
						}
					}
				});
			}
			
		}else{
			$('body').loading('stop');
			alert("비밀서로가 서로 같지 않거나 아이디를 적지 않았습니다.");
		}
	}
	
	
	function openSetting(){
		//location.href="${ctxPath}/chart/mobile_alarm.do";
	    //window.addEventListener('popstate', hideModal, { once: true });
		settingDialog.open()
		window.history.pushState('settingDialogBackPressed', null, null);
	    window.history.pushState('dummy1', null, null);
	}
	
	function openSettingDisplay(){
		location.href="${ctxPath}/chart/mobile_screen.do";
	}
	
	function openSettingAccount(){
		location.href="${ctxPath}/chart/mobile_account.do";
	}
	
	function signDialogClose(){
		
		signDialog.close();
		
		$(".k-widget").css({
			"top" : getElSize(500)
		})
		
		$("#btn-login").css({
			"background" : "#4DA63A",
			"color" : "white"
		})
		
		$("#btn-sign").css({
			"background" : "#3871A6",
			"color" : "white"
		})
		
		$("#sign-id-input").val("") 
		$("#sign-password-input").val("")
		$("#sign-repeat-password-input").val("")
		$("#sign-name-input").val("")
		
	}
	
	function settingDialogClose(){
		settingDialog.close()
	}
	
	function settingDisplayDialogClose(){
		settingDialogDisplay.close()
	}
	
	function settingAccountDialogClose(){
		settingDialogAccount.close()
	}
	
	function successLogin(args){
		window.sessionStorage.setItem("uuid", args);
		uuid=args
	}
	
	var open = 1;
	
	function openSettingBtn(){
		
		if(open==1){
			$("#gear_img").css({
				"right" : getElSize(1300)
			})
			
			$("#login-img").css({
				"display" : "inline"
			})
			
			$("#setting-btn").css({
				"display" : "inline"
			})
			
			$("#setting-btn2").css({
				"display" : "inline"
			})
			
			$("#setting-btn3").css({
				"display" : "inline"
			})
			
			window.history.pushState('openSettingBtnBackPressed', null, null);
	    	//window.history.pushState('dummy2', null, null);
			
			open=0;
		}else{
			$("#gear_img").css({
				"right" : getElSize(0)
			})
			
			$("#login-img").css({
				"display" : "none"
			})
			
			$("#setting-btn").css({
				"display" : "none"
			})
			
			$("#setting-btn2").css({
				"display" : "none"
			})
			
			$("#setting-btn3").css({
				"display" : "none"
			})
			
			open=1;
		}
	}
	
	
	function debug(){
		$("#gear_img").css({
			"display" : "inline"
		})
	}
	
	
	function backPressed(){
		
		if(window1==true){
			
		}else if(windows2==true){
			
		}else if(windows3==true){
			
		}else{
			
		}
		
	}
	
	var isPmc = 1;
	var isLamp = 0;
	
	function getPmcData(){
		
		if(isPmc == 1){
			$('#pmc').hide();
			
			$("#pmc-btn").css({
				"color" : "#9999FF",
				"background" : "white"		
			});
			isPmc = 0;
		} else {
			$('#pmc').show();
			
			$("#pmc-btn").css({
				"color" : "white",
				"background" : "blue"		
			});
			isPmc = 1;
		}		
	}

	function getLampData(){
		
		if(isLamp == 1){		
			$('#lamp').hide();
			
			$("#lamp-btn").css({
				"color" : "#9999FF",
				"background" : "white"
			});
			isLamp = 0;
		} else {
			$('#lamp').show();
			
			$("#lamp-btn").css({
				"color" : "white",
				"background" : "blue"			
			});
			isLamp = 1;
		}
	}
	
	</script>



	<Center>
		<div id="header" ></div>
	</Center>	
	<img id="login-img" onclick="logout()" src="/DMT/images/logout.png"></img>
	
	<button id="setting-btn" onclick="openSetting()">알림 설정</button>
	<button id="setting-btn2" onclick="openSettingDisplay()">화면 설정</button>
	<button id="setting-btn3" onclick="openSettingAccount()">계정 정보</button>
	
	<img src="${ctxPath }/images/DashBoard/title_left.svg" id="title_left" class="title">
	<img src="${ctxPath }/images/DashBoard/title_main.svg" id="title_main" class="title">
	<button id="gear_img" class="title" onclick="openSettingBtn()"><img src="${ctxPath }/images/Gear.png" id="btn-img"></button>
	
	<div id="title_right" class="title"></div>
	
	<div id="name-div"></div>
	
	<font id="date"></font>
	<font id="time"></font>
	
	<span id="pmc_lamp">
		<button id="pmc-btn" onclick="getPmcData()">PMC</button>						
		<button id="lamp-btn" onclick="getLampData()">LAMP</button>						
	</span>
	
	<br>
	<br>
	<center>
		<div id="wraper">
			<table id="mainTable" border="2px" style="color: white" > 
				<tr style="font-size: 40px; font-weight: bold;" class="tr_table_fix_header">
					<td align="center">
						직
					</td>
					<td align="center">
						WC구분	
					</td>
					<td align="center">
						장비
					</td>
					<td align="center" id="pmc">
						PMC
					</td>
					<td align="center" id="lamp">
						LAMP
					</td>
					<td align="center">
						가동시간
					</td>
				</tr>
			</table>
		</div>
	</center>
	
	<div id="login-dialog">
		<table id="login-table">
			<tr id="img-tr">
				<td>
					<center><img src="/DMT/images/SmartFactory.png" id="logo" style="width: 219.213px; margin-top: 21.9213px;"></center>
					<center><img src="/DMT/images/logo.png" id="logo2" style="width: 131.528px; margin-bottom: 21.9213px;"></center>
				</td>
			</tr>
			<tr>
				<td>
					<center>
						<input id="id-input" autocomplete="off" onkeyup="this.value=this.value.replace(/[^a-zA-Z0-9]/g,'');"  type="text" maxlength=20 placeholder="ID">
						<br>
						<input id="password-input" autocomplete="off" onkeyup="this.value=this.value.replace(/[^a-zA-Z0-9]/g,'');"  type="password" maxlength=20 placeholder="PASSWORD">
					</center>
				</td>
			</tr>
			<tr>
				<td>
					<center>
					<button class="btn" id="btn-login" onclick="login()">LOGIN</button>
					<br>
					<button class="btn"  id="btn-sign" onclick="signUp()">가입하기</button>
					</center>
				</td>
			</tr>
		</table>
	</div>
	
	<div id="signUp-dialog">
		<table id="sign-login-table">
			<tr id="sign-img-tr">
				<td>
					<center><img src="/DMT/images/SmartFactory.png" id="sign-logo"></center>
					<center><img src="/DMT/images/logo.png" id="sign-logo2"></center>
				</td>
			</tr>
			<tr>
				<td>
					<center>
						<input class="id" id="sign-id-input" onkeyup="this.value=this.value.replace(/[^a-zA-Z0-9]/g,'');" autocomplete="off" type="text" maxlength=20 placeholder="ID">
						<br>
						<input class="name" id="sign-name-input" autocomplete="off" type="text" maxlength=10 placeholder="NAME">
						<br>
						<input id="sign-password-input" onkeyup="this.value=this.value.replace(/[^a-zA-Z0-9]/g,'');" autocomplete="off" type="password" maxlength=20 placeholder="PASSWORD">
						<br>
						<input id="sign-repeat-password-input" onkeyup="this.value=this.value.replace(/[^a-zA-Z0-9]/g,'');" autocomplete="off" type="password" maxlength=20 placeholder="PASSWORD">
					</center>
				</td>
			</tr>
			<tr>
				<td>
					<center>
					<button class="btn"  id="sign-btn-sign" onclick="setSignInfo()">가입하기</button>
					<br>
					<button class="btn"  id="close-btn" onclick="signDialogClose()">CLOSE</button>
					</center>
				</td>
			</tr>
		</table>
	</div>
	
	<div id="setting-dialog">
		<!-- wilson2 -->
		<div id="mobileSetting">
			<label>알림톡 : </label> <input type="tel" id="mobile" placeholder="전화번호" >
		</div>
		<div id="alarmSetting">
			<label id="weekday_title">평일주간 <input type="checkbox" id="weekday" class="chkbox"></label>
			<label id="night_title">평일야간 <input type="checkbox" id="nightTime" class="chkbox"></label>
			<label id="holiday_title">주말/휴일 <input type="checkbox" id="holiday" class="chkbox"></label>
		</div>
		<div id="setting-grid">
		</div>
		<button class="btn save-setting" onclick="pushSave()"> 저장 </button>
		<br>
		<button class="btn close-setting" onclick="settingDialogClose()"> 닫기 </button>
	</div>
	
	<div id="setting-dialog-display">
		<div id="setting-grid-display">
		</div>
		<button class="btn save-setting" onclick="pushSave()"> 저장 </button>
		<br>
		<button class="btn close-setting" onclick="settingDisplayDialogClose()"> 닫기 </button>
	</div>
	
	<div id="setting-dialog-account">
		<div id="setting-grid-account">
		</div>
		<button class="btn save-setting" onclick="pushSave()"> 저장 </button>
		<br>
		<button class="btn close-setting" onclick="settingAccountDialogClose()"> 닫기 </button>
	</div>
</body>
</html>