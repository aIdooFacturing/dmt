package com.unomic.cnc.communication.params;

import com.unomic.cnc.StringUtils;

public class RepHistoryParam
{
	private String name;
	private int size;
	private int result;
	
	public String getName()
	{
		return name;
	}
	public void setName(String name)
	{
		name = StringUtils.resize(name, 96);
		this.name = name;
	}
	public int getSize()
	{
		return size;
	}
	public void setSize(int size)
	{
		this.size = size;
	}
	public int getResult()
	{
		return result;
	}
	public void setResult(int result)
	{
		this.result = result;
	}
}
