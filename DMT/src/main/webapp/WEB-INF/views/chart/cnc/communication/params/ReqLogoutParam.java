package com.unomic.cnc.communication.params;

import com.unomic.cnc.StringUtils;

public class ReqLogoutParam implements Params
{
	private String account;
	private String passwd;
	
	public String getAccount()
	{
		return account;
	}
	public void setAccount(String account)
	{
		account = StringUtils.resize(account, CMD_LOGIN_REQ_FIELD_ACCOUNT_SIZE);
		this.account = account;
	}
	public String getPasswd()
	{
		return passwd;
	}
	public void setPasswd(String passwd)
	{
		passwd = StringUtils.resize(passwd, CMD_LOGIN_REQ_FIELD_PASSWD_SIZE);
		this.passwd = passwd;
	}
	
	@Override
	public String resizeString()
	{
		return account + passwd;
	}
	
	@Override
	public int getMessageLength()
	{
		return resizeString().length();
	}
}
