package com.unomic.cnc.data;

import java.io.UnsupportedEncodingException;
import java.nio.ByteBuffer;
import java.util.ArrayList;

import com.unomic.cnc.CNCUtils;
import com.unomic.cnc.Var;

public class PathMemory implements MTES_TYPES
{
    private static final boolean DEBUG = true;
    private final String CLASSNAME = this.getClass().getSimpleName();
    
//	private final static int MAX_NUM_OF_PATH		= 2;
	private final static int MAX_NUM_OF_AXIS		= 8;
	private final static int MAX_NUM_OF_COORDINATE	= 7;
	private final static int SIZE_OF_ALARM_LIST		= 16;
	private final static int MAX_NUM_OF_SPINDLE		= 6;
	
//	private final static int MAX_NUM_OF_TIME		= 4;
	private final static int MAX_G_GROUP			= 32;
	
	private final static int INDEX_OF_ABSOLUTE		= 0;
	private final static int INDEX_OF_RELATIVE		= 1;
	private final static int INDEX_OF_MACHINE		= 2;
	private final static int INDEX_OF_DIST_TO_GO	= 3;
	
	
	/**
	 * The order in which Axis is shown.
	 */
	int[] 		axis_order 	= new int[MAX_NUM_OF_AXIS];			// newly added by DOOSAN 2011.04.20

	/**
	 * Axis Name
	 */
	byte[][][] axes_name = new byte[2][MAX_NUM_OF_AXIS][3];	// axes_name[0] : Absolute type
															// axes_name[1] : Increment type
	/**
	 * Axis Position
	 */
	byte[][][] axes = new byte[MAX_NUM_OF_COORDINATE][MAX_NUM_OF_AXIS][12];	// [type][axis][position]
	                                                                        // 2013.4.23 [10] -> [12]
	
	ArrayList<Axis>[] axesList;
	
	/**
	 * Alarm
	 */
	Alarm[] alarm = new Alarm[SIZE_OF_ALARM_LIST];				// Use structure type
	
	/**
	 * Spindle
	 */
	byte[][] spindle_name = new byte[MAX_NUM_OF_SPINDLE][3];	
	double[] spindle_actual_speed = new double[MAX_NUM_OF_SPINDLE];
	double[] spindle_target_speed = new double[MAX_NUM_OF_SPINDLE];
	float[] spindle_load = new float[MAX_NUM_OF_SPINDLE];
	
	/*
	 * Feed
	 * UINT16 -> INT
	 */
	float		actual_feed;
	float		target_feed;
	int			feed_unit;
	
	/*
	 * Time
	 * UINT32 -> LONG
	 */
	long[]		time_min 	= new long[4];				
	long[]     	time_msec	= new long[4];               
	/*
	 * Modal
 	 * UINT32 -> LONG
	 */
	short[] 	G_modal 	= new short[MAX_G_GROUP];	
	short[]		Aux_code	= new short[32];
	long		S_code;

	String[] 	G_modals 	= new String[MAX_G_GROUP];
	
	/*
	 * NC Status
	 */
	String		nc_status;
	String		nc_mode;
	
	/*
	 * machined part count
	 * UINT32	->	long
	 */
	long num_of_machined_parts;
	long total_num_of_machined_parts;
	long num_of_required_parts;
	
	/*
	 * program
	 */
	byte[] prog_name = new byte[12];
	
	
	byte[][] program = new byte[17][60];
	long CaretLineNo;
	
	ByteBuffer byteBuffer = ByteBuffer.allocate(Var.byteLength);

	public PathMemory()
	{
	}
	
	public PathMemory(byte[] buf)
	{		
	}
	
	public void refresh(byte[] buf, int length)
	{
		byteBuffer.put(buf).rewind();
		byteBuffer.rewind();
		
		// INT32 axis_order[MAX_NUM_OF_AXIS]
		for(int i=0; i < axis_order.length; i++)
			axis_order[i] = byteBuffer.getInt();
	
		// char axes_name[2][MAX_NUM_OF_AXIS][3]
		for(int i=0; i < axes_name.length; i++)
		{
			for(int j=0; j < axes_name[i].length; j++)
			{
				for(int k=0; k < axes_name[i][j].length; k++)
				{
					axes_name[i][j][k] = byteBuffer.get();
				}
			}
		}
		
		// char axes[MAX_NUM_OF_COORDINATE][MAX_NUM_OF_AXIS][10];
		for (int i=0; i < axes.length; i++)
		{
			for (int j=0; j < axes[i].length; j++)
			{
				for (int k=0; k < axes[i][j].length; k++)
				{
					axes[i][j][k] = byteBuffer.get();
				}
			}
		}
		
		// ALARM alarm[SIZE_OF_ALARM_LIST]; // Use structure type.
		for (int i=0; i < alarm.length; i++)
		{
			short alarm_type;
			byte alarm_number[] = new byte[6];		// 7 byte
			byte alarm_message[] = new byte[256];	// 256 byte
			byte mtes_action = 0;
			
			alarm_type = byteBuffer.getShort();
			byteBuffer.get(alarm_number);
			byteBuffer.get(alarm_message);
			byteBuffer.get(mtes_action);
			if(DEBUG) CNCUtils.Logi(CLASSNAME, "pathMemory -> alarm["+i+"] alarm_type: " + alarm[i].alarm_type + ", alarm_number: " + byteToString(alarm_number) + ", alarm_message: " + byteToString(alarm_message));
			alarm[i] = new Alarm(alarm_type, alarm_number, alarm_message, mtes_action);
		}
		
		// char spindle_name[MAX_NUM_OF_SPINDLE][2];
		//Log.d(TAG, "spindle_name.length : " + spindle_name.length);
		for (int i=0; i < spindle_name.length; i++)
		{
			for(int j=0; j < spindle_name[i].length; j++)
			{
				spindle_name[i][j] = byteBuffer.get();			
			}
		}
		
		// dummy bytes passes
		byteBuffer.position(byteBuffer.position() + 6);	
		
		// unsigned INT32 spindle_actual_speed[MAX_NUM_OF_SPINDLE];
		for (int i=0; i < spindle_actual_speed.length; i++)
			spindle_actual_speed[i] = byteBuffer.getDouble();	
//			spindle_actual_speed[i] = Double.longBitsToDouble((long)byteBuffer.getDouble());	
		
		// unsigned INT32 spindle_target_speed[MAX_NUM_OF_SPINDLE];
		for (int i=0; i < spindle_target_speed.length; i++)
			spindle_target_speed[i] = byteBuffer.getDouble();
//			spindle_target_speed[i] = Double.longBitsToDouble((long)byteBuffer.getDouble());	
		
		// unsigned float spindle_load[MAX_NUM_OF_SPINDLE];
		for (int i=0; i < spindle_load.length; i++)
		{
//			spindle_load[i] = Float.intBitsToFloat((int)byteBuffer.getFloat());
			spindle_load[i] = byteBuffer.getFloat();
		}
		
		// float actual_feed;
		actual_feed = byteBuffer.getFloat();

		// float target_feed;
		target_feed = byteBuffer.getFloat();

		// EnFeedType feed_unit;
		feed_unit = unsigned16(byteBuffer.getShort());
		
		// dummy bytes passes
		byteBuffer.position(byteBuffer.position() + 2);
	
		// unsigned INT32 time[4]
		for (int i=0; i < time_min.length; i++)
			time_min[i] = unsigned32(byteBuffer.getInt());
		
		// unsigned INT32 time[4]
		for (int i=0; i < time_msec.length; i++)
			time_msec[i] = unsigned32(byteBuffer.getInt());
								
		// unsigned INT16 G_modal[MAX_G_GROUP];
		for (int i=0; i < G_modal.length; i++)
			G_modal[i] = byteBuffer.getShort();
		
		// unsigned INT16 G_modal[MAX_G_GROUP];
		for (int i=0; i < Aux_code.length; i++)
			Aux_code[i] = byteBuffer.getShort();
		
		// unsigned INT16 S_code;
		S_code = unsigned32(byteBuffer.getInt());

		// EnNCStatus nc_status;
		byte[] strBytes = new byte[8];
		byteBuffer.get(strBytes);
		nc_status = new String(strBytes).trim();
		//Log.d(TAG, "EnNCStatus");
		//for(int i=0;i<strBytes.length;i++){
			//Log.d(TAG, String.format("[0x%02x]", strBytes[i]));
		//}
		
		// EnNCMode nc_mode;
		strBytes = new byte[8];
		byteBuffer.get(strBytes);
		nc_mode = new String(strBytes).trim();
		//Log.d(TAG, "EnNCMode");
		//for(int i=0;i<strBytes.length;i++){
			//Log.d(TAG, String.format("[0x%02x]", strBytes[i]));
		//}
		
		// unsigned INT32 num_of_machined_parts;
		num_of_machined_parts = unsigned32(byteBuffer.getInt());
//		num_of_machined_parts = byteBuffer.getInt();
		
		// unsigned INT32 total_num_of_machined_parts;
		total_num_of_machined_parts = unsigned32(byteBuffer.getInt());
//		total_num_of_machined_parts = byteBuffer.getInt();
		
		// unsigned INT32 num_of_required_parts;	
		num_of_required_parts = unsigned32(byteBuffer.getInt());
//		num_of_required_parts = byteBuffer.getInt();
		
		// char prog_name[12];
		for(int i=0; i < prog_name.length; i++)
			prog_name[i] = byteBuffer.get();
				
		// char program_view[17][60];
		for(int i=0; i < program.length; i++)
		{
			byte[] temp = new byte[60] ;
			byteBuffer.get(temp);
			program[i] = temp;
			//Log.d(TAG, "program_view" + new String(program[i]) + " : "+ new String(temp));
		}
		//Log.d(TAG, "program_view[0]" + new String(program[0]));
		
		// dummy bytes passes
		byteBuffer.position(byteBuffer.position() + 4);
		
		// INT64 CaretLineNo;
		CaretLineNo = byteBuffer.getLong();
		/*
		// string used_ip;
		byte[] strBuffer = new byte[15];
		byteBuffer.get(strBuffer);
		used_ip = new String(strBuffer).trim();
		
		// string used_port;
		strBuffer = new byte[5];
		byteBuffer.get(strBuffer);
		used_port = new String(strBuffer).trim();*/
		
		
		// string etc;
		//strBuffer = new byte[256];
		//strBuffer = byteBuffer.get(strBuffer, byteBuffer.position(), 256).array();
		
		byteBuffer.rewind();
	}
	
	public PathMemory(byte[] buf, int length)
	{
		byteBuffer.put(buf).rewind();
		byteBuffer.rewind();
		
		// INT32 axis_order[MAX_NUM_OF_AXIS]
		for(int i=0; i < axis_order.length; i++)
			axis_order[i] = byteBuffer.getInt();
	
		// char axes_name[2][MAX_NUM_OF_AXIS][3]
		for(int i=0; i < axes_name.length; i++)
		{
			for(int j=0; j < axes_name[i].length; j++)
			{
				for(int k=0; k < axes_name[i][j].length; k++)
				{
					axes_name[i][j][k] = byteBuffer.get();
				}
			}
		}
		
		// char axes[MAX_NUM_OF_COORDINATE][MAX_NUM_OF_AXIS][10];
		for (int i=0; i < axes.length; i++)
		{
			for (int j=0; j < axes[i].length; j++)
			{
				for (int k=0; k < axes[i][j].length; k++)
				{
					axes[i][j][k] = byteBuffer.get();
				}
			}
		}

		// ALARM alarm[SIZE_OF_ALARM_LIST]; // Use structure type.
		for (int i=0; i < alarm.length; i++)
		{
			short alarm_type;
			byte alarm_number[] = new byte[6];		// 7 byte
			byte alarm_message[] = new byte[256];	// 256 byte
			byte mtes_action = 0;
			
			alarm_type = byteBuffer.getShort();
			byteBuffer.get(alarm_number);
			byteBuffer.get(alarm_message);
			byteBuffer.get(mtes_action);

			alarm[i] = new Alarm(alarm_type, alarm_number, alarm_message, mtes_action);
		}
		
		// char spindle_name[MAX_NUM_OF_SPINDLE][2];
		//Log.d(TAG, "spindle_name.length : " + spindle_name.length);
		for (int i=0; i < spindle_name.length; i++)
		{
			for(int j=0; j < spindle_name[i].length; j++)
			{
				spindle_name[i][j] = byteBuffer.get();			
			}
		}
		
		// dummy bytes passes
		byteBuffer.position(byteBuffer.position() + 6);	
		
		// unsigned INT32 spindle_actual_speed[MAX_NUM_OF_SPINDLE];
		for (int i=0; i < spindle_actual_speed.length; i++)
			spindle_actual_speed[i] = byteBuffer.getDouble();	
//			spindle_actual_speed[i] = Double.longBitsToDouble((long)byteBuffer.getDouble());	
		
		// unsigned INT32 spindle_target_speed[MAX_NUM_OF_SPINDLE];
		for (int i=0; i < spindle_target_speed.length; i++)
			spindle_target_speed[i] = byteBuffer.getDouble();
//			spindle_target_speed[i] = Double.longBitsToDouble((long)byteBuffer.getDouble());	
		
		// unsigned float spindle_load[MAX_NUM_OF_SPINDLE];
		for (int i=0; i < spindle_load.length; i++)
		{
//			spindle_load[i] = Float.intBitsToFloat((int)byteBuffer.getFloat());
			spindle_load[i] = byteBuffer.getFloat();
		}
		
		// float actual_feed;
		actual_feed = byteBuffer.getFloat();

		// float target_feed;
		target_feed = byteBuffer.getFloat();

		// EnFeedType feed_unit;
		feed_unit = unsigned16(byteBuffer.getShort());
		
		// dummy bytes passes
		byteBuffer.position(byteBuffer.position() + 2);
		
		// unsigned INT32 time[4]
		for (int i=0; i < time_min.length; i++)
			time_min[i] = unsigned32(byteBuffer.getInt());
		
		// unsigned INT32 time[4]
		for (int i=0; i < time_msec.length; i++)
			time_msec[i] = unsigned32(byteBuffer.getInt());
								
		// unsigned INT16 G_modal[MAX_G_GROUP];
		for (int i=0; i < G_modal.length; i++)
			G_modal[i] = byteBuffer.getShort();
		
		// unsigned INT16 G_modal[MAX_G_GROUP];
		for (int i=0; i < Aux_code.length; i++)
			Aux_code[i] = byteBuffer.getShort();
		
		// unsigned INT16 S_code;
		S_code = unsigned32(byteBuffer.getInt());

		// EnNCStatus nc_status;
		byte[] strBytes = new byte[8];
		byteBuffer.get(strBytes);
		nc_status = new String(strBytes).trim();
		//Log.d(TAG, "EnNCStatus");
		//for(int i=0;i<strBytes.length;i++){
			//Log.d(TAG, String.format("[0x%02x]", strBytes[i]));
		//}
		
		// EnNCMode nc_mode;
		strBytes = new byte[8];
		byteBuffer.get(strBytes);
		nc_mode = new String(strBytes).trim();
		//Log.d(TAG, "EnNCMode");
		//for(int i=0;i<strBytes.length;i++){
			//Log.d(TAG, String.format("[0x%02x]", strBytes[i]));
		//}
		
		// unsigned INT32 num_of_machined_parts;
		num_of_machined_parts = unsigned32(byteBuffer.getInt());
//		num_of_machined_parts = byteBuffer.getInt();
		
		// unsigned INT32 total_num_of_machined_parts;
		total_num_of_machined_parts = unsigned32(byteBuffer.getInt());
//		total_num_of_machined_parts = byteBuffer.getInt();
		
		// unsigned INT32 num_of_required_parts;	
		num_of_required_parts = unsigned32(byteBuffer.getInt());
//		num_of_required_parts = byteBuffer.getInt();
		
		// char prog_name[12];
		for(int i=0; i < prog_name.length; i++)
			prog_name[i] = byteBuffer.get();
				
		// char program_view[17][60];
		for(int i=0; i < program.length; i++)
		{
			byte[] temp = new byte[60] ;
			byteBuffer.get(temp);
			program[i] = temp;
			//Log.d(TAG, "program_view" + new String(program[i]) + " : "+ new String(temp));
		}
		//Log.d(TAG, "program_view[0]" + new String(program[0]));
		
		// dummy bytes passes
		byteBuffer.position(byteBuffer.position() + 4);
		
		// INT64 CaretLineNo;
		CaretLineNo = byteBuffer.getLong();
		/*
		// string used_ip;
		byte[] strBuffer = new byte[15];
		byteBuffer.get(strBuffer);
		used_ip = new String(strBuffer).trim();
		
		// string used_port;
		strBuffer = new byte[5];
		byteBuffer.get(strBuffer);
		used_port = new String(strBuffer).trim();*/
		
		
		// string etc;
		//strBuffer = new byte[256];
		//strBuffer = byteBuffer.get(strBuffer, byteBuffer.position(), 256).array();
		
		byteBuffer.rewind();
	}
	
	public float arr2float (byte[] arr, int start) 
	{
        int i = 0;
        int len = 4;
        int cnt = 0;
        byte[] tmp = new byte[len];

        for (i = start; i < (start + len); i++) 
        {
              tmp[cnt] = arr[i];
              cnt++;
        }

        int accum = 0;
        i = 0;
        for ( int shiftBy = 0; shiftBy < 32; shiftBy += 8 ) 
        {
              accum |= ( (long)( tmp[i] & 0xff ) ) << shiftBy;
              i++;
        }
        return Float.intBitsToFloat(accum);
	}
	
	public float int2float (int bits)
	{
		int s = ((bits >> 31) == 0) ? 1 : -1;
		int e = ((bits >> 23) & 0xff);
		int m = (e == 0) ?
	                 (bits & 0x7fffff) << 1 :
	                 (bits & 0x7fffff) | 0x800000;

		float f = (float)(s * m * Math.pow(2, e-150));
		return f;
	}
	
	public String getAbsoluteAxis()
	{
		String makeString = "";
		
		int maxDataIndex = 0;
		
		for(int i=0; i<axis_order.length;i++, maxDataIndex++)
		{
			//Log.i(TAG,"axis order : "+axis_order[i]);
			if(axis_order[i] == 0)
			{
				break;
			}
		}
			
		//Log.d(TAG, "ArrayList index : " + maxDataIndex);
		ArrayList<Axis> axesList = new ArrayList<Axis>(maxDataIndex);
		for(int i=0;i < maxDataIndex; i++ )
		{
			int index = axis_order[i];
			Axis axis = new Axis(new String(axes_name[0][i]).trim(),new String(axes[INDEX_OF_ABSOLUTE][i]).trim());
			axesList.add(index-1, axis);
		}
		
		for(Axis axis : axesList){
			makeString += axis.toString();
		}
		
		return makeString;
	}
	
	public ArrayList<Axis> getAbsoluteAxisArray()
	{
		int maxDataIndex = 0;
		
		for(int i=0; i<axis_order.length;i++, maxDataIndex++)
		{
			//Log.i(TAG,"axis order : "+axis_order[i]);
			if(axis_order[i] == 0)
			{
				break;
			}
		}
			
		//Log.d(TAG, "ArrayList index : " + maxDataIndex);
		ArrayList<Axis> axesList = new ArrayList<Axis>(maxDataIndex);
		for(int i=0;i < maxDataIndex; i++ ){
			int index = axis_order[i];
			Axis axis = new Axis(new String(axes_name[0][i]).trim(),new String(axes[INDEX_OF_ABSOLUTE][i]).trim());
			axesList.add(index-1, axis);
		}
		
		return axesList;
	}

	public String getRelativeAxis()
	{
		String makeString = "";
		
		int maxDataIndex = 0;
		
		for(int i=0; i<axis_order.length;i++, maxDataIndex++)
			if(axis_order[i] == 0)
				break;

		//Log.d(TAG, "ArrayList index : " + maxDataIndex);
		ArrayList<Axis> axesList = new ArrayList<Axis>(maxDataIndex);
		for(int i=0;i < maxDataIndex; i++ )
		{
			int index = axis_order[i];
			Axis axis = new Axis(new String(axes_name[1][i]).trim(),new String(axes[INDEX_OF_RELATIVE][i]).trim());
			axesList.add(index-1, axis);			
		}
		
		for(Axis axis : axesList)
		{
			makeString += axis.toString();
		}
		return makeString;
	}

	public ArrayList<Axis> getRelativeAxisArray()
	{
		int maxDataIndex = 0;
		
		for(int i=0; i<axis_order.length;i++, maxDataIndex++)
			if(axis_order[i] == 0)
				break;

		//Log.d(TAG, "ArrayList index : " + maxDataIndex);
		ArrayList<Axis> axesList = new ArrayList<Axis>(maxDataIndex);
		for(int i=0;i < maxDataIndex; i++ )
		{
			int index = axis_order[i];
			Axis axis = new Axis(new String(axes_name[1][i]).trim(),new String(axes[INDEX_OF_RELATIVE][i]).trim());
			axesList.add(index-1, axis);			
		}
		return axesList;
	}

	public String getMachine()
	{
		String makeString = "";
		
		int maxDataIndex = 0;
		
		for(int i=0; i<axis_order.length;i++, maxDataIndex++)
			if(axis_order[i] == 0)
				break;

		//Log.d(TAG, "ArrayList index : " + maxDataIndex);
		ArrayList<Axis> axesList = new ArrayList<Axis>(maxDataIndex);
		for(int i=0;i < maxDataIndex; i++ )
		{
			int index = axis_order[i];
			Axis axis = new Axis(new String(axes_name[0][i]).trim(),new String(axes[INDEX_OF_MACHINE][i]).trim());
			axesList.add(index-1, axis);			
		}
		
		for(Axis axis : axesList)
		{
			makeString += axis.toString();
		}
		return makeString;
	}

	public ArrayList<Axis> getMachineArray()
	{
		int maxDataIndex = 0;
		
		for(int i=0; i<axis_order.length;i++, maxDataIndex++)
			if(axis_order[i] == 0)
				break;

		//Log.d(TAG, "ArrayList index : " + maxDataIndex);
		ArrayList<Axis> axesList = new ArrayList<Axis>(maxDataIndex);
		for(int i=0;i < maxDataIndex; i++ )
		{
			int index = axis_order[i];
			Axis axis = new Axis(new String(axes_name[0][i]).trim(),new String(axes[INDEX_OF_MACHINE][i]).trim());
			axesList.add(index-1, axis);			
		}
		
		return axesList;
	}

	public String getDistToGo()
	{
		String makeString = "";
		int maxDataIndex = 0;
		
		for(int i=0; i<axis_order.length;i++, maxDataIndex++)
			if(axis_order[i] == 0)
				break;

		//Log.d(TAG, "ArrayList index : " + maxDataIndex);
		ArrayList<Axis> axesList = new ArrayList<Axis>(maxDataIndex);
		for(int i=0;i < maxDataIndex; i++ )
		{
			int index = axis_order[i];
			Axis axis = new Axis(new String(axes_name[0][i]).trim(),new String(axes[INDEX_OF_DIST_TO_GO][i]).trim());
			axesList.add(index-1, axis);			
		}
		
		for(Axis axis : axesList)
		{
			makeString += axis.toString();
		}
		return makeString;
	}

	public ArrayList<Axis> getDistToGoArray()
	{
		int maxDataIndex = 0;
		
		for(int i=0; i<axis_order.length;i++, maxDataIndex++)
			if(axis_order[i] == 0)
				break;

		//Log.d(TAG, "ArrayList index : " + maxDataIndex);
		ArrayList<Axis> axesList = new ArrayList<Axis>(maxDataIndex);
		for(int i=0;i < maxDataIndex; i++ )
		{
			int index = axis_order[i];
			Axis axis = new Axis(new String(axes_name[0][i]).trim(),new String(axes[INDEX_OF_DIST_TO_GO][i]).trim());
			axesList.add(index-1, axis);			
		}
		return axesList;
	}

 	public String getSpindleName()
 	{
 		return new String(spindle_name[0]);
 	}
 	public double getSpindleActualSpeed()
 	{
 		return spindle_actual_speed[0] + 0.5;
 	}
 	public double getSpindleTargetSpeed()
 	{
 		return spindle_target_speed[0] + 0.5;
 	}
 	public float getSpindleLoad()
 	{
 		return spindle_load[0];
 	}
	public float getActualFeed()
	{
		return actual_feed + 0.5f;
	}
	public float getTargetFeed()
	{
		return target_feed + 0.5f;
	}

	public int getEnFeedType()
	{
		return feed_unit;
	}

	public String getRunTimeMainHHMMSS()
	{
		String makeString = "";
		
		makeString += "" + time_min[0]/60 + "H\n" + (time_min[0]%60) + "M\n" + time_msec[0]/1000 + "S";
		return makeString; 
	}
	
	public String getRunTimeMainHH()
	{
		String makeString = "";
		
		makeString = "" + time_min[0]/60 + "H";
		return makeString; 
	}
	public String getRunTimeMainMM()
	{
		String makeString = "";
		
		makeString = "" + (time_min[0]%60) + "M";
		return makeString; 
	}
	public String getRunTimeMainSS()
	{
		String makeString = "";
		
		makeString = "" + time_msec[0]/1000 + "S";
		return makeString; 
	}
	
	public String getRunTimeHHMMSS()
	{
		String makeString = "";
		
		String m = ""+ (time_min[0]%60);
		if(m.length()==1)
		{
			m = "0"+m;
		}
		String s = ""+ ((time_msec[0]/1000)%60);
		if(s.length()==1)
		{
			s = "0"+s;
		}
		
		makeString += "" + time_min[0]/60 + "H " + m + "M " + s + "S";
		return makeString;
	}
	
	public String getCutTimeHHMMSS()
	{
		String makeString = "";
		
		String m = ""+ (time_min[1]%60);
		if(m.length()==1)
		{
			m = "0"+m;
		}
		String s = ""+ ((time_msec[1]/1000)%60);
		if(s.length()==1)
		{
			s = "0"+s;
		}
		
		makeString += "" + time_min[1]/60 + "H " + m + "M " + s + "S";
		return makeString;
	}
	
	public String getTimerTimeHHMMSS()
	{
		String makeString = "";
		
		String m = ""+ (time_min[2]%60);
		if(m.length()==1)
		{
			m = "0"+m;
		}
		String s = ""+ ((time_msec[2]/1000)%60);
		if(s.length()==1)
		{
			s = "0"+s;
		}
		
		makeString += "" + time_min[2]/60 + "H " + m + "M " + s + "S";
		return makeString;
	}
	
	public String getCycleTimeHHMMSS()
	{
		String makeString = "";
		
		String m = ""+ (time_min[3]%60);
		if(m.length()==1)
		{
			m = "0"+m;
		}
		String s = ""+ ((time_msec[3]/1000)%60);
		if(s.length()==1)
		{
			s = "0"+s;
		}
		
		makeString += "" + time_min[3]/60 + "H " + m + "M " + s + "S";
		return makeString;
	}
	
	public String[] getG_modals()
	{
		for (int i=0; i < G_modal.length; i++)
		{
			String G_modala, G_modalf, G_modald;
			G_modala = ""+G_modal[i];
			//G_modala = "999";
			//Log.i(TAG,"modal : " + i + ":" + G_modala + ":");
			
			if(G_modala.length()>2)
			{
				G_modalf = G_modala.substring(0, 2);
				G_modald = G_modala.substring(2, 3);
				if(G_modald.equals("0"))
				{
					G_modals[i] = G_modalf;
				}
				else
				{
					G_modals[i] = G_modalf + "." + G_modald;
				}
				
			}
			else if(G_modala.length()==2)
			{
				if(G_modala.equals("-1"))
				{
					G_modals[i] = "";
				}
				else
				{
					G_modalf = "0" + G_modala.substring(0, 1);
					G_modald = G_modala.substring(1);
					if(G_modald.equals("0"))
					{
						G_modals[i] = G_modalf;
					}
					else
					{
						G_modals[i] = G_modalf + "." + G_modald;
					}
				}
				
			}
			else
			{
				if(G_modala.equals("0"))
				{
					G_modals[i] = G_modala;
				}
				else
				{
					G_modals[i] = "0." + G_modala;
				}
			}
		}
		return G_modals;
	}
	
	public short[] getG_modal()
	{
		return G_modal;
	}
	
	public short[] getAux_code()
	{
		return Aux_code;
	}
	
	public long getS_code(){
		return S_code;
	}

	public String getNcStatus()
	{
		return nc_status;
	}
	public String getNcMode()
	{
		return nc_mode;
	}

	public long getNumOfMachinedParts()
	{
		return num_of_machined_parts;
	}
	public long getTotalNumOfMachinedParts()
	{
		return total_num_of_machined_parts;
	}
	public long getNumOfRequiredParts()
	{
		return num_of_required_parts;
	}

	public String getProg_name()
	{
		return new String(prog_name).trim();
	}
		
	public byte[][] getProgram_view()
	{
		/*String makeString = "";
		
		for (int i=0;i<program.length;i++){
			Log.d(TAG,"program_view : " + new String(program[i]));
			makeString += (new String(program[i])).trim() + "\n";

		}*/
		return program;
	}

	public long getCaretLineNo()
	{
		return CaretLineNo;
	};
	
	public static long unsigned32(int n) 
	{
		return n & 0xFFFFFFFFL;
	}
	public static int unsigned16(short n) 
	{
		return n & 0xFFFF;
	}
	
	public int getAlarmCount()
	{
		int alarmCount = 0;
		for (int i=0; i < alarm.length; i++)
		{
			if(!new String(alarm[i].alarm_number).trim().equals(""))
			{
				alarmCount++;
			}
		}
		return alarmCount;
	}
	
	public String getAlarmNumber()
	{
		String alarmNumber = "";
		for (int i=0; i < alarm.length; i++)
		{
			if(alarm[i].alarm_type != 0)
			{
				alarmNumber = new String(alarm[i].alarm_number).trim();
				break;
			}
		}
		CNCUtils.Logi("Smart i", "getAlarmNumber() : " + alarmNumber);
		return alarmNumber;
	}
	
	public ArrayList<Alarm> getAlarm()
	{
		ArrayList<Alarm> alarmList = new ArrayList<Alarm>(SIZE_OF_ALARM_LIST);
		int j = 0;
		for (int i=0; i < alarm.length; i++)
		{
			if(new String(alarm[i].alarm_number).trim().equals(""))
			{
				// nothing
			}
			else
			{
				Alarm alarml = new Alarm(alarm[i].alarm_type,alarm[i].alarm_number,alarm[i].alarm_message, alarm[i].mtes_action);
				alarmList.add(j, alarml);			
				j++;
			}
		}
		return alarmList;
	}
	
	public String getAlarmArray()
	{
		String txtAlarm = "";
		for (int i=0; i < alarm.length; i++)
		{
			if(new String(alarm[i].alarm_number).trim().equals(""))
			{
				// nothing
			}
			else
			{
				txtAlarm += "" + new String(alarm[i].alarm_number).trim() + " : " +
				new String(alarm[i].alarm_message).trim() + "\n"; 
			}
		}
		return txtAlarm;
	}
	
	public String byteToString(byte [] b)
    {
        try
        {
            return new String(b, "EUC-KR").trim();
        } 
        catch (UnsupportedEncodingException e)
        {
        }
        return "";
    }
    
}
