<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ include file="/WEB-INF/views/include/lib.jsp"%>
<!DOCTYPE html>
<html> 
<head>
<meta charset="UTF-8">
<link rel="shortcut icon" href="${ctxPath }/images/dulink_favicon.ico">
<title>Dash Board</title>
<script type="text/javascript" src="${ctxPath }/js/jquery-1.11.2.min.js"></script>
<script type="text/javascript" src="${ctxPath }/js/jqMap.js"></script>
<script type="text/javascript" src="${ctxPath }/js/highcharts.js"></script>
<script type="text/javascript" src="${ctxPath }/js/highcharts-3d.js"></script>
<script type="text/javascript" src="${ctxPath }/js/highcharts-more.js"></script>
<script type="text/javascript" src="${ctxPath }/js/solid-gauge.js"></script>
<script src="${ctxPath }/js/chart/multicolor_series.js"></script>

<script type="text/javascript">
	var targetWidth = 3840;
	var targetHeight = 2160;
	
	var originWidth = window.innerWidth;
	var originHeight = window.innerHeight;
	
	var contentWidth = originWidth;
	var contentHeight = targetHeight/(targetWidth/originWidth);
	
	var screen_ratio = getElSize(240);
	
	if(originHeight/screen_ratio<9){
		contentWidth = targetWidth/(targetHeight/originHeight)
		contentHeight = originHeight; 
	};
	
	function getElSize(n){
		return contentWidth/(targetWidth/n);
	};

	var status1, status2;
	
	var loopFlag = null;
	var flag = false;
	function stopLoop(){
		flag = !flag;
		
		if(!flag){
			clearInterval(loopFlag);	
		}else{
			startPageLoop();	
		};
		
		alert("페이지 자동 이동 : " + flag);
	};
	
	var menu = false;
	function getMousePos(){
		$(document).on("mousemove", function(e){
			var target = $("#menu_btn").width();

			if((e.pageX <= target && e.pageY <= target) || panel){
				if(!menu){
					$("#menu_btn").animate({
						"left" : 0,
						"top" : getElSize(20)
					});
					menu = true;
				};
			}else{
				if(menu){
					$("#menu_btn").animate({
						"left" : -getElSize(100),
					});	
					menu = false;
				};
			};
		});
	};
	
	var panel = false;
	function showPanel(){
		$("#panel").animate({
			"left" : 0
		});
		
		$("#menu_btn").animate({
			"left" : contentWidth * 0.2 + getElSize(50)	
		});
		
		$("#corver").css({
			"z-index" : 99999
		});
	};

	function closePanel(){
		$("#panel").animate({
			"left" : -contentWidth * 0.2 - getElSize(40)
		});
		
		$("#menu_btn").animate({
			"left" : -getElSize(100)
		});
		
		$("#corver").css({
			"z-index" : -1
		});
	};
	
	function goReport(){
		var type = this.id;
		var url;
		if(type=="menu0"){
			url = "${ctxPath}/chart/main.do";
			location.href = url;
		}else if(type=="menu1"){
			url = "${ctxPath}/chart/performanceReport.do";
			location.href = url;
		}else{
			url = "${ctxPath}/chart/alarmReport.do";
			location.href = url;
		};
	};
	
	$(function(){
		$(".menu").click(goReport);
		getMousePos();
		$("#menu_btn").click(function(){
			if(!panel){
				showPanel();
			}else{
				closePanel();
			};
			panel = !panel;
		});
		
		$("#title_main").click(function(){
			location.href = "${ctxPath}/chart/multiVision.do";
		});
		
		$("#title_right").click(function(){
			location.href = "${ctxPath}/chart/main3.do";
		});
		
		$("#title_left").click(function(){
			location.href = "${ctxPath}/chart/main.do";
		});
		
		setElement();
		statusChart("status1");
		statusChart("status2");
		popup("popupChart");
		
		pieChart("pie1");
		pieChart("pie2");
		
		drawGaugeChart("opTime1", "cuttingTime1", "가동율", "절분율","%");
		drawGaugeChart("spdLoad1", "feedOverride1", "Spindle Load", "Feed Override","");
		
		drawGaugeChart("opTime2", "cuttingTime2", "가동율", "절분율","%");
		drawGaugeChart("spdLoad2", "feedOverride2", "Spindle Load", "Feed Override","");
		detailBar = $("#popupChart").highcharts();
		setInterval(time, 1000);
		
		detailBar = $("#popupChart").highcharts();
		detailOptions = detailBar.options;
		
		getAllDvcId();
		
		var pie1 = $("#pie1").highcharts();
		var pie2 = $("#pie2").highcharts();
		
		for(var i=0; i<=4; i++){
			pie1.series[0].data[i].update(0);
			pie2.series[0].data[i].update(0);
		};
		
		$("html").click(function(){
			$("#chartDataBox").fadeOut();
			$("#popup").fadeOut();
		});
		
		$("#mainTable").css({
			"left" : $("#container").offset().left + contentWidth/2 - $("#mainTable").width()/2
		});
	});
	
	function parsingModal(obj, idx){
		var json = $.parseJSON(obj);
	
		var auxCode = json.AUX_CODE;
		var gModal = json.G_MODAL;
		
		var auxTR = "<tr>";
		var auxTD;
		var gModalTR = "<tr>";
		var gModalTD;
		
		$.each(auxCode, function(key, data){
			$.each(auxCode[key], function (key, data){
				auxTD += "<td>" + key + " : " + data + "</td>"; 
			});
		}); 
		
		auxTR += auxTD + "</tr>";
		
		$.each(gModal, function(key, data){
			$.each(gModal[key], function (key, data){
				gModalTD += "<td>" + key + " : " + data + "</td>";
			});
		}); 
		
		gModalTR += gModalTD + "</tr>";
		
		$("#modalTbl" + idx).append(auxTR);
		$("#modalTbl" + idx).append(gModalTR);
	};

	function popup(id){
		$('#' + id).highcharts({
			chart : {
				
				type : 'bar',
				backgroundColor : 'rgba(255, 255, 255, 0)',
				height: 200
			},
			credits : false,
			//exporting: false,
			title : false,
			xAxis : {
				categories : [""],
				labels : {
					style : {
						fontSize : '35px',
						fontWeight:"bold",
						color : "white"
					}
				}
			},
			tooltip : {
				headerFormat : "",
				style : {
					fontSize : '10px',
				},
				enabled : false
			}, 
			yAxis : {
				min : 0,
				max : 59,
				tickInterval:1,
				reversedStacks: false,
				title : {
					text : false
				},
				labels: {
	                formatter: function () {
	                	var value = labelsArray_hour[this.value]
	                    return value;
	                },
	                style :{
	                	color : "black",
	                	fontSize : "20px"
	                },
	            },
			},
			legend : {
				enabled : false
			},
			plotOptions : {
				series : {
					  dataGrouping : {
		                    forced : true,
		                    units : [['minute', [5]]]
		                },
					stacking : 'normal',
					pointWidth:120, 
					borderWidth: 0,
					animation: false,
					cursor : 'pointer',
					point : {
						events: {
							click: function (e) {
		                   	}
		              	}
					}
				},
			},
			series : [{
				data :[0.16],
				color : "Red"
			},
			{
				data :[0.16],
				color : "green"
			},
			{
				data :[0.16],
				color : "yellow"
			},
			{
				data :[0.16],
				color : "Red"
			},
			{
				data :[0.16],
				color : "green"
			},
			{
				data :[0.16],
				color : "Red"
			}]
		});
	};
	
	var labelsArray_hour = [0,1,2,3,4,5,6,7,8,9,
	                        10,11,12,13,14,15,16,17,18,19,
	                        20,21,22,23,24,25,26,27,28,29,
	                        30,31,32,33,34,35,36,37,38,39,
	                        40,41,42,43,44,45,46,47,48,49,
	                        50,51,52,53,54,55,56,57,58,59
	                        ];
	
	var dvcArray = new Array();
	var dvcIndex = 0;
	function getAllDvcId(){
		var url = "${ctxPath}/chart/getAdapterId.do";
		
		$.ajax({
			url : url,
			dataType : "json",
			type : "post",
			success : function(data){
				var json = data.dvcId;
				$(json).each(function(i, obj){
					var dvc = new Array();
					dvc.push(obj.dvcId, obj.name);
					dvcArray.push(dvc);
				});
				
				getDvcStatus(dvcArray[dvcIndex], 1);
				getDvcStatus(dvcArray[dvcIndex+1], 2);
				
				setInterval(resetArray,1000 * 20);
				
				currentStatusLoop1= setInterval(function(){getCurrentDvcStatus(dvcArray[dvcIndex][0], 1)}, 3000)
				currentStatusLoop2= setInterval(function(){getCurrentDvcStatus(dvcArray[dvcIndex+1][0], 2)}, 3000)
			}
		});
	};
	
	function resetArray(){
		dvcMap1.put("flag", true);
		dvcMap1.put("initFlag", true);
		dvcMap1.put("currentFlag", true);
		dvcMap1.put("noSeries", false);
		clearInterval(currentStatusLoop1);
		clearInterval(statusLoop1);
		dvcMap2.put("flag", true);
		dvcMap2.put("initFlag", true);
		dvcMap2.put("currentFlag", true);
		dvcMap2.put("noSeries", false);
		clearInterval(currentStatusLoop2);
		clearInterval(statusLoop1);
		
		dvcIndex+=2;
		if(dvcIndex>=dvcArray.length)dvcIndex=0;
		
		getDvcStatus(dvcArray[dvcIndex], 1);
		getDvcStatus(dvcArray[dvcIndex+1], 2);
	};
	var statusLoop1, statusLoop2;
	var currentStatusLoop1, currentStatusLoop2;
	
	function isItemInArray(array, item) {
		for (var i = 0; i < array.length; i++) {
			for (var j = 0; j < array[i].length; j++) {
				if (array[i][j] == item) {
					return true;   // Found it
		        };
	        };
	    };
		return false;   // Not found
	};
	
	var block = 1/6;
	function getDvcStatus(dvc, idx){
		var dvcId = dvc[0];
		var name = dvc[1];
		var date = new Date();
		var year = date.getFullYear();
		var month = addZero(String(date.getMonth()+1));
		var day = addZero(String(date.getDate()));
		var hour = addZero(String(date.getHours()));
		var minute = addZero(String(date.getMinutes()));
		var second = addZero(String(date.getSeconds()));
		
		var today = year + "-" + month + "-" + day + "%20" + hour + ":" + minute + ":" + second;
		
		var url = "${ctxPath}/chart/getTimeChart.do";
		var param = "dvcId=" + dvcId + 
					"&targetDateTime=" + today;
		
		/* setInterval(function (){
			var minute = String(new Date().getMinutes());
			if(minute.length!=1){
				minute = minute.substr(1,2);
			};
			
			if(minute==5 && eval("dvcMap" + idx).get("initFlag")){
				console.log(minute,eval("dvcMap" + idx).get("initFlag"))
				console.log("init")
				getDvcStatus(dvc, idx);
				eval("dvcMap" + idx).put("initFlag", false);
				eval("dvcMap" + idx).put("currentFlag", true);
			}else if(minute!=5){
				eval("dvcMap" + idx).put("initFlag", true);
			};
		}, 1000 * 10); */
		
		$.ajax({
			url : url,
			data : param,
			dataType : "text",
			type : "post",
			success : function(data){
				$("#loader").css("opacity", 0);
				$("#mainTable").css("opacity", 1);
					
				if(data==null || data==""){
					eval("dvcMap" + idx).put("noSeries", true);
					getCurrentDvcStatus(dvcId, idx);
					return;
				};
				
				var json = $.parseJSON(data);
				
				var status = $("#status" + idx).highcharts();
				var options = status.options;
				
				options.series = [];
				options.title = null;
				
				$(json).each(function (i, data){
					var bar = data.data[0].y;
					var startTime = data.data[0].startTime;
					var endTime = data.data[0].endTime;
					var color = eval(data.color);
					
					if(eval("dvcMap" + idx).get("flag")){
						options.series.push({
					        data: [{
					        		y : Number(200),
					        		segmentColor : color
					        	}],
					    });	
					}else{
						options.series[0].data.push({
							y : Number(200),
							segmentColor : color
						});
					};
					eval("dvcMap" + idx).put("flag", false);
				});  
				
				status = new Highcharts.Chart(options);
				
				getCurrentDvcStatus(dvcId, idx);
			}
		});
	};

	var labelsArray = [20,21,22,23,24,1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20];
	
	function drawPieData(chartStatus, inCycleTime, waitTime, alarmTime, offTime, chart){
		chart.series[0].data[0].update(inCycleTime);		
		chart.series[0].data[1].update(waitTime);
		chart.series[0].data[2].update(alarmTime);
		chart.series[0].data[3].update(offTime);
		
		var day = 24*60*60;
		var blank = day-(inCycleTime + waitTime + alarmTime + offTime);
		chart.series[0].data[4].update(blank);
	};
	
	function calcTime(startTime, endTime){
		var startH = Number(startTime.substr(0,2));
		var startM = Number(startTime.substr(3,2));
		var startS = Number(startTime.substr(6,2));

		var endH = Number(endTime.substr(0,2));
		var endM = Number(endTime.substr(3,2)); 
		var endS = Number(endTime.substr(6,2));
		
		return ((endH*60*60) + (endM*60) + endS) - ((startH*60*60) + (startM*60) + startS);  
	};
	
	function removeSpace(str){
		return str = str.replace(/ /gi, "");
	};
	
	var dvcMap1 = new JqMap();
	var dvcMap2 = new JqMap();
	
	dvcMap1.put("flag", true);
	dvcMap1.put("initFlag", true);
	dvcMap1.put("currentFlag", true);
	dvcMap1.put("noSeries", false);
	
	dvcMap2.put("flag", true);
	dvcMap2.put("initFlag", true);
	dvcMap2.put("currentFlag", true);
	dvcMap2.put("noSeries", false);
	
	function getCurrentDvcStatus(dvcId, idx){
		var date = new Date();
		var year = date.getFullYear();
		var month = addZero(String(date.getMonth()+1));
		var day = addZero(String(date.getDate()));
		var workDate = year + "-" + month + "-" + day;
		
		var url = "${ctxPath}/chart/getCurrentDvcData.do";
		var param = "dvcId=" + dvcId + 
					"&workDate=" + workDate;
		$.ajax({
			url : url,
			data : param,
			dataType : "json",
			type : "post",
			success : function(data){
				var chartStatus = data.chartStatus;
				var type = data.type;
				var progName = data.lastProgramName;
				var progHeader = data.lastProgramHeader;
				var name = data.name;
				var inCycleTime = data.inCycleTime;
				var waitTime = data.waitTime;
				var alarmTime = data.alarmTime;
				var noConTime = data.noConnectionTime;
				var spdLoad = data.spdLoad;
				var feedOverride = data.feedOverride;
				var opRatio = data.opRatio;
				var cuttingRatio = data.cuttingRatio;
				var alarm = data.alarm;
				
				$("#alarmCode" + idx +"_0").html("");
				$("#alarmMsg" + idx +"_0").html("");
				$("#alarmCode" + idx +"_1").html("");
				$("#alarmMsg" + idx +"_1").html("");
				
				if(alarm!=null && alarm!="" && typeof(alarm)!="undefined") parsingAlarm(idx, alarm);
				
				var lamp = "<img src=${ctxPath}/images/DashBoard/" + chartStatus +".png style='height:" + getElSize(50) +"px;vertical-align: text-top;'>";
				$("#progName" + idx).html(progName);
				$("#progHeader" + idx).html(progHeader);
				$("#dvcName" + idx).html(name + lamp);
				
				
				if(eval("dvcMap" + idx).get("currentFlag")){
					var status = $("#status" + idx).highcharts();
					var options = status.options;
					
					if(eval("dvcMap" + idx).get("noSeries")){
						options.series = [];
						options.title = null;
						
		      			options.series.push({
					        data: [{
					        		y : Number(200),
					        		segmentColor : slctChartColor(chartStatus) 
					        	}],
					    });	
					}else{
						options.title = null;
						options.series[0].data.push({
			        		y : Number(200),
			        		segmentColor : slctChartColor(chartStatus)
			  			});		
					};
					
		      	  	var now = options.series[0].data.length;
					var blank = 144 - now;
					
					for(var i = 0; i <= blank; i++){
						options.series[0].data.push({
			        		y : Number(200),
			        		segmentColor : "rgba(0,0,0,0.0)"
			  			});	
					};
					
					status = new Highcharts.Chart(options);
					
					eval("dvcMap" + idx).put("currentFlag", false)
		      	};
		      	
				var pieChart = $("#pie" + idx).highcharts();
				drawPieData(chartStatus, inCycleTime, waitTime, alarmTime, noConTime, pieChart);
				
				var opTimeChart = $("#opTime" + idx).highcharts().series[0].points[0];
				opTimeChart.update(Number(opRatio));
			
				var cuttingTime = $("#cuttingTime" + idx).highcharts().series[0].points[0];
				cuttingTime.update(Number(cuttingRatio));
				
				var spd = $("#spdLoad" + idx).highcharts().series[0].points[0];
				spd.update(Number(spdLoad));
				
				var feed = $("#feedOverride" + idx).highcharts().series[0].points[0];
				feed.update(Number(feedOverride));
				
				var statusLoop;
				if(idx==1){
					statusLoop = currentStatusLoop1;
				}else{
					statusLoop = currentStatusLoop1;
				};
			}
		});
		
	};
	
	function slctChartColor(status){
		var color;
		if(status.toLowerCase()=="in-cycle"){
			color = colors[0];
		}else if(status.toLowerCase()=="wait"){
			color = colors[1];
		}else if(status.toLowerCase()=="alarm"){
			color = colors[2];
		}else if(status.toLowerCase()=="no-connection"){
			color = colors[3];
		};
		
		return color;
	};
	
	function addZero(n){
		if(n.length=="1"){
			n = "0" + n;
		};
		return n;
	};
	
	function removeHypen(str){
		str = str.replace(/-/gi,"");
		return str;
	};
	
	var block = 1/6;

	function parsingAlarm(idx,alarm){
		var json = $.parseJSON(alarm);
		
		$(json).each(function (i,data){
			$("#alarmCode" + idx + "_" + i).html(data.alarmCode + " - ");
			$("#alarmMsg" + idx + "_" + i).html(data.alarmMsg);
		});
	};
	
	function getAlarmList(dvcId){
		var url = "${ctxPath}/chart/getAlarmList.do";
		
		var workDate = $("#sdate").val();
		var param = "dvcId=" + dvcId +
					"&workDate=" + workDate;
		
		$.ajax({
			url : url,
			data : param,
			dataType : "json",
			type : "post",
			success : function(data){
				var json = data.alarmList;
				
				$(json).each(function (i,data){
					$("#alarmCode1_" + i).html("[" + data.startDateTime.substr(5,14)+"] " + data.alarmCode + " - ");
					$("#alarmMsg1_" + i).html(data.alarmMsg);
				});
			}
		});
		
	};
	
	function drawGaugeChart(el1, el2, title1, title2, unit){
		var gaugeOptions = {
				chart: {
					type: 'solidgauge',
			     	backgroundColor : 'rgba(255, 255, 255, 0)',
			  	},
			  	title : false,
				credits : false,
			   	exporting : false,
			 	pane: {
			 		center: ['50%', '50%'],
			      	size: '100%',
	         		startAngle: -90,
	         		endAngle: 90,
	         		background: {
	             	backgroundColor: (Highcharts.theme && Highcharts.theme.background2) || '#EEE',
	             	innerRadius: '60%',
	             	outerRadius: '100%',
	             	shape: 'arc'
	         		}
			 	},
        		tooltip: {
        			enabled: false
			  	},

			  	// the value axis
			 	yAxis: {
			 		stops: [
			 		        [1, '#13E000']
			            	],
			            	lineWidth: 0,
			            	minorTickInterval: null,
			            	tickPixelInterval: 400,
			            	tickWidth: 0,
			            	title: {
			                y: -70
			            	},
			            labels: {
			                y: 16
			            	}
			 	},
			 	plotOptions: {
			            solidgauge: {
			                dataLabels: {
			                    y: 5,
			                    borderWidth: 0,
			                    useHTML: true
			                }
			            }
			        }
			    };

				var max;
				if(unit=="%"){
					max = 100
				}else{
					max = 200;
				};
			    // The speed gauge
			    $('#' + el1).highcharts(Highcharts.merge(gaugeOptions, {
			    	  yAxis: {
				            min: 0,
				            max: max,
				            title: {
				                text: title1,
				               y: -getElSize(85),
				               style:{
				            	   color : "black",
				            	   fontSize : getElSize(20) + 'px',
									fontWeight:"bold"
				            	   
				               }
				    		},
				        },

			        credits: {
			            enabled: false
			        },

			        series: [{
			            name: '가동률',
			            data: [0],
			            dataLabels: {
			                format: '<div style="text-align:center"><span style="font-size:'+ getElSize(50 )+ 'px;color:white">{y}</span>' +
			                       '<span style="font-size:' + getElSize(30) + ';color:white">' + unit + '</span></div>'
			            },
			            tooltip: {
			                valueSuffix: ' %'
			            }
			        }]

			    }));

			    // The RPM gauge
			    $('#' + el2).highcharts(Highcharts.merge(gaugeOptions, {
			    	 yAxis: {
				            min: 0,
				            max: max,
				            title: {
				                text: title2,
				               y: -getElSize(85),
				               style:{
				            	   color : "black",
				            	   fontSize : getElSize(20) + 'px',
									fontWeight:"bold"
				               }
									
				            },
				        },

			        series: [{
			            name: '절분률',
			            data: [0],
			            dataLabels: {
			                format: '<div style="text-align:center"><span style="font-size:' + getElSize(50) + 'px;color:white">{y}</span>' +
			                       '<span style="font-size:' + getElSize(30) + 'px;color:white">' + unit + '</span></div>'
			            },
			            tooltip: {
			                valueSuffix: '%'
			            }
			        }]

			    }));
	};
	
	function pieChart(id){
		Highcharts.setOptions({
			//green yellow red black
			   colors: ['#148F01', '#C7C402', '#ff0000', '#8C9089', '#ffffff']
		    });
		
	    
	    // Radialize the colors
	    Highcharts.getOptions().colors = Highcharts.map(Highcharts.getOptions().colors, function (color) {
	        return {
	            radialGradient: { cx: 0.5, cy: 0.5, r: 0.7 },
	            stops: [
	                [0, Highcharts.Color(color).brighten(0.5).get('rgb')], // darken
	                [1, color]
	                
	            ]
	        };
	    });
	    
		$('#' + id)
		.highcharts(
				{
					chart : {
						plotBackgroundColor : null,
						plotBorderWidth : null,
						plotShadow : false,
						backgroundColor : 'rgba(255, 255, 255, 0)',
						type: 'pie',
						options3d: {
			                enabled: true,
			                alpha: 45
			            } 
					},
					credits : false,
					title : {
						text : false
					},
					tooltip : {
						pointFormat : '{series.name}: <b>{point.percentage:.1f}%</b>',
						enabled : false
					},
					plotOptions : {
						pie : {
							innerSize: getElSize(130),
				      		depth: contentHeight/(targetHeight/45),
							size:'95%',
							allowPointSelect : true,
							cursor : 'pointer',
							dataLabels : {	 
								enabled : true,
							 	formatter : function(){
							 		var value;
							 		if(this.y!=0.0) value = Number(this.y/60/60).toFixed(1);
							 		return value;
							 	},
							 	connectorColor: '#000000',
								distance : 1,
							 	style : {
							 		 color: 'black',
							 		 textShadow: '0px 1px 2px black',
							 		 fontSize : getElSize(23),
								}
							}
						}
					},
					exporting: false,
					series : [ {
						type : 'pie',
						name : 'Chart Status',
						data : [ [ 'In-Cycle',1], 
						         [ 'Wait', 1 ],
						         [ 'Alarm', 1 ],
						         [ 'Power-Off', 1],
						         [ 'blank', 1],
								]
					} ]
				});
	};
	
	var detailOptions;
	var detailBarArray = new Array();
	function addDetailBar(bar, color){
		var _bar = new Array();
		_bar.push(bar);
		_bar.push(color);
		
		detailBarArray.push(_bar);
	};
	
	function reDrawDetailBar(){
		detailOptions.series = [];
		detailOptions.title = null;
		for (var i = 0; i < detailBarArray.length; i++){
			detailOptions.series.push({
		        data: [{
		        		y : detailBarArray[i][0],
		        	}],
		        color : detailBarArray[i][1]
		    });	
		};
		
		detailBar = new Highcharts.Chart(detailOptions);
	};
	
	var barSize = 1/6;
	var detailBarMap = new JqMap();
	
	function getDetailStatus(dvcId, dvcName, hour){
		if(detailBarArray.length!=0){
			detailBarArray = new Array();
			for(var i = 0; i < detailBarArray.length; i++){
				detailBar.series[0].remove(true);
			};			
		};
		
		if(hour==24) hour=0;
		
		var url = "${ctxPath}/chart/getDetailStatus.do";
		var data = "dvcId=" + dvcId + 
					"&startDateTime=" + hour;
		
		$.ajax({
			url : url,
			data : data,
			type : "post",
			dataType : "json",
			success : function(data){
				var json = data.chartStatus;
				
				$(json).each(function(i, data){
					var startTime = removeHypen(data.startDateTime, "690").substr(9,8);
					var endTime = removeHypen(data.endDateTime, "691").substr(9,8);
					var color = slctChartColor(removeSpace(data.chartStatus));
					
					var startH = Number(startTime.substr(0,2));
					var startM = String(startTime.substr(3,2));
					var startS = Number(startTime.substr(6,2));

					var endH = Number(endTime.substr(0,2));
					var endM = String(endTime.substr(3,2)); 
					var endS = Number(endTime.substr(6,2));

					var start = (startH*60*60) + (startM*60) + (startS);
					var end = (endH*60*60) + (endM*60) + (endS);
					
					if(i==0)start=hour*60*60;
					var bar = (end - start)/10*barSize;
					
					if(bar<0.16)bar=0.16;
					detailBarMap.put("chart", detailBar);
					
					console.log(startTime, endTime, removeSpace(data.chartStatus), bar);
					addDetailBar(bar, color)
				});
				
				reDrawDetailBar();
				
				$("#popup").fadeToggle();
				$("#hour").html(dvcName + " (" + hour + ":00 ~ " + hour + ":59)");	
			}
		});
	};
	
	var colors;
	function statusChart(id){
		var perShapeGradient = {
	            x1: 0,
	            y1: 0,
	            x2: 1,
	            y2: 0
	        };
	        colors = Highcharts.getOptions().colors;
	        colors = [{
	            linearGradient: perShapeGradient,
	            stops: [
	                [0, '#148F01'],
	                [1, '#1ABB02']
	                ]
	            }, {
	            linearGradient: perShapeGradient,
	            stops: [
	                [0, '#C7C402'],
	                [1, '#F5F104']
	                ]
	            }, {
	            linearGradient: perShapeGradient,
	            stops: [
	                [0, '#ff0000'],
	                [1, '#FF2626']
	                ]
	           },{
		            linearGradient: perShapeGradient,
		            stops: [
		                [0, '#8c9089'],
		                [1, '#A9ADA6']
		                ]}
	        ]
	         
	//	$('#' + id).highcharts({
		options = {
			chart : {
				
				type : 'coloredarea',
				backgroundColor : 'rgba(255, 255, 255, 0)',
				height: getElSize(300),
				marginTop: -100
			},
			credits : false,
			exporting: false,
			title : false,
			yAxis : {
				labels : {
					enabled : false,
				},
				title : {
					text : false
				},
			},
			xAxis:{
		           categories:[20,0,0,0,0,0,
		                       21,0,0,0,0,0,
		                       22,0,0,0,0,0,
		                       23,0,0,0,0,0,
		                       24,0,0,0,0,0,
		                       1,0,0,0,0,0,
		                       2,0,0,0,0,0,
		                       3,0,0,0,0,0,
		                       4,0,0,0,0,0,
		                       5,0,0,0,0,0,
		                       6,0,0,0,0,0,
		                       7,0,0,0,0,0,
		                       8,0,0,0,0,0,
		                       9,0,0,0,0,0,
		                       10,0,0,0,0,0,
		                       11,0,0,0,0,0,
		                       12,0,0,0,0,0,
		                       13,0,0,0,0,0,
		                       14,0,0,0,0,0,
		                       15,0,0,0,0,0,
		                       16,0,0,0,0,0,
		                       17,0,0,0,0,0,
		                       18,0,0,0,0,0,
		                       19,0,0,0,0,0,
		                       20,0,0,0,0,0,
		                       ],
		            labels:{
		               
		                 formatter: function () {
			                        	var val = this.value
			                        	if(val==0){
			                        		val = "";
			                        	};
			                        	return val;   
			                        },
			                        style :{
			    	                	color : "white",
			    	                	fontSize : 11
			    	                },
		            }
		        },
		       
			tooltip : {
				headerFormat : "",
				style : {
					fontSize : '10px',
				},
				enabled : false
			}, 
			plotOptions: {
			    line: {
			        marker: {
			            enabled: false
			        }
			    }
			},
			legend : {
				enabled : false
			},
			series: []
		}
		
		//});
	        
	      /*   Highcharts.setOptions({
    	        yAxis:{
    	        	labels:{
    	        		formatter: function () {
    	                	var value = labelsArray[this.value];
    	                    return value;
    	                }
    	         	}   
    	      	}
    	    }); */

	   	$("#" +id).highcharts(options);
	};
	
	function time(){
		var date = new Date();
		var month = date.getMonth()+1;
		var date_ = addZero(String(date.getDate()));
		var day = date.getDay();
		var hour = addZero(String(date.getHours()));
		var minute = addZero(String(date.getMinutes()));
		var second = addZero(String(date.getSeconds()));
		
		if(day==1){
			day = "Mon";
		}else if(day==2){
			day = "Tue";
		}else if(day==3){
			day = "Wed";
		}else if(day==4){
			day = "Thu";
		}else if(day==5){
			day = "Fri";
		}else if(day==6){
			day = "Sat";
		}else if(day==0){
			day = "Sun";
		};
		
		$("#date").html(month + " / " + date_ + " (" + day + ")");
		$("#time").html(hour + " : " + minute + " : " + second);
	};
	
	function setElement(){
		var width = window.innerWidth;
		var height = window.innerHeight;
		
		$("#popup").css({
			"top" : $("#status1").offset().top,
			"left" : width/2 - ($("#popup").width()/2),
			"z-index" : 99999
		});
		
		$("#title_main").css({
			"left" : width/2 - $("#title_main").width()/2
		});
		
		$("hr").css({
			"left" : width/2 - $("hr").width()/2
		});
		
		
		$("#loader").css({
			"left" : width/2 - $("#loader").width()/2,
			"top" : height/2 - $("#loader").height()/2
		});
		
		$("#container").css({
			"width": contentWidth,
			"height" : contentHeight,
		});
		
		$("#container").css({
			"margin-top" : height/2 - ($("#container").height()/2),
			"margin-left" : width/2 - ($("#container").width()/2),
		});
		
		$("#title_main").css({
			"width" : getElSize(1000),
			"top" : $("#container").offset().top + (getElSize(50)),
		});
		
		$("#title_main").css({
			"left" : originWidth/2 - ($("#title_main").width()/2)
		});
		
		$("#title_left").css({
			"width" : getElSize(300),
			"left" : $("#container").offset().left + getElSize(50),
			"top" : $("#container").offset().top + (getElSize(50))
		});
		
		$("#title_right").css({
			"width" : getElSize(300),
			"right" : $("#container").offset().left + getElSize(50),
			"top" : $("#container").offset().top + (getElSize(50))
		});
		
		$("#time").css({
			"top": $("#container").offset().top + getElSize(170),
			"right": $("#container").offset().left + getElSize(50),
			"font-size" : getElSize(30)
		});
		
		$("#date").css({
			"top": $("#container").offset().top + getElSize(170),
			"right": $("#container").offset().left + getElSize(250),
			"font-size" : getElSize(30)
		});
		
		$(".status").css({
			"width" : contentWidth * 0.4
		});
		
		$("hr").css({
			"width" : contentWidth * 0.9,
			"top" : $("#container").offset().top + getElSize(250),
		});
		
		$("hr").css({
			"left" : $("#container").offset().left + contentWidth/2 - $("hr").width()/2,
		});
		
		$("#mainTable").css({
			"width" : contentWidth * 0.9,
			"top" : $("#container").offset().top + getElSize(300)
		});
		
		
		$(".font_dvc_name").css({
			"font-size" : getElSize(50) + "px"
		});
		
		$(".mode").css({
			"font-size": getElSize(40) + "px",
			"margin-right": getElSize(20) + "px",
			"margin-top": getElSize(100) + "px"		
		});
		
		$(".tableTitle").css({
			"font-size" : getElSize(40) + "px"
		});
		
		$(".title_td").css({
			"padding" : getElSize(20) + "px"
		});
		
		$(".pie_cell").css({
			"height" : getElSize(1000)
		});
		
		$("#gaugeDiv1").css({
			"width": getElSize(700),
			"height": getElSize(400),
			"left": getElSize(920),
			"top": getElSize(680)
		});
		
		$("#gaugeDiv2").css({
			"width": getElSize(700),
			"height": getElSize(400),
			"left": getElSize(920),
			"top": getElSize(980)
		});
		
		$("#gaugeDiv3").css({
			"width": getElSize(700),
			"height": getElSize(400),
			"left": getElSize(2640),
			"top": getElSize(680)
		});
		
		$("#gaugeDiv4").css({
			"width": getElSize(700),
			"height": getElSize(400),
			"left": getElSize(2640),
			"top": getElSize(980)
		});
		
		$(".mainTd").css({
			"padding" : "0 " + getElSize(50) + " 0 " + getElSize(50)
		});
		
		$(".pie").css({
			"top" : getElSize(800),
			"height" : getElSize(800)
		});
		
		$(".alarmDiv").css({
			"margin-top": getElSize(600),
			"margin-left":getElSize(50),
			"margin-right":getElSize(50),
			"margin-bottom":getElSize(50),
			"line-height": getElSize(50) + "px"
		});
		
		$(".alarmText").css({
			"font-size" : getElSize(30)
		});
		
		$(".gauge").css({
			"height" : getElSize(400)
		});
		$("#menu_btn").css({
			"position" : "absolute",
			"width" : getElSize(100),
			"left" : -getElSize(100),
			"top" : getElSize(20),
			"cursor" : "pointer",
			"z-index" : 999999
		});
		$("#panel").css({
			"background-color" : "rgb(34,34,34)",
			"border" : getElSize(20) + "px solid rgb(50,50,50)",
			"position" : "absolute",
			"width" : contentWidth * 0.2,
			"top" : 0,
			"left" : -contentWidth * 0.2 - getElSize(40),
			"z-index" : 99999
		});
		
		$("#panel_table").css({
			"color" : "white",
			"font-size" : getElSize(60)
		});

		$("#panel_table td").css({
			"padding" : getElSize(50),
			"cursor" : "pointer"
		});
		
		$("#panel_table td").addClass("unSelected_menu");
		
		$("#panel_table").css({
			"color" : "white",
			"font-size" : getElSize(50)
		});
	};
	
	function addZero(n){
		if(n.length=="1"){
			n = "0" + n;
		};
		return n;
	};
</script>
<style>

#container{
	background: url("../images/DashBoard/Background.png");
	background-size : 100% 100%;
	background-color: white;
}

body{
	margin: 0px;
	padding: 0px;
	width: 100%;
	height: 100%;
	background-color: black;
  	font-family:'Helvetica';
	background-size : 100% 100%;
	overflow: hidden;
}

.title{
	top : 50px;
	position: absolute;
}

#title_main{
	width: 1000px;
	z-index: 999;
}
#title_left{
	left : 50px;
	width: 300px;
}

#title_right{
	right: 50px;
	width: 300px;
}

#pieChart1{
	position: absolute;
	left : 0px;
	z-index: 99;
	top: 800px;
	height: 500px;
}

#pieChart2{
	position: absolute;
	right : 0px;
	z-index: 99;
	top: 800px;
	height: 500px;
} 

#time{
	position: absolute;
	top: 170px;
	right: 50px;	
	font-size : 30px;
	color: white;
} 
#date{
	position: absolute;
	top: 170px;
	right: 250px;	
	font-size : 30px;
	color: white;
} 

hr{
	position: absolute;
	top: 250px;
	z-index: 99;
	border: 2px solid white;
}

#mainTable{
	position: absolute;
	top: 270px;
	z-index: 99;
	opacity : 0;
}

#mainTable tr:last-child td:first-child {
    -moz-border-radius-bottomleft:10px;
    -webkit-border-bottom-left-radius:10px;
    border-bottom-left-radius:10px;
    
    -moz-border-radius-topleft:10px;
    -webkit-border-top-left-radius:10px;
    border-top-left-radius:10px
}

#mainTable tr:last-child td:last-child {
    -moz-border-radius-bottomright:10px;
    -webkit-border-bottom-right-radius:10px;
    border-bottom-right-radius:10px;
    
    -moz-border-radius-topright:10px;
    -webkit-border-top-right-radius:10px;
    border-top-right-radius:10px
}

.statusIcon{
	width: 80px;
	height: 80px;
}

.pie{
	width: 95%;
	height: 800px;
	margin: 0 auto;
}

.tr{
  background: linear-gradient(white, gray);
  opacity : 0.7;
  
}
#mainTable{
	opacity : 0;
}
#loader{
	position: absolute;
	width: 500px;
	-webkit-filter: brightness(10);
	/* background-color: white;
	-webkit-border-radius: 50em;
	-moz-border-radius: 50em;
	border-radius: 50em; */
}
#popup{
	width: 100%;
}

.unSelected_menu{
	background-color :gray;
	color : white
}

.selected_menu{
	background-color :rgb(33,128,250);
	color : white
}

.menu:HOVER{
	background-color: white;
	color: gray;
}
</style>
<script type="text/javascript">
	
</script>

</head>


<body>
	<div id="container" >
		<div id="panel">
			<table id="panel_table" width="100%">
				<tr>
					<td id="menu0" class="menu">샵 레이아웃</td>
				</tr>
				<tr>
					<td id="menu1" class="menu">장비별 가동실적 분석</td>
				</tr>
				<tr>
					<td id="menu2" class="menu">장비 Alarm 발생내역 조회</td>
				</tr>
			</table>
		</div>
		<img src="${ctxPath }/images/menu.png" id="menu_btn" >
		<div id="svg"></div>
	</div>
	
	
	<div id="popup" style="background-color:white; position: absolute; display: none;border-radius : 20px;">
		<center><div id="hour" style="font-size: 60px; font-weight: bold;"></div></center>
		<div id='popupChart' style="width: 100%" ></div>
	</div>
	
	<img src="${ctxPath }/images/DashBoard/title2.svg" id="title_main" class="title">
	<img src="${ctxPath }/images/DashBoard/title_left.svg" id="title_left" class="title">
	<img src="${ctxPath }/images/DashBoard/title_right.svg" id="title_right" class="title">	
	
	<hr>

	<font id="date"></font>
	<font id="time"></font>
	
	<img alt="" src="${ctxPath }/images/DashBoard/loader.png" id="loader">
	<table id="mainTable">
		<tr>
			<td style="border-right: 2px solid white; width: 50%; padding: 0 50 0 50" class="mainTd">
				<font style="font-weight: bold; color: white; font-size: 50px;" id="dvcName1" class="font_dvc_name"></font><br>
				<span style="font-weight: bold; color: white; font-size: 50px" id="progName1" class="font_dvc_name"></span> 
				<span style="font-weight: bold; color: white; font-size: 50px" id="progHeader1" class="font_dvc_name"></span>

				
				<div id="status1" class="status"></div>
				
				<div style="float: right; font-weight: bold; font-size: 40px; color: white; margin-right: 20px" class="mode">Mode : Automatic</div>
				<table class="subTable" style="width:100%"> 
					<tr style="color: black; font-size: 40px; background-color: #fff8dc; font-weight: bold;" class="tableTitle">
						<td width="50%" align="center" style="padding: 20px;" class="title_td">
							일 누적 가동현황
						</td>
						<td width="50%" align="center">
							현 가동 상태
						</td>
					</tr>
					<tr class="tr">
						<td width="50%" height="1000px;" class="pie_cell">
							<div class="pie" id="pie1"></div>
						</td>
						<td >
							<div style="width: 700px; position: absolute; left: 920px; top: 680px" id="gaugeDiv1">
								<div id="opTime1" style="float: left; width: 40%; " class="gauge"> </div>
								<div id="cuttingTime1" style="float: right; width: 40%" class="gauge"></div>
							</div>
							<div style="width: 700px; position: absolute; left: 920; top: 980px" id="gaugeDiv2">
								<div id="spdLoad1" style="float: left; width: 40%" class="gauge"></div>
								<div id="feedOverride1" style="float: right; width: 40%" class="gauge"></div>
							</div>
							<div style="margin-top:600px;margin-left:50px;margin-right:50px;margin-bottom:50px;  text-align: left; line-height: 50px" class="alarmDiv">
								<font style="font-size: 30px; font-weight: bold; color: red;"  class="alarmText">Alarm</font><br>
								<font style="font-size: 30px; font-weight: bold; color: black;" id="alarmCode1_0" class="alarmText"></font>
								<font style="font-size: 30px; font-weight: bold; color: black;" id="alarmMsg1_0" class="alarmText"></font><br>
								<font style="font-size: 30px; font-weight: bold; color: black;" id="alarmCode1_1" class="alarmText"></font>
								<font style="font-size: 30px; font-weight: bold; color: black;" id="alarmMsg1_1" class="alarmText"></font><br>
							</div>
						</td>
					</tr>
				</table>
			</td>
			<td style="border-right: 2px solid white; width: 50%; padding: 0 50 0 50" class="mainTd">
				<font style="font-weight: bold; color: white; font-size: 50px" id="dvcName2" class="font_dvc_name"></font><br>
				<span style="font-weight: bold; color: white; font-size: 50px" id="progName2" class="font_dvc_name"></span> 
				<span style="font-weight: bold; color: white; font-size: 50px" id="progHeader2" class="font_dvc_name"></span>
				<br>
				
				<div id="status2" class="status"></div>
				
				<div style="float: right; font-weight: bold; font-size: 40px; color: white; margin-right: 20px" class="mode">Mode : Automatic</div>
				<table class="subTable" style="width:100%"> 
					<tr style="color: black; font-size: 40px; background-color: #fff8dc; font-weight: bold;" class="tableTitle">
						<td width="50%" align="center" style="padding: 20px;" class="title_td">
							일 누적 가동현황
						</td>
						<td width="50%" align="center">
							현 가동 상태
						</td>
					</tr>
					<tr class="tr">
						<td width="50%" height="1000px;" class="pie_cell">
							<div class="pie" id="pie2"></div>
						</td>
						<td >
							<div style="width: 700px; position: absolute; left: 2640px; top: 680px" id="gaugeDiv3">
								<div id="opTime2" style="float: left; width: 40%" class="gauge"></div>
								<div id="cuttingTime2" style="float: right; width: 40%" class="gauge"></div>
							</div>
							<div style="width: 700px; position: absolute; left: 2640; top: 980px" id="gaugeDiv4">
								<div id="spdLoad2" style="float: left; width: 40%" class="gauge"></div>
								<div id="feedOverride2" style="float: right; width: 40%" class="gauge"></div>
							</div>
							<div style="margin-top:600px;margin-left:50px;margin-right:50px;margin-bottom:50px;text-align: left; line-height: 50px" class="alarmDiv">
								<font style="font-size: 30px; font-weight: bold; color: red;" class="alarmText">Alarm</font><br>
								<font style="font-size: 30px; font-weight: bold; color: black;" id="alarmCode2_0" class="alarmText"></font>
								<font style="font-size: 30px; font-weight: bold; color: black;" id="alarmMsg2_0" class="alarmText"></font><br>
								<font style="font-size: 30px; font-weight: bold; color: black;" id="alarmCode2_1" class="alarmText"></font>
								<font style="font-size: 30px; font-weight: bold; color: black;" id="alarmMsg2_1" class="alarmText"></font><br>
							</div>
						</td>
					</tr>
				</table>
			</td>
		</tr>
	</table>
</body>
</html>