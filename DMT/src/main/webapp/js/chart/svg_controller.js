var NS="http://www.w3.org/2000/svg";
var draw;
var background_img;
var marker;
/*var imgPath = "../images/DashBoard/";*/

var imgPath = "/Device_Status/resources/upload/";

var pieChart1;
var pieChart2;
var borderColor = "";
var shopId = 1;
var totalOperationRatio = 0;

$(function() {
	pieChart1 = $("#pieChart1").highcharts();
	pieChart2 = $("#pieChart2").highcharts();

	$("#title_main").css({
		left : window.innerWidth/2 - ($("#title_main").width()/2)
	});
	
	draw = SVG("svg");
	
	//getMarker();
	
	var timeStamp = new Date().getMilliseconds();
	
//	background_img = draw.image(imgPath+"Road.png?dummy=" + timeStamp).size(contentWidth * 0.9, contentHeight);
//	background_img.x(getElSize(450));
//	background_img.y(contentHeight/(targetHeight/100));
	
	
//	var logoText =  draw.text(function(add) {
//		  add.tspan("　　　성역 없는 ").fill("#313131");
//		  add.tspan("혁신!").fill("#ff0000");
//		  add.tspan("전원이 참여하는 ").fill("#0005D5").newLine();
//		  add.tspan("혁신!").fill("#ff0000");
//		  add.tspan("　　나로 부터의 ").fill("#c00000").newLine();
//		  add.tspan("혁신!").fill("#ff0000");
//		  
//	});
//		
//	logoText.font({
//			'family':'Helvetica'
//			, size:     getElSize(75)
//			, anchor:   'right'
//			, leading:  '1.3em'
//		   ,'font-weight':'bolder',
//	});
//	
//	logoText.x(getElSize(2600));
//	logoText.y(contentHeight/(targetHeight/1730));
	
	getMachineInfo();
});

function alarmTimer(){
	setInterval(function(){
		if(s2==10){
    		s++;
    		$("#second1").html(s);
    		s2 = 0;	
    	};
    	
    	if(s==6){
    		s = 0;
    		m2++;
    		
    		$("#second1").html(s);
    		$("#minute2").html(m2);
    	};
    	
    	if(m2==10){
    		m2 = 0;
    		m++;
    		$("#minute1").html(m);
    	};
		
    	$("#second2").html(s2);
    	s2++;
    	
	},1000);
};

function redrawPieChart(){
	
	var url = ctxPath + "/svg/getMachineInfo.do";
	var param = "shopId=" + shopId;
	$.ajax({
		url : url,
		data : param,
		type : "post",
		dataType : "json",
		success : function (data){
			var json = data.machineList;
			inCycleMachine = 0;
			waitMachine = 0;
			alarmMachine = 0;
			powerOffMachine = 0;
			
			//console.log(json)
			
			$(json).each(function(key, data){
				
				if(data.type=='MTC'){
					
					// wilson
					// LAMP
					if(isLamp == true){
//						if(data.green==1){
//							data.lampStatus="IN-CYCLE";
//							inCycleMachine++;
//						}else if(data.greenBlink==1){
//							data.lampStatus="IN-CYCLE-BLINK";
//							inCycleMachine++;
//						}else if(data.red==1){
//							data.lampStatus="ALARM-NOBLINK";
//							alarmMachine++;
//						}else if(data.redBlink==1){
//							data.lampStatus="ALARM";
//							alarmMachine++;
//						}else if(data.yellow==1){
//							data.lampStatus="WAIT-NOBLINK";
//							waitMachine++;
//						}else if(data.yellowBlink==1){
//							data.lampStatus="WAIT";
//							waitMachine++;
//						}else{
//							data.lampStatus="NO-CONNECTION";
//							powerOffMachine++;
//						}
						
						//priority : red > yellow > green					
						if(data.red==1){
							alarmMachine++;
//							data.lampStatus="ALARM-NOBLINK";
						}else if(data.redBlink==1){
							alarmMachine++;
//							data.lampStatus="ALARM";
						}else if(data.yellow==1){
							waitMachine++;
//							data.lampStatus="WAIT-NOBLINK";
						}else if(data.yellowBlink==1){
							waitMachine++;
//							data.lampStatus="WAIT";
						}else if(data.green==1){
//							data.lampStatus="IN-CYCLE";
							inCycleMachine++;
						}else if(data.greenBlink==1){
//							data.lampStatus="IN-CYCLE-BLINK";
							inCycleMachine++;
						}else{
							powerOffMachine++;
//							data.lampStatus="NO-CONNECTION";
						}
					
					// PMC
					} else{						
//						if(data.lastChartStatus == "CUT"){
//							data.lampStatus = "IN-CYCLE"
//						}else{
//							data.lampStatus = data.lastChartStatus
//						}
						
						// PMC용 파이차트 개수 세기
						if(data.lastChartStatus == "IN-CYCLE"){
							inCycleMachine++;
						}else if(data.lastChartStatus == "CUT"){
							inCycleMachine++;
						}else if(data.lastChartStatus == "ALARM"){
							alarmMachine++;
						}else if(data.lastChartStatus == "WAIT"){
							waitMachine++;
						}else{
							powerOffMachine++;
						}
						//console.log("PMC " + inCycleMachine + " " + waitMachine + " " + alarmMachine + " " + powerOffMachine + " = " + (inCycleMachine + waitMachine + alarmMachine + powerOffMachine) );
					}
					
				// IOL
				}else{
					
//					if(data.lastChartStatus=="IN-CYCLE" || data.lastChartStatus=="CUT"){
//						replaceAll(replaceAll(decodeURI(data.name), "%23","#"),"%2F","/")
//						inCycleMachine++;
//					}else if(data.lastChartStatus=="WAIT"){
//						waitMachine++;
//					}else if(data.lastChartStatus=="ALARM"){
//						alarmMachine++;
//					}else if(data.lastChartStatus=="NO-CONNECTION"){
//						powerOffMachine++;
//					};
					
					//priority : red > yellow > green
					if(data.lastChartStatus=="ALARM"){
						alarmMachine++;
					}else if(data.lastChartStatus=="WAIT"){
						waitMachine++;
					}else if(data.lastChartStatus=="IN-CYCLE" || data.lastChartStatus=="CUT"){
						replaceAll(replaceAll(decodeURI(data.name), "%23","#"),"%2F","/")
						inCycleMachine++;
					}else if(data.lastChartStatus=="NO-CONNECTION"){
						powerOffMachine++;
					};
				}
				
			});
			
			if(waitMachine==0 && alarmMachine==0){
				borderColor = "green";
			}else if(alarmMachine==0){
				borderColor = "yellow";
			}else if(alarmMachine!=0){
				borderColor = "red";
			};
			
			//bodyNeonEffect(borderColor);
			
			$(".status_span").remove();
			var inCycleSpan = "<span id='inCycleSpan' class='status_span'>" + addZero(String(inCycleMachine)) + "</span>";
			$("#container").append(inCycleSpan);

			var waitSpan = "<span id='waitSpan' class='status_span'>" + addZero(String(waitMachine)) + "</span>";
			$("#container").append(waitSpan);

			var alarmSpan = "<span id='alarmSpan' class='status_span'>" + addZero(String(alarmMachine)) + "</span>";
			$("#container").append(alarmSpan);

			var noConnSpan = "<span id='noConnSpan' class='status_span'>" + addZero(String(powerOffMachine)) + "</span>";
			$("#container").append(noConnSpan);

			var totalSpan = "<div id='totalSpan' class='total'><div id='total_title'>Total</div><div>" + (inCycleMachine + waitMachine + alarmMachine + powerOffMachine) + "<div></div>";
			$("#container").append(totalSpan);

			$("#inCycleSpan").css({
				"position" : "absolute",
				"font-size" : getElSize(70),
				"z-index" : 10,
				"color" : "rgb(28,198,28)"
			});

			$("#inCycleSpan").css({
				"top" : $("#status_chart").offset().top + getElSize(50),
				"left" : $("#status_chart").offset().left + ($("#status_chart").width()/2) - ($("#inCycleSpan").width()/2)
			});
			
			$("#waitSpan").css({
				"position" : "absolute",
				"font-size" : getElSize(70),
				"z-index" : 10,
				"color" : "yellow",
			});
			
			$("#waitSpan").css({
				"left" : $("#status_chart").offset().left + $("#status_chart").width() - getElSize(50) - $("#waitSpan").height(),
				"top" : $("#status_chart").offset().top + ($("#status_chart").height()/2) - ($("#waitSpan").height()/2),
			});
			

			$("#alarmSpan").css({
				"position" : "absolute",
				"font-size" : getElSize(70),
				"z-index" : 10,
				"color" : "#FF0000",
			});
			
			$("#alarmSpan").css({
				"top" : $("#status_chart").offset().top + $("#status_chart").height() - getElSize(50) - $("#alarmSpan").height(),
				"left" : $("#status_chart").offset().left + ($("#status_chart").width()/2) - ($("#alarmSpan").width()/2)
			});

			$("#noConnSpan").css({
				"position" : "absolute",
				"font-size" : getElSize(70),
				"z-index" : 10,
				"color" : "#CFD1D2"
			});
			
			$("#noConnSpan").css({
				"left" : $("#status_chart").offset().left + getElSize(50),
				"top" : $("#status_chart").offset().top + ($("#status_chart").height()/2) - ($("#noConnSpan").height()/2),
			});

			$("#totalSpan").css({
				"position" : "absolute",
				"font-size" : getElSize(40),
				"z-index" : 10,
				"color" : "#ffffff",
				"text-align" : "center"
			});

			$("#total_title").css({
				"font-size" : getElSize(45),
			});

			$("#totalSpan").css({
				"top" : $("#status_chart").offset().top + ($("#status_chart").height()/2) - ($("#totalSpan").height()/2),
				"left" : $("#status_chart").offset().left + ($("#status_chart").width()/2) - ($("#totalSpan").width()/2)  
			});
		}
	});
};

function printMachineName(arrayIdx, x, y, w, h, name, color, fontSize, dvcId){
	//machineName[arrayIdx] = draw.text(nl2br(decodeURIComponent(name))).fill(color);
//	machineName[arrayIdx].font({
//		  family:   'Helvetica'
//		, size:     fontSize
//		, anchor:   'middle'
//		, leading:  '0em'
//	});
	
	if(arrayIdx==46){
		machineName[arrayIdx].click(function(){
			var date = new Date();
			var year = date.getFullYear();
			var month = addZero(String(date.getMonth()+1));
			var day = addZero(String(date.getDate()));
			var today = year + "-" + month + "-" + day;
			var url = ctxPath + "/device/getNightlyMachineStatus.do";
			var param = "stDate=" + $("#sDate").val() + 
						"&edDate=" + $("#sDate").val() +
						"&shopId=" + shopId;
			$("#tableSubTitle").html("기준 시간 : 18시 ~ 07시");
			if(!table_8){
				url = ctxPath + "/device/getNightlyMachineStatus_8.do";
				param = "stDate=" + today + 
						"&edDate=" + today + 
						"&shopId=" + shopId;
				$("#tableSubTitle").html("기준 시간 : 18시 ~ 08시");
			};
			
			$.ajax({
				url : url,
				data: param,
				dataType : "text",
				type : "post",
				success : function(data){
					var json = $.parseJSON(data);
					
					var tr = "<tr style='font-size: 30px; font-weight: bold;' class='tr' >" + 
								"<td class='td'>No</td>" + 
								"<td class='td'>설비명</td>" + 
								"<td class='td'>날짜</td>" + 
								"<td class='td'>정지시간</td>" + 
								"<td class='td'>상태</td>" + 
							"</tr>";
					csvData = "NO., 설비명, 날짜, 정지시간, 상태LINE";
					var status = "";
					var fontSize = getElSize(25);
					var backgroundColor = "";
					var fontColor = "";

					$(json).each(function (idx, data){
						if(data.status=="WAIT" || data.status=="NO-CONNECTION"){
							status = "완료";
							fontColor = "black";
							backgroundColor = "yellow";
						}else if(data.status=='ALARM'){
							status = "중단";
							fontColor = "white";
							backgroundColor = "red";
						}else if(data.status=='IN-CYCLE'){
							status = "가동";
							fontColor = "white";
							backgroundColor = "green";
						};
						
						if(data.isOld=="OLD"){
							status = "미가동";
							fontColor = "white";
							backgroundColor = "black";
						};
						
						tr += "<tr class='contentTr'>" +
								"<td style='border:1px solid white;font-Size:" + fontSize + "px;'>" + (idx+1) + " </td>" +
								"<td style='border:1px solid white;font-Size:" + fontSize + "px;'> " + data.name + "</td>" +
								"<td style='border:1px solid white;font-Size:" + fontSize + "px;'>" + data.stopDate + "</td>" +
								"<td style='border:1px solid white;font-Size:" + fontSize + "px;'>" + data.stopTime + "</td>" + 
								"<td style='border:1px solid white;font-Size:" + fontSize + "px; color:" + fontColor + "; background-color:" + backgroundColor + "'>" + status + "</td>" +
							"</tr>";
						csvData += (idx+1) + "," +
									data.name + "," + 
									data.stopDate + "," + 
									data.stopTime + "," + 
									status + "LINE";
					});
					
					if(json.length==0){
						tr += "<tr  style='border:1px solid white; font-Size:" + fontSize + "px;'>" + 
								"<td colspan='5'>없음</tb>" + 
							"<tr>";
					};
					
					$("#table").html(tr);
					$(".tr").css("font-size", getElSize(30));
					$(".td").css("padding", getElSize(10));
					
					$("#tableDiv").show();
					table_8 = !table_8;
					table_flag = true;
				}
			});
		});
	};
	
//	machineName[arrayIdx].dblclick(function(){
//		if(dvcId!=0){
//			window.sessionStorage.setItem("dvcId", dvcId);
//			window.sessionStorage.setItem("name", name);
//			
//			window.localStorage.setItem("dvcId",dvcId);
//			location.href="/Single_Chart_Status/index.do";
//		};
//	})
	
	var wMargin = 0;
	var hMargin = 0;
	
	if(name=="SET UP"){
		hMargin = contentHeight/(targetHeight/30);
	}else if(name.indexOf("WASHING")!=-1){
		wMargin = contentHeight/(targetHeight/10);
	}else if(name.indexOf("NHM")!=-1){
		wMargin = getElSize(-10);
	}

//	machineName[arrayIdx].x(x + (w/2) - wMargin);
//	machineName[arrayIdx].y(y + (h/2)-(contentHeight/(targetHeight/40)+hMargin));
//	
//	machineName[arrayIdx].leading(1);
	
	var name = "<span id='n" + dvcId + "' class='name'>" + name + "</span>";
	$("body").append(name)
	$("#n" +dvcId).css({
		"position" : "absolute",
		"color" : color,
		"fontSize" : fontSize,
		"z-index" : 9
	});
	
	$("#n" +dvcId).css({
		"top" : $("#svg_td").offset().top + y + (h/2) - ($("#n" + dvcId).height()/2),
		"left" : $("#svg_td").offset().left + x + (w/2) - ($("#n" + dvcId).width()/2),
	}).dblclick(function(){
		if(dvcId!=0){
			window.sessionStorage.setItem("dvcId", dvcId);
			window.sessionStorage.setItem("name", name);
			window.sessionStorage.removeItem("date");
			
			window.localStorage.setItem("dvcId",dvcId);
			location.href="/Single_Chart_Status/index.do?lang=" + window.localStorage.getItem("lang");
		};
	});
};

function nl2br(value) { 
	  return value.replace(/<br>/g, "\n");
};

var machineProp = new Array();
var machineList = new Array();
var machineName = new Array();
var machineStatus = new Array();
var newMachineStatus = new Array();
var machineArray2 = new Array();
var first = true;
var SETTIMEOUT = null;

function getMachineInfo(){
	
	var url = ctxPath + "/svg/getMachineInfo.do";
	var param = "shopId=" + shopId;
	
	$.ajax({
		url : url,
		data : param,
		type : "post",
		dataType : "json",
		success : function (data){
			
			clearInterval(SETTIMEOUT);
			
			$(".name").remove();
			
			var json = data.machineList;
			
			newMachineStatus = new Array();
			machineProp = new Array();
			inCycleMachine = 0;
			waitMachine = 0;
			alarmMachine = 0;
			powerOffMachine = 0;
			operationTime = 0;
			
			//$(".wifi").remove()
			$(json).each(function(key, data){
				if(data.type=="IOL"){
//					if(data.lastChartStatus=="IN-CYCLE" || data.lastChartStatus=="CUT"){
//						replaceAll(replaceAll(decodeURI(data.name), "%23","#"),"%2F","/")
//						inCycleMachine++;
//					}else if(data.lastChartStatus=="WAIT"){
//						waitMachine++;
//					}else if(data.lastChartStatus=="ALARM"){
//						alarmMachine++;
//					}else if(data.lastChartStatus=="NO-CONNECTION"){
//						powerOffMachine++;
//					};
					
					//priority : red > yellow > green
					if(data.lastChartStatus=="ALARM"){
						alarmMachine++;
					}else if(data.lastChartStatus=="WAIT"){
						waitMachine++;
					}else if(data.lastChartStatus=="IN-CYCLE" || data.lastChartStatus=="CUT"){
						replaceAll(replaceAll(decodeURI(data.name), "%23","#"),"%2F","/")
						inCycleMachine++;
					}else if(data.lastChartStatus=="NO-CONNECTION"){
						powerOffMachine++;
					};
					
				}
				
				
				if(data.type=='MTC'){	
					// wilson
					// LAMP
					if(isLamp == true){
											
						//priority : red > yellow > green					
						if(data.red==1){
							alarmMachine++;
							data.lampStatus="ALARM-NOBLINK";
						}else if(data.redBlink==1){
							alarmMachine++;
							data.lampStatus="ALARM";
						}else if(data.yellow==1){
							waitMachine++;
							data.lampStatus="WAIT-NOBLINK";
						}else if(data.yellowBlink==1){
							waitMachine++;
							data.lampStatus="WAIT";
						}else if(data.green==1){
							data.lampStatus="IN-CYCLE";
							inCycleMachine++;
						}else if(data.greenBlink==1){
							data.lampStatus="IN-CYCLE-BLINK";
							inCycleMachine++;
						}else{
							powerOffMachine++;
							data.lampStatus="NO-CONNECTION";
						}
						
					// PMC
					} else{	
						if(data.lastChartStatus == "CUT"){
							data.lampStatus = "IN-CYCLE";
						}else{
							data.lampStatus = data.lastChartStatus;
						}						
					}
				}else{	// IOL
					data.lampStatus = data.lastChartStatus;
				}
				
				var text_color;
				if(data.lampStatus=="WAIT" || data.lampStatus=="WAIT-NOBLINK" || data.lampStatus=="NO-CONNECTION" || data.lampStatus=="ALARM" || data.lampStatus=="ALARM-NOBLINK"){
					text_color = "#000000";
				}else{
					text_color = "#ffffff";
				};
				
				var array = new Array();
				var machineColor = new Array();
				
				array.push(data.id);
				array.push(replaceAll(replaceAll(decodeURI(data.name), "%23","#"),"%2F","/"));
				array.push(getElSize(data.x));
				array.push(getElSize(data.y));
				array.push(getElSize(data.w));
				array.push(getElSize(data.h));
				array.push(data.pic);
				array.push(data.lampStatus);
				array.push(data.dvcId);
				array.push(getElSize(data.fontSize));
				
				operationTime += Number(data.operationTime);
				
				//idx, x, y, w, h, name
				printMachineName(null, getElSize(data.x), getElSize(data.y), getElSize(data.w), getElSize(data.h), replaceAll(replaceAll(decodeURI(data.name), "%23","#"),"%2F","/"), text_color, getElSize(data.fontSize), data.dvcId);
				
				var img_width = getElSize(30);
				
				if(String(data.type).indexOf("IO") == -1 && data.type != null){
					/*var wifi = document.createElement("div");
					wifi.setAttribute("class", "wifi");*/
					var wifi = document.createElement("img");
					wifi.setAttribute("src", ctxPath + "/images/wifi.png");
					wifi.setAttribute("class", "wifi");
					wifi.style.cssText = "position : absolute" + 
					"; width : " + getElSize(50) + "px" +
					"; height : " + getElSize(50) + "px" +
					"; border-radius :50%" + 
					"; top: " + ($("#n" + data.dvcId).offset().top - img_width) + "px" + 
					"; background : white" + 
					"; z-index : " + 9;
					
					if(data.dvcId==66 || data.dvcId==67 || data.dvcId==68){
						wifi.style.cssText +="; left : " + ($("#n" + data.dvcId).offset().left - getElSize(80)) + "px"; 
					}else if(data.dvcId==16){
						wifi.style.cssText +="; left : " + ($("#n" + data.dvcId).offset().left - getElSize(150)) + "px"; 
					}else if(data.dvcId==6 || data.dvcId==20 || data.dvcId==15){
						wifi.style.cssText +="; left : " + ($("#n" + data.dvcId).offset().left - getElSize(120)) + "px"; 
						wifi.style.cssText +="; left : " + ($("#n" + data.dvcId).offset().left - getElSize(120)) + "px"; 
					}else if(data.dvcId==65){
						wifi.style.cssText +="; left : " + ($("#n" + data.dvcId).offset().left - getElSize(60)) + "px" +
						"; top: " + ($("#n" + data.dvcId).offset().top - img_width - getElSize(70)) + "px" ; 
					}else if(data.dvcId==7){
						wifi.style.cssText +="; left : " + ($("#n" + data.dvcId).offset().left - getElSize(50)) + "px" +
						"; top: " + ($("#n" + data.dvcId).offset().top - img_width - getElSize(30)) + "px" ; 
					}else if(data.dvcId==1){
						wifi.style.cssText +="; left : " + ($("#n" + data.dvcId).offset().left - getElSize(30)) + "px" +
						"; top: " + ($("#n" + data.dvcId).offset().top - img_width - getElSize(30)) + "px" ; 
					}else if(data.dvcId==25){
						wifi.style.cssText +="; left : " + ($("#n" + data.dvcId).offset().left - getElSize(30)) + "px" +
						"; top: " + ($("#n" + data.dvcId).offset().top - img_width - getElSize(30)) + "px" ; 
					}else{
						wifi.style.cssText +="; left : " + ($("#n" + data.dvcId).offset().left - getElSize(15)) + "px" +
						"; top: " + ($("#n" + data.dvcId).offset().top - img_width - getElSize(70)) + "px" ; 
					}
					
					$("body").append(wifi)
					
				}
				
				//윌슨
//				$(".status_span").remove();
//				var inCycleSpan = "<span id='inCycleSpan' class='status_span'>" + addZero(String(inCycleMachine)) + "</span>";
//				$("#container").append(inCycleSpan);
//
//				var waitSpan = "<span id='waitSpan' class='status_span'>" + addZero(String(waitMachine)) + "</span>";
//				$("#container").append(waitSpan);
//
//				var alarmSpan = "<span id='alarmSpan' class='status_span'>" + addZero(String(alarmMachine)) + "</span>";
//				$("#container").append(alarmSpan);
//
//				var noConnSpan = "<span id='noConnSpan' class='status_span'>" + addZero(String(powerOffMachine)) + "</span>";
//				$("#container").append(noConnSpan);

				$("#inCycleSpan").css({
					"position" : "absolute",
					"font-size" : getElSize(70),
					"z-index" : 10,
					"color" : "rgb(28,198,28)"
				});

				$("#inCycleSpan").css({
					"top" : $("#status_chart").offset().top + getElSize(50),
					"left" : $("#status_chart").offset().left + ($("#status_chart").width()/2) - ($("#inCycleSpan").width()/2)
				});
				
				$("#waitSpan").css({
					"position" : "absolute",
					"font-size" : getElSize(70),
					"z-index" : 10,
					"color" : "yellow",
				});
				
				$("#waitSpan").css({
					"left" : $("#status_chart").offset().left + $("#status_chart").width() - getElSize(50) - $("#waitSpan").height(),
					"top" : $("#status_chart").offset().top + ($("#status_chart").height()/2) - ($("#waitSpan").height()/2),
				});
				

				$("#alarmSpan").css({
					"position" : "absolute",
					"font-size" : getElSize(70),
					"z-index" : 10,
					"color" : "#FF0000",
				});
				
				$("#alarmSpan").css({
					"top" : $("#status_chart").offset().top + $("#status_chart").height() - getElSize(50) - $("#alarmSpan").height(),
					"left" : $("#status_chart").offset().left + ($("#status_chart").width()/2) - ($("#alarmSpan").width()/2)
				});

				$("#noConnSpan").css({
					"position" : "absolute",
					"font-size" : getElSize(70),
					"z-index" : 10,
					"color" : "#CFD1D2"
				});
				
				$("#noConnSpan").css({
					"left" : $("#status_chart").offset().left + getElSize(50),
					"top" : $("#status_chart").offset().top + ($("#status_chart").height()/2) - ($("#noConnSpan").height()/2),
				});

				$("#totalSpan").css({
					"position" : "absolute",
					"font-size" : getElSize(40),
					"z-index" : 10,
					"color" : "#ffffff",
					"text-align" : "center"
				});

				$("#total_title").css({
					"font-size" : getElSize(45),
				});

				$("#totalSpan").css({
					"top" : $("#status_chart").offset().top + ($("#status_chart").height()/2) - ($("#totalSpan").height()/2),
					"left" : $("#status_chart").offset().left + ($("#status_chart").width()/2) - ($("#totalSpan").width()/2)  
				});
				
				machineProp.push(array);
				machineColor.push(data.id);
				machineColor.push(data.lampStatus);
				newMachineStatus.push(machineColor);
			});
			// wilson
//			var totalSpan = "<div id='totalSpan' class='total'><div id='total_title'>Total</div><div>" + (inCycleMachine + waitMachine + alarmMachine + powerOffMachine) + "<div></div>";
//			$("#container").append(totalSpan);
			
			if(!compare(machineStatus,newMachineStatus)){
				//clearInterval(border_interval);
				reDrawMachine();
				redrawPieChart();
			};
			
			if(first){
				for(var i = 0; i < machineProp.length; i++){
					//array = id, name, x, y, w, h, pic, status, idx, dvcId, fontSize
					
					drawMachine(machineProp[i][0],
							machineProp[i][1],
							machineProp[i][2],
							machineProp[i][3],
							machineProp[i][4],
							machineProp[i][5],
							machineProp[i][6],
							machineProp[i][7],
							i,
							machineProp[i][8],
							machineProp[i][9]);
				};
				first = false;
			};
			
			//cal totalOperationRatio
			totalOperationRatio = 0;
			for(var i = 0; i < machineArray2.length; i++){
				totalOperationRatio += machineArray2[i][20];
			};
			
			var totalMachine = 0;
			totalMachine += (inCycleMachine + waitMachine + alarmMachine);
			var totalOperationRatio = Number(inCycleMachine / totalMachine * 100).toFixed(1);
			
			SETTIMEOUT = setInterval(getMachineInfo, 5000);
		}
	});
	
	
};

function replaceAll(str, src, target){
	return str.split(src).join(target)
};

function reDrawMachine(){
	for(var i = 0; i < machineStatus.length; i++){
		if(machineStatus[i][1]!=newMachineStatus[i][1]){
			machineList[i].remove();
			
			//array = id, name, x, y, w, h, pic, status
			
			drawMachine(machineProp[i][0],
					machineProp[i][1],
					machineProp[i][2],
					machineProp[i][3],
					machineProp[i][4],
					machineProp[i][5],
					machineProp[i][6],
					newMachineStatus[i][1],
					i,
					machineProp[i][8],
					machineProp[i][9]);
		}
	};
	
	machineStatus = newMachineStatus;
};

function setScrollAble(){
	for(var i = 0; i < machineStatus.length; i++){
		setDraggable(i, machineProp[i][0],machineProp[i][4], machineProp[i][5]);
	}
}


function compare ( a, b ){
	var type = typeof a, i, j;
	 
	if( type == "object" ){
		if( a === null ){
			return a === b;
		}else if( Array.isArray(a) ){ //배열인 경우
			//기본필터
	      if( !Array.isArray(b) || a.length != b.length ) return false;
	 
	      //요소를 순회하면서 재귀적으로 검증한다.
	      for( i = 0, j = a.length ; i < j ; i++ ){
	        if(!compare(a[i], b[i]))return false;
	      	};
	      return true;
	    };
	  };
	 
	  return a === b;
};

function getMarker(){
	var url = ctxPath + "/svg/getMarker.do";
	
	$.ajax({
		url : url,
		type: "post",
		dataType : "json",
		success : function(data){
			marker = draw.image(imgPath + data.pic + ".png").size(data.w, data.h);
			marker.x(data.x);
			marker.y(data.y);
			
			
			//marker.draggable();
			marker.dragmove  = function(delta, evt){
				var id = data.id;
				var x = marker.x();
				var y = marker.y();
				
				//DB Access
				setMachinePos(id, x, y);
			};
		}
	});
};

var table_8 = true
function drawMachine(id, name, x, y, w, h, pic, status, i, dvcId, fontSize){
//	if(dvcId==16){
//		console.log(dvcId + " :" + status)
//	}
	
	var svgFile;
	var timeStamp = new Date().getMilliseconds();
	if(status==null){
		svgFile = ".svg";
	}else{
		if(status=='CUT'){
			status='IN-CYCLE'
		}
		svgFile = "_" + status + ".svg";
	};
	
	machineList[i] = draw.image(imgPath + pic + svgFile + "?dummy=" + timeStamp).size(w, h);
	machineList[i].x(x);
	machineList[i].y(y);
	
	var text_color;
	if(status=="WAIT" || status=="NO-CONNECTION"){
		text_color = "#000000";
	}else{
		text_color = "#ffffff";
	};
	
	
	//idx, machineId, w, h
	
	
	
	if(id==46){
		machineList[i].click(function(){
			var date = new Date();
			var year = date.getFullYear();
			var month = addZero(String(date.getMonth()+1));
			var day = addZero(String(date.getDate()));
			var today = year + "-" + month + "-" + day;
			var url = ctxPath + "/device/getNightlyMachineStatus.do";
			var param = "stDate=" + $("#sDate").val() + 
						"&edDate=" + $("#sDate").val() + 
						"&shopId=" + shopId;
			$("#tableSubTitle").html("기준 시간 : 18시 ~ 07시");
			if(!table_8){
				url = ctxPath + "/device/getNightlyMachineStatus_8.do";
				param = "stDate=" + today + 
						"&edDate=" + today + 
						"&shopId=" + shopId;
				$("#tableSubTitle").html("기준 시간 : 18시 ~ 08시");
			};
			
			$.ajax({
				url : url,
				data: param,
				dataType : "text",
				type : "post",
				success : function(data){
					var json = $.parseJSON(data);
					
					var tr = "<tr style='font-size: 30px; font-weight: bold;' class='tr' >" + 
								"<td class='td'>No</td>" + 
								"<td class='td'>설비명</td>" + 
								"<td class='td'>날짜</td>" + 
								"<td class='td'>정지시간</td>" + 
								"<td class='td'>상태</td>" + 
							"</tr>";
					csvData = "NO., 설비명, 날짜, 정지시간, 상태LINE";
					var status = "";
					var fontSize = getElSize(25);
					var backgroundColor = "";
					var fontColor = "";

					$(json).each(function (idx, data){
						if(data.status=="WAIT" || data.status=="NO-CONNECTION"){
							status = "완료";
							fontColor = "black";
							backgroundColor = "yellow";
						}else if(data.status=='ALARM'){
							status = "중단";
							fontColor = "white";
							backgroundColor = "red";
						}else if(data.status=='IN-CYCLE'){
							status = "가동";
							fontColor = "white";
							backgroundColor = "green";
						};
						
						if(data.isOld=="OLD"){
							status = "미가동";
							fontColor = "white";
							backgroundColor = "black";
						};
						
						tr += "<tr class='contentTr'>" +
								"<td style='border:1px solid white;font-Size:" + fontSize + "px;'>" + (idx+1) + " </td>" +
								"<td style='border:1px solid white;font-Size:" + fontSize + "px;'> " + data.name + "</td>" +
								"<td style='border:1px solid white;font-Size:" + fontSize + "px;'>" + data.stopDate + "</td>" +
								"<td style='border:1px solid white;font-Size:" + fontSize + "px;'>" + data.stopTime + "</td>" + 
								"<td style='border:1px solid white;font-Size:" + fontSize + "px; color:" + fontColor + "; background-color:" + backgroundColor + "'>" + status + "</td>" +
							"</tr>";
						csvData += (idx+1) + "," +
									data.name + "," + 
									data.stopDate + "," + 
									data.stopTime + "," + 
									status + "LINE";
					});
					
					if(json.length==0){
						tr += "<tr  style='border:1px solid white; font-Size:" + fontSize + "px;'>" + 
								"<td colspan='5'>없음</tb>" + 
							"<tr>";
					};
					
					$("#table").html(tr);
					$(".tr").css("font-size", getElSize(30));
					$(".td").css("padding", getElSize(10));
					
					$("#tableDiv").show();
					table_8 = !table_8;
					table_flag = true;
				}
			});
		});
	};
	
	machineList[i].dblclick(function(){
		if(dvcId!=0){
			window.sessionStorage.setItem("dvcId", dvcId);
			window.sessionStorage.setItem("name", name);
			window.sessionStorage.removeItem("date");
			
			window.localStorage.setItem("dvcId",dvcId);
			
			location.href="/Single_Chart_Status/index.do?lang=" + window.localStorage.getItem("lang");
		};
	})
};


function drawAlarmTimer(x, y, w, h, id){
	var timerDiv = $("<div id='timer'>"+
								"<table>"+
								"<tr>"+
									"<tD>"+
										"<div id='minute1-" + id + "' class='odometer'>0</div>"+
									"</tD>"+
									"<tD>"+
										"<div id='minute2-" + id + "' class='odometer'>0</div>"+
									"</tD>"+
									"<tD valign='middle'>"+
										"<font  style='font-size: 30px; font-weight:bolder;'>:</font>"+
									"</tD>"+
									"<tD>"+
										"<div id='second1-" + id + "' class='odometer'>0</div>"+
									"</tD>"+
									"<tD>"+
										"<div id='second2-" + id + "' class='odometer'>0</div>"+
									"</tD>"+
								"</tr>"+
							"</table>"+
						"</div>");
	
	$(timerDiv).css({
		"position" : "absolute",
		//"width" : 200,
		"left" : x + (w/2) - (100),
		"top" : y + (h+5)
	});
	
	$("#odometerDiv").append(timerDiv);
};

function setDraggable(arrayIdx, id, w, h){
	
		console.log("false")
		machineList[arrayIdx].draggable(false);
	
};

function setMachinePos(id, x, y){
	
	var url = ctxPath + "/svg/setMachinePos.do";
	var param = "id=" + id + 
					"&x=" + x + 
					"&y=" + y;
	
	$.ajax({
		url : url,
		data : param,
		type : "post",
		success : function(data){}
	});
};
