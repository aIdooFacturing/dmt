//machineList id, company, name, comIp, x, y
var dvcId = window.localStorage.getItem("dvcId");

var cPage = 1;
var panel = false;
var colors, colors_a;
var subMenuTr = false;
var status3, status4;
var circleWidth = contentWidth * 0.07;
var canvas;
var ctx;
var dvcName = [];

$(function() {
	getStartTime();	
	BindEvt();
	getToday();
	$("#excel").click(csvSend);

	getDetailData();
	
	setToday();
	
	$("#sDate, #eDate").change(getReportBarData);
	
	$("#panel_table tr:nth(1) td:nth(0) img").css({
		"border" : getElSize(20) + "px solid rgb(33,128,250)"
	});
	
	drawChart();
	
	getMachineName();
	getRapairDataList();
	getAlarmList();
	setEl();
	setTimeout(labelDayNight, 1000);
});

var startTimeLabel = new Array();

var startHour, startMinute; 

function getStartTime(){
	var url = ctxPath + "/chart/getStartTime.do";
	var param = "shopId=6";
	
	$.ajax({
		url : url,
		data : param,
		dataType : "text",
		type : "post",
		success : function(data){
			startHour = data.split("-")[0]
			startMinute = data.split("-")[1]

			drawBarChart("container");
		}, error : function(e1,e2,e3){
			console.log(e1,e2,e3)
		}
	});
};

function getAlarmList(){
	var url = ctxPath + "/chart/getAlarmList.do";
	var param = "dvcId=" + dvcId;
	
	$.ajax({
		url : url,
		data : param,
		type : "post",
		dataType : "json",
		success : function(data){
			var json = data.dataList;
				
			var tr = "";
			var sTime = "";
			var eTime = "";
			
			$(json).each(function(idx, data){
				sTime = data.startDateTime.substr(0, 19);
				eTime = data.endDateTime;
				if(eTime!="") eTime = data.endDateTime.substr(0, 19);
				
				if(data.ncAlarmNum1!=""){
					tr += "<tr>" + 
						"<td align='center'>" + sTime + "</td>" +
						"<td align='center'>" + eTime + "</td>" + 
						"<td>" + data.ncAlarmNum1 + "-" + decodeURIComponent(data.ncAlarmMsg1).replace(/\+/gi, " ") + "</td>" + 
					"</tr>";
				};
				if(data.ncAlarmNum2!=""){
					tr += "<tr>" + 
						"<td align='center'>" + sTime + "</td>" +
						"<td align='center'>" + eTime + "</td>" + 
						"<td>" + data.ncAlarmNum2 + "-" + decodeURIComponent(data.ncAlarmMsg2).replace(/\+/gi, " ") + "</td>" + 
					"</tr>";
				};
				if(data.ncAlarmNum3!=""){
					tr += "<tr>" + 
						"<td align='center'>" + sTime + "</td>" +
						"<td align='center'>" + eTime + "</td>" + 
						"<td>" + data.ncAlarmNum3 + "-" + decodeURIComponent(data.ncAlarmMsg3).replace(/\+/gi, " ") + "</td>" + 
					"</tr>";
				};
				if(data.ncAlarmNum4!=""){
					tr += "<tr>" + 
						"<td align='center'>" + sTime + "</td>" +
						"<td align='center'>" + eTime + "</td>" + 
						"<td>" + data.ncAlarmNum4 + "-" + decodeURIComponent(data.ncAlarmMsg4).replace(/\+/gi, " ") + "</td>" + 
					"</tr>";
				};
				if(data.ncAlarmNum5!=""){
					tr += "<tr>" + 
						"<td align='center'>" + sTime + "</td>" +
						"<td align='center'>" + eTime + "</td>" + 
						"<td>" + data.ncAlarmNum5 + "-" + decodeURIComponent(data.ncAlarmMsg5).replace(/\+/gi, " ") + "</td>" + 
					"</tr>";
				};
				if(data.pmcAlarmNum1!=""){
					tr += "<tr>" + 
						"<td align='center'>" + sTime + "</td>" +
						"<td align='center'>" + eTime + "</td>" + 
						"<td>" + data.pmcAlarmNum1 + "-" + decodeURIComponent(data.pmcAlarmMsg1).replace(/\+/gi, " ") + "</td>" + 
					"</tr>";
				};
				if(data.pmcAlarmNum2!=""){
					tr += "<tr>" + 
						"<td align='center'>" + sTime + "</td>" +
						"<td align='center'>" + eTime + "</td>" + 
						"<td>" + data.pmcAlarmNum2 + "-" + decodeURIComponent(data.pmcAlarmMsg2).replace(/\+/gi, " ") + "</td>" + 
					"</tr>";
				};
				if(data.pmcAlarmNum3!=""){
					tr += "<tr>" + 
						"<td align='center'>" + sTime + "</td>" +
						"<td align='center'>" + eTime + "</td>" + 
						"<td>" + data.pmcAlarmNum3 + "-" + decodeURIComponent(data.pmcAlarmMsg3).replace(/\+/gi, " ") + "</td>" + 
					"</tr>";
				};
				if(data.pmcAlarmNum4!=""){
					tr += "<tr>" + 
					
						"<td align='center'>" + sTime + "</td>" +
						"<td align='center'>" + eTime + "</td>" + 
						"<td>" + data.pmcAlarmNum4 + "-" + decodeURIComponent(data.pmcAlarmMsg4).replace(/\+/gi, " ") + "</td>" + 
					"</tr>";
				};
				if(data.pmcAlarmNum5!=""){
					tr += "<tr>" + 
						"<td align='center'>" + sTime + "</td>" +
						"<td align='center'>" + eTime + "</td>" + 
						"<td>" + data.pmcAlarmNum5 + "-" + decodeURIComponent(data.pmcAlarmMsg5).replace(/\+/gi, " ") + "</td>" + 
					"</tr>";
				};
				
			});
			
			if(json.length==0){
				tr = "<tr><td colspan='3' align='center'>기록된 알람이 없습니다</td></tr>";
			}
			
			$("#alarmTable").append(tr).css({
				"color" : "white"
			});
			
			$("#alarmTable td").css({
				"padding" : getElSize(10),
				"font-size" : getElSize(35)
			})
		}
	});
};

function getRapairDataList(){
	var url = ctxPath + "/chart/getMcNo.do";
	var param = "dvcId=" + dvcId;
	
	$.ajax({
		url : url,
		data : param,
		dataType : "json",
		type : "post",
		success : function(data){
			var mcNo = data.mcNo;
			
			$.get(ctxPath + "/pop/getListRep.do?mcNo=" + mcNo, function(data, status){
				var table = "";
				var stDate;
				var fnDate;
				
				$(data).each(function(idx, data){
					stDate = data.stDate;
					fnDate = data.fnDate;
					if(data==null) stDate = "-";
					if(data==null) fnDate = "-";
					table += "<Tr onclick=showDetailRep('" + idx + "','" + mcNo + "')>" +
									"<td align='center'>" + stDate + "</td>" + 
									"<td align='center'>" + fnDate + "</td>" + 
									"<td align='center'>" + data.ename + "</td>" +
									"<td>" + data.repCause.substr(0,30) + "</td>" + 
							"</tr>";
					
					//console.log(data.ename + "," + data.fnDate + "," + data.mcNo + "," + data.repCause + "," + data.stDate)
				});
				
				if(data==null || data==""){
					table = "<tr><td colspan='4' align='center'>이력 없음</td></tr>";
				};
				
				$("#repairTable").append(table)
				$("#repairTable td").css({
					"color" : "white",
					"font-size" : getElSize(40),
					"padding" : getElSize(10)
				});
				
				$(".repair_header td").css({
					"color" : "black",
				});
			});
			
			
		}, error : function(e1,e2,e3){
		}
	});
};

function showDetailRep(n, mcNo){
	$("#detailRepBox").fadeOut(500, function(){
		$.get(ctxPath + "/pop/getListRep.do?mcNo=" + mcNo, function(data, status){
			$("#detailRepBox").html(data[n].repCause.replace(/\n/g, "<br />"));  
			
			$("#detailRepBox").css({
				"left" : (originWidth/2) - ($("#detailRepBox").width()/2),
				"top" :(originHeight/2) - ($("#detailRepBox").height()/2),
				"line-height" : "150%"
			});
			
			$("#detailRepBox").fadeIn(500);
			$("#detailRepBox").click(function(){
				$("#detailRepBox").fadeOut(500);
			});
		});
	});
};

function getMachineName(){
	var url = ctxPath + "/chart/getMachineName.do";
	
	var param = "dvcId=" + dvcId;
	$.ajax({
		url : url,
		data : param,
		dataType : "text",
		type : "post",
		success : function(data){
			$("#machine_name").html(decodeURIComponent(data).replace(/\+/gi, " "));
			$("#machine_name").css({
				"margin-top" : $("#machine_name_td").height() / 2
								- $("#machine_name").height() / 2
								- $(".subTitle").height()
			});
		}
	});
	
};

/**
 * 
 */
function getDetailData(){
	var url = ctxPath + "/chart/getDetailBlockData.do";
	var param = "dvcId=" + dvcId;
	
	$.ajax({
		url : url,
		data : param,
		dataType : "json",
		type : "post",
		success : function(data){
			var json = data.dataList;
			
			var toolTr = "<Tr style='background-color:rgb(34,34,34); color:white; font-weight: bolder;'>"+
							"<td>T.NO</td>"+
							"<td>가공시간</td>"+
							"<td>T.NO</td>"+
							"<td>가공시간</td>"+
						"</Tr>";
			var alarm = "";
			$(json).each(function(idx, data){
				var hour = new Date().getHours()-8;
				if(hour==0) hour=1;
				 
				var sign = "";
				
				$("#daily_target_cycle").html(data.tgCnt).css("font-size",getElSize(200));
				$("#complete_cycle").html(data.lastFnPrdctNum*2);
				$("#daily_avg_cycle_time").html(Number(data.LastAvrCycleTime/60).toFixed(1)).css("font-size",getElSize(200));
				$("#daily_length").html(Number(data.lastFnPrdctNum/hour).toFixed(1)).css("font-size",getElSize(200));
				
				
				
				//var remainCnt = data.remainCnt;
				var remainCnt = data.tgCnt - (data.lastFnPrdctNum*2);
				var color = "";
				if(Number(remainCnt)>0){
					sign = "-";
					color = "red"
				}else{
					sign = "+";
					remainCnt = Math.abs(remainCnt);
					color = "blue";
				};
				
				$("#downValue").html(sign + remainCnt).css({
					"font-size" : getElSize(100),
					"color" : color
				})
				
				
				//data.lastAlarmCode + " - " + data.lastAlarmMsg;
				var alarm1, alarm2, alarm3, alarm4, alarm5, alarm6, alarm7, alarm8, alarm9, alarm10;
				if(data.ncAlarmNum1==""){
					alarm1 = "";
				}else{
					alarm1 = data.ncAlarmNum1 + " - " +  decodeURIComponent(data.ncAlarmMsg1).replace(/\+/gi, " ") + "<br>";
				};
				if(data.ncAlarmNum2==""){
					alarm2 = "";
				}else{
					alarm2 = data.ncAlarmNum2 + " - " +  decodeURIComponent(data.ncAlarmMsg2).replace(/\+/gi, " ") + "<br>";
				};
				if(data.ncAlarmNum3==""){
					alarm3 = "";
				}else{
					alarm3 = data.ncAlarmNum3 + " - " +  decodeURIComponent(data.ncAlarmMsg3).replace(/\+/gi, " ") + "<br>";
				};
				if(data.ncAlarmNum4==""){
					alarm4 = "";
				}else{
					alarm4 = data.ncAlarmNum4 + " - " +  decodeURIComponent(data.ncAlarmMsg4).replace(/\+/gi, " ") + "<br>";
				};
				if(data.ncAlarmNum5==""){
					alarm5 = "";
				}else{
					alarm5 = data.ncAlarmNum5 + " - " +  decodeURIComponent(data.ncAlarmMsg5).replace(/\+/gi, " ") + "<br>";
				};
				if(data.pmcAlarmNum1==""){
					alarm6 = "";
				}else{
					alarm6 = data.pmcAlarmNum1 + " - " +  decodeURIComponent(data.pmcAlarmMsg1).replace(/\+/gi, " ") + "<br>";
				};
				if(data.pmcAlarmNum1==""){
					alarm7 = "";
				}else{
					alarm7 = data.pmcAlarmNum2 + " - " +  decodeURIComponent(data.pmcAlarmMsg2).replace(/\+/gi, " ") + "<br>";
				};
				if(data.pmcAlarmNum3==""){
					alarm8 = "";
				}else{
					alarm8 = data.pmcAlarmNum3 + " - " +  decodeURIComponent(data.pmcAlarmMsg3).replace(/\+/gi, " ") + "<br>";
				};
				if(data.pmcAlarmNum4==""){
					alarm9 = "";
				}else{
					alarm9 = data.pmcAlarmNum4 + " - " +  decodeURIComponent(data.pmcAlarmMsg4).replace(/\+/gi, " ") + "<br>";
				};
				if(data.pmcAlarmNum5==""){
					alarm10 = "";
				}else{
					alarm10 = data.pmcAlarmNum5 + " - " +  decodeURIComponent(data.pmcAlarmMsg5).replace(/\+/gi, " ") + "<br>";
				};
				
				
				alarm += alarm1 +
						alarm2 +
						alarm3 + 
						alarm4 + 
						alarm5 + 
						alarm6 +
						alarm7 + 
						alarm8 + 
						alarm9 +
						alarm10;
						
				$("#alarm").html(alarm);	
				
				//toolTable
				
				toolTr += "<tr>" +  
							"<td style='background-color:rgb(34,34,34); color:white; font-weight: bolder;'>" + data.toolNum1 + "</td>" + 
							"<td style='color:white;text-align:right; padding-right:" + getElSize(30) + "'>" + data.toolTime1 + "</td>" + 
							"<td style='background-color:rgb(34,34,34); color:white; font-weight: bolder;'>" + data.toolNum7 + "</td>" + 
							"<td style='color:white;text-align:right; padding-right:" + getElSize(30) + "'>" + data.toolTime7 + "</td>" +  
						"</tr>" +
						"<tr>" +  
							"<td style='background-color:rgb(34,34,34); color:white; font-weight: bolder;'>" + data.toolNum2 + "</td>" + 
							"<td style='color:white;text-align:right; padding-right:" + getElSize(30) + "'>" + data.toolTime2 + "</td>" + 
							"<td style='background-color:rgb(34,34,34); color:white; font-weight: bolder;'>" + data.toolNum8 + "</td>" + 
							"<td style='color:white;text-align:right; padding-right:" + getElSize(30) + "'>" + data.toolTime8 + "</td>" +  
						"</tr>" +
						"<tr>" +  
							"<td style='background-color:rgb(34,34,34); color:white; font-weight: bolder;'>" + data.toolNum3 + "</td>" + 
							"<td style='color:white;text-align:right; padding-right:" + getElSize(30) + "'>" + data.toolTime3 + "</td>" + 
							"<td style='background-color:rgb(34,34,34); color:white; font-weight: bolder;'>" + data.toolNum9 + "</td>" + 
							"<td style='color:white;text-align:right; padding-right:" + getElSize(30) + "'>" + data.toolTime9 + "</td>" +  
						"</tr>" +
						"<tr>" +  
							"<td style='background-color:rgb(34,34,34); color:white; font-weight: bolder;'>" + data.toolNum4 + "</td>" + 
							"<td style='color:white;text-align:right; padding-right:" + getElSize(30) + "'>" + data.toolTime4 + "</td>" + 
							"<td style='background-color:rgb(34,34,34); color:white; font-weight: bolder;'>" + data.toolNum10 + "</td>" + 
							"<td style='color:white;text-align:right; padding-right:" + getElSize(30) + "'>" + data.toolTime10 + "</td>" +  
						"</tr>" +
						"<tr>" +  
							"<td style='background-color:rgb(34,34,34); color:white; font-weight: bolder;'>" + data.toolNum5 + "</td>" + 
							"<td style='color:white;text-align:right; padding-right:" + getElSize(30) + "'>" + data.toolTime5 + "</td>" + 
							"<td style='background-color:rgb(34,34,34); color:white; font-weight: bolder;'>" + data.toolNum11 + "</td>" + 
							"<td style='color:white;text-align:right; padding-right:" + getElSize(30) + "'>" + data.toolTime11 + "</td>" +  
						"</tr>" +
						"<tr>" +  
							"<td style='background-color:rgb(34,34,34); color:white; font-weight: bolder;'>" + data.toolNum6 + "</td>" + 
							"<td style='color:white;text-align:right; padding-right:" + getElSize(30) + "'>" + data.toolTime6 + "</td>" + 
							"<td style='background-color:rgb(34,34,34); color:white; font-weight: bolder;'>" + data.toolNum12 + "</td>" + 
							"<td style='color:white;text-align:right; padding-right:" + getElSize(30) + "'>" + data.toolTime12 + "</td>" +  
						"</tr>";
				
				$("#toolTable").html(toolTr);
				
				var n = (data.lastFnPrdctNum*2)/data.tgCnt*100;
				//barChart.series[0].data[0].update(Number(Number(n).toFixed(1)));
				
				barChart.series[0].data[0].update(100);
			});
			
			
			$("#downValue").css({
				"position" : "absolute",
				"top" : $("#complete_cycle").offset().top + $("#complete_cycle").height()
			});

			$("#downValue").css({
				"left" : $("#complete_cycle").offset().left + $("#complete_cycle").width() - $("#downValue").width(),
			});
			
			setTimeout(getDetailData, 5000)
			
		}
	});
};

var barChart;
var colors;
function drawChart(){
	var perShapeGradient = {
            x1: 0,
            y1: 0,
            x2: 0,
            y2: 1
        };
        colors = Highcharts.getOptions().colors;
        colors = [{
            linearGradient: perShapeGradient,
            stops: [
                    [0, '#6EF8FC'],
                    [1, '#BBFAF4']
                 ]
            }];
        
	$('#barChart').highcharts({
        chart: {
            type: 'bar',
            backgroundColor : "rgba(0,0,0,0)",
            width : originWidth * 0.95,
            height : getElSize(150),
            marginTop :0,
            marginBottom : 0,
            marginLeft : 0,
            marginRight : 0
        },
        exporting : false,
        credits :false,
        title: {
            text: false,
        },
        xAxis: {
//            categories: [''],
            labels : {
        		enabled : false
        	},
        },
        yAxis: {
            min: 0,
            max : 100,
            title: {
                text: false
            },
            gridLineWidth: 0,
        },
        legend: {
            reversed: true,
        		enabled : false
        },
        tooltip: {
            enabled: false
        },

        plotOptions: {
            series: {
                stacking: 'normal',
                pointWidth : getElSize(200),
                dataLabels: {
                	format: '일일 목표 달성률 : {point.y:.1f}%',
                    enabled: true,
                    color: (Highcharts.theme && Highcharts.theme.dataLabelsColor) || 'white',
                    style: {
                        textShadow: '0 0 3px black',
                        fontSize : getElSize(100)
                    }
                }
            }
        },
        series: [{
            name: 'John',
            data: [0],
            borderWidth: 0,
            color : colors[0],
            shadow: {
                color: '#6EF8FC',
                width: getElSize(100),
                offsetX: 0,
                offsetY: 0
            }
        }]
    });
	
	barChart = $('#barChart').highcharts();
};

function labelDayNight(){
	var left = $(".highcharts-series-group").offset().left;
	var hour = $("#container").highcharts().chartWidth/24;
	var top = $(".highcharts-series-group").offset().top;
	var margin = $("#container").highcharts().marginRight+getElSize(10);
	var day = document.createElement("div");
	var night = document.createElement("div");
	
	var sun = document.createElement("img");
	sun.setAttribute("src",ctxPath + "/images/moon.png");
	$(day).append(sun);
	
	var moon = document.createElement("img");
	moon.setAttribute("src",ctxPath + "/images/sun.png");
	$(night).append(moon);
	
	$(sun).css({
		"height" : "95%" 
	});
	
	$(moon).css({
		"height" : "100%" 
	});
	
	$("#part1").prepend(day);
	$("#part1").prepend(night);
	
	
	$(day).css({
		"position" : "absolute",
		"left" : left + 10,
		"width" : (hour*11) + (hour/2) -  margin,
		"height" : getElSize(100),
		"top" : top - getElSize(100),
		"border-top" : "1px solid white",
		"border-left" : "1px solid white",
		"border-right" : "1px solid white",
		"color" : "white",
		"font-size" : getElSize(50),
		"text-align" : "center",
		"background-color":"rgb(34,34,34)"
	});
	
	$(night).css({
		"position" : "absolute",
		"left" : $(day).offset().left + $(day).width(),
		"width" : (hour*13) - (hour/2) - margin,
		"height" : getElSize(100),
		"top" : top - getElSize(100),
		"border-top" : "1px solid white",
		"border-left" : "1px solid white",
		"border-right" : "1px solid white",
		"color" : "white",
		"font-size" : getElSize(50),
		"text-align" : "center",
		"background-color":"rgb(34,34,34)"
	});
};

function togglePanel() {
	var panelDist;
	var btnDist;

	if (panel) {
		panelDist = -(originWidth * 0.2) - getElSize(20) * 2;
		btnDist = getElSize(30);
		
		hideCorver();
	} else {
		panelDist = 0;
		btnDist = (originWidth * 0.2) + ($("#menu_btn").width() / 3.5)
				+ getElSize(20);
		
		showCorver();
	};

	panel = !panel;

	$("#panel").animate({
		"left" : panelDist
	});
	$("#menu_btn").animate({
		"left" : btnDist
	});
};

function BindEvt(){
	$("#DashBoard_1").click(function(){
		location.href = ctxPath + "/chart/chart_block.do";
	});
	
	$("#Rail").click(function(){
		location.href = ctxPath + "/chart/chart_rail.do";
	});
	
	$("#history").click(function(){
		location.href = ctxPath + "/chart/timeLine.do";
	});
	
	$("#menu_btn").click(togglePanel);
	$("#main_logo").click(function(){
		//location.href = ctxPath + "/chart/chart_real.do";
	});
	
	$("#time_table").click(function(){
		//location.href = ctxPath + "/chart/barChart.do";
	});
	
	$("#tool").click(showRepairList);
	$("#alarm_mark").click(showAlarmList);
	$("#close_btn").click(closeBox);
};

var opendBox = "";
function showRepairList(){
	opendBox = "repair";
	
	$("#alarmBox").fadeOut(500);
	$("#repairBox").fadeIn(500);
	$("#close_btn").fadeIn(500);
	showCorver();
};

function showAlarmList(){
	opendBox = "alarm";
	
	$("#repairBox").fadeOut(500);
	$("#alarmBox").fadeIn(500)
	$("#close_btn").fadeIn(500);
	showCorver();
};

function closeBox(){
	$("#" + opendBox + "Box").fadeOut(500);
	$("#close_btn").fadeOut(500);
	$("#detailRepBox").fadeOut(500);
	hideCorver();
};

function enableDrawIcon(){
	$(".init_machine_icon").draggable({
		start: function(){
		},
		stop : function(){
			var offset = $(this).offset();
            var x = offset.left - marginWidth;
            var y = offset.top - marginHeight;
            var id = this.id;
            id = id.substr(6);
            
           setInitCardPos(id, setElSize(x), setElSize(y));
		},
	});
};


var blocked_icon = [];
var init_box_top = 0,
	init_box_left = 0;
var init_icon_offset = [];


function getToday(){
	var date = new Date();
	var year = date.getFullYear();
	var month = addZero(String(date.getMonth() + 1));
	var day = addZero(String(date.getDate()));
	var hour = addZero(String(date.getHours()));
	var minute = addZero(String(date.getMinutes()));
	var second = addZero(String(date.getSeconds()));
	
	$("#today").html(year + ". " + month + ". " + day + "<br>" + hour + ":" + minute + ":" + second)
	
	setTimeout(getToday,1000);
}

var report_opRatio
function getDvcTemper(dvcId){
	var url = ctxPath + "/chart/getDvcTemper.do";
	var param = "dvcId=" + dvcId;
	
	$.ajax({
		url : url,
		data : param,
		dataType : "json",
		type : "post",
		success : function(data){
			thermometer(data.temperMill, "thermometer");
			thermometer(data.temperLeft, "thermometer2");
			thermometer(data.temperRight, "thermometer3");
		}
	});
};

var opRatio = 0;
var auto_flag = false;
function getReportBarData(){
	 var url = ctxPath + "/chart/getReportBarData.do";
	 var sDate = $("#sDate").val();
	 var eDate = $("#eDate").val();
	 var param = "sDate=" + sDate + 
	 			"&eDate=" + eDate;
	 
	 report_opRatio = 0;
	 $.ajax({
		url : url,
		type : "post",
		data : param,
		dataType : "json",
		success  : function(data){
			var json = data.dataList;
			reportChartName = new Array();
			noconnBar = new Array();
			alarmBar = new Array();
			waitBar = new Array();
			inCycleBar = new Array();
			opTime = 0;
			incycleTime_avg = 0;
			alatmTime_avg = 0;
			opRatio = 0;
			$(json).each(function(idx, data){
				reportChartName.push(data.name);
				//if(data.noconnTime>0){
					noconnBar.push(0);
//				};
				if(data.alarmTime>0){
					alarmBar.push(Number(Number(Number(data.alarmTime)/60/60).toFixed(1)));
				};
				if(data.waitTime>0){
					waitBar.push(Number(Number(Number(data.waitTime)/60/60).toFixed(1)));
				};
				if(data.incycleTime>0){
					inCycleBar.push(Number(Number(Number(data.incycleTime)/60/60).toFixed(1)));
				};
				
				//opTime += Number(data.incycleTime)/60/60/10;
				opRatio += Number(data.opRatio)
				incycleTime_avg += Number(data.incycleTime);
				alatmTime_avg += Number(data.alarmTime);
			});
			
			drawReportColumnChart("columnChart");
			$("#incycleTime_avg").html(Math.floor(incycleTime_avg/60/60/json.length))
			$("#alarmTime_avg").html(Math.floor(alatmTime_avg/60/60/json.length))

			$("#diagram").circleDiagram({
				textSize: getElSize(70), // text color
				percent : Number(opRatio/json.length).toFixed(1) + "%",
				size: getElSize(400), // graph size
				borderWidth: getElSize(30), // border width
				bgFill: "#95a5a6", // background color
				frFill: "#1abc9c", // foreground color
				//font: "serif", // font
				textColor: 'black' // text color
			});
		}
	 });
};

var opTime = 0;
var alatmTime_avg = 0;
var incycleTime_avg = 0;
var noconnBar = [];
var alarmBar = [];
var waitBar = [];
var inCycleBar = [];
function addSeries(){
	reportBar.addSeries({
		color : "gray",
		data : noconnBar
	}, true);
	
	reportBar.addSeries({
		color : "red",
		data : alarmBar
	}, true);
	
	reportBar.addSeries({
		color : "yellow",
		data : waitBar
	}, true);
	
	reportBar.addSeries({
		color : "green",
		data : inCycleBar
	});
}
var reportChartName = [];
function drawLine(x1,y1, x2,y2, x3,y3, x4,y4){
	ctx.moveTo(x1,y1);
	ctx.lineTo(x2,y2);
	ctx.lineTo(x3,y3);
	ctx.lineTo(x4,y4);
	ctx.lineWidth = getElSize(10);
	ctx.strokeStyle = "#ffffff";
	ctx.stroke();
};

function addZero(str){
	if(str.length==1) str = "0" + str;
	return str;
};

function setToday(){
	var date = new Date();
	var year = date.getFullYear();
	var month = addZero(String(date.getMonth() + 1));
	var day = addZero(String(date.getDate()));
	
//	$("#sDate").val(year + "-" + month + "-" + addZero(String(date.getDate()-1)));
	$("#sDate, #eDate").val(year + "-" + month + "-" + day);
};

//originally from http://stackoverflow.com/questions/149055/how-can-i-format-numbers-as-money-in-javascript
function formatCurrency(n, c, d, t) {
    "use strict";

    var s, i, j;

    c = isNaN(c = Math.abs(c)) ? 2 : c;
    d = d === undefined ? "." : d;
    t = t === undefined ? "," : t;

    s = n < 0 ? "-" : "";
    i = parseInt(n = Math.abs(+n || 0).toFixed(c), 10) + "";
    j = (j = i.length) > 3 ? j % 3 : 0;

    return s + (j ? i.substr(0, j) + t : "") + i.substr(j).replace(/(\d{3})(?=\d)/g, "$1" + t) + (c ? d + Math.abs(n - i).toFixed(c).slice(2) : "");
}

function thermometer(temper, div) {
	var animate = false;

    var $thermo = $("#" + div),
        $progress = $(".progress", $thermo),
        $goal = $(".goal", $thermo),
        percentageAmount;

    var goalAmount = parseFloat( $goal.text() ),
    progressAmount = Number(temper),
    percentageAmount =  Math.min( Math.round(progressAmount / goalAmount * 1000) / 10, 100); //make sure we have 1 decimal point

    //let's format the numbers and put them back in the DOM
    $goal.find(".amount").text(formatCurrency( goalAmount ) );
    $progress.find(".amount").text(progressAmount + "°C");


    //let's set the progress indicator
    $progress.find(".amount").hide();
    if (animate !== false) {
        $progress.animate({
            "height": percentageAmount + "%"
        }, 1200, function(){
            $(this).find(".amount").fadeIn(500);
        });
    }else{
        $progress.css({
            "height": percentageAmount + "%"
        });
        $progress.find(".amount").fadeIn(500);
    };
};

function drawLineChart(id){
	$('#' + id).highcharts({
		chart : {
			height : getElSize(300),
			backgroundColor : "rgba(0,0,0,0)",
			marginBottom : 0,
			marginLeft :0,
			marginRight:0
		},
		exporting : false,
		credits : false,
        title: {
            text: false,
        },
        subtitle: {
            text: false,
        },
        xAxis: {
        	lineWidth: 0,
        	minorGridLineWidth: 0,
        	minorTickLength: 0,
        	tickLength: 0,
        	lineColor: 'transparent',
        	labels : {
        		enabled : false
        	},
        	gridLineWidth: 0
        },
        yAxis: {
        	labels : {
        		enabled : false
        	},
        	gridLineWidth: 0,
            title: {
                text: false
            },
            plotLines: [{
                value: 0,
                width: 1,
                color: '#808080'
            }]
        },
        plotOptions : {
        	series : {
        		dataLabels: {
                    enabled: true,
                    style : {
                    	fontSize : getElSize(30),
                    	color : "black",
                    	textShadow: 0
                    },
                    formatter: function () {
                        if (this.point.options.showLabel) {
                            return this.y;
                        }
                        return null;
                    }
                },
            	lineWidth : getElSize(10),
            	marker: {
                    lineWidth: getElSize(10),
                    lineColor: null // inherit from series
                }
            },
        },
        tooltip: {
            //valueSuffix: '°C'
        },
        legend: {
        	enabled : false
        },
        series: []
    });
	
	var chart = $("#lineChart").highcharts()
	var data = Number(Number(Math.random() * 10).toFixed(1));
	chart.addSeries({data:[data], name : "Data"});
	
	for(var i = 0; i < 9; i++){
		data = Number(Number(Math.random() * 10).toFixed(1));
	 	chart.series[0].addPoint(data);
	 	$("#lineChartLabel").html(data);
	};
	
	callback();
};

function drawLabelPoint(){
	var i = 0;
	$(".highcharts-axis:nth(0) path").each(function(idx, data){
		var x = $(data).offset().left;
		var y = $(data).offset().top - getElSize(8);
		if(i==15){
			var div = document.createElement("div");
			$("body").prepend(div);
			$(div).css({
				"position" : "absolute",
				"z-index" : 9,
				"top" : y,
				"left" : x,
				"width" :getElSize(8),
				"height" : getElSize(16),
				"background-color" : "black"
			});
		}
		if(i==45){
			var div = document.createElement("div");
			$("body").prepend(div);
			$(div).css({
				"position" : "absolute",
				"z-index" : 9,
				"top" : y,
				"left" : x,
				"width" :getElSize(8),
				"height" : getElSize(16),
				"background-color" : "black"
			});
			
			i = 15;
		};
		
		i++;
	});
};

function callback(){
	var chart = $("#lineChart").highcharts();
	var series = chart.series[0];
    var points = series.points;
    var pLen = points.length;
    var i = 0;
    var lastIndex = pLen - 1;
    var minIndex = series.processedYData.indexOf(series.dataMin);
    var maxIndex = series.processedYData.indexOf(series.dataMax);

    points[minIndex].options.showLabel = true;
	points[maxIndex].options.showLabel = true;
	//  points[lastIndex].options.showLabel = true;
	series.isDirty = true;
	chart.redraw();
};

function drawReportColumnChart(id){
	Highcharts.createElement('link', {
		   href: '//fonts.googleapis.com/css?family=Unica+One',
		   rel: 'stylesheet',
		   type: 'text/css'
		}, null, document.getElementsByTagName('head')[0]);
	
	 $('#' + id).highcharts({
		 	chart : {
		 		height : getElSize(1100),
		 		type: 'column',
		 		backgroundColor : "white",
		 		 style: {
		 	         fontFamily: "'Unica One', sans-serif"
		 	      },
		 	},
	        title: {
	            text: false,
	        },
	        exporting : false,
	        credits : false,
	        subtitle: {
	            text: false,
	        },
	        xAxis: {
	            categories: reportChartName,
                labels : {
	            	style : {
	   	        	 color : "white",
	   	        	 fontSize :getElSize(50)
	   	           },
	            },
	        },
	        yAxis: {
	        	max : 24,
	        	step : 2,
	            title: {
	                text: false
	            },
	            labels : {
	            	style : {
	   	        	 color : "white",
	   	        	 fontSize :getElSize(30)
	   	           },
//	   	           formatter: function () {
//	   	        	   if(this.value%2==0) return this.value;
//	   	           }
	            },
	        },
	        tooltip: {
	            enabled :false
	        },
	        plotOptions: {
	            series: {
	                lineWidth: getElSize(15),
	                borderWidth: 0,
	            },
	            column: {
	                stacking: 'normal',
	                dataLabels: {
	                	formatter : function(){
	                		if(this.y!=0){
	                			return this.y;
	                		}else{
	                			return null;
	                		};
	                	},
	                    enabled: true,
	                    color: (Highcharts.theme && Highcharts.theme.dataLabelsColor) || 'black',
	                    style: {
	                        fontSize : getElSize(30),
	                        textShadow: '0 0 3px white'
	                    }
	                }
	            },
	        },
	        legend: {
	        	enabled : false
	        },
	        series: []
	         
	    });
	 
	 Highcharts.setOptions(Highcharts.theme);
	 
	 reportBar = $("#" + id).highcharts();
	 
	 addSeries();
};
var reportBar;
var block = 1/6;

var displayMachine = new Array();

var lineWidth = getElSize(5);
var toggle = true;

var alarmList = new Array();
var machineList = new Array();
var cardMachine = new Array();

function reArrangeIcon(){
	$(cardMachine).each(function(idx, data){
		$("#circle" + data[0]).animate({
			"left" : getElSize(data[1]) + marginWidth,
			"top" : getElSize(data[2]) + marginHeight + getElSize(100)
		}, 1000, function(){
			$("#circle" + data[0] + ", #canvas").animate({
				"opacity" : 0
			});
		});
	});
	
	setTimeout(function(){
		$("#svg").hide();
		$("#cards").animate({
			"opacity" : 1
		});
		autoSlide();
		clearInterval(border_interval);
		stateBorder();
	}, 1500);
	
	getMachineStatus();
	
	//setInterval(getMachineStatus, 1000*5);
};


function setDiagram(id, ratio){
	$("#diagram" + id).circleDiagram({
		textSize: getElSize(50), // text color
		percent : Number(ratio) + "%",
		size: getElSize(170), // graph size
		borderWidth: getElSize(20), // border width
		bgFill: "#95a5a6", // background color
		frFill: "#1abc9c", // foreground color
		//font: "serif", // font
		textColor: 'black' // text color
	});
};

var machine_card = "";
var card_bar_data_array = [];
var new_card_bar_data_array = [];
var status_interval = null;


var today;

function getTimeData(options){
	var url = ctxPath + "/chart/getTimeData.do";
	var date = new Date();
	var year = date.getFullYear();
	var month = date.getMonth() + 1;
	var day = addZero(String(date.getDate()));
	var hour = date.getHours();
	var minute = addZero(String(date.getMinutes())).substr(0,1);
	
	
	if(hour>startHour || (hour>=startHour &&minute >= startMinute)){
		day = addZero(String(new Date().getDate()+1));
	};
	
	
	var today = year + "-" + month + "-" + day;
	var param = "workDate=" + today + 
				"&dvcId=" + dvcId;
	$.ajax({
		url : url,
		dataType : "json",
		type : "post",
		data : param,
		success : function(data){
			var json = data.statusList;
			
			console.log("length : " + json.length)
			var color = "";
			
			var status = json[0].status;
			if(status=="IN-CYCLE"){
				color = "green"
			}else if(status=="WAIT"){
				color = "yellow";
			}else if(status=="ALARM"){
				color = "red";
			}else if(status=="NO-CONNECTION"){
				color = "gray";
			};
			
			options.series.push({
				data : [ {
					y : Number(20),
					segmentColor : color
				} ],
			});
			
			$(json).each(function(idx, data){
				if(data.status=="IN-CYCLE"){
					color = "green"
				}else if(data.status=="WAIT"){
					color = "yellow";
				}else if(data.status=="ALARM"){
					color = "red";
				}else if(data.status=="NO-CONNECTION"){
					color = "gray";
				};
				options.series[0].data.push({
					y : Number(20),
					segmentColor : color
				});
			});
			
			for(var i = 0; i < 719-json.length; i++){
				options.series[0].data.push({
					y : Number(20),
					segmentColor : "rgba(0,0,0,0)"
				});
			};
			
			status = new Highcharts.Chart(options);
			
			drawLabelPoint();
		},error : function(e1,e2,e3){
		}
	});
};


var statusColor = [];
var labelsArray = [ 20, 21, 22, 23, 24, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12,
		13, 14, 15, 16, 17, 18, 19, 20 ];

function flipCard_r() {
	$("#part2").append($("#mainTable2"));
	$("#back").animate({
		"width" : card_width,
		"height" : card_height,
		"top" : card_top,
		"left" : card_left,
		"background-color" : "white"
	}, function() {
		//$("#part2").append($("#mainTable2"));
		$("#back").remove();
		$(".wrap").css({
			//"transition" : "0.5s",
			"-webkit-transform" : "rotateY(0deg)",
		});
	});
};

function parsingAlarm(str){
	var alarm = JSON.parse(str);
	
	var alarmMsg = " ";
	$(alarm).each(function(idx, data){
		if(data.ALARMMSG!="NULL" && typeof(data.ALARMMSG)!="undefined") alarmMsg += data.ALARMCODE + " - " + data.ALARMMSG + "<br>";
	});
	
	$("#alarm").css({
		"height" : getElSize(200)
	})
	
	$("#alarm").html(alarmMsg);
};

var card_width;
var card_height;
var card_top;
var card_left;
var spd_feed_interval = null;

function hideRemoveBtn(){
	if(!rm_btn) return;	
	//$(".removeIcon").hide(500);
};

var rm_btn = false;

var upPage = new Array();
var downPage = new Array();
var cPage;

function showCorver(){
	$("#corver").css({
		"z-index":4,
		"opacity":0.7
	});
};

function hideCorver(){
	$("#corver").css({
		"z-index":-1,
		"opacity":0
	});
};

function togglePanel() {
	var panelDist;
	var btnDist;

	if (panel) {
		panelDist = -(originWidth * 0.2) - getElSize(20) * 2;
		btnDist = getElSize(30);
		
		hideCorver();
	} else {
		panelDist = 0;
		btnDist = (originWidth * 0.2) + ($("#menu_btn").width() / 3.5)
				+ getElSize(20);
		
		showCorver();
	};

	panel = !panel;

	$("#panel").animate({
		"left" : panelDist
	});
	$("#menu_btn").animate({
		"left" : btnDist
	});
};


function drawBarChart(id, idx) {
	var m0 = "",
		m02 = "",
		m04 = "",
		m06 = "",
		m08 = "",
		
		m1 = "";
		m12 = "",
		m14 = "",
		m16 = "",
		m18 = "",
		
		m2 = "";
		m22 = "",
		m24 = "",
		m26 = "",
		m28 = "",
		
		m3 = "";
		m32 = "",
		m34 = "",
		m36 = "",
		m38 = "",
		
		m4 = "";
		m42 = "",
		m44 = "",
		m46 = "",
		m48 = "",
		
		m5 = "";
		m52 = "",
		m54 = "",
		m56 = "",
		m58 = "";
	
	var n = Number(startHour);
	if(startMinute!=0) n+=1;
	
	for(var i = 0, j = n ; i < 24; i++, j++){
		eval("m" + startMinute + "=" + j);
		
		startTimeLabel.push(m0);
		startTimeLabel.push(m02);
		startTimeLabel.push(m04);
		startTimeLabel.push(m06);
		startTimeLabel.push(m08);
		
		startTimeLabel.push(m1);
		startTimeLabel.push(m12);
		startTimeLabel.push(m14);
		startTimeLabel.push(m16);
		startTimeLabel.push(m18);
		
		startTimeLabel.push(m2);
		startTimeLabel.push(m22);
		startTimeLabel.push(m24);
		startTimeLabel.push(m26);
		startTimeLabel.push(m28);
		
		startTimeLabel.push(m3);
		startTimeLabel.push(m32);
		startTimeLabel.push(m34);
		startTimeLabel.push(m36);
		startTimeLabel.push(m38);
		
		startTimeLabel.push(m4);
		startTimeLabel.push(m42);
		startTimeLabel.push(m44);
		startTimeLabel.push(m46);
		startTimeLabel.push(m48);
		
		startTimeLabel.push(m5);
		startTimeLabel.push(m52);
		startTimeLabel.push(m54);
		startTimeLabel.push(m56);
		startTimeLabel.push(m58);
		
		if(j==24){ j = 0}
	};

	var perShapeGradient = {
		x1 : 0,
		y1 : 0,
		x2 : 1,
		y2 : 0
	};
	colors = Highcharts.getOptions().colors;
	colors = [ {
		linearGradient : perShapeGradient,
		stops : [ [ 0, 'rgb(100,238,92 )' ], [ 1, 'rgb(100,238,92 )' ] ]
	}, {
		linearGradient : perShapeGradient,
		stops : [ [ 0, 'rgb(250,210,80 )' ], [ 1, 'rgb(250,210,80 )' ] ]
	}, {
		linearGradient : perShapeGradient,
		stops : [ [ 0, 'rgb(231,71,79 )' ], [ 1, 'rgb(231,71,79 )' ] ]
	}, {
		linearGradient : perShapeGradient,
		stops : [ [ 0, '#8C9089' ], [ 1, '#8C9089' ] ]
	}, ]

	var height = window.innerHeight;

	var options = {
		chart : {
			type : 'coloredarea',
			backgroundColor : 'rgba(0,0,0,0)',
			height : getElSize(800),
			width : getElSize(2500),
			marginTop : -(height * 0.2),
		},
		credits : false,
		title : false,
		xAxis : {
			categories : startTimeLabel,
			labels : {
				step: 1,
				formatter : function() {
					var val = this.value

					return val;
				},
				style : {
					color : "white",
					fontSize : getElSize(60),
					fontWeight : "bold"
				},
			}
		},
		yAxis : {
			labels : {
				enabled : false,
			},
			title : {
				text : false
			},
		},
		tooltip : {
			enabled : false
		},
		plotOptions : {
			line : {
				marker : {
					enabled : false
				}
			}
		},
		legend : {
			enabled : false
		},
		series : []
	};

	$("#" + id).highcharts(options);

	var status = $("#container").highcharts();
	var options = status.options;

	options.series = [];
	options.title = null;
	options.exporting = false;
	
	getTimeData(options);
	
	///////////////////////// demo data
	
	
	/*options.series.push({
		data : [ {
			y : Number(20),
			segmentColor : "red"
		} ],
	});
	
	
	for(var i = 0; i < 143; i++){
		options.series[0].data.push({
			y : Number(20),
			segmentColor : "red"
		});
	};
	
	status = new Highcharts.Chart(options);
	*/
	
	
	////////////////////////////////
	
	setInterval(function(){
		var cMinute = String(addZero(new Date().getMinutes())).substr(0,1);
		if(targetMinute!=cMinute){
			targetMinute = cMinute;
			drawBarChart(id, idx);
		};
	},5000)
};

var targetMinute = String(addZero(new Date().getMinutes())).substr(0,1);

function timeConverter(UNIX_timestamp) {
	var a = new Date(UNIX_timestamp * 1000);
	var months = [ 'Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug',
			'Sep', 'Oct', 'Nov', 'Dec' ];
	var year = a.getFullYear();
	var month = months[a.getMonth()];
	var date = a.getDate();
	var hour = a.getHours();
	var min = a.getMinutes();
	var sec = a.getSeconds();
	var time = date + ' ' + month + ' ' + year + ' ' + hour + ':' + min + ':'
			+ sec;
	return time;
}

function pieChart(idx) {
	Highcharts.setOptions({
		// green yellow red gray
		colors : [ 'rgb(100,238,92 )', 'rgb(250,210,80 )', 'rgb(231,71,79 )',
				'#8C9089' ]
	});

	$('#pie')
			.highcharts(
					{
						chart : {
							plotBackgroundColor : null,
							plotBorderWidth : null,
							plotShadow : false,
							type : 'pie',
							backgroundColor : "rgba(0,0,0,0)"
						},
						credits : false,
						exporting : false,
						title : {
							text : false
						},
						legend : {
							enabled : false
						},
						tooltip : {
							pointFormat : '{series.name}: <b>{point.percentage:.1f}%</b>'
						},
						plotOptions : {
							pie : {
								allowPointSelect : true,
								cursor : 'pointer',
								dataLabels : {
									enabled : true,
									format : '<b>{point.name}</b>: {point.percentage:.1f} %',
									style : {
										color : (Highcharts.theme && Highcharts.theme.contrastTextColor)
												|| 'white',
										fontSize : getElSize(30),
										textShadow: 0
									}
								},
								showInLegend : true
							}
						},
						series : [ {
							name : "Brands",
							colorByPoint : true,
							data : [ {
								name : "In-Cycle",
								y : 13
							}, {
								name : "Wait",
								y : 2
							}, {
								name : "Alarm",
								y : 1
							}, {
								name : "No-Connection",
								y : 0
							}]
						} ]
					});
	
	var chart = $("#pie").highcharts();
	
	var incycle = 0;
	var wait = 0;
	var alarm = 0;
	var noconn = 0;

	console.log(card_bar_data_array)
//	for(var i = 0; i < card_bar_data_array.length; i++){
//		if(card_bar_data_array[i].get("id")==idx){
//			incycle = Number(card_bar_data_array[i].get("incycleTime"));
//			wait = Number(card_bar_data_array[i].get("waitTime"));
//			alarm = Number(card_bar_data_array[i].get("alarmTime"));
//			noconn = Number(card_bar_data_array[i].get("noconnTime"));
//		}
//	};
//		
//	var sum = incycle + wait + alarm + noconn;	
//	
//	chart.series[0].data[0].update(Number(Number(incycle/sum*100).toFixed(1)));
//	chart.series[0].data[1].update(Number(Number(wait/sum*100).toFixed(1)));
//	chart.series[0].data[2].update(Number(Number(alarm/sum*100).toFixed(1)));
	//chart.series[0].data[3].update(Number(Number(noconn/sum*100).toFixed(1)));
};

function drawStatusPie(id){
	$('#' + id).highcharts({
        chart: {
            plotBackgroundColor: null,
            plotBorderWidth: null,
            plotShadow: false,
            type: 'pie',
            backgroundColor : "rgba(0,0,0,0)",
            marginTop : 0,
            marginBottom : 0,
            marginLeft : 0,
            marginRight : 0
        },
        title: {
            text: false
        },
        credits :false,
        tooltip: {
            pointFormat: '{series.name}: <b>{point.percentage:.1f}%</b>'
        },
        plotOptions: {
            pie: {
            	size : "100%",
                allowPointSelect: true,
                cursor: 'pointer',
                dataLabels: {
                    enabled: true,
                    format: '<b>{point.name}</b>: {point.y} ',
                    style: {
                        color: 'black',
                        textShadow: false ,
                        fontSize : getElSize(35)
                    }
                }
            }
        },
        series: [{
            name: 'Brands',
            colorByPoint: true,
            data: [{
                name: '가동',
                y: 5,
                color : "rgb(104,206,19)"
            }, {
                name: '비가동',
                y: 17,
                color : "rgb(109,109,109)"
            }], dataLabels: {
                distance: -getElSize(60)
            }
        }]
    });
};

function drawStatusPie2(id){
	$('#' + id).highcharts({
        chart: {
            plotBackgroundColor: null,
            plotBorderWidth: null,
            plotShadow: false,
            type: 'pie',
            backgroundColor : "rgba(0,0,0,0)",
            marginTop : 0,
            marginBottom : 0,
            marginLeft : 0,
            marginRight : 0
        },
        title: {
            text: false
        },
        credits :false,
        tooltip: {
            pointFormat: '{series.name}: <b>{point.percentage:.1f}%</b>'
        },
        plotOptions: {
            pie: {
            	size : "100%",
                allowPointSelect: true,
                cursor: 'pointer',
                dataLabels: {
                    enabled: true,
                    format: '{point.y} ',
                    style: {
                        color: 'black',
                        textShadow: false ,
                        fontSize : getElSize(35)
                    }
                }
            }
        },
        series: [{
            name: 'Brands',
            colorByPoint: true,
            data: [{
                name: 'In-Cycle',
                y: 3,
                color : "rgb(104,206,19)"
            }, {
                name: 'Wait',
                y: 1,
                color : "yellow"
            }, {
                name: 'Alarm',
                y: 1,
                color : "red"
            }, {
                name: 'No-Connection',
                y: 17,
                color : "rgb(109,109,109)"
            }], dataLabels: {
                distance: -getElSize(60)
            }
        }]
    });
};

function setEl() {
	$("#canvas").css({
		"position" : "absolute",
		"background" : "whtie",
		"margin-top" : originHeight/2 - ($(".container").height()/2)
	});
	
	$(".container").css({
		"width": contentWidth,
		"height" : contentHeight,
		"margin-top" : originHeight/2 - ($(".container").height()/2)
	});
	
	$("#stateBorder").css({
		"position": "absolute",
		"width": contentWidth,
		"height" : contentHeight,
		"top" : marginHeight,
		"left" : marginWidth,
	});
	
	$("#detailRepBox").css({
		"position" : "absolute",
		"color" : "white",
		"font-size" : getElSize(40),
		"padding" : getElSize(20),
		"background-color" : "rgb(34,34,34)",
		"display" : "none",
		"z-index" : 11,
		"border-radius" : getElSize(30),
		"border" : getElSize(10) + "px solid white",
	});

	$(".container").css({
		"margin-top" : originHeight/2 - ($(".container").height()/2),
		"margin-left" : originWidth/2 - ($(".container").width()/2),
	});
	
	$(".layoutTd").css({
		height : contentHeight * 0.85
	});

	$(".page").css({
		"height" : originHeight
	});
	
	$(".machine_icon, .init_machine_icon").css({
		"position" : "absolute",
		"margin" : getElSize(20),
//		"width" : contentWidth * 0.1,
//		"height" : contentWidth * 0.1,
		"cursor" : "pointer"
	});

	$(".circle, .init_circle").css({
		"background-color" : "white",
		"width" : contentWidth * 0.07,
		"height" : contentWidth * 0.07,
		"border-radius" : "50%",
	});
	
	
	// 페이지 위치 조정
//	for (var i = 2; i <= 5; i++) {
//		$("#part" + i).css({
//			"top" : $("#part" + (i - 1)).height() + originHeight
//		});
//	};

	$(".page").css({
		"top" : originHeight
	});
	
	$("#part1").css({
		"top" : 0,
		"overflow" : "hidden"
	});
	
	$(".title").css({
		"padding" : getElSize(40),
		"font-size" : getElSize(70)
	});

	$(".subTitle").css({
		"padding" : getElSize(40),
		"font-size" : getElSize(45)
	});

	$(".tr2").css({
		//"height" : contentHeight * 0.3
	})

	$("#chartTr").css({
		"height" : contentHeight * 0.25
	});

	$("#pie").css({
		"height" : contentHeight * 0.32
	});

	$(".neon1,.neon3").css({
		"font-size" : getElSize(250),
		"font-weight" : "bolder",
		//"height" : getElSize(450)
	});

	$(".neon2").css({
		"font-size" : getElSize(200),
		"font-weight" : "bolder",
	});
	
	
	$(".neon1, .neon2, .neon3").css({
		"margin-top" : $(".tr2").height() / 2 - $(".neon3").height() / 2 - $(".subTitle").height(),
	});
	
	$("#repairBox, #alarmBox").css({
		"position" : "absolute",
		"width" :getElSize(2000),
		"height" : getElSize(1500),
		"background-color" : "rgb(34,34,34)",
		"z-index" : 9,
		"display" : "none",
		"color" : "white",
		"font-size" : getElSize(50),
		"padding" : getElSize(50),
		"overflow" : "auto",
		"border-radius" : getElSize(50),
		"border" : getElSize(10) + "px solid white"
	});
	
	$("#repairBox, #alarmBox").css({
		"left" : (originWidth/2) - ($("#repairBox").width()/2),
		"top" : (originHeight/2) - ($("#repairBox").height()/2)
	});
	
	$("#machine_name").css({
		"color" : "white",
		"font-weight" : "bolder",
		"font-size" : getElSize(170),
	});

	$("#machine_name_td, #alarm_td").css({
		"height" : getElSize(500)
	});

	$("#machine_name").css({
		"margin-top" : $("#machine_name_td").height() / 2
						- $("#machine_name").height() / 2
						- $(".subTitle").height()
	});

	$(".title_left").css({
		"float" : "left",
		"width" : contentWidth * 0.1,
		"margin-left" : getElSize(150)
	});

	$(".title_right").css({
		"float" : "right",
		"width" : contentWidth * 0.1
	});

	$("#menu_btn").css({
		"width" : contentWidth * 0.04,
		"top" : marginHeight + (contentHeight * 0.01),
		"left" : getElSize(30),
		"z-index" : 5
	});

	$("#panel").css({
		"height" : originHeight,
		"background-color" : "rgb(34,34,34)",
		"border" : getElSize(20) + "px solid rgb(50,50,50)",
		"width" : contentWidth * 0.2,
		"position" : "absolute",
		"color" : "white",
		"z-index" : 999999,
		"left" : -contentWidth * 0.2 - (getElSize(20) * 2)
	});

	$("#corver").css({
		"width" : originWidth,
		"height" : originHeight,
		"position" : "absolute",
		"z-index" : -1,
		"background-color" : "black",
		"opacity" : 0
	});

	$("#panel_table").css({
		"color" : "white",
		"font-size" : getElSize(60)
	});

	$("#panel_table td").css({
		"padding" : getElSize(50),
		"cursor" : "pointer"
	});


	$(".dvcName_p1").css({
		"font-weight" : "bolder",
		"font-size" : getElSize(40)
	});

	
	$(".wrap").css({
		"position" : "absolute",
		"width" : contentWidth * 0.2,
		"padding" : getElSize(10),
		"border-radius" : getElSize(30),
		"padding-bottom" : getElSize(10),
		"margin" : getElSize(50)
	});
	
	$(".waitCnt, .alarmCnt, .noConCnt").css({
		"font-size" : getElSize(20),
		"margin" :getElSize(20)
	});
	
	$(".waitCnt").css({
		"color" : "yellow"
	});
	
	$(".alarmCnt").css({
		"color" : "red"
	});
	
	$(".noConCnt").css({
		"color" : "gray"
	});
	
	var chart_height = getElSize(60);
	if(chart_height<20) chart_height = 20;
	
	$(".card_status").css({
		"height" : chart_height,
	});
	
	$(".card_table").css({
		"border-spacing" : getElSize(10)
	});
	
	$(".comName").css({
		"font-size" :getElSize(60)
	});
	
	$("#toolTable").css({
		"font-size" : getElSize(55)
	});
	
	$("#barChart").css({
		"position" : "absolute",
		"bottom" :getElSize(10),
		"border" : "1px solid white"
	});
	
	$(".machine_cam").css({
		"width" : contentWidth * 0.07,
		"height" : contentHeight * 0.08,
		//"border" : getElSize(10) + "px solid rgb(50,50,50)" 
	});
	
	$(".frame").css({
		"width" : contentWidth * 0.07,
		"height" : contentHeight * 0.15
	});
	
	$(".statusCnt").css({
		"height" : getElSize(70),
		"vertical-align" : "bottom",
		"box-shadow ": "inset 0 5px 0 #ddd"
	});
	
	$(".opTime").css({
		"font-weight" : "bolder",
		"top" : getElSize(400),
		"right" : getElSize(10)
	});
	
	$(".logo").css({
		"height" : contentWidth * 0.015
	});
	
	$(".menu_icon").css({
		"width" : getElSize(250),
		"border-radius" : "50%",
	});

	$("p").css({
		"font-size" : getElSize(40),
		"font-weight" : "bolder"
	});
	
	$(".mainTable").not("#mainTable").css({
		"border-spacing" : getElSize(30)
	});
	
	$(".td_header").css({
		"color" : "white",
		"font-size" : getElSize(50),
		"background-color" : "rgb(34,34,34)",
		"padding" : getElSize(20),
		"margin" : getElSize(10)
	});
	
	
	$("#incycleTime_avg").css({
		"color" : "white"
	});
	
	$(".upDown").css({
		"width" : getElSize(100),
		"margin-top" : getElSize(100)
	});
	
	$("#upFont").css({
		"color" : "rgb(124,224,76)",
		"font-size" : getElSize(100),
		"margin-left" : getElSize(50)
	});
	
	$("#downFont").css({
		"color" : "#FF3A3A",
		"font-size" : getElSize(100),
		"margin-left" : getElSize(50)
	});
	
	$("#thermometer, #thermometer2, #thermometer3").css({
		"width" : getElSize(80),
		"height" : getElSize(500),
		"margin-bottom" : getElSize(30),
		"margin-right" : getElSize(20),
		"margin-left" : getElSize(80),
		"border-radius" : getElSize(35),
		"margin- top" : getElSize(50),
		"float" : "left"
	});
	
	$("#thermometer .track, #thermometer2 .track, #thermometer3 .track").css({
		"width" : getElSize(30),
		"height" : getElSize(450),
		"top" : getElSize(25),
	});
	
	$("#thermometer .progress .amount, #thermometer2 .progress .amount, #thermometer3 .progress .amount").css({
		"padding": 0 + " " + getElSize(80) + " 0 " + getElSize(0),
		"font-size" : getElSize(30)
	});
	
	$("#diagram").css({
		"margin-top" : getElSize(50)
	});
	
	$("#reportDateDiv").css({
		"position" : "absolute",
		"right" : getElSize(50),
	});
	
	$("#sDate, #eDate").css({
//		"width" : getElSize(400),
//		"height" : getElSize(50),
		"font-size" : getElSize(40)
	});
	
	$("#map").css({
		"left" : (originWidth/2) - ($("#map").width()/2) - marginWidth 
	});
	
	$("#comName").css({
		"font-size" : getElSize(70)
	});
	
	$("#alarm").css({
		"color" : "white",
		"font-weight" : "bolder",
		"font-size" : getElSize(60),
		"margin-top" : getElSize(30),
		"margin-left" : getElSize(50)
	});
	
	$(".upDownSpan").css({
		"font-size" : getElSize(300)
	});
	
	$("#main_logo").css({
		"position" : "absolute",
		"width" : getElSize(600),
		//"top" : $("#menu_btn").offset().top + getElSize(30), 
		"top" : marginHeight + (contentHeight * 0.01) + getElSize(30),
		//"left" : $("#menu_btn").offset().left + $("#menu_btn").width() + getElSize(20),
		"left" : getElSize(30) + $("#menu_btn").width() + getElSize(20),
		"display" : "inline",
		"z-index" : 1
	});
	
	$("#time_table").css({
		"color" : "white",
		"position" : "absolute",
		"right" : marginWidth + getElSize(50),
		"top" : marginHeight + getElSize(20),
		"text-align" : "right",
		"font-size" : getElSize(40),
		"z-index" : 9
	});
	
	$("#lastCell").css({
		"font-size" : getElSize(100),
		"height" : getElSize(200)
	});
	
	$("#alarm_mark").css({
		"width" : getElSize(60),
		"cursor" : "pointer"
	});
	
	$("#alarm_mark").css({
		"top" : $("#alarm_mark").parent("div").offset().top + ($("#alarm_mark").parent("div").height()/2)
	});
	
//	"width" :getElSize(2000)
//	"height" : getElSize(1500)
	
	$("#close_btn").css({
		"position" : "absolute",
		"display" : "none",
		"width" : getElSize(100),
		"z-index" : 10,
		"top" : (originHeight/2) - (getElSize(1500)/2) - getElSize(50),
		"left" : (originWidth/2) - (getElSize(2000)/2) + getElSize(2000) + getElSize(50),
		"cursor" : "pointer"
 	});
	
	$("#tool").css({
		"width" : getElSize(60),
		//"position" : "absolute",
		//"top" : $("#toolLabel").offset().top,
		//"margin-left" : getElSize(20),
		"cursor" : "pointer"
	});
};

var csvData = "";
function csvSend(){
	csvData = '설비, 형번, 길 이(m)	, 설비가동시간 (시간), 현 비가동시간 (시간), 시간가동율 (%), 사이클타임 (분), 현재가동시간 (분), 완료사이클,생산량,대기상황,알람내역LINE'
	for(var i = 1; i <=23; i++){
		csvData+=(i + "호기") + ", , , , ,,,, , ,LINE";
	};
	
	var sDate, eDate;
	var csvOutput;
	
	sDate = $("#sDate").val();
	eDate = $("#eDate").val();
	csvOutput = csvData;
	
	csvOutput = csvOutput.replace(/\n/gi,"");
	
	f.csv.value=encodeURIComponent(csvOutput);
	f.startDate.value = sDate;
	f.endDate.value = eDate;
	f.submit(); 
};

function drawStockChart() {
	var seriesOptions = [], seriesCounter = 0, names = [ 'MSFT', 'AAPL', 'GOOG' ],
	// create the chart when all data is loadedx
	createChart = function() {
		$('#container').highcharts('StockChart', {
			chart : {
				height : originHeight * 0.45
			},
			exporting : false,
			credits : false,
			rangeSelector : {
				selected : 4
			},

			rangeSelector : {
				buttons : [ {
					type : 'hour',
					count : 1,
					text : '1h'
				}, {
					type : 'day',
					count : 1,
					text : '1d'
				}, {
					type : 'month',
					count : 1,
					text : '1m'
				}, {
					type : 'year',
					count : 1,
					text : '1y'
				}, {
					type : 'all',
					text : 'All'
				} ],
				inputEnabled : true, // it supports only days
				selected : 4
			// all
			},

			yAxis : {
				labels : {
					formatter : function() {
						// return (this.value > 0 ? ' + ' : '') + this.value +
						// '%';
						return this.value
					}
				},
				plotLines : [ {
					value : 0,
					width : 2,
					color : 'silver'
				} ]
			},

			/*
			 * plotOptions: { series: { compare: 'percent' } },
			 */

			tooltip : {
				// pointFormat: '<span
				// style="color:{series.color}">{series.name}</span>:
				// <b>{point.y}</b> ({point.change}%)<br/>',
				valueDecimals : 2
			},

			series : seriesOptions
		});
	};

	seriesOptions[0] = {
		name : names[0],
		data : data_
	};

	/*
	 * $.each(names, function (i, name) {
	 * 
	 * $.getJSON('http://www.highcharts.com/samples/data/jsonp.php?filename=' +
	 * name.toLowerCase() + '-c.json&callback=?', function (data) {
	 * seriesOptions[i] = { name: name, data: data_ };
	 * 
	 * seriesCounter += 1;
	 * 
	 * if (seriesCounter === names.length) { createChart(); } }); });
	 */

	Highcharts.createElement('link', {
		href : '//fonts.googleapis.com/css?family=Unica+One',
		rel : 'stylesheet',
		type : 'text/css'
	}, null, document.getElementsByTagName('head')[0]);

	Highcharts.theme = {
		colors : [ "#2b908f", "#90ee7e", "#f45b5b", "#7798BF", "#aaeeee",
				"#ff0066", "#eeaaee", "#55BF3B", "#DF5353", "#7798BF",
				"#aaeeee" ],
		chart : {
			backgroundColor : {
				linearGradient : {
					x1 : 0,
					y1 : 0,
					x2 : 1,
					y2 : 1
				},
				stops : [ [ 0, '#2a2a2b' ], [ 1, '#3e3e40' ] ]
			},
			style : {
				fontFamily : "'Unica One', sans-serif"
			},
			plotBorderColor : '#606063'
		},
		title : {
			style : {
				color : '#E0E0E3',
				textTransform : 'uppercase',
				fontSize : '20px'
			}
		},
		subtitle : {
			style : {
				color : '#E0E0E3',
				textTransform : 'uppercase'
			}
		},
		xAxis : {
			gridLineColor : '#707073',
			labels : {
				style : {
					color : '#E0E0E3'
				}
			},
			lineColor : '#707073',
			minorGridLineColor : '#505053',
			tickColor : '#707073',
			title : {
				style : {
					color : '#A0A0A3'

				}
			}
		},
		yAxis : {
			gridLineColor : '#707073',
			labels : {
				style : {
					color : '#E0E0E3'
				}
			},
			lineColor : '#707073',
			minorGridLineColor : '#505053',
			tickColor : '#707073',
			tickWidth : 1,
			title : {
				style : {
					color : '#A0A0A3'
				}
			}
		},
		tooltip : {
			backgroundColor : 'rgba(0, 0, 0, 0.85)',
			style : {
				color : '#F0F0F0'
			}
		},
		plotOptions : {
			series : {
				dataLabels : {
					color : '#B0B0B3'
				},
				marker : {
					lineColor : '#333'
				}
			},
			boxplot : {
				fillColor : '#505053'
			},
			candlestick : {
				lineColor : 'white'
			},
			errorbar : {
				color : 'white'
			}
		},
		legend : {
			itemStyle : {
				color : '#E0E0E3'
			},
			itemHoverStyle : {
				color : '#FFF'
			},
			itemHiddenStyle : {
				color : '#606063'
			}
		},
		credits : {
			style : {
				color : '#666'
			}
		},
		labels : {
			style : {
				color : '#707073'
			}
		},

		drilldown : {
			activeAxisLabelStyle : {
				color : '#F0F0F3'
			},
			activeDataLabelStyle : {
				color : '#F0F0F3'
			}
		},

		navigation : {
			buttonOptions : {
				symbolStroke : '#DDDDDD',
				theme : {
					fill : '#505053'
				}
			}
		},

		// scroll charts
		rangeSelector : {
			buttonTheme : {
				fill : '#505053',
				stroke : '#000000',
				style : {
					color : '#CCC'
				},
				states : {
					hover : {
						fill : '#707073',
						stroke : '#000000',
						style : {
							color : 'white'
						}
					},
					select : {
						fill : '#000003',
						stroke : '#000000',
						style : {
							color : 'white'
						}
					}
				}
			},
			inputBoxBorderColor : '#505053',
			inputStyle : {
				backgroundColor : '#333',
				color : 'silver'
			},
			labelStyle : {
				color : 'silver'
			}
		},

		navigator : {
			handles : {
				backgroundColor : '#666',
				borderColor : '#AAA'
			},
			outlineColor : '#CCC',
			maskFill : 'rgba(255,255,255,0.1)',
			series : {
				color : '#7798BF',
				lineColor : '#A6C7ED'
			},
			xAxis : {
				gridLineColor : '#505053'
			}
		},

		scrollbar : {
			barBackgroundColor : '#808083',
			barBorderColor : '#808083',
			buttonArrowColor : '#CCC',
			buttonBackgroundColor : '#606063',
			buttonBorderColor : '#606063',
			rifleColor : '#FFF',
			trackBackgroundColor : '#404043',
			trackBorderColor : '#404043'
		},

		// special colors for some of the
		legendBackgroundColor : 'rgba(0, 0, 0, 0.5)',
		background2 : '#505053',
		dataLabelsColor : '#B0B0B3',
		textColor : '#C0C0C0',
		contrastTextColor : '#F0F0F3',
		maskColor : 'rgba(255,255,255,0.3)'
	};

	// Apply the theme
	Highcharts.setOptions(Highcharts.theme);
	createChart();
};