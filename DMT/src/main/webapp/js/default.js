var shopId = 1;

var is_login = window.sessionStorage.getItem("login");
function createCorver(){
	var corver = document.createElement("div");
	corver.setAttribute("id", "corver");
	corver.style.cssText = "width : " + originWidth + 
						"; height : " + originHeight + 
						"; position : absolute" + 
						"; z-index : 9" + 
						"; background-color : black";
	
	$("body").prepend(corver);
};

function login(){
	var url = ctxPath + "/chart/login.do";
	var $id = $("#email").val();
	var $pwd = $("#pwd").val();
	var param = "id=" + $id + 
				"&pwd=" + $pwd + 
				"&shopId=" + shopId;
	
	$.ajax({
		url : url,
		data : param,
		type : "post",
		dataType : "json",
		success : function(data){
			
			if(data.message!="failed"){
				window.sessionStorage.setItem("login_time", new Date().getTime());
				window.sessionStorage.setItem("login", "success");
				window.sessionStorage.setItem("user_id", $id);
				window.sessionStorage.setItem("level", data.level);
				loginSuccess();
				drawLogout()
			}else{
				$("#errMsg").html("계정 정보가 올바르지 않습니다.")
				
				 $("#popup").kendoPopup({
				        anchor: $("#email")
			    }).data("kendoPopup").open();
			}
		}
	});
	
};

function loginSuccess(){ 
//	$("img, span").css({
//		"transition" : "0.5s",
//		"-webkit-filter" : "blur(0px)"
//	});
	
	$("#loginForm").css({
		"z-index" : "-99",
		"opacity" : 0
	});
	
	closeCorver();
};

function chkKeyCd(evt){
	if(evt.keyCode==13) login();
};

function createLoginForm(){
	
	
	var loginForm = "<div id='loginForm' style='display: none'>" +
						"<form id='login' onsubmit='return false;'>" +
							"<Center>" +
								"<img src='" + ctxPath + "/images/SmartFactory.png' id='logo'><br>" +
								"<img src='" + ctxPath + "/images/logo.png' id='logo2'>" +
								/*"<div><select id='factory-select'><option>신규가입</option></select></div>"+*/
								"<br>" +
								"<input type='text' placeholder='ID' id='email' data-role='none'  onkeyup='chkKeyCd(event)'>" +
								"<input type='password' autocomplete='new-password' placeholder='Password' id='pwd' data-role='none' onkeyup='chkKeyCd(event)'><br>" +
								"<br>" +
								"<input type='submit' value='ENTER' id='enter' onclick='login()' style='cursor : pointer;'>" +
								/*"<button id='sign-in' style='cursor : pointer;'>Sign In</button>" +*/
							"</Center>" +
						"</form>" +
					"</div>";
							
	var url = ctxPath + "/chart/getComName.do";
	var param = "shopId=" + shopId;
	
	$.ajax({
		url : url,
		data : param,
		type : "post",
		dataType : "text",
		success : function(data){
			$("#title_right").html(decode(data));
		}
	});

	
							
	$("body").prepend(loginForm);
	
	$("#enter").css({
		"width" : getElSize(600),
		"font-size" : getElSize(60),
		"background" : "white",
		"border" : getElSize(10) + "px solid black"
	});
	
	$("#enter").hover(function(){
		$("#enter").css({
			"background" : "black",
			"color" : "white"
		});
	})
	
	$("#loginForm").css({
		"width" : getElSize(1000),
		"height" : getElSize(950),
		"z-index" : 10,
		"background-color" : "white", 
		"position" : "absolute",
		"border-radius" : getElSize(30) + "px"
	});
	
	$("#loginForm").css({
		"left" : (window.innerWidth/2) - ($("#loginForm").width()/2),
		"top" : (window.innerHeight/2) - ($("#loginForm").height()/2)
	});
	
	$("#login #email").css({
		"height" :  getElSize(150) + "px",
		"width" : getElSize(700) + "px", 
		"font-size" : getElSize(50) + "px", 
		"padding" :  getElSize(30) + "px" , 
		"border-top-left-radius" : getElSize(30) + "px", 
		"border-top-right-radius" : getElSize(30) + "px", 
		"outline" : "0", 
		"border" :  getElSize(3) + "px solid rgb(235,234,219)"
	}).focus();

	$("#login #pwd").css({
		"height" :  getElSize(150) + "px",
		"width" : getElSize(700) + "px", 
		"font-size" : getElSize(50) + "px", 
		"padding" :  getElSize(30) + "px" , 
		"border-bottom-left-radius" : getElSize(30) + "px", 
		"border-bottom-right-radius" : getElSize(30) + "px", 
		"outline" : "0", 
		"border" :  getElSize(5) + "px solid rgb(235,234,219)"
	}); 

	$("#logo").css({ 
		"width" : getElSize(500) + "px",
		"margin-top" : getElSize(50) + "px"
	});

	$("#logo2").css({
		"width" : getElSize(300) + "px", 
		"margin-bottom" : getElSize(50) + "px",
		"-ms-interpolation-mode" : "bicubic"
	});

	$("#hr").css({
		"margin-top" : getElSize(50) + "px",
		"width" : "80%"	,
		"border" : "1px solid black"
	});

	$("#saveIdText").css({
		"margin-top" : getElSize(20),
		"font-size" : getElSize(40)	
	});
	
	$(".errMsg").css({
		"color" : "red", 
		"font-size" : getElSize(40) + "px"	
	});
	
	/*$("#sign-in").css({
		"position" : "absolute",
		"height" : getElSize(100),
		"width" : getElSize(600),
		"font-weight" : "bolder",
		"margin-top" : getElSize(20),
		"right" : getElSize(200),
		"top" : getElSize(800)
	})*/
	
	$("#enter").on("mouseleave", function() {
		$("#enter").css({
			"background" : "white",
			"color" : "black"
		});
	});
	
	$("#factory-select").css({
		"font-size" : getElSize(50),
		"width" : getElSize(600),
		"height" : getElSize(100) 
	})
	
};


$(function(){
	
	/*
	* Date : 19.05.07
	* Author : wilson
	* index1.do 으로 접근 시, 자동 로그인
	* when access 'index1', auto login
	*/
	if(location.href.indexOf("index1") != -1){
    	window.sessionStorage.setItem("level", "1");
		window.sessionStorage.setItem("login", "success");	 
		window.sessionStorage.setItem("login_time", new Date().getTime());
		window.sessionStorage.setItem("user_id", "aIdoo"); 
    }
	
	var is_login = window.sessionStorage.getItem("login");
	var login_time = window.sessionStorage.getItem("login_time");
	var time = new Date().getTime();
	
	var head = document.getElementsByTagName('head')[0];
    var script= document.createElement('script');
    
    var address = ctxPath;
    
//	if(is_login==null || (is_login !=null && (time - login_time) / 1000 > 60 * 60 * 24)) {
    if(is_login==null) {
		window.sessionStorage.removeItem("login_time");
		window.sessionStorage.removeItem("login");
		window.sessionStorage.removeItem("user_id");
		window.sessionStorage.removeItem("level");
		
		// console.log(window.location.pathname);
		// console.log(ctxPath);
		
		address += "/chart/index.do";
		
		if(window.location.pathname != address){
			location.href = ctxPath + "/chart/index.do";
		}else{
			createCorver();
			createLoginForm();
		}
	}else{
		createCorver();
		createLoginForm();
	}
	
	
	$("#home").click(function(){
		var url = location.href.substr(0,location.href.indexOf(ctxPath));
    	location.href = ctxPath + "/chart/index.do";
	});
	
	getComName();
	bindEvt();
	drawFlag();
	
	
	/*
	* Date : 19.04.18
	* Author : wilson
	* 두산 IP(211.220.163.6) 에서는 'aIdoo' 계정으로 자동 로그인, 로그아웃 후에는 다른 계정으로 로그인 가능 
	* In Doosan IP (211.220.163.6), you can log in automatically with your 'aIdoo' account and log in with a different account after you log out.
	*/
	window.getIP = function(json) {
        var url = ctxPath + "/chart/chkIp.do";
        var param = "ip=" + json.ip;
        
//        console.log(json);
//        console.log(window.getIP);
        
        $.ajax({
        		url : url,
        		data : param,
        		dataType : "text",
        		type : "get",
        		success : function(data){
        			
//        			console.log(data);
        			if(data != "true"){	// 같은 서버가 아닐 경우 
	        			
        				
        				/*
        				* Date : 20.04.21
        				* Author : wilson
        				* 24시간 뒤에 로그아웃 해제
        				*/ 
        				
//	        			if(is_login==null || (is_login !=null && (time - login_time) / 1000 > 60 * 60 * 24)) {
        				if(is_login==null) {
        	        		window.sessionStorage.removeItem("login_time");
        	        		window.sessionStorage.removeItem("login");
        	        		window.sessionStorage.removeItem("user_id");
        	        		window.sessionStorage.removeItem("level");
        	        		
        	        		console.log("here2")
	        				
        	        		showCorver();
        	        		$("#loginForm").css("display", "block");
        	        		$("#email").focus();
        	        		
        	        	}else{
        	        		loginSuccess();
        	        		drawLogout();
        	        	}	
	        			
        			}else{	// 같은 서버일 경우
        				
        				//window.sessionStorage.setItem("login_time", new Date().getTime() - 90000000);        				
        				
        				console.log("로그인 시간(분) : " + Math.round( (time - login_time) / 1000 / 60 ) );
        				
//        				if(is_login==null || (is_login !=null && (time - login_time) / 1000 > 60 * 60 * 24)) {
        				if(is_login==null) {
        	        		window.sessionStorage.removeItem("login_time");
        	        		window.sessionStorage.removeItem("login");
        	        		window.sessionStorage.removeItem("user_id");
        	        		window.sessionStorage.removeItem("level");
        	        		
        	        		if(window.sessionStorage.getItem("logout") == "1"){	// 로그아웃 후 로그인 시도 시 
        	        			showCorver();
            	        		$("#loginForm").css("display", "block");
            	        		$("#email").focus();
            	        		
            				}else{	// 최초 접속 시
            					window.sessionStorage.setItem("level", "1");
        						window.sessionStorage.setItem("login", "success");	 
        						window.sessionStorage.setItem("login_time", new Date().getTime());
        						window.sessionStorage.setItem("user_id", "aIdoo"); 
        						
        						loginSuccess();
                				drawLogout();
            				}
        				}else{ 
        	        		loginSuccess();
        	        		drawLogout();
        	        	}
        			}
        		}
        })
	};
	
	script.type= 'text/javascript';
	script.src= 'https://api.ipify.org?format=jsonp&callback=getIP';
	head.appendChild(script);
		
	chkBanner();
});


function drawLogout(){
	var logout = "<img src=" + ctxPath + "/images/logout.png id='logout' />";
	$("body").prepend(logout);
	$("#logout").css({
		"position" : "absolute",
		"left" : $("#flagDiv").offset().left + $("#flagDiv").width() + getElSize(50),
		"top" : marginHeight + getElSize(20),
		"width" : getElSize(200),
		"cursor" : "pointer"
	}).click(function(){
		window.sessionStorage.removeItem("login_time");
		window.sessionStorage.removeItem("login");
		window.sessionStorage.removeItem("user_id");
		window.sessionStorage.removeItem("level");
		window.sessionStorage.setItem("logout", "1");
		location.href = "/DMT/chart/index.do"
	})
	
	var userId = window.sessionStorage.getItem("user_id")
	var id = "<font id='id_font'>ID : " + userId + "</font>";
	$("body").append(id);
	
	$("#id_font").css({
		"position" : "absolute",
		"left" : $("#flagDiv").offset().left + $("#flagDiv").width() + getElSize(270),
		"top" : marginHeight + getElSize(20),
		"color" : "white",
		"font-size" : getElSize(50),
		"cursor" : "pointer"
	});
};

function drawFlag(){
	var ko = "<img src=" + ctxPath + "/images/ko.png id='ko' class='flag'/>";
	var cn = "<img src=" + ctxPath + "/images/cn.png id='cn' class='flag'/>";
	var en = "<img src=" + ctxPath + "/images/en.png id='en' class='flag'/>";
	var de = "<img src=" + ctxPath + "/images/de.png id='de' class='flag'/>";
	
	var div = "<div id='flagDiv'>" + ko 
									+ cn
									+ en 
									+ de
									+ "</div>";
	$("body").prepend(div);
	
	$("#flagDiv").css({
		"position" : "absolute",
		"left" : marginWidth,
		"top" : marginHeight + getElSize(20)
	});
	
	$(".flag").css({
		"width" : getElSize(100),
		"cursor" : "pointer"
	}).click(changeLang);
	
	$(".flag").css("filter", "grayscale(100%)");
	var lang = window.localStorage.getItem("lang");
	$("#" + lang).css("filter", "grayscale(0%)");
};

function bindEvt(){
};

function changeLang(){
	var lang = this.id;
	var url = window.location.href;
	var param = "?lang=" + lang;
	
	window.localStorage.setItem("lang", lang)
	
	
	if(url.indexOf("fromDashBoard")!=-1){
		param = "&lang=" + lang;
		if(url.indexOf("&lang")!=-1){
			url = url.substr(0, url.lastIndexOf("&lang"));
		}
	}else{
		url = url.substr(0, url.lastIndexOf("?"))
	}
	
	location.href = url + param;
	
	$(".flag").css("filter", "grayscale(100%)");
	var lang = window.localStorage.getItem("lang");
	$("#" + lang).css("filter", "grayscale(0%)");
}

function decode(str){
	return decodeURIComponent(str).replace(/\+/gi, " ")
};

function getComName(){
	var url = ctxPath + "/chart/getComName.do";
	var param = "shopId=" + shopId;
	
	$.ajax({
		url : url,
		data : param,
		type : "post",
		dataType : "text",
		success : function(data){
			$("#title_right").html(decode(data));
		}
	});
};

function addZero(str){
	if(str.length==1) str = "0" + str;
	return str;
}

function showCorver(){
	$("#corver").css({
		"z-index" : 8,
		"background-color" : "black",
		//"opacity" : 0.6
	});
};

function closeCorver(){
	$("#corver").css({
		"z-index" : -999,
	});
}


function getBanner(){
	var url = ctxPath + "/chart/getBanner.do";
	var param = "shopId=" + shopId;
	

	$.ajax({
		url : url,
		data : param,
		type : "post",
		dataType : "json",
		success : function(data){
			if(window.localStorage.getItem("banner")=="true"){
				$("#intro").html(data.msg).css({
					"color" : data.rgb,
					"right" : - window.innerWidth
				})
				
				$("#intro").html(data.msg).css({
					"right" : - $("#intro").width()
				})
			}
			
			bannerWidth = $("#intro").width() + getElSize(100); 
			$("#intro").width(bannerWidth);
			$("#banner").val(data.msg);
			$("#color").val(data.rgb);
			
			//twinkle();
			bannerAnim();
		}
	});		
}

var twinkle_opct = false;
function twinkle(){
	var opacity;
	if(twinkle_opct){
		opacity = 0;
	}else{
		opacity = 1;
	}
	$("#intro").css("opacity",opacity);
	
	twinkle_opct = !twinkle_opct;
	setTimeout(twinkle, 300)
};

var bannerWidth;
function bannerAnim(){
	$("#intro").width(bannerWidth - getElSize(10));
	$("#intro").animate({
		"right" : window.innerWidth  - getElSize(100)
	},8000 * 2, function(){
		$("#intro").css("right" , - $("#intro").width())
		//$(this).remove();
		bannerAnim();
	});
};

function chkBanner(){
	if(window.localStorage.getItem("banner")=="true"){
		getBanner();
		$("#intro_back").css("display","block");
	}else{
		$("#intro").html("");
		$("#intro_back").css("display","none");
	}
};




