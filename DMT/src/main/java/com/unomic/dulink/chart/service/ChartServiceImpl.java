package com.unomic.dulink.chart.service;

import java.net.URLEncoder;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import org.apache.ibatis.session.SqlSession;
import org.codehaus.jackson.map.ObjectMapper;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.mybatis.spring.support.SqlSessionDaoSupport;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.unomic.dulink.chart.domain.ChartVo;
import com.unomic.dulink.chart.domain.TimeChartVo;
import com.unomic.dulink.chart.domain.UserVO;
import com.unomic.dulink.common.domain.CommonCode;
import com.unomic.dulink.common.domain.CommonFunction;

@Service
@Repository
public class ChartServiceImpl extends SqlSessionDaoSupport implements ChartService {
	private final static String namespace = "com.unomic.dulink.chart.";
	
	private static final Logger logger = LoggerFactory.getLogger(ChartServiceImpl.class);

	@Autowired
	SqlSession sqlSessionTemplate;

	Map statusMap = new HashMap();
	int chartOrder = 0;

	public boolean chkOrderStatus(String status) {
		boolean result = false;
		// if(chartOrder<=Integer.parseInt((String) statusMap.get(status))){
		// chartOrder = Integer.parseInt((String) statusMap.get(status));
		// result = true;
		// };
		return result;
	};

	public String addZero(String str) {
		if (str.length() == 1) {
			return "0" + str;
		} else {
			return str;
		}
	};

	@Override
	public String getStatusData(ChartVo chartVo) throws Exception {
		SqlSession sql = getSqlSession();
		return (String) sql.selectOne(namespace + "getStatusData", chartVo);
	};

	@Override
	public ChartVo getCurrentDvcData(ChartVo chartVo) throws Exception {
		SqlSession sql = getSqlSession();
		chartVo = (ChartVo) sql.selectOne(namespace + "getCurrentDvcData", chartVo);
		// chartVo.setEndDateTime((String)sql.selectOne(namespace + "getLastUpdateTime",
		// chartVo));

		if (chartVo == null) {
			chartVo = new ChartVo();
			chartVo.setChartStatus("null");
		}
		;

		return chartVo;
	};

	@Override
	public void addData() throws Exception {
		SqlSession sql = getSqlSession();
		ChartVo chartVo = new ChartVo();

		for (int i = 0; i <= 1; i++) {
			String status = "";
			int random = (int) (Math.random() * 10 + 1) + (int) (Math.random() * 10 + 1)
					+ (int) (Math.random() * 10 + 1) + (int) (Math.random() * 10 + 1);
			if (random == 40) {
				status = "Alarm";
			} else if (random >= 38) {
				status = "Wait";
			} else {
				status = "In-Cycle";
			}
			;

			Date d = new Date();
			SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
			String date = sdf.format(d);

			SimpleDateFormat sdf2 = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
			String time = sdf2.format(d);

			String spdLoad = String
					.valueOf(Math
							.floor((Math.random() * 10 + 1) + (Math.random() * 10 + 1) + (Math.random() * 10 + 1) + 60))
					.substring(0, 2);
			if (spdLoad.equals("90"))
				spdLoad = "0";
			String feedOverride = String
					.valueOf(Math
							.floor((Math.random() * 10 + 1) + (Math.random() * 10 + 1) + (Math.random() * 10 + 1) + 60))
					.substring(0, 2);
			if (feedOverride.equals("90"))
				spdLoad = "0";

			int dvcId = 0, adtId = 0;
			if (i == 0) {
				dvcId = 1;
				adtId = 13;
			} else if (i == 1) {
				dvcId = 5;
				adtId = 14;
			}

			chartVo.setDvcId(Integer.toString(dvcId));
			chartVo.setLastUpdateTime(time);
			chartVo.setStartDateTime(time);
			chartVo.setEndDateTime(time);
			chartVo.setAdtId(Integer.toString(adtId));
			chartVo.setSpdLoad(spdLoad);
			chartVo.setChartStatus(status);
			chartVo.setFeedOverride(feedOverride);
			chartVo.setDate(date);

			String chartStatus = (String) sql.selectOne(namespace + "getLastChartStatus", chartVo);

			if (chartStatus.equals(status)) {
				sql.update(namespace + "updateDeviceStatus", chartVo);
			} else {
				sql.update(namespace + "updateDeviceStatus", chartVo);
				chartVo.setStartDateTime((String) sql.selectOne(namespace + "getStartDateTime", chartVo));
				sql.update(namespace + "setChartEndTime", chartVo);
				chartVo.setStartDateTime(time);
				sql.insert(namespace + "addChartStatus", chartVo);
			}
		}
		;
	}

	@Override
	public String getDvcName(ChartVo chartVo) throws Exception {
		SqlSession sql = getSqlSession();
		String dvcName = (String) sql.selectOne(namespace + "getDvcName", chartVo);
		return dvcName;
	}

	@Override
	public String getAllDvcStatus(ChartVo chartVo) throws Exception {
		SqlSession sql = getSqlSession();
		List<ChartVo> chartData = sql.selectList(namespace + "getAllDvcStatus", chartVo);
		List list = new ArrayList();
		for (int i = 0; i < chartData.size(); i++) {
			Map map = new HashMap();
			map.put("adt_id", chartData.get(i).getAdtId());
			map.put("startDateTime", chartData.get(i).getStartDateTime());
			map.put("endDateTime", chartData.get(i).getEndDateTime());
			map.put("chartStatus", chartData.get(i).getChartStatus());
			map.put("alarm", chartData.get(i).getAlarm());
			map.put("spdLoad", chartData.get(i).getSpdLoad());
			map.put("spdActualSpeed", chartData.get(i).getSpdActualSpeed());
			map.put("feedOverride", chartData.get(i).getFeedOverride());
			map.put("actualFeed", chartData.get(i).getActualFeed());
			map.put("date", chartData.get(i).getDate());
			map.put("name", chartData.get(i).getName());
			map.put("dvcId", chartData.get(i).getDvcId());

			list.add(map);
		}
		;

		String str = "";

		Map resultMap = new HashMap();
		resultMap.put("chartStatus", list);
		ObjectMapper om = new ObjectMapper();
		str = om.defaultPrettyPrintingWriter().writeValueAsString(resultMap);
		return str;
	}

	@Override
	public String getAllDvcId(ChartVo chartVo) throws Exception {
		SqlSession sql = getSqlSession();
		List<ChartVo> dvcIdList = sql.selectList(namespace + "getAllDvcId", chartVo);

		List list = new ArrayList();
		
			
		
		/*
		* Date : 19.04.08 
		* Author : wilson
		* if 'type' is 'IOL', show data into 'PMC' not 'LAMP' 
		*/

		for (int i = 0; i < dvcIdList.size(); i++) {
			Map map = new HashMap();
			map.put("dvcId", dvcIdList.get(i).getDvcId());
			map.put("jig", dvcIdList.get(i).getJig());
			map.put("wc", dvcIdList.get(i).getWC());
			map.put("name", dvcIdList.get(i).getName());			
			map.put("red", dvcIdList.get(i).getRed());
			map.put("redBlink", dvcIdList.get(i).getRedBlink());
			map.put("yellow", dvcIdList.get(i).getYellow());
			map.put("yellowBlink", dvcIdList.get(i).getYellowBlink());
			map.put("green", dvcIdList.get(i).getGreen());
			map.put("greenBlink", dvcIdList.get(i).getGreenBlink());
			map.put("operationTime", dvcIdList.get(i).getOperationTime());
			map.put("cuttingTime", dvcIdList.get(i).getCuttingTime());
			map.put("type", dvcIdList.get(i).getType());

			if(dvcIdList.get(i).getType().equals("MTC")) {
				map.put("chartStatus", dvcIdList.get(i).getChartStatus());
			} 
			else {
				map.put("chartStatus", "IOL");
			}
			
			/*
			* Date : 19.04.05 
			* Author : wilson
			* include 'LAMP' value 
			*/
			
			String tmpLampStatus="";
			
			if(dvcIdList.get(i).getType().equals("IOL")) {
				tmpLampStatus = dvcIdList.get(i).getChartStatus();
			}else {	//MTC
				if (dvcIdList.get(i).getGreen().equals("1")) {
					tmpLampStatus = "IN-CYCLE";
				}else if(dvcIdList.get(i).getGreenBlink().equals("1")) {
					tmpLampStatus = "IN-CYCLE";
				}else if(dvcIdList.get(i).getRed().equals("1")) {
					tmpLampStatus = "ALARM";
				}else if(dvcIdList.get(i).getRedBlink().equals("1")) {
					tmpLampStatus = "ALARM";
				}else if(dvcIdList.get(i).getYellow().equals("1")) {
					tmpLampStatus = "WAIT";
				}else if(dvcIdList.get(i).getYellowBlink().equals("1")) {
					tmpLampStatus = "WAIT";
				}else {
					 tmpLampStatus = "NO-CONNECTION";			
				}
			}
			
			map.put("lampStatus", tmpLampStatus);
			
			String jig = "";
			if (dvcIdList.get(i).getJig() != null) {
				jig = URLEncoder.encode(dvcIdList.get(i).getJig(), "UTF-8");
				jig = jig.replaceAll("\\+", "%20");
			}
			;

			String wc = "";
			if (dvcIdList.get(i).getWC() != null) {
				wc = URLEncoder.encode(dvcIdList.get(i).getWC(), "UTF-8");
				wc = wc.replaceAll("\\+", "%20");
			}
			;

			map.put("jig", jig);
			map.put("WC", wc);

			list.add(map);
		}
		;

		Map resultMap = new HashMap();
		resultMap.put("dvcId", list);
		String str = "";
		ObjectMapper om = new ObjectMapper();
		str = om.defaultPrettyPrintingWriter().writeValueAsString(resultMap);

		return str;
	}

	@Override
	public String polling1() throws Exception {
		SqlSession sql = getSqlSession();
		return (String) sql.selectOne(namespace + "getVideoMachine");
	};

	@Override
	public String setVideo(String id) throws Exception {
		SqlSession sql = getSqlSession();
		String rtn = "";

		try {
			sql.update(namespace + "setVideo", id);
			rtn = "success";
		} catch (Exception e) {
			rtn = "fail";
		}
		;
		return rtn;
	};

	@Override
	public void initVideoPolling() throws Exception {
		SqlSession sql = getSqlSession();
		sql.update(namespace + "initVideoPolling");
	};

	@Override
	public void initPiePolling() throws Exception {
		SqlSession sql = getSqlSession();
		sql.update(namespace + "initPiePolling");
	}

	@Override
	public String piePolling1() throws Exception {
		SqlSession sql = getSqlSession();
		return (String) sql.selectOne(namespace + "piePolling1");
	};

	@Override
	public String piePolling2() throws Exception {
		SqlSession sql = getSqlSession();
		return (String) sql.selectOne(namespace + "piePolling2");
	};

	@Override
	public String setChart1(String id) throws Exception {
		SqlSession sql = getSqlSession();
		String rtn = "";

		try {
			sql.update(namespace + "setChart1", id);
			rtn = "success";
		} catch (Exception e) {
			rtn = "fail";
		}
		return rtn;
	};

	@Override
	public String setChart2(String id) throws Exception {
		SqlSession sql = getSqlSession();
		String rtn = "";

		try {
			sql.update(namespace + "setChart2", id);
			rtn = "success";
		} catch (Exception e) {
			rtn = "fail";
		}
		return rtn;
	}

	@Override
	public void delVideoMachine(String id) throws Exception {
		SqlSession sql = getSqlSession();
		sql.update(namespace + "delVideoMachine", id);
	}

	@Override
	public void delChartMachine(String id) throws Exception {
		SqlSession sql = getSqlSession();
		String machine = (String) sql.selectOne(namespace + "chkChartMachine1", id);

		if (machine == null) {
			sql.update(namespace + "delChartMachine2");
		} else {
			sql.update(namespace + "delChartMachine1");
		}
		;
	}

	@Override
	public void test(ChartVo chartVo) throws Exception {

		SqlSession sql = getSqlSession();
		List<ChartVo> chartData = sql.selectList(namespace + "getAllDvcStatus", chartVo);
	}

	@Override
	public String getAdapterId(ChartVo chartVo) throws Exception {
		SqlSession sql = getSqlSession();
		List<ChartVo> dvcIdList = sql.selectList(namespace + "getAdapterId", chartVo);

		List list = new ArrayList();

		for (int i = 0; i < dvcIdList.size(); i++) {
			Map map = new HashMap();
			map.put("dvcId", dvcIdList.get(i).getDvcId());
			map.put("name", dvcIdList.get(i).getName());
			map.put("chartStatus", dvcIdList.get(i).getChartStatus());
			map.put("operationTime", dvcIdList.get(i).getOperationTime());
			map.put("adtId", dvcIdList.get(i).getAdtId());

			list.add(map);
		}
		;

		Map resultMap = new HashMap();
		resultMap.put("dvcId", list);
		String str = "";
		ObjectMapper om = new ObjectMapper();
		str = om.defaultPrettyPrintingWriter().writeValueAsString(resultMap);

		return str;
	}

	@Override
	public String getBarChartDvcId(ChartVo chartVo) throws Exception {
		SqlSession sql = getSqlSession();
		List<ChartVo> dvcIdList = sql.selectList(namespace + "getBarChartDvcId", chartVo);

		List list = new ArrayList();

		for (int i = 0; i < dvcIdList.size(); i++) {
			Map map = new HashMap();
			map.put("dvcId", dvcIdList.get(i).getDvcId());
			map.put("name", dvcIdList.get(i).getName());
			map.put("chartStatus", dvcIdList.get(i).getChartStatus());
			map.put("operationTime", dvcIdList.get(i).getOperationTime());

			list.add(map);
		}
		;

		Map resultMap = new HashMap();
		resultMap.put("dvcId", list);
		String str = "";
		ObjectMapper om = new ObjectMapper();
		str = om.defaultPrettyPrintingWriter().writeValueAsString(resultMap);

		return str;
	}

	@Override
	public String showDetailEvent(ChartVo chartVo) throws Exception {
		SqlSession sql = getSqlSession();
		List<ChartVo> chartData = sql.selectList(namespace + "showDetailEvent", chartVo);

		List list = new ArrayList();
		for (int i = 0; i < chartData.size(); i++) {
			Map map = new HashMap();
			map.put("startDateTime", chartData.get(i).getStartDateTime());
			map.put("endDateTime", chartData.get(i).getEndDateTime());
			map.put("chartStatus", chartData.get(i).getChartStatus());
			map.put("alarm", chartData.get(i).getAlarm());
			map.put("spdLoad", chartData.get(i).getSpdLoad());
			map.put("feedOverride", chartData.get(i).getFeedOverride());

			list.add(map);
		}
		;

		String str = "";
		Map resultMap = new HashMap();
		resultMap.put("chartData", list);

		ObjectMapper om = new ObjectMapper();
		str = om.defaultPrettyPrintingWriter().writeValueAsString(resultMap);

		return str;
	}

	@Override
	public String getDetailStatus(ChartVo chartVo) throws Exception {
		SqlSession sql = getSqlSession();

		List<ChartVo> chartData = sql.selectList(namespace + "getDetailChart", chartVo);

		List statusList = new ArrayList();
		for (int i = 0; i < chartData.size(); i++) {
			Map map = new HashMap();
			map.put("adt_id", chartData.get(i).getAdtId());
			map.put("startDateTime", chartData.get(i).getStartDateTime());
			map.put("endDateTime", chartData.get(i).getEndDateTime());
			map.put("chartStatus", chartData.get(i).getChartStatus());

			statusList.add(map);
		}
		;

		Map map = new HashMap();
		map.put("chartStatus", statusList);

		String str = "";
		ObjectMapper om = new ObjectMapper();

		str = om.defaultPrettyPrintingWriter().writeValueAsString(map);
		return str;
	}

	@Override
	public ChartVo getOtherChartsData(ChartVo chartVo) throws Exception {
		SqlSession sql = getSqlSession();
		chartVo = (ChartVo) sql.selectOne(namespace + "getOtherChartsData", chartVo);
		return chartVo;
	};

	// 濡쒖쭅 援ъ꽦. 1遺��꽣 144源뚯� 鍮덉뭏 留뚮뱾湲�.
	// default no-connection
	// �쁽�옱�떆媛� 10遺� �쟾源뚯� 怨꾩궛.
	// 媛숈� �떆媛� �엳�쑝硫� �꽔湲�.
	// �뱾�뼱媛� �궇吏쒕줈 議고쉶�븯�뒗 猷⑦떞 �븘�슂.
	@Override
	public List<TimeChartVo> getTimeChartData(ChartVo inputVo) {
		SqlSession sql = getSqlSession();
		List<ChartVo> listChartData = sql.selectList(namespace + "getTimeChartData", inputVo);

		if (listChartData.size() < 1) {
			return null;
		}

		List<TimeChartVo> tmpTimeChart = new ArrayList();
		int CHART_SIZE = 144;

		String stTime = CommonFunction.getChartStartTime(inputVo.getTargetDateTime());
		Long unixTime = CommonFunction.dateTime2Mil(stTime);
		String stDate = stTime;

		// 10遺� 異붽�.
		String edDate = CommonFunction.unixTime2Datetime(CommonFunction.dateTime2Mil(stDate) + 600000L);
		for (int i = 0; i < CHART_SIZE; i++) {

			TimeChartVo tmpVo = new TimeChartVo();
			tmpVo.setStartTime(stDate);
			tmpVo.setEndTime(edDate);
			tmpVo.setY(CommonCode.MSG_CHART_DIST);

			Iterator<ChartVo> ite = listChartData.iterator();
			tmpVo.setColor(CommonCode.MSG_COLOR_NO_CONNECTION);
			while (ite.hasNext()) {
				ChartVo tmpChartVo = ite.next();
				if (CommonFunction.cutRight(tmpChartVo.getStartDateTime(), 2).equals(tmpVo.getStartTime())) {
					if (tmpChartVo.getChartStatus().equals(CommonCode.MSG_ALARM)) {
						tmpVo.setColor(CommonCode.MSG_COLOR_ALARM);
					} else if (tmpChartVo.getChartStatus().equals(CommonCode.MSG_WAIT)) {
						tmpVo.setColor(CommonCode.MSG_COLOR_WAIT);
					} else if (tmpChartVo.getChartStatus().equals(CommonCode.MSG_IN_CYCLE)) {
						tmpVo.setColor(CommonCode.MSG_COLOR_IN_CYCLE);
					}
				}

				// 媛��졇�삩 留덉�留� �뜲�씠�꽣�씪�븣... loop �깉異�.
				if (!ite.hasNext()
						&& CommonFunction.cutRight(tmpChartVo.getStartDateTime(), 2).equals(tmpVo.getStartTime())) {
					tmpTimeChart.add(tmpVo);

					return tmpTimeChart;

				}
			}
			stDate = edDate;
			edDate = CommonFunction.unixTime2Datetime(CommonFunction.dateTime2Mil(stDate) + 600000L);
			tmpTimeChart.add(tmpVo);
		}
		return tmpTimeChart;
	};

	@Override
	public List<ChartVo> testTimeChartData(ChartVo inputVo) {
		SqlSession sql = getSqlSession();
		List<ChartVo> listChartData = sql.selectList(namespace + "getTimeChartData", inputVo);

		return listChartData;
	}

	@Override
	public String getJicList(ChartVo chartVo) throws Exception {
		SqlSession sql = getSqlSession();
		List<ChartVo> jigList = sql.selectList(namespace + "getJicList", chartVo);

		List jigs = new ArrayList();
		for (int i = 0; i < jigList.size(); i++) {
			Map map = new HashMap();
			map.put("jig", URLEncoder.encode(jigList.get(i).getJig(), "utf-8"));

			jigs.add(map);
		}
		;

		Map jigMap = new HashMap();
		jigMap.put("dataList", jigs);

		String str = "";
		ObjectMapper ob = new ObjectMapper();
		str = ob.defaultPrettyPrintingWriter().writeValueAsString(jigMap);
		return str;
	};

	@Override
	public String getWcList(ChartVo chartVo) throws Exception {
		SqlSession sql = getSqlSession();

		List<ChartVo> jigList = sql.selectList(namespace + "getWcList", chartVo);

		List jigs = new ArrayList();
		for (int i = 0; i < jigList.size(); i++) {
			Map map = new HashMap();
			map.put("WC", URLEncoder.encode(jigList.get(i).getWC(), "utf-8"));

			jigs.add(map);
		}
		;

		Map jigMap = new HashMap();
		jigMap.put("wcList", jigs);

		String str = "";
		ObjectMapper ob = new ObjectMapper();
		str = ob.defaultPrettyPrintingWriter().writeValueAsString(jigMap);
		return str;
	}

	@Override
	public String getTableData(ChartVo chartVo) throws Exception {
		SqlSession sql = getSqlSession();
		List<ChartVo> dataList = sql.selectList(namespace + "getDataList", chartVo);

		List list = new ArrayList();

		for (int i = 0; i < dataList.size(); i++) {
			Map map = new HashMap();
			map.put("jig", URLEncoder.encode(dataList.get(i).getJig(), "utf-8"));
			map.put("WC", URLEncoder.encode(dataList.get(i).getWC(), "utf-8"));
			map.put("WCG", URLEncoder.encode(dataList.get(i).getGRNM(), "utf-8"));
			map.put("name", URLEncoder.encode(dataList.get(i).getName(), "utf-8"));
			map.put("target_time", dataList.get(i).getTargetDateTime());
			map.put("inCycle_time", dataList.get(i).getInCycleTime());
			map.put("wait_time", dataList.get(i).getWaitTime());
			map.put("alarm_time", dataList.get(i).getAlarmTime());
			map.put("noConnTime", dataList.get(i).getNoConnectionTime());
			map.put("line", dataList.get(i).getLine());

			list.add(map);
		}
		;

		Map dataMap = new HashMap();
		dataMap.put("tableData", list);

		String str = "";
		ObjectMapper om = new ObjectMapper();
		str = om.defaultPrettyPrintingWriter().writeValueAsString(dataMap);
		return str;
	}

	@Override
	public String getWcData(ChartVo chartVo) throws Exception {
		SqlSession sql = getSqlSession();
		List<ChartVo> wcList = sql.selectList(namespace + "getWcData", chartVo);

		List list = new ArrayList();

		for (int i = 0; i < wcList.size(); i++) {
			Map map = new HashMap();
			map.put("name", URLEncoder.encode(wcList.get(i).getName(), "utf-8"));
			map.put("target_time", wcList.get(i).getTargetDateTime());
			map.put("inCycle_time", wcList.get(i).getInCycleTime());
			map.put("wait_time", wcList.get(i).getWaitTime());
			map.put("alarm_time", wcList.get(i).getAlarmTime());
			map.put("noConnTime", wcList.get(i).getNoConnectionTime());
			map.put("workDate", wcList.get(i).getWorkDate());

			list.add(map);
		}
		;

		Map dataMap = new HashMap();
		dataMap.put("wcList", list);

		String str = "";
		ObjectMapper om = new ObjectMapper();
		str = om.defaultPrettyPrintingWriter().writeValueAsString(dataMap);
		return str;
	}

	@Override
	public String getWcDataByDvc(ChartVo chartVo) throws Exception {
		SqlSession sql = getSqlSession();
		List<ChartVo> wcList = sql.selectList(namespace + "getWcDataByDvc", chartVo);

		List list = new ArrayList();

		for (int i = 0; i < wcList.size(); i++) {
			Map map = new HashMap();

			map.put("target_time", wcList.get(i).getTargetDateTime());
			map.put("inCycle_time", wcList.get(i).getInCycleTime());
			map.put("wait_time", wcList.get(i).getWaitTime());
			map.put("alarm_time", wcList.get(i).getAlarmTime());
			map.put("noConnTime", wcList.get(i).getNoConnectionTime());
			map.put("workDate", wcList.get(i).getWorkDate());

			list.add(map);
		}
		;

		Map dataMap = new HashMap();
		dataMap.put("wcList", list);

		String str = "";
		ObjectMapper om = new ObjectMapper();
		str = om.defaultPrettyPrintingWriter().writeValueAsString(dataMap);
		return str;
	}

	@Override
	public String getDvcList(ChartVo chartVo) throws Exception {
		SqlSession sql = getSqlSession();
		List<ChartVo> dvcList = sql.selectList(namespace + "getDvcList", chartVo);

		List list = new ArrayList();
		for (int i = 0; i < dvcList.size(); i++) {
			Map map = new HashMap();
			map.put("wc", dvcList.get(i).getWC());

			list.add(map);
		}
		;

		Map dvcMap = new HashMap();
		dvcMap.put("dvcList", list);
		String str = "";
		ObjectMapper om = new ObjectMapper();

		str = om.defaultPrettyPrintingWriter().writeValueAsString(dvcMap);

		return str;
	}

	@Override
	public String getAlarmData(ChartVo chartVo) throws Exception {
		SqlSession sql = getSqlSession();
		List<ChartVo> alarmList = sql.selectList(namespace + "getAlarmData", chartVo);

		List list = new ArrayList();
		for (int i = 0; i < alarmList.size(); i++) {
			Map map = new HashMap();
			map.put("name", alarmList.get(i).getName());
			map.put("startDateTime", alarmList.get(i).getStartDateTime());
			map.put("endDateTime", alarmList.get(i).getEndDateTime());
			map.put("delayTimeSec", alarmList.get(i).getDelayTimeSec());
			map.put("ncAlarmNum1", alarmList.get(i).getNcAlarmNum1());
			map.put("ncAlarmMsg1", URLEncoder.encode(alarmList.get(i).getNcAlarmMsg1(), "utf-8"));
			map.put("ncAlarmNum2", alarmList.get(i).getNcAlarmNum2());
			map.put("ncAlarmMsg2", URLEncoder.encode(alarmList.get(i).getNcAlarmMsg2(), "utf-8"));
			map.put("ncAlarmNum3", alarmList.get(i).getNcAlarmNum3());
			map.put("ncAlarmMsg3", URLEncoder.encode(alarmList.get(i).getNcAlarmMsg3(), "utf-8"));

			list.add(map);
		}
		;

		Map alarmMap = new HashMap();
		alarmMap.put("alarmList", list);

		String str = "";
		ObjectMapper om = new ObjectMapper();
		str = om.defaultPrettyPrintingWriter().writeValueAsString(alarmMap);

		return str;
	}

	@Override
	public String getJigList4Report(ChartVo chartVo) throws Exception {
		SqlSession sql = getSqlSession();
		List<ChartVo> dvcList = sql.selectList(namespace + "getJigList4Report", chartVo);

		List list = new ArrayList();
		for (int i = 0; i < dvcList.size(); i++) {
			Map map = new HashMap();
			map.put("name", URLEncoder.encode(dvcList.get(i).getName(), "utf-8"));
			map.put("dvcId", URLEncoder.encode(dvcList.get(i).getDvcId(), "utf-8"));
			list.add(map);
		}
		;

		Map dvcMap = new HashMap();
		dvcMap.put("dvcList", list);

		String str = "";
		ObjectMapper om = new ObjectMapper();
		str = om.defaultPrettyPrintingWriter().writeValueAsString(dvcMap);

		return str;
	}

	@Override
	public String getAlarmList(ChartVo chartVo) throws Exception {
		SqlSession sql = getSqlSession();
		List<ChartVo> dataList = sql.selectList(namespace + "getAlarmList", chartVo);

		List list = new ArrayList();
		for (int i = 0; i < dataList.size(); i++) {
			
			Map map = new HashMap();
			map.put("startDateTime", dataList.get(i).getStartDateTime());
			map.put("alarmCode", dataList.get(i).getAlarmCode());
			map.put("alarmMsg", dataList.get(i).getAlarmMsg());
			
			list.add(map);
		}
		;

		String str = "";
		Map dataMap = new HashMap();
		dataMap.put("alarmList", list);

		ObjectMapper om = new ObjectMapper();
		str = om.defaultPrettyPrintingWriter().writeValueAsString(dataMap);
		return str;
	}

	@Override
	public String getStartTime(ChartVo chartVo) throws Exception {
		SqlSession sql = getSqlSession();
		String rtn = (String) sql.selectOne(namespace + "getStartTime", chartVo);
		return rtn;
	}

	@Override
	public String getMachineName(ChartVo chartVo) throws Exception {
		SqlSession sql = getSqlSession();
		String str = (String) sql.selectOne(namespace + "getMachineName", chartVo);

		return URLEncoder.encode(str, "utf-8");
	};

	@Override
	public String getTargetData(ChartVo chartVo) throws Exception {
		SqlSession sql = getSqlSession();
		List<ChartVo> dataList = sql.selectList(namespace + "getTargetData", chartVo);

		List list = new ArrayList();
		for (int i = 0; i < dataList.size(); i++) {
			Map map = new HashMap();
			map.put("dvcId", dataList.get(i).getDvcId());
			map.put("name", URLEncoder.encode(dataList.get(i).getName(), "utf-8"));
			// map.put("tgCnt", dataList.get(i).getTgCnt());
			// map.put("tgRunTime", dataList.get(i).getTgRunTime());
			// map.put("cntPerCyl", dataList.get(i).getCntPerCyl());
			map.put("tgCntD", dataList.get(i).getTgCntD());
			map.put("tgCntN", dataList.get(i).getTgCntN());
			map.put("cntPerCylD", dataList.get(i).getCntPerCylD());
			map.put("cntPerCylN", dataList.get(i).getCntPerCylN());
			map.put("tgRunTimeD", dataList.get(i).getTgRunTimeD());
			map.put("tgRunTimeN", dataList.get(i).getTgRunTimeN());

			list.add(map);
		}
		;

		String str = "";
		Map dataMap = new HashMap();
		dataMap.put("dataList", list);

		ObjectMapper om = new ObjectMapper();
		str = om.defaultPrettyPrintingWriter().writeValueAsString(dataMap);
		return str;
	}

	@Override
	@Transactional
	public String addTargetCnt(String val) throws Exception {
		SqlSession sql = getSqlSession();

		JSONParser jsonParser = new JSONParser();
		JSONObject jsonObj = (JSONObject) jsonParser.parse(val);
		JSONArray jsonArray = (JSONArray) jsonObj.get("val");

		List<ChartVo> list = new ArrayList<ChartVo>();
		for (int i = 0; i < jsonArray.size(); i++) {
			JSONObject tempObj = (JSONObject) jsonArray.get(i);

			ChartVo chartVo = new ChartVo();
			chartVo.setDvcId(tempObj.get("dvcId").toString());
			chartVo.setTgCyl(tempObj.get("tgCyl").toString());
			chartVo.setTgRunTime(tempObj.get("tgRunTime").toString());
			chartVo.setTgDate(tempObj.get("tgDate").toString());
			chartVo.setType(tempObj.get("type").toString());
			chartVo.setCntPerCyl(tempObj.get("cntPerCyl").toString());

			list.add(chartVo);
		}

		String str = "";

		try {
			int exist = (Integer) sql.selectOne(namespace + "cntDuplData_nd", list.get(0).getTgDate());
			if (exist > 0) {
				sql.delete(namespace + "delTargetCnt_nd", list.get(0).getTgDate());
			}
			sql.insert(namespace + "addTargetCnt", list);
			str = "success";
		} catch (Exception e) {
			str = "fail";
		}

		return str;
	}

	@Override
	public String getTimeData(ChartVo chartVo) throws Exception {
		SqlSession sql = getSqlSession();
		List<ChartVo> statusList = sql.selectList(namespace + "getTimeData", chartVo);

		List list = new ArrayList();
		for (int i = 0; i < statusList.size(); i++) {
			Map map = new HashMap();
			map.put("status", statusList.get(i).getStatus());
			map.put("startDateTime", statusList.get(i).getStartDateTime());
			map.put("spdLoad", statusList.get(i).getSpdLoad());
			map.put("spdOverride", statusList.get(i).getFeedOverride());

			list.add(map);
		}
		;

		Map statusMap = new HashMap();
		statusMap.put("statusList", list);

		ObjectMapper om = new ObjectMapper();
		String str = "";
		str = om.defaultPrettyPrintingWriter().writeValueAsString(statusMap);
		return str;
	}

	@Override
	public String getDetailBlockData(ChartVo chartVo) throws Exception {
		SqlSession sql = getSqlSession();
		List<ChartVo> dataList = sql.selectList(namespace + "getDetailBlockData", chartVo);

		List list = new ArrayList();
		for (int i = 0; i < dataList.size(); i++) {
			Map map = new HashMap();
			map.put("lastAlarmCode", dataList.get(i).getLastAlarmCode());
			map.put("lastAlarmMsg", dataList.get(i).getLastAlarmMsg());
			map.put("lastTgPrdctNum", dataList.get(i).getLastTgPrdctNum());
			map.put("lastFnPrdctNum", dataList.get(i).getLastFnPrdctNum());
			map.put("LastAvrCycleTime", dataList.get(i).getLastAvrCycleTime());
			map.put("tgCnt", dataList.get(i).getTgCnt());
			map.put("status", dataList.get(i).getStatus());
			map.put("prdctPerHour", dataList.get(i).getPrdctPerHour());

			map.put("prdctPerCyl", dataList.get(i).getPrdctPerCyl());
			map.put("remainCnt", dataList.get(i).getRemainCnt());
			map.put("feedOverride", dataList.get(i).getFeedOverride());
			map.put("spdLoad", dataList.get(i).getSpdLoad());
			map.put("ncAlarmNum1", dataList.get(i).getNcAlarmNum1());
			map.put("ncAlarmMsg1", URLEncoder.encode(dataList.get(i).getNcAlarmMsg1(), "utf-8"));
			map.put("ncAlarmNum2", dataList.get(i).getNcAlarmNum2());
			map.put("ncAlarmMsg2", URLEncoder.encode(dataList.get(i).getNcAlarmMsg2(), "utf-8"));
			map.put("ncAlarmNum3", dataList.get(i).getNcAlarmNum3());
			map.put("ncAlarmMsg3", URLEncoder.encode(dataList.get(i).getNcAlarmMsg3(), "utf-8"));

			map.put("programHeader", dataList.get(i).getProgramHeader());
			map.put("programName", dataList.get(i).getProgramName());
			map.put("type", dataList.get(i).getType());
			list.add(map);
		}
		;

		String str = "";
		Map listMap = new HashMap();
		listMap.put("dataList", list);

		ObjectMapper om = new ObjectMapper();
		str = om.defaultPrettyPrintingWriter().writeValueAsString(listMap);
		return str;
	}

	@Override
	public String getRepairList(ChartVo chartVo) throws Exception {
		SqlSession sql = getSqlSession();
		List<ChartVo> dataList = sql.selectList(namespace + "getRepairList", chartVo);

		List list = new ArrayList();
		for (int i = 0; i < dataList.size(); i++) {
			Map map = new HashMap();
			map.put("stDate", dataList.get(i).getStDate());
			map.put("fnDate", dataList.get(i).getFnDate());
			map.put("rpCause", URLEncoder.encode(dataList.get(i).getRpCause(), "utf-8"));
			map.put("eName", URLEncoder.encode(dataList.get(i).getEName(), "utf-8"));

			list.add(map);
		}
		;

		String str = "";
		Map listMap = new HashMap();
		listMap.put("dataList", list);

		ObjectMapper om = new ObjectMapper();
		str = om.defaultPrettyPrintingWriter().writeValueAsString(listMap);
		return str;
	}

	@Override
	public String getDvcNameList(ChartVo chartVo) throws Exception {
		SqlSession sql = getSqlSession();
		List<ChartVo> dataList = sql.selectList(namespace + "getDvcNameList", chartVo);

		List list = new ArrayList();
		for (int i = 0; i < dataList.size(); i++) {
			Map map = new HashMap();
			map.put("dvcId", dataList.get(i).getDvcId());
			map.put("status", dataList.get(i).getStatus());
			map.put("name", URLEncoder.encode(dataList.get(i).getName(), "utf-8"));
			map.put("lastAlarmCode", dataList.get(i).getLastAlarmCode());
			map.put("lastAlarmMsg", dataList.get(i).getLastAlarmMsg());

			list.add(map);
		}
		;

		String str = "";
		Map listMap = new HashMap();
		listMap.put("dataList", list);

		ObjectMapper om = new ObjectMapper();
		str = om.defaultPrettyPrintingWriter().writeValueAsString(listMap);
		return str;
	}

	@Override
	public String getDvcIdForSign(ChartVo chartVo) throws Exception {
		SqlSession sql = getSqlSession();
		List<ChartVo> dataList = sql.selectList(namespace + "getDvcIdForSign", chartVo);

		List list = new ArrayList();
		for (int i = 0; i < dataList.size(); i++) {
			Map map = new HashMap();
			map.put("id", dataList.get(i).getDvcId());
			map.put("status", dataList.get(i).getStatus());
			map.put("name", URLEncoder.encode(dataList.get(i).getName(), "utf-8"));
			map.put("lastAlarmCode", dataList.get(i).getLastAlarmCode());
			map.put("lastAlarmMsg", dataList.get(i).getLastAlarmMsg());

			list.add(map);
		}
		;

		String str = "";
		Map listMap = new HashMap();
		listMap.put("dataList", list);

		ObjectMapper om = new ObjectMapper();
		str = om.defaultPrettyPrintingWriter().writeValueAsString(listMap);
		return str;
	}

	@Override
	public String getDvcId(ChartVo chartVo) throws Exception {
		SqlSession sql = getSqlSession();
		List<ChartVo> dataList = sql.selectList(namespace + "getDvcId", chartVo);

		List list = new ArrayList();
		for (int i = 0; i < dataList.size(); i++) {
			Map map = new HashMap();
			map.put("dvcId", dataList.get(i).getDvcId());
			map.put("name", URLEncoder.encode(dataList.get(i).getName(), "utf-8"));
			map.put("status", dataList.get(i).getStatus());

			list.add(map);
		}
		;

		String str = "";
		Map listMap = new HashMap();
		listMap.put("dataList", list);

		ObjectMapper om = new ObjectMapper();
		str = om.defaultPrettyPrintingWriter().writeValueAsString(listMap);
		return str;
	}

	@Override
	public ChartVo getData(ChartVo chartVo) throws Exception {
		SqlSession sql = getSqlSession();
		chartVo = (ChartVo) sql.selectOne(namespace + "getData", chartVo);

		return chartVo;
	}

	@Override
	public String getFacilitiesStatus(ChartVo chartVo) throws Exception {
		SqlSession sql = getSqlSession();
		List<ChartVo> dataList = sql.selectList(namespace + "getFacilitiesStatus", chartVo);

		List list = new ArrayList();
		for (int i = 0; i < dataList.size(); i++) {
			Map map = new HashMap();
			map.put("name", URLEncoder.encode(dataList.get(i).getName(), "utf-8"));
			map.put("wc", URLEncoder.encode(dataList.get(i).getWC(), "utf-8"));
			map.put("jig", URLEncoder.encode(dataList.get(i).getJig(), "utf-8"));
			map.put("mcTy", URLEncoder.encode(dataList.get(i).getMcTy(), "utf-8"));
			map.put("group", URLEncoder.encode(dataList.get(i).getGroup(), "utf-8"));
			map.put("ncTy", URLEncoder.encode(dataList.get(i).getNcTy(), "utf-8"));

			list.add(map);
		}
		;

		String str = "";
		Map listMap = new HashMap();
		listMap.put("dataList", list);

		ObjectMapper om = new ObjectMapper();
		str = om.defaultPrettyPrintingWriter().writeValueAsString(listMap);
		return str;
	}

	@Override
	public String getTraceHist(ChartVo chartVo) throws Exception {
		SqlSession sql = getSqlSession();

		List<ChartVo> dataList = sql.selectList(namespace + "getTraceHist", chartVo);

		List list = new ArrayList();
		for (int i = 0; i < dataList.size(); i++) {
			Map map = new HashMap();
			map.put("SN", dataList.get(i).getSN());
			map.put("OP80", dataList.get(i).getOP80());
			map.put("OP90", dataList.get(i).getOP90());
			map.put("OP100", dataList.get(i).getOP100());
			map.put("OP110", dataList.get(i).getOP110());
			map.put("OP150", dataList.get(i).getOP150());
			map.put("OP8090INSP", dataList.get(i).getOP8090INSP());
			map.put("OP100110INSP", dataList.get(i).getOP100110INSP());
			map.put("OP150INSP", dataList.get(i).getOP150INSP());
			map.put("START_TIME", dataList.get(i).getStartTime());
			map.put("END_TIME", dataList.get(i).getEndTime());
			map.put("REGDT", dataList.get(i).getREGDT());

			list.add(map);
		}
		;

		String str = "";
		Map listMap = new HashMap();
		listMap.put("dataList", list);

		ObjectMapper om = new ObjectMapper();
		str = om.defaultPrettyPrintingWriter().writeValueAsString(listMap);
		return str;
	}

	@Override
	public String getToolList(ChartVo chartVo) throws Exception {
		SqlSession sql = getSqlSession();

		List<ChartVo> dataList = sql.selectList(namespace + "getToolList", chartVo);

		List list = new ArrayList();
		for (int i = 0; i < dataList.size(); i++) {
			Map map = new HashMap();
			map.put("toolNm", dataList.get(i).getToolNm());
			map.put("spec", dataList.get(i).getSpec());
			map.put("portNo", dataList.get(i).getPortNo());
			map.put("prdCntLmt", dataList.get(i).getPrdCntLmt());
			map.put("prdCntCrt", dataList.get(i).getPrdCntCrt());
			map.put("cycleRemain", dataList.get(i).getCycleRemain());
			map.put("cycleUsedRate", dataList.get(i).getCycleUsedRate());
			map.put("runTimeLimit", dataList.get(i).getRunTimeLimit());
			map.put("runTimeCurrent", dataList.get(i).getRunTimeCurrent());
			map.put("runTimeRemain", dataList.get(i).getRunTimeRemain());
			map.put("runTimeUsedRate", dataList.get(i).getRunTimeUsedRate());
			map.put("offsetDLimit", dataList.get(i).getOffsetDLimit());
			map.put("offsetDInit", dataList.get(i).getOffsetDInit());
			map.put("offsetDCurrent", dataList.get(i).getOffsetDCurrent());
			map.put("offsetHLimit", dataList.get(i).getOffsetHLimit());
			map.put("offsetHInit", dataList.get(i).getOffsetHInit());
			map.put("offsetHCurrent", dataList.get(i).getOffsetHCurrent());
			map.put("date", dataList.get(i).getDate());
			map.put("resetDt", dataList.get(i).getResetDt());

			list.add(map);
		}
		;

		String str = "";
		Map listMap = new HashMap();
		listMap.put("dataList", list);

		ObjectMapper om = new ObjectMapper();
		str = om.defaultPrettyPrintingWriter().writeValueAsString(listMap);
		return str;
	}

	@Override
	public void resetTool(ChartVo chartVo) throws Exception {
		SqlSession sql = getSqlSession();

		// reset date
		sql.update(namespace + "resetTool", chartVo);

		sql.update(namespace + "setHstNull", chartVo);
		// sql.update(namespace + "setMstNull", chartVo);

	}

	@Override
	public void setLimit(ChartVo chartVo) throws Exception {
		SqlSession sql = getSqlSession();
		sql.update(namespace + "setLimit", chartVo);
	}

	@Override
	public void addBanner(ChartVo chartVo) throws Exception {
		SqlSession sql = getSqlSession();
		sql.update(namespace + "addBanner", chartVo);
	}

	@Override
	public ChartVo getBanner(ChartVo chartVo) throws Exception {
		SqlSession sql = getSqlSession();
		chartVo = (ChartVo) sql.selectOne(namespace + "getBanner", chartVo);
		return chartVo;
	}

	@Override
	public String getGroup(ChartVo chartVo) throws Exception {
		SqlSession sql = getSqlSession();

		List<ChartVo> dataList = sql.selectList(namespace + "getGroup", chartVo);

		List list = new ArrayList();
		for (int i = 0; i < dataList.size(); i++) {
			Map map = new HashMap();
			map.put("group", dataList.get(i).getGroup());
			list.add(map);
		}
		;

		String str = "";
		Map listMap = new HashMap();
		listMap.put("dataList", list);

		ObjectMapper om = new ObjectMapper();
		str = om.defaultPrettyPrintingWriter().writeValueAsString(listMap);
		return str;
	}

	@Override
	public String getPerformanceAgainstGoal(ChartVo chartVo) throws Exception {
		SqlSession sql = getSqlSession();

		List<ChartVo> dataList = sql.selectList(namespace + "getPerformanceAgainstGoal", chartVo);

		List list = new ArrayList();
		for (int i = 0; i < dataList.size(); i++) {
			Map map = new HashMap();
			map.put("group", dataList.get(i).getGroup());
			map.put("name", URLEncoder.encode(dataList.get(i).getName(), "utf-8"));
			map.put("opTime", dataList.get(i).getOpTime());
			map.put("cntCyl", dataList.get(i).getCntCyl());
			map.put("dvcCntCyl", dataList.get(i).getDvcCntCyl());
			map.put("tgCyl", dataList.get(i).getTgCnt());
			map.put("faultCnt", dataList.get(i).getFaultCnt());
			map.put("targetRatio", dataList.get(i).getTargetRatio());
			map.put("target_time", dataList.get(i).getTargetDateTime());
			map.put("inCycle_time", dataList.get(i).getInCycleTime());
			map.put("wait_time", dataList.get(i).getWaitTime());
			map.put("alarm_time", dataList.get(i).getAlarmTime());
			map.put("noConnTime", dataList.get(i).getNoConnectionTime());
			map.put("lastAvrCycleTime", dataList.get(i).getLastAvrCycleTime());
			map.put("opRatio", dataList.get(i).getOpRatio());

			map.put("capa", dataList.get(i).getCapa());
			map.put("cycleTmM", dataList.get(i).getCycleTmM());
			map.put("cycleTmS", dataList.get(i).getCycleTmS());
			map.put("cvt", dataList.get(i).getCvt());
			map.put("uph", dataList.get(i).getUph());
			map.put("workerD", URLEncoder.encode(dataList.get(i).getWorkerD(), "utf-8"));
			map.put("workerN", URLEncoder.encode(dataList.get(i).getWorkerN(), "utf-8"));
			map.put("type", dataList.get(i).getType());
			map.put("workTmMD", dataList.get(i).getWorkTmMD());
			map.put("workTmMN", dataList.get(i).getWorkTmMN());
			map.put("nonOpTimeD", dataList.get(i).getNonOpTimeD());
			map.put("nonOpTimeN", dataList.get(i).getNonOpTimeN());

			list.add(map);
		}
		;

		String str = "";
		Map listMap = new HashMap();
		listMap.put("dataList", list);

		ObjectMapper om = new ObjectMapper();
		str = om.defaultPrettyPrintingWriter().writeValueAsString(listMap);
		return str;
	}

	@Override
	public String getAllDvc(ChartVo chartVo) throws Exception {
		SqlSession sql = getSqlSession();

		List<ChartVo> dataList = sql.selectList(namespace + "getAllDvc", chartVo);

		List list = new ArrayList();
		for (int i = 0; i < dataList.size(); i++) {
			Map map = new HashMap();
			map.put("name", URLEncoder.encode(dataList.get(i).getName(), "utf-8"));
			map.put("dvcId", dataList.get(i).getDvcId());

			list.add(map);
		}
		;

		String str = "";
		Map listMap = new HashMap();
		listMap.put("dataList", list);

		ObjectMapper om = new ObjectMapper();
		str = om.defaultPrettyPrintingWriter().writeValueAsString(listMap);
		return str;
	}

	@Override
	public String getPrgCdList(ChartVo chartVo) throws Exception {
		SqlSession sql = getSqlSession();
		List<ChartVo> dataList = sql.selectList(namespace + "getPrgCdList", chartVo);

		List list = new ArrayList();
		for (int i = 0; i < dataList.size(); i++) {
			Map map = new HashMap();
			map.put("prgCd", URLEncoder.encode(dataList.get(i).getPrgCd(), "utf-8"));

			list.add(map);
		}
		;

		String str = "";
		Map listMap = new HashMap();
		listMap.put("dataList", list);

		ObjectMapper om = new ObjectMapper();
		str = om.defaultPrettyPrintingWriter().writeValueAsString(listMap);
		return str;
	}

	@Override
	public String getProgCd(ChartVo chartVo) throws Exception {
		SqlSession sql = getSqlSession();
		List<ChartVo> dataList = sql.selectList(namespace + "getProgCd", chartVo);

		List list = new ArrayList();
		for (int i = 0; i < dataList.size(); i++) {
			Map map = new HashMap();
			map.put("line", dataList.get(i).getLine());
			map.put("cd", dataList.get(i).getCd());

			list.add(map);
		}
		;

		String str = "";
		Map listMap = new HashMap();
		listMap.put("dataList", list);

		ObjectMapper om = new ObjectMapper();
		str = om.defaultPrettyPrintingWriter().writeValueAsString(listMap);
		return str;
	}

	@Override
	public String getSLCR(ChartVo chartVo) throws Exception {
		SqlSession sql = getSqlSession();
		List<ChartVo> dataList = sql.selectList(namespace + "getSLCR", chartVo);

		List list = new ArrayList();
		for (int i = 0; i < dataList.size(); i++) {
			Map map = new HashMap();
			map.put("slCr", dataList.get(i).getSlCr());

			list.add(map);
		}
		;

		String str = "";
		Map listMap = new HashMap();
		listMap.put("dataList", list);

		ObjectMapper om = new ObjectMapper();
		str = om.defaultPrettyPrintingWriter().writeValueAsString(listMap);
		return str;
	}

	@Override
	public String getPrgInfo(ChartVo chartVo) throws Exception {
		SqlSession sql = getSqlSession();
		List<ChartVo> dataList = sql.selectList(namespace + "getPrgInfo", chartVo);

		List list = new ArrayList();
		for (int i = 0; i < dataList.size(); i++) {
			Map map = new HashMap();
			map.put("slAv", dataList.get(i).getSlAv());
			map.put("slMx", dataList.get(i).getSlMx());
			map.put("slMn", dataList.get(i).getSlMn());
			map.put("slCr", dataList.get(i).getSlCr());
			map.put("sec", dataList.get(i).getSec());

			list.add(map);
		}
		;

		String str = "";
		Map listMap = new HashMap();
		listMap.put("dataList", list);

		ObjectMapper om = new ObjectMapper();
		str = om.defaultPrettyPrintingWriter().writeValueAsString(listMap);
		return str;
	}

	@Override
	public ChartVo getCurrentProg(ChartVo chartVo) throws Exception {
		SqlSession sql = getSqlSession();
		chartVo = (ChartVo) sql.selectOne(namespace + "getCurrentProg", chartVo);

		return chartVo;
	}

	@Override
	public String getTemplate(ChartVo chartVo) throws Exception {
		SqlSession sql = getSqlSession();
		List<ChartVo> dataList = sql.selectList(namespace + "getTemplate", chartVo);

		List list = new ArrayList<ChartVo>();
		for (int i = 0; i < dataList.size(); i++) {
			Map map = new HashMap();
			map.put("id", dataList.get(i).getId());
			map.put("name", dataList.get(i).getName());
			map.put("x", dataList.get(i).getX());
			map.put("y", dataList.get(i).getY());
			map.put("width", dataList.get(i).getWidth());
			map.put("height", dataList.get(i).getHeight());
			map.put("viewBox", dataList.get(i).getViewBox());
			map.put("transform", dataList.get(i).getTransform());
			map.put("d", dataList.get(i).getD());
			map.put("status", dataList.get(i).getStatus());

			list.add(map);
		}
		;

		Map dataMap = new HashMap();
		dataMap.put("dataList", list);
		ObjectMapper om = new ObjectMapper();
		String str = "";
		str = om.defaultPrettyPrintingWriter().writeValueAsString(dataMap);
		return str;
	}

	@Override
	public String getAdtList(ChartVo chartVo) throws Exception {
		SqlSession sql = getSqlSession();
		List<ChartVo> dataList = sql.selectList(namespace + "getAdtList", chartVo);

		List list = new ArrayList<ChartVo>();
		for (int i = 0; i < dataList.size(); i++) {
			Map map = new HashMap();
			map.put("id", dataList.get(i).getId());
			map.put("name", dataList.get(i).getName());
			map.put("x", dataList.get(i).getX());
			map.put("y", dataList.get(i).getY());
			map.put("width", dataList.get(i).getWidth());
			map.put("height", dataList.get(i).getHeight());
			map.put("viewBox", dataList.get(i).getViewBox());
			map.put("transform", dataList.get(i).getTransform());
			map.put("d", dataList.get(i).getD());
			map.put("status", dataList.get(i).getStatus());

			list.add(map);
		}
		;

		Map dataMap = new HashMap();
		dataMap.put("dataList", list);
		ObjectMapper om = new ObjectMapper();
		String str = "";
		str = om.defaultPrettyPrintingWriter().writeValueAsString(dataMap);
		return str;
	}

	@Override
	public String getLeadTime(ChartVo chartVo) throws Exception {
		SqlSession sql = getSqlSession();
		List<ChartVo> dataList = sql.selectList(namespace + "getLeadTime", chartVo);

		List list = new ArrayList<ChartVo>();
		for (int i = 0; i < dataList.size(); i++) {
			Map map = new HashMap();
			map.put("prdPrc", URLEncoder.encode(dataList.get(i).getPrdPrc(), "utf-8"));
			map.put("inCycleTime", dataList.get(i).getInCycleTime());
			map.put("cuttingTime", dataList.get(i).getCuttingTime());
			map.put("noConnectionTime", dataList.get(i).getNoConnectionTime());
			map.put("waitTime", dataList.get(i).getWaitTime());

			list.add(map);
		}
		;

		Map dataMap = new HashMap();
		dataMap.put("dataList", list);
		ObjectMapper om = new ObjectMapper();
		String str = "";
		str = om.defaultPrettyPrintingWriter().writeValueAsString(dataMap);
		return str;
	}

	@Override
	public String getFaultyList(ChartVo chartVo) throws Exception {
		SqlSession sql = getSqlSession();
		List<ChartVo> dataList = sql.selectList(namespace + "getFaultyList", chartVo);

		List list = new ArrayList<ChartVo>();
		for (int i = 0; i < dataList.size(); i++) {
			Map map = new HashMap();
			map.put("sDate", dataList.get(i).getSDate());
			map.put("prdNo", URLEncoder.encode(dataList.get(i).getPrdNo(), "utf-8"));
			map.put("prdCnt", dataList.get(i).getPrdCnt());
			map.put("prd", URLEncoder.encode(dataList.get(i).getPrd(), "utf-8"));
			// map.put("opPrdCnt",
			// URLEncoder.encode(dataList.get(i).getOpPrdCnt(),"utf-8"));
			// map.put("cstmPrdCnt",
			// URLEncoder.encode(dataList.get(i).getCstmPrdCnt(),"utf-8"));
			map.put("processFaultyCnt", dataList.get(i).getProcessFaultyCnt());
			map.put("customerFaultyCnt", dataList.get(i).getCustomerFaultyCnt());
			// map.put("processFaultyRate", dataList.get(i).getProcessFaultyRate());
			// map.put("customerFaultyRate", dataList.get(i).getCustomerFaultyRate());

			list.add(map);
		}
		;

		Map dataMap = new HashMap();
		dataMap.put("dataList", list);
		ObjectMapper om = new ObjectMapper();
		String str = "";
		str = om.defaultPrettyPrintingWriter().writeValueAsString(dataMap);
		return str;
	}

	@Override
	public String getPrdNoList(ChartVo chartVo) throws Exception {
		SqlSession sql = getSqlSession();
		List<ChartVo> dataList = sql.selectList(namespace + "getPrdNoList", chartVo);

		List list = new ArrayList<ChartVo>();
		for (int i = 0; i < dataList.size(); i++) {
			Map map = new HashMap();
			map.put("prdNo", URLEncoder.encode(dataList.get(i).getPrdNo(), "utf-8"));

			list.add(map);
		}
		;

		Map dataMap = new HashMap();
		dataMap.put("dataList", list);
		ObjectMapper om = new ObjectMapper();
		String str = "";
		str = om.defaultPrettyPrintingWriter().writeValueAsString(dataMap);
		return str;
	}

	@Override
	public String getCheckTyList(ChartVo chartVo) throws Exception {
		SqlSession sql = getSqlSession();
		List<ChartVo> dataList = sql.selectList(namespace + "getCheckTyList", chartVo);

		List list = new ArrayList<ChartVo>();
		for (int i = 0; i < dataList.size(); i++) {
			Map map = new HashMap();
			map.put("id", dataList.get(i).getId());
			map.put("name", URLEncoder.encode(dataList.get(i).getName(), "utf-8"));

			list.add(map);
		}
		;

		Map dataMap = new HashMap();
		dataMap.put("dataList", list);
		ObjectMapper om = new ObjectMapper();
		String str = "";
		str = om.defaultPrettyPrintingWriter().writeValueAsString(dataMap);
		return str;
	}

	@Override
	public String getDevieList(ChartVo chartVo) throws Exception {
		SqlSession sql = getSqlSession();
		List<ChartVo> dataList = sql.selectList(namespace + "getDevieList", chartVo);

		List list = new ArrayList<ChartVo>();
		for (int i = 0; i < dataList.size(); i++) {
			Map map = new HashMap();
			map.put("id", dataList.get(i).getId());
			map.put("name", URLEncoder.encode(dataList.get(i).getName(), "utf-8"));

			list.add(map);
		}
		;

		Map dataMap = new HashMap();
		dataMap.put("dataList", list);
		ObjectMapper om = new ObjectMapper();
		String str = "";
		str = om.defaultPrettyPrintingWriter().writeValueAsString(dataMap);
		return str;
	}

	@Override
	public String addFaulty(ChartVo chartVo) throws Exception {
		SqlSession sql = getSqlSession();
		String str = "";

		sql.insert(namespace + "addFaulty", chartVo);
		str = chartVo.getId();

		return str;
	}

	@Override
	public String updateCd(ChartVo chartVo) throws Exception {
		SqlSession sql = getSqlSession();
		String str = "";
		try {
			sql.update(namespace + "updateCd", chartVo);
			str = "success";
		} catch (Exception e) {
			e.printStackTrace();
			str = "fail";
		}
		;
		return str;
	}

	@Override
	public String getFaultList(ChartVo chartVo) throws Exception {
		SqlSession sql = getSqlSession();
		List<ChartVo> dataList = sql.selectList(namespace + "getFaultList", chartVo);

		List list = new ArrayList<ChartVo>();
		for (int i = 0; i < dataList.size(); i++) {
			Map map = new HashMap();
			map.put("id", dataList.get(i).getId());
			map.put("cnt", dataList.get(i).getCnt());
			map.put("chkTy", URLEncoder.encode(dataList.get(i).getChkTy(), "utf-8"));
			map.put("prdNo", URLEncoder.encode(dataList.get(i).getPrdNo(), "utf-8"));
			map.put("prdPrc", URLEncoder.encode(dataList.get(i).getPrdPrc(), "utf-8"));
			map.put("dvcName", URLEncoder.encode(dataList.get(i).getDvcName(), "utf-8"));
			map.put("sDate", dataList.get(i).getSDate());
			map.put("checker", URLEncoder.encode(dataList.get(i).getChecker(), "utf-8"));
			map.put("part", URLEncoder.encode(dataList.get(i).getPart(), "utf-8"));
			map.put("situTy", URLEncoder.encode(dataList.get(i).getSituTy(), "utf-8"));
			map.put("situ", URLEncoder.encode(dataList.get(i).getSitu(), "utf-8"));
			map.put("cause", URLEncoder.encode(dataList.get(i).getCause(), "utf-8"));
			map.put("gchTy", URLEncoder.encode(dataList.get(i).getGchTy(), "utf-8"));
			map.put("gch", URLEncoder.encode(dataList.get(i).getGch(), "utf-8"));
			map.put("action", URLEncoder.encode(dataList.get(i).getAction(), "utf-8"));

			list.add(map);
		}
		;

		Map dataMap = new HashMap();
		dataMap.put("dataList", list);
		ObjectMapper om = new ObjectMapper();
		String str = "";
		str = om.defaultPrettyPrintingWriter().writeValueAsString(dataMap);
		return str;
	}

	@Override
	public String getChecker(ChartVo chartVo) throws Exception {
		SqlSession sql = getSqlSession();
		String str = URLEncoder.encode((String) sql.selectOne(namespace + "getChecker", chartVo), "utf-8");
		return str;
	}

	@Override
	public String getCnt(ChartVo chartVo) throws Exception {
		SqlSession sql = getSqlSession();
		String str = (String) sql.selectOne(namespace + "getCnt", chartVo);
		return str;
	}

	@Override
	public String okDel(ChartVo chartVo) throws Exception {
		SqlSession sql = getSqlSession();
		sql.delete(namespace + "okDel", chartVo);
		return null;
	}

	@Override
	public String addMachine(ChartVo chartVo) throws Exception {
		SqlSession sql = getSqlSession();
		String str = "";
		try {
			sql.insert(namespace + "addMachine", chartVo);
			str = "success";
		} catch (Exception e) {
			e.printStackTrace();
			str = e.toString();
		}
		return str;
	}

	@Override
	public String getMachine(ChartVo chartVo) throws Exception {
		SqlSession sql = getSqlSession();
		List<ChartVo> dataList = sql.selectList(namespace + "getMachine", chartVo);

		List list = new ArrayList<ChartVo>();
		for (int i = 0; i < dataList.size(); i++) {
			Map map = new HashMap();
			map.put("id", dataList.get(i).getId());
			map.put("name", URLEncoder.encode(dataList.get(i).getName(), "utf-8"));
			map.put("viewBox", dataList.get(i).getViewBox());
			map.put("transform", dataList.get(i).getTransform());
			map.put("d", dataList.get(i).getD());
			map.put("x", dataList.get(i).getX());
			map.put("y", dataList.get(i).getY());
			map.put("w", dataList.get(i).getWidth());
			map.put("h", dataList.get(i).getHeight());

			list.add(map);
		}
		;

		Map dataMap = new HashMap();
		dataMap.put("dataList", list);
		ObjectMapper om = new ObjectMapper();
		String str = "";
		str = om.defaultPrettyPrintingWriter().writeValueAsString(dataMap);
		return str;
	}

	@Override
	public void setMachinePos_edit(ChartVo chartVo) throws Exception {
		SqlSession sql = getSqlSession();

		sql.update(namespace + "setMachinePos_edit", chartVo);
	}

	@Override
	public String delMachine(ChartVo chartVo) throws Exception {
		SqlSession sql = getSqlSession();
		String str = "";
		try {
			sql.delete(namespace + "delMachine", chartVo);
			str = "success";
		} catch (Exception e) {
			e.printStackTrace();
			str = "fail";
		}
		return str;
	}

	@Override
	public String getBGImg(ChartVo chartVo) throws Exception {
		SqlSession sql = getSqlSession();
		String str = URLEncoder.encode((String) sql.selectOne(namespace + "getBGImg", chartVo), "utf-8");

		return str;
	}

	@Override
	public String updateBGImg(ChartVo chartVo) throws Exception {
		SqlSession sql = getSqlSession();
		String str = "";
		try {
			sql.update(namespace + "updateBGImg", chartVo);
			str = "success";
		} catch (Exception e) {
			str = "fail";
		}
		return str;
	}

	@Override
	public String getPrdNo(ChartVo chartVo) throws Exception {
		SqlSession sql = getSqlSession();
		List<ChartVo> dataList = sql.selectList(namespace + "getPrdNo", chartVo);

		List list = new ArrayList<ChartVo>();
		for (int i = 0; i < dataList.size(); i++) {
			Map map = new HashMap();
			map.put("prdNo", dataList.get(i).getPrdNo());

			list.add(map);
		}
		;

		Map dataMap = new HashMap();
		dataMap.put("dataList", list);
		ObjectMapper om = new ObjectMapper();
		String str = "";
		str = om.defaultPrettyPrintingWriter().writeValueAsString(dataMap);
		return str;
	}

	@Override
	public String getMatInfo(ChartVo chartVo) throws Exception {
		SqlSession sql = getSqlSession();
		List<ChartVo> dataList = sql.selectList(namespace + "getMatInfo", chartVo);

		List list = new ArrayList<ChartVo>();
		for (int i = 0; i < dataList.size(); i++) {
			Map map = new HashMap();
			map.put("prdNo", URLEncoder.encode(dataList.get(i).getPrdNo(), "utf-8"));

			list.add(map);
		}
		;

		Map dataMap = new HashMap();
		dataMap.put("dataList", list);
		ObjectMapper om = new ObjectMapper();
		String str = "";
		str = om.defaultPrettyPrintingWriter().writeValueAsString(dataMap);
		return str;
	}

	@Override
	public String getRcvInfo(ChartVo chartVo) throws Exception {
		SqlSession sql = getSqlSession();
		List<ChartVo> dataList = sql.selectList(namespace + "getRcvInfo", chartVo);

		List list = new ArrayList<ChartVo>();
		for (int i = 0; i < dataList.size(); i++) {
			Map map = new HashMap();
			map.put("id", dataList.get(i).getId());
			map.put("comName", URLEncoder.encode(dataList.get(i).getComName(), "utf-8"));
			map.put("prdNo", URLEncoder.encode(dataList.get(i).getPrdNo(), "utf-8"));
			map.put("spec", dataList.get(i).getSpec());
			map.put("lotNo", dataList.get(i).getLotNo());
			map.put("lotCnt", dataList.get(i).getLotCnt());
			map.put("smplCnt", dataList.get(i).getSmplCnt());
			map.put("notiCnt", dataList.get(i).getNotiCnt());
			map.put("rcvCnt", dataList.get(i).getRcvCnt());
			map.put("inputDate", dataList.get(i).getInputDate());

			list.add(map);
		}
		;

		Map dataMap = new HashMap();
		dataMap.put("dataList", list);
		ObjectMapper om = new ObjectMapper();
		String str = "";
		str = om.defaultPrettyPrintingWriter().writeValueAsString(dataMap);
		return str;
	}

	@Override
	public ChartVo getNewMatInfo(ChartVo chartVo) throws Exception {
		SqlSession sql = getSqlSession();
		chartVo = (ChartVo) sql.selectOne(namespace + "getNewMatInfo", chartVo);
		return chartVo;
	}

	@Override
	public String addStock(String val) throws Exception {
		SqlSession sql = getSqlSession();

		JSONParser jsonParser = new JSONParser();
		JSONObject jsonObj = (JSONObject) jsonParser.parse(val);
		JSONArray jsonArray = (JSONArray) jsonObj.get("val");

		List<ChartVo> list = new ArrayList<ChartVo>();
		for (int i = 0; i < jsonArray.size(); i++) {
			JSONObject tempObj = (JSONObject) jsonArray.get(i);

			ChartVo chartVo = new ChartVo();
			chartVo.setId(tempObj.get("id").toString());
			chartVo.setPrdNo(tempObj.get("prdNo").toString());
			chartVo.setDate(tempObj.get("date").toString());
			chartVo.setLotNo(tempObj.get("lotNo").toString());
			chartVo.setVndNo(tempObj.get("vndNo").toString());
			chartVo.setLotCnt(Integer.parseInt(tempObj.get("lotCnt").toString()));
			chartVo.setSmplCnt(Integer.parseInt(tempObj.get("smplCnt").toString()));
			chartVo.setNotiCnt(Integer.parseInt(tempObj.get("notiCnt").toString()));
			chartVo.setRcvCnt(Integer.parseInt(tempObj.get("rcvCnt").toString()));

			list.add(chartVo);

			int checkStock = (int) sql.selectOne(namespace + "checkStock", chartVo);
			if (checkStock == 0) {
				sql.insert(namespace + "insertStock", chartVo);
			} else {
				sql.update(namespace + "modifyStock", chartVo);
			}
		}

		String str = "";

		try {
			sql.insert(namespace + "addStock", list);

			str = "success";
		} catch (Exception e) {
			e.printStackTrace();
			str = "fail";
		}
		return str;
	}

	@Override
	public String okDelStock(ChartVo chartVo) throws Exception {
		SqlSession sql = getSqlSession();
		String str = "";
		try {
			chartVo = (ChartVo) sql.selectOne(namespace + "getStockPrdNo", chartVo);
			sql.delete(namespace + "okDelStock", chartVo);

			sql.update(namespace + "modifyStock", chartVo);
			str = "success";
		} catch (Exception e) {
			str = "fail";
		}
		return str;
	}

	@Override
	public ChartVo getStockInfo(ChartVo chartVo) throws Exception {
		SqlSession sql = getSqlSession();
		chartVo = (ChartVo) sql.selectOne(namespace + "getStockInfo", chartVo);
		return chartVo;
	}

	@Override
	public String updateStock(ChartVo chartVo) throws Exception {
		SqlSession sql = getSqlSession();
		String str = "";
		try {
			sql.delete(namespace + "updateStock", chartVo);
			// if(chartVo.getUpdatedRcvCnt()!=0){
			// chartVo.setRcvCnt(chartVo.getUpdatedRcvCnt());
			// }
			//
			// chartVo.setNotiCnt(chartVo.getUpdatedNotiCnt());

			chartVo.setRcvCnt(chartVo.getUpdatedRcvCnt());
			chartVo.setNotiCnt(chartVo.getUpdatedNotiCnt());

			sql.update(namespace + "modifyStock", chartVo);
			str = "success";
		} catch (Exception e) {
			str = "fail";
		}
		return str;
	}

	@Override
	public String getNonOpInfo(ChartVo chartVo) throws Exception {
		SqlSession sql = getSqlSession();
		List<ChartVo> dataList = sql.selectList(namespace + "getNonOpInfo", chartVo);

		List list = new ArrayList<ChartVo>();
		for (int i = 0; i < dataList.size(); i++) {
			Map map = new HashMap();
			map.put("id", dataList.get(i).getId());
			map.put("prdNo", dataList.get(i).getPrdNo());
			map.put("oprNm", URLEncoder.encode(dataList.get(i).getOprNm(), "utf-8"));
			map.put("name", URLEncoder.encode(dataList.get(i).getName(), "utf-8"));
			map.put("date", dataList.get(i).getDate());
			map.put("ty", dataList.get(i).getTy());
			map.put("cuttingTime", dataList.get(i).getCuttingTime());
			map.put("inCycleTime", dataList.get(i).getInCycleTime());
			map.put("waitTime", dataList.get(i).getWaitTime());
			map.put("alarmTime", dataList.get(i).getAlarmTime());
			map.put("noConnectionTime", dataList.get(i).getNoConnectionTime());
			map.put("opRatio", dataList.get(i).getOpRatio());
			map.put("cuttingTimeRatio", dataList.get(i).getCuttingTimeRatio());
			map.put("worker", URLEncoder.encode(dataList.get(i).getWorker(), "utf-8"));
			map.put("totalTime", dataList.get(i).getTotalTime());
			map.put("nonOpTime", dataList.get(i).getNonOpTime());
			map.put("workerOpTime", dataList.get(i).getWorkerOpTime());
			map.put("workerOpRatio", dataList.get(i).getWorkerOpRatio());
			map.put("nonOpTy", dataList.get(i).getNonOpTy());
			map.put("nonOpTyTxt", URLEncoder.encode(dataList.get(i).getNonOpTyTxt(), "utf-8"));
			map.put("startTime", dataList.get(i).getStartTime());
			map.put("endTime", dataList.get(i).getEndTime());
			map.put("time", dataList.get(i).getTime());

			list.add(map);
		}
		;

		Map dataMap = new HashMap();
		dataMap.put("dataList", list);
		ObjectMapper om = new ObjectMapper();
		String str = "";
		str = om.defaultPrettyPrintingWriter().writeValueAsString(dataMap);
		return str;
	}

	@Override
	public ChartVo getNonOpData(ChartVo chartVo) throws Exception {
		SqlSession sql = getSqlSession();
		chartVo = (ChartVo) sql.selectOne(namespace + "getNonOpData", chartVo);
		return chartVo;
	}

	@Override
	public String getOprNm(ChartVo chartVo) throws Exception {
		SqlSession sql = getSqlSession();
		List<ChartVo> dataList = sql.selectList(namespace + "getOprNm", chartVo);

		List list = new ArrayList<ChartVo>();
		for (int i = 0; i < dataList.size(); i++) {
			Map map = new HashMap();
			map.put("oprNm", URLEncoder.encode(dataList.get(i).getOprNm(), "utf-8"));

			list.add(map);
		}
		;

		Map dataMap = new HashMap();
		dataMap.put("dataList", list);
		ObjectMapper om = new ObjectMapper();
		String str = "";
		str = om.defaultPrettyPrintingWriter().writeValueAsString(dataMap);
		return str;
	}

	@Override
	public String getNonOpTy(ChartVo chartVo) throws Exception {
		SqlSession sql = getSqlSession();
		List<ChartVo> dataList = sql.selectList(namespace + "getNonOpTy", chartVo);

		List list = new ArrayList<ChartVo>();
		for (int i = 0; i < dataList.size(); i++) {
			Map map = new HashMap();
			map.put("id", dataList.get(i).getId());
			map.put("nonOpTy", URLEncoder.encode(dataList.get(i).getNonOpTy(), "utf-8"));

			list.add(map);
		}
		;

		Map dataMap = new HashMap();
		dataMap.put("dataList", list);
		ObjectMapper om = new ObjectMapper();
		String str = "";
		str = om.defaultPrettyPrintingWriter().writeValueAsString(dataMap);
		return str;
	}

	@Override
	public String updateNonOp(ChartVo chartVo) throws Exception {
		SqlSession sql = getSqlSession();
		String str = "";
		try {

			sql.update(namespace + "updateNonOp", chartVo);
			str = "success";
		} catch (Exception e) {
			str = "fail";
		}
		return str;
	}

	@Override
	public String okDelNonOp(ChartVo chartVo) throws Exception {
		SqlSession sql = getSqlSession();
		String str = "";
		try {

			sql.update(namespace + "okDelNonOp", chartVo);
			str = "success";
		} catch (Exception e) {
			str = "fail";
		}
		return str;
	}

	@Override
	public String addNonOp(ChartVo chartVo) throws Exception {
		SqlSession sql = getSqlSession();
		String str = "";
		try {

			sql.insert(namespace + "addNonOp", chartVo);
			str = "success";
		} catch (Exception e) {
			str = "fail";
		}
		return str;
	}

	@Override
	public String getDvcListByPrdNo(ChartVo chartVo) throws Exception {
		SqlSession sql = getSqlSession();
		List<ChartVo> dataList = sql.selectList(namespace + "getDvcListByPrdNo", chartVo);

		List list = new ArrayList<ChartVo>();
		for (int i = 0; i < dataList.size(); i++) {
			Map map = new HashMap();
			map.put("dvcId", dataList.get(i).getDvcId());
			map.put("name", URLEncoder.encode(dataList.get(i).getName(), "utf-8"));

			list.add(map);
		}
		;

		Map dataMap = new HashMap();
		dataMap.put("dataList", list);
		ObjectMapper om = new ObjectMapper();
		String str = "";
		str = om.defaultPrettyPrintingWriter().writeValueAsString(dataMap);
		return str;
	}

	@Override
	public String getCheckType(ChartVo chartVo) throws Exception {
		SqlSession sql = getSqlSession();
		List<ChartVo> dataList = sql.selectList(namespace + "getCheckType", chartVo);

		List list = new ArrayList<ChartVo>();
		for (int i = 0; i < dataList.size(); i++) {
			Map map = new HashMap();
			map.put("id", dataList.get(i).getId());
			map.put("codeName", URLEncoder.encode(dataList.get(i).getCodeName(), "utf-8"));

			list.add(map);
		}
		;

		Map dataMap = new HashMap();
		dataMap.put("dataList", list);
		ObjectMapper om = new ObjectMapper();
		String str = "";
		str = om.defaultPrettyPrintingWriter().writeValueAsString(dataMap);
		return str;
	}

	@Override
	public String getLotNoByPrdNo(ChartVo chartVo) throws Exception {
		SqlSession sql = getSqlSession();
		List<ChartVo> dataList = sql.selectList(namespace + "getLotNoByPrdNo", chartVo);

		List list = new ArrayList<ChartVo>();
		for (int i = 0; i < dataList.size(); i++) {
			Map map = new HashMap();
			map.put("id", dataList.get(i).getId());
			map.put("lotNo", URLEncoder.encode(dataList.get(i).getLotNo(), "utf-8"));

			list.add(map);
		}
		;

		Map dataMap = new HashMap();
		dataMap.put("dataList", list);
		ObjectMapper om = new ObjectMapper();
		String str = "";
		str = om.defaultPrettyPrintingWriter().writeValueAsString(dataMap);
		return str;
	}

	@Override
	public String getToolInfo(ChartVo chartVo) throws Exception {
		SqlSession sql = getSqlSession();
		List<ChartVo> dataList = sql.selectList(namespace + "getToolInfo", chartVo);

		List list = new ArrayList<ChartVo>();
		for (int i = 0; i < dataList.size(); i++) {
			Map map = new HashMap();
			map.put("portNo", dataList.get(i).getPortNo());
			map.put("prdCnt", dataList.get(i).getPrdCnt());
			map.put("runTime", dataList.get(i).getRunTime());
			map.put("limitPrdCnt", dataList.get(i).getLimitPrdCnt());
			map.put("limitRunTime", dataList.get(i).getLimitRunTime());
			map.put("offsetD", dataList.get(i).getOffsetD());
			map.put("offsetH", dataList.get(i).getOffsetH());

			map.put("programName", dataList.get(i).getProgramName());
			map.put("mode", dataList.get(i).getMode());
			map.put("spdLoad", dataList.get(i).getSpdLoad());
			map.put("feedOverride", dataList.get(i).getFeedOverride());

			map.put("sec", dataList.get(i).getSec());
			map.put("avrCycleTimeSec", dataList.get(i).getAvrCycleTimeSec());
			map.put("prdNo", dataList.get(i).getPrdNo());

			list.add(map);

		}
		;

		Map dataMap = new HashMap();
		dataMap.put("dataList", list);
		ObjectMapper om = new ObjectMapper();
		String str = "";
		str = om.defaultPrettyPrintingWriter().writeValueAsString(dataMap);
		return str;
	}

	@Override
	public String getLotInfo(ChartVo chartVo) throws Exception {
		SqlSession sql = getSqlSession();
		List<ChartVo> dataList = sql.selectList(namespace + "getLotInfo", chartVo);

		List list = new ArrayList<ChartVo>();
		for (int i = 0; i < dataList.size(); i++) {
			Map map = new HashMap();

			map.put("id", dataList.get(i).getId());
			map.put("lotNo", dataList.get(i).getLotNo());
			map.put("prdNo", URLEncoder.encode(dataList.get(i).getPrdNo(), "utf-8"));
			map.put("spec", dataList.get(i).getSpec());
			map.put("rcvCnt", dataList.get(i).getRcvCnt());
			map.put("sendCnt", dataList.get(i).getSendCnt());
			map.put("stockCnt", dataList.get(i).getStockCnt());

			list.add(map);
		}
		;

		Map dataMap = new HashMap();
		dataMap.put("dataList", list);
		ObjectMapper om = new ObjectMapper();
		String str = "";
		str = om.defaultPrettyPrintingWriter().writeValueAsString(dataMap);
		return str;
	}

	@Override
	public String saveUpdatedStock(String val) throws Exception {
		SqlSession sql = getSqlSession();

		JSONParser jsonParser = new JSONParser();
		JSONObject jsonObj = (JSONObject) jsonParser.parse(val);
		JSONArray jsonArray = (JSONArray) jsonObj.get("val");

		List<ChartVo> list = new ArrayList<ChartVo>();
		for (int i = 0; i < jsonArray.size(); i++) {
			JSONObject tempObj = (JSONObject) jsonArray.get(i);

			ChartVo chartVo = new ChartVo();
			chartVo.setId(tempObj.get("id").toString());
			chartVo.setPrdNo(tempObj.get("prdNo").toString());
			chartVo.setSendCnt(Integer.parseInt(tempObj.get("sendCnt").toString()));
			chartVo.setPreSendCnt(Integer.parseInt(tempObj.get("preSendCnt").toString()));
			chartVo.setDate(tempObj.get("date").toString());
			chartVo.setLotNo(tempObj.get("lotNo").toString());
			chartVo.setOprNm(tempObj.get("oprNm").toString());

			// �냼�옱�옱怨� -
			sql.update(namespace + "updateMstStock", chartVo);
			list.add(chartVo);

			int exist = (Integer) sql.selectOne(namespace + "chkStock", chartVo);

			// 1李� 媛�怨�+
			if (exist > 0) {
				sql.update(namespace + "updateComplStock", chartVo);
			} else {
				sql.insert(namespace + "addCmplStock", chartVo);
			}

			// 異쒗븯 �엳�뒪�넗由�
			int shipExist = (Integer) sql.selectOne(namespace + "chkShip", chartVo);
			if (shipExist > 0) {
				sql.update(namespace + "updateShipCnt", chartVo);
			} else {
				sql.insert(namespace + "addShipCnt", chartVo);
			}
		}

		Map<String, Object> map = new HashMap<String, Object>();
		map.put("list", list);

		String str = "";

		try {
			// 遺덉텧 �엳�뒪�넗由�
			sql.insert(namespace + "addReleaseStock", list);

			str = "success";
		} catch (Exception e) {
			str = "fail";
		}

		return str;
	}

	@Override
	public String getOprNmList(ChartVo chartVo) throws Exception {
		SqlSession sql = getSqlSession();
		List<ChartVo> dataList = sql.selectList(namespace + "getOprNmList", chartVo);

		List list = new ArrayList<ChartVo>();
		for (int i = 0; i < dataList.size(); i++) {
			Map map = new HashMap();

			map.put("val", dataList.get(i).getVal());

			list.add(map);
		}
		;

		Map dataMap = new HashMap();
		dataMap.put("dataList", list);
		ObjectMapper om = new ObjectMapper();
		String str = "";
		str = om.defaultPrettyPrintingWriter().writeValueAsString(dataMap);

		return str;
	}

	@Override
	public ChartVo getSpdLoadInfo(ChartVo chartVo) throws Exception {
		SqlSession sql = getSqlSession();
		chartVo = (ChartVo) sql.selectOne(namespace + "getSpdLoadInfo", chartVo);
		if (chartVo == null) {
			chartVo.setZTo50("0");
			chartVo.setFTo100("0");
			chartVo.setOver100("0");
		}
		return chartVo;
	}

	@Override
	public String setSpdLoad(ChartVo chartVo) throws Exception {
		SqlSession sql = getSqlSession();
		String str = "";
		try {
			sql.delete(namespace + "delSpdLoadSet", chartVo);
			sql.insert(namespace + "addSpdLoadSet", chartVo);
			str = "success";
		} catch (Exception e) {
			e.printStackTrace();
			str = "fail";
		}

		return str;
	}

	@Override
	public String getProgInfo(ChartVo chartVo) throws Exception {
		SqlSession sql = getSqlSession();
		List<ChartVo> dataList = sql.selectList(namespace + "getProgInfo", chartVo);

		List list = new ArrayList<ChartVo>();
		for (int i = 0; i < dataList.size(); i++) {
			Map map = new HashMap();

			map.put("prgCd", dataList.get(i).getPrgCd());
			map.put("ratio", dataList.get(i).getRatio());

			list.add(map);
		}
		;

		Map dataMap = new HashMap();
		dataMap.put("dataList", list);
		ObjectMapper om = new ObjectMapper();
		String str = "";
		str = om.defaultPrettyPrintingWriter().writeValueAsString(dataMap);

		return str;
	}

	@Override
	public String setPrgSet(String val) throws Exception {
		SqlSession sql = getSqlSession();

		JSONParser jsonParser = new JSONParser();
		JSONObject jsonObj = (JSONObject) jsonParser.parse(val);
		JSONArray jsonArray = (JSONArray) jsonObj.get("val");

		List<ChartVo> list = new ArrayList<ChartVo>();
		for (int i = 0; i < jsonArray.size(); i++) {
			JSONObject tempObj = (JSONObject) jsonArray.get(i);

			ChartVo chartVo = new ChartVo();
			chartVo.setPrgCd(tempObj.get("prgCd").toString());
			chartVo.setRatio(tempObj.get("ratio").toString());
			chartVo.setDvcId(tempObj.get("dvcId").toString());

			list.add(chartVo);
		}

		Map<String, Object> map = new HashMap<String, Object>();
		map.put("list", list);

		String str = "";

		try {
			sql.delete(namespace + "delPrgSet", list);
			sql.delete(namespace + "addPrgSet", list);

			str = "success";
		} catch (Exception e) {
			str = "fail";
		}

		return str;
	}

	@Override
	public String setCalcTy(ChartVo chartVo) throws Exception {
		SqlSession sql = getSqlSession();
		String str = "";
		try {
			sql.delete(namespace + "delCalcTy", chartVo);
			sql.delete(namespace + "addCalcTy", chartVo);

			str = "success";
		} catch (Exception e) {
			str = "fail";
			e.printStackTrace();
		}
		return str;
	}

	@Override
	public ChartVo getCalcTy(ChartVo chartVo) throws Exception {
		SqlSession sql = getSqlSession();
		chartVo = (ChartVo) sql.selectOne(namespace + "getCalcTy", chartVo);
		if (chartVo == null) {
			chartVo.setIsSpd("0");
			chartVo.setCalcTy(0);
		}
		return chartVo;
	}

	@Override
	public String saveRow(String val) throws Exception {
		SqlSession sql = getSqlSession();

		JSONParser jsonParser = new JSONParser();
		JSONObject jsonObj = (JSONObject) jsonParser.parse(val);
		JSONArray jsonArray = (JSONArray) jsonObj.get("val");

		List<ChartVo> list = new ArrayList<ChartVo>();
		for (int i = 0; i < jsonArray.size(); i++) {
			JSONObject tempObj = (JSONObject) jsonArray.get(i);

			ChartVo chartVo = new ChartVo();
			chartVo.setChkTy(tempObj.get("chkTy").toString());
			chartVo.setPrdNo(tempObj.get("prdNo").toString());
			chartVo.setPrdPrc(tempObj.get("prdPrc").toString());
			chartVo.setDvcId(tempObj.get("dvcId").toString());
			chartVo.setDate(tempObj.get("date").toString());
			chartVo.setChecker(tempObj.get("checker").toString());
			chartVo.setPart(tempObj.get("part").toString());
			chartVo.setSituationTy(tempObj.get("situationTy").toString());
			chartVo.setSituation(tempObj.get("situation").toString());
			chartVo.setCause(tempObj.get("cause").toString());
			chartVo.setGchTy(tempObj.get("gchTY").toString());
			chartVo.setGch(tempObj.get("gch").toString());
			chartVo.setCnt(tempObj.get("cnt").toString());
			chartVo.setAction(tempObj.get("action").toString());

			list.add(chartVo);

		}

		String str = "";

		sql.delete(namespace + "delPrcDfct", list);
		sql.insert(namespace + "addPrdFaulty", list);

		try {
			// sql.delete(namespace + "delPrcDfct", list);
			// sql.insert(namespace + "addReleaseStock", list);

			str = "success";
		} catch (Exception e) {
			str = "fail";
		}

		return str;
	}

	@Override
	public String getJSGroupCode(ChartVo chartVo) throws Exception {
		SqlSession sql = getSqlSession();
		List<ChartVo> dataList = sql.selectList(namespace + "getJSGroupCode", chartVo);

		List list = new ArrayList<ChartVo>();
		for (int i = 0; i < dataList.size(); i++) {
			Map map = new HashMap();

			map.put("id", dataList.get(i).getId());
			map.put("name", URLEncoder.encode(dataList.get(i).getName(), "utf-8"));

			list.add(map);
		}
		;

		Map dataMap = new HashMap();
		dataMap.put("dataList", list);
		ObjectMapper om = new ObjectMapper();
		String str = "";
		str = om.defaultPrettyPrintingWriter().writeValueAsString(dataMap);

		return str;
	}

	@Override
	public String addCheckStandard(String val) throws Exception {
		SqlSession sql = getSqlSession();

		JSONParser jsonParser = new JSONParser();
		JSONObject jsonObj = (JSONObject) jsonParser.parse(val);
		JSONArray jsonArray = (JSONArray) jsonObj.get("val");

		List<ChartVo> list = new ArrayList<ChartVo>();
		for (int i = 0; i < jsonArray.size(); i++) {
			JSONObject tempObj = (JSONObject) jsonArray.get(i);

			ChartVo chartVo = new ChartVo();
			chartVo.setId(tempObj.get("id").toString());
			chartVo.setPrdNo(tempObj.get("prdNo").toString());
			chartVo.setDvcId(tempObj.get("dvcId").toString());
			chartVo.setChkTy(tempObj.get("chkTy").toString());
			chartVo.setAttrTy(tempObj.get("attrTy").toString());
			chartVo.setAttrCd(tempObj.get("attrCd").toString());
			chartVo.setAttrNameKo(tempObj.get("attrNameKo").toString());
			chartVo.setDp(tempObj.get("dp").toString());
			chartVo.setUnit(tempObj.get("unit").toString());
			chartVo.setSpec(tempObj.get("spec").toString());
			chartVo.setTarget(tempObj.get("target").toString());
			chartVo.setLow(tempObj.get("low").toString());
			chartVo.setUp(tempObj.get("up").toString());
			chartVo.setMeasurer(tempObj.get("measurer").toString());
			chartVo.setJsGroupCd(tempObj.get("jsGroupCd").toString());
			chartVo.setAttrNameOthr(tempObj.get("attrNameOthr").toString());

			if (chartVo.getId().equals("0")) {
				list.add(chartVo);
			} else {
				sql.update(namespace + "updateCheckStandard", chartVo);
			}
		}

		String str = "";

		try {
			if (list.size() > 0)
				sql.insert(namespace + "addChkStandard", list);
			str = "success";
		} catch (Exception e) {
			str = "fail";
		}

		return str;
	}

	@Override
	public String getChkStandardList(ChartVo chartVo) throws Exception {
		SqlSession sql = getSqlSession();
		List<ChartVo> dataList = sql.selectList(namespace + "getChkStandardList", chartVo);

		List list = new ArrayList<ChartVo>();
		for (int i = 0; i < dataList.size(); i++) {
			Map map = new HashMap();

			map.put("id", dataList.get(i).getId());
			map.put("prdNo", dataList.get(i).getPrdNo());
			map.put("name", URLEncoder.encode(dataList.get(i).getName(), "utf-8"));
			map.put("dvcId", dataList.get(i).getDvcId());
			map.put("chkTy", dataList.get(i).getChkTy());
			map.put("attrTy", dataList.get(i).getAttrTy());
			map.put("attrCd", dataList.get(i).getAttrCd());
			map.put("attrNameKo", URLEncoder.encode(dataList.get(i).getAttrNameKo(), "utf-8"));
			map.put("dp", dataList.get(i).getDp());
			map.put("unit", dataList.get(i).getUnit());
			map.put("spec", URLEncoder.encode(dataList.get(i).getSpec(), "utf-8"));
			map.put("target", dataList.get(i).getTarget());
			map.put("low", dataList.get(i).getLow());
			map.put("up", dataList.get(i).getUp());
			map.put("measurer", URLEncoder.encode(dataList.get(i).getMeasurer(), "utf-8"));
			map.put("jsGroupCd", dataList.get(i).getJsGroupCd());
			map.put("attrNameOthr", dataList.get(i).getAttrNameOthr());

			list.add(map);
		}
		;

		Map dataMap = new HashMap();
		dataMap.put("dataList", list);
		ObjectMapper om = new ObjectMapper();
		String str = "";
		str = om.defaultPrettyPrintingWriter().writeValueAsString(dataMap);

		return str;
	}

	@Override
	public String delChkStandard(ChartVo chartVo) throws Exception {
		SqlSession sql = getSqlSession();
		String str = "";
		try {
			sql.delete(namespace + "delChkStandard", chartVo);
			str = "success";
		} catch (Exception e) {
			str = "fail";
		}
		return str;
	}

	@Override
	public String getCheckList(ChartVo chartVo) throws Exception {
		SqlSession sql = getSqlSession();
		List<ChartVo> dataList = sql.selectList(namespace + "getCheckList", chartVo);

		List list = new ArrayList<ChartVo>();
		for (int i = 0; i < dataList.size(); i++) {
			Map map = new HashMap();

			map.put("id", dataList.get(i).getId());
			map.put("listId", dataList.get(i).getListId());
			map.put("prdNo", dataList.get(i).getPrdNo());
			map.put("name", URLEncoder.encode(dataList.get(i).getName(), "utf-8"));
			map.put("chkTy", URLEncoder.encode(dataList.get(i).getChkTy(), "utf-8"));
			map.put("attrTy", dataList.get(i).getAttrTy());
			map.put("attrNameKo", URLEncoder.encode(dataList.get(i).getAttrNameKo(), "utf-8"));
			map.put("spec", URLEncoder.encode(dataList.get(i).getSpec(), "utf-8"));
			map.put("target", dataList.get(i).getTarget());
			map.put("low", dataList.get(i).getLow());
			map.put("up", dataList.get(i).getUp());
			map.put("measurer", dataList.get(i).getMeasurer());
			map.put("workTy", dataList.get(i).getWorkTy());
			map.put("fResult", dataList.get(i).getFResult());
			map.put("result", URLEncoder.encode(dataList.get(i).getResult(), "utf-8"));
			map.put("result2", URLEncoder.encode(dataList.get(i).getResult2(), "utf-8"));
			map.put("result3", URLEncoder.encode(dataList.get(i).getResult3(), "utf-8"));
			map.put("result4", URLEncoder.encode(dataList.get(i).getResult4(), "utf-8"));
			map.put("date", dataList.get(i).getDate());
			map.put("checkCycle", dataList.get(i).getCheckCycle());

			list.add(map);
		}
		;

		Map dataMap = new HashMap();
		dataMap.put("dataList", list);
		ObjectMapper om = new ObjectMapper();
		String str = "";
		str = om.defaultPrettyPrintingWriter().writeValueAsString(dataMap);

		return str;
	}

	@Override
	public String addCheckStandardList(String val) throws Exception {
		SqlSession sql = getSqlSession();

		JSONParser jsonParser = new JSONParser();
		JSONObject jsonObj = (JSONObject) jsonParser.parse(val);
		JSONArray jsonArray = (JSONArray) jsonObj.get("val");

		List<ChartVo> list = new ArrayList<ChartVo>();
		for (int i = 0; i < jsonArray.size(); i++) {
			JSONObject tempObj = (JSONObject) jsonArray.get(i);

			ChartVo chartVo = new ChartVo();
			chartVo.setId(tempObj.get("chkId").toString());
			chartVo.setListId(tempObj.get("id").toString());
			chartVo.setResult(tempObj.get("result").toString());
			chartVo.setResult2(tempObj.get("result2").toString());
			chartVo.setResult3(tempObj.get("result3").toString());
			chartVo.setResult4(tempObj.get("result4").toString());
			chartVo.setFResult(tempObj.get("fResult").toString());
			chartVo.setTy(tempObj.get("workTy").toString());
			chartVo.setDate(tempObj.get("date").toString());
			chartVo.setChecker(tempObj.get("checker").toString());
			chartVo.setCheckCycle(tempObj.get("chkCycle").toString());

			if (chartVo.getListId().equals("0")) {
				list.add(chartVo);
			} else {
				sql.update(namespace + "updateCheckStandardList", chartVo);
			}
		}

		String str = "";

		try {
			if (list.size() > 0)
				sql.insert(namespace + "addChkStandardList", list);
			str = "success";
		} catch (Exception e) {
			str = "fail";
		}

		return str;
	}

	@Override
	public String getPrdData(ChartVo chartVo) throws Exception {
		SqlSession sql = getSqlSession();
		List<ChartVo> dataList = sql.selectList(namespace + "getPrdData", chartVo);

		List list = new ArrayList<ChartVo>();
		for (int i = 0; i < dataList.size(); i++) {
			Map map = new HashMap();

			map.put("prdNo", dataList.get(i).getPrdNo());
			map.put("oprNm", dataList.get(i).getOprNm());
			map.put("oprCd", dataList.get(i).getOprCd());
			map.put("name", URLEncoder.encode(dataList.get(i).getName(), "utf-8"));
			map.put("cylTmMi", dataList.get(i).getCycleTmM());
			map.put("cylTmSec", dataList.get(i).getCycleTmS());
			map.put("cvt", dataList.get(i).getCvt());
			map.put("uph", dataList.get(i).getUph());
			map.put("dvcId", dataList.get(i).getDvcId());
			map.put("worker", URLEncoder.encode(dataList.get(i).getWorker(), "utf-8"));
			map.put("workerCnt", URLEncoder.encode(dataList.get(i).getWorkerCnt(), "utf-8"));

			map.put("workTm", dataList.get(i).getWorkTm());
			map.put("capa", dataList.get(i).getCapa());
			map.put("target", dataList.get(i).getTarget());
			map.put("cnt", dataList.get(i).getCnt());
			map.put("faultCnt", dataList.get(i).getFaultCnt());
			map.put("okRatio", dataList.get(i).getOkRatio());
			map.put("nd", dataList.get(i).getNd());
			map.put("goalRatio", dataList.get(i).getGoalRatio());

			list.add(map);
		}
		;

		Map dataMap = new HashMap();
		dataMap.put("dataList", list);
		ObjectMapper om = new ObjectMapper();
		String str = "";
		str = om.defaultPrettyPrintingWriter().writeValueAsString(dataMap);

		return str;
	}

	@Override
	public String showPopup(ChartVo chartVo) throws Exception {
		SqlSession sql = getSqlSession();
		List<ChartVo> dataList = sql.selectList(namespace + "showPopup", chartVo);

		List list = new ArrayList<ChartVo>();
		for (int i = 0; i < dataList.size(); i++) {
			Map map = new HashMap();

			map.put("lotNo", dataList.get(i).getLotNo());
			map.put("lotCnt", dataList.get(i).getLotCnt());
			map.put("baseCnt", dataList.get(i).getBaseCnt());
			map.put("inputCnt", dataList.get(i).getInputCnt());
			map.put("prdCnt", dataList.get(i).getPrdCnt());
			map.put("outCnt", dataList.get(i).getOutCnt());
			map.put("oprNm", URLEncoder.encode(dataList.get(i).getOprNm(), "utf-8"));
			map.put("id", dataList.get(i).getId());

			list.add(map);
		}
		;

		Map dataMap = new HashMap();
		dataMap.put("dataList", list);
		ObjectMapper om = new ObjectMapper();
		String str = "";
		str = om.defaultPrettyPrintingWriter().writeValueAsString(dataMap);

		return str;
	}

	@Override
	public String getOprNo(ChartVo chartVo) throws Exception {
		SqlSession sql = getSqlSession();
		List<ChartVo> dataList = sql.selectList(namespace + "getOprNo", chartVo);

		List list = new ArrayList<ChartVo>();
		for (int i = 0; i < dataList.size(); i++) {
			Map map = new HashMap();

			map.put("name", URLEncoder.encode(dataList.get(i).getName(), "utf-8"));
			map.put("oprNo", dataList.get(i).getOprNo());

			list.add(map);
		}
		;

		Map dataMap = new HashMap();
		dataMap.put("dataList", list);
		ObjectMapper om = new ObjectMapper();
		String str = "";
		str = om.defaultPrettyPrintingWriter().writeValueAsString(dataMap);

		return str;
	}

	@Override
	public String addPrdData(String val) throws Exception {
		SqlSession sql = getSqlSession();

		JSONParser jsonParser = new JSONParser();
		JSONObject jsonObj = (JSONObject) jsonParser.parse(val);
		JSONArray jsonArray = (JSONArray) jsonObj.get("val");

		List<ChartVo> list = new ArrayList<ChartVo>();
		for (int i = 0; i < jsonArray.size(); i++) {
			JSONObject tempObj = (JSONObject) jsonArray.get(i);

			ChartVo chartVo = new ChartVo();
			chartVo.setId(tempObj.get("id").toString());
			chartVo.setPrdNo(tempObj.get("prdNo").toString());
			chartVo.setLotNo(tempObj.get("lotNo").toString());
			chartVo.setOprNo(tempObj.get("oprNo").toString());
			chartVo.setDvcId(tempObj.get("dvcId").toString());
			chartVo.setNd(Integer.parseInt(tempObj.get("nd").toString()));
			chartVo.setBaseCnt(tempObj.get("baseCnt").toString());
			chartVo.setInputCnt(tempObj.get("inputCnt").toString());
			chartVo.setPrdCnt(tempObj.get("prdCnt").toString());
			chartVo.setOutCnt(tempObj.get("outCnt").toString());
			chartVo.setDate(tempObj.get("date").toString());
			chartVo.setWorker(tempObj.get("worker").toString());
			chartVo.setFromOprNm(tempObj.get("fromOprNm").toString());

			// if(chartVo.getListId().equals("0")){
			// list.add(chartVo);
			// }else{
			// sql.update(namespace + "updateCheckStandardList", chartVo);
			// }

			if (chartVo.getOutCnt().equals("0")) {

			} else {
				list.add(chartVo);
			}
		}

		String str = "";

		try {
			if (list.size() > 0)
				sql.insert(namespace + "addPrdData", list);
			str = "success";
		} catch (Exception e) {
			str = "fail";
		}

		return str;
	}

	@Override
	public String getComName(ChartVo chartVo) throws Exception {
		SqlSession sql = getSqlSession();
		String str = URLEncoder.encode((String) sql.selectOne(namespace + "getComName", chartVo), "utf-8");
		return str;
	}

	@Override
	public String getLatestDate() throws Exception {
		SqlSession sql = getSqlSession();
		String str = (String) sql.selectOne(namespace + "getLatestDate");
		return str;
	}

	@Override
	public String getWorkerList(ChartVo chartVo) throws Exception {
		SqlSession sql = getSqlSession();
		List<ChartVo> dataList = sql.selectList(namespace + "getWorkerList", chartVo);

		List list = new ArrayList<ChartVo>();
		for (int i = 0; i < dataList.size(); i++) {
			Map map = new HashMap();

			map.put("id", dataList.get(i).getId());
			map.put("name", URLEncoder.encode(dataList.get(i).getName(), "utf-8"));

			list.add(map);
		}
		;

		Map dataMap = new HashMap();
		dataMap.put("dataList", list);
		ObjectMapper om = new ObjectMapper();
		String str = "";
		str = om.defaultPrettyPrintingWriter().writeValueAsString(dataMap);

		return str;
	}

	@Override
	public String getAllWorkerList(ChartVo chartVo) throws Exception {
		SqlSession sql = getSqlSession();
		List<ChartVo> dataList = sql.selectList(namespace + "getAllWorkerList", chartVo);

		List list = new ArrayList<ChartVo>();
		for (int i = 0; i < dataList.size(); i++) {
			Map map = new HashMap();

			map.put("id", dataList.get(i).getId());
			map.put("name", URLEncoder.encode(dataList.get(i).getName(), "utf-8"));
			map.put("part", URLEncoder.encode(dataList.get(i).getPart(), "utf-8"));
			map.put("email", dataList.get(i).getEmail());

			list.add(map);
		}
		;

		Map dataMap = new HashMap();
		dataMap.put("dataList", list);
		ObjectMapper om = new ObjectMapper();
		String str = "";
		str = om.defaultPrettyPrintingWriter().writeValueAsString(dataMap);

		return str;
	}

	@Override
	public String getNextWorkerId(ChartVo chartVo) throws Exception {
		SqlSession sql = getSqlSession();
		String rtn = (String) sql.selectOne(namespace + "getNextWorkerId", chartVo);
		return rtn;
	}
	
	@Override
	public String addWorker(ChartVo chartVo) throws Exception {
		SqlSession sql = getSqlSession();
		String str = "";
		try {

			int exist = (Integer) sql.selectOne(namespace + "chkWorker", chartVo);
			if (exist > 0) {
				sql.update(namespace + "updateWorker", chartVo);
			} else {
				sql.insert(namespace + "addWorker", chartVo);
			}
			str = "success";
		} catch (Exception e) {
			str = "fail";
		}
		return str;
	}

	@Override
	public ChartVo getWorkerInfo(ChartVo chartVo) throws Exception {
		SqlSession sql = getSqlSession();
		chartVo = (ChartVo) sql.selectOne(namespace + "getWorkerInfo", chartVo);
		return chartVo;
	}

	@Override
	public String getComList(ChartVo chartVo) throws Exception {
		SqlSession sql = getSqlSession();
		List<ChartVo> dataList = sql.selectList(namespace + "getComList", chartVo);

		List list = new ArrayList<ChartVo>();
		for (int i = 0; i < dataList.size(); i++) {
			Map map = new HashMap();

			map.put("id", dataList.get(i).getId());
			map.put("name", URLEncoder.encode(dataList.get(i).getName(), "utf-8"));

			list.add(map);
		}
		;

		Map dataMap = new HashMap();
		dataMap.put("dataList", list);
		ObjectMapper om = new ObjectMapper();
		String str = "";
		str = om.defaultPrettyPrintingWriter().writeValueAsString(dataMap);

		return str;
	}

	@Override
	public String getLotInfoHistory(ChartVo chartVo) throws Exception {
		SqlSession sql = getSqlSession();
		List<ChartVo> dataList = sql.selectList(namespace + "getLotInfoHistory", chartVo);

		List list = new ArrayList<ChartVo>();
		for (int i = 0; i < dataList.size(); i++) {
			Map map = new HashMap();

			map.put("lotNo", dataList.get(i).getLotNo());
			map.put("id", dataList.get(i).getId());
			map.put("prdNo", URLEncoder.encode(dataList.get(i).getPrdNo(), "utf-8"));
			map.put("oprNm", dataList.get(i).getOprNm());
			map.put("spec", dataList.get(i).getSpec());
			map.put("sendCnt", dataList.get(i).getSendCnt());
			map.put("date", dataList.get(i).getDate());

			list.add(map);
		}
		;

		Map dataMap = new HashMap();
		dataMap.put("dataList", list);
		ObjectMapper om = new ObjectMapper();
		String str = "";
		str = om.defaultPrettyPrintingWriter().writeValueAsString(dataMap);
		return str;
	}

	@Override
	public String searchWorker(ChartVo chartVo) throws Exception {
		SqlSession sql = getSqlSession();
		List<ChartVo> dataList = sql.selectList(namespace + "searchWorker", chartVo);

		List list = new ArrayList<ChartVo>();
		for (int i = 0; i < dataList.size(); i++) {
			Map map = new HashMap();

			map.put("name", URLEncoder.encode(dataList.get(i).getName(), "utf-8"));
			map.put("id", dataList.get(i).getId());

			list.add(map);
		}
		;

		Map dataMap = new HashMap();
		dataMap.put("dataList", list);
		ObjectMapper om = new ObjectMapper();
		String str = "";
		str = om.defaultPrettyPrintingWriter().writeValueAsString(dataMap);
		return str;
	}

	@Override
	public String getReleaseInfo(ChartVo chartVo) throws Exception {
		SqlSession sql = getSqlSession();
		List<ChartVo> dataList = sql.selectList(namespace + "getReleaseInfo", chartVo);

		List list = new ArrayList<ChartVo>();
		for (int i = 0; i < dataList.size(); i++) {
			Map map = new HashMap();

			map.put("id", dataList.get(i).getId());
			map.put("prdNo", dataList.get(i).getPrdNo());
			map.put("spec", dataList.get(i).getSpec());
			map.put("lotNo", dataList.get(i).getLotNo());
			map.put("rcvCnt", dataList.get(i).getRcvCnt());
			map.put("sendCnt", dataList.get(i).getSendCnt());
			map.put("prdCnt", dataList.get(i).getPrdCnt());
			map.put("cmplCnt", dataList.get(i).getCmplCnt());
			map.put("stockCnt", dataList.get(i).getStockCnt());

			list.add(map);
		}
		;

		Map dataMap = new HashMap();
		dataMap.put("dataList", list);
		ObjectMapper om = new ObjectMapper();
		String str = "";
		str = om.defaultPrettyPrintingWriter().writeValueAsString(dataMap);
		return str;
	}

	@Override
	public String addPrdCmpl(ChartVo chartVo) throws Exception {
		SqlSession sql = getSqlSession();
		String str = "";

		try {
			int exist = (Integer) sql.selectOne(namespace + "chkCmpl", chartVo);

			if (exist > 0) {
				sql.update(namespace + "updateCmpl", chartVo);
			} else {
				sql.insert(namespace + "addPrdCmpl", chartVo);
			}

			chartVo.setOprNm((String) sql.selectOne(namespace + "getOpr", chartVo));

			if (chartVo.getOprNm().equals("0010")) {
				chartVo.setPreOpr("0000");
			} else if (chartVo.getOprNm().equals("0020")) {
				chartVo.setPreOpr("0010");
			} else if (chartVo.getOprNm().equals("0030")) {
				chartVo.setPreOpr("0020");
			} else if (chartVo.getOprNm().equals("1000")) {
				chartVo.setPreOpr("0030");
			}

			if (!chartVo.getOprNm().equals("0010")) {
				// 怨듭젙 �옱怨� +
				int stockExist = (Integer) sql.selectOne(namespace + "chkCurrentCmplStock", chartVo);
				if (stockExist > 0) {
					sql.update(namespace + "updateCurrnetCmplStock", chartVo);
				} else {
					sql.insert(namespace + "addCurrnetCmplStock", chartVo);
				}

				// �씠�쟾 怨듭젙 -
				int stockeNextExist = (Integer) sql.selectOne(namespace + "chkNextCmplStock", chartVo);
				if (stockeNextExist > 0) {
					sql.update(namespace + "minusNextCmplStock", chartVo);
				} else {
					sql.insert(namespace + "addNextCmplStock", chartVo);
				}
			}

			// mat_out_history
			// sql.insert(namespace + "addMatOut",chartVo);

			// if(chartVo.getOprNm().equals("0040")){
			// //異쒗븯 history
			//
			// for(int i = 1; i <=5 ; i++){
			// String lotNo = null;
			// int lotCnt = 0;
			// if(i==1){
			// lotNo = chartVo.getLot1();
			// lotCnt = chartVo.getCnt1();
			// }else if(i==2){
			// lotNo = chartVo.getLot2();
			// lotCnt = chartVo.getCnt2();
			// }else if(i==3){
			// lotNo = chartVo.getLot3();
			// lotCnt = chartVo.getCnt3();
			// }else if(i==4){
			// lotNo = chartVo.getLot4();
			// lotCnt = chartVo.getCnt4();
			// }else if(i==5){
			// lotNo = chartVo.getLot5();
			// lotCnt = chartVo.getCnt5();
			// }
			//
			// chartVo.setLotNo(lotNo);
			// chartVo.setLotCnt(lotCnt);
			//
			// int shipExist = (Integer) sql.selectOne(namespace + "chkShip", chartVo);
			//
			// if(shipExist>0){
			// sql.update(namespace + "updateShipCnt", chartVo);
			// }else{
			// sql.insert(namespace + "addShipCnt", chartVo);
			// }
			// }
			// }
			str = "success";
		} catch (Exception e) {
			str = "fail";
		}
		return str;
	}

	@Override
	public String addCmplStockHistory(String val) throws Exception {
		SqlSession sql = getSqlSession();

		JSONParser jsonParser = new JSONParser();
		JSONObject jsonObj = (JSONObject) jsonParser.parse(val);
		JSONArray jsonArray = (JSONArray) jsonObj.get("val");

		List<ChartVo> list = new ArrayList<ChartVo>();
		for (int i = 0; i < jsonArray.size(); i++) {
			JSONObject tempObj = (JSONObject) jsonArray.get(i);

			ChartVo chartVo = new ChartVo();
			chartVo.setId(tempObj.get("id").toString());
			chartVo.setPrdNo(tempObj.get("prdNo").toString());
			chartVo.setSendCnt(Integer.parseInt(tempObj.get("sendCnt").toString()));
			chartVo.setLotNo(tempObj.get("lotNo").toString());
			chartVo.setDate(tempObj.get("date").toString());
			chartVo.setOutLot(tempObj.get("outLot").toString());
			// chartVo.setPreSend(Integer.parseInt(tempObj.get("preSend").toString()));

			list.add(chartVo);

			// 異쒗븯 �엳�뒪�넗由�
			sql.update(namespace + "setLatestData", chartVo);
			chartVo.setCnt((String) sql.selectOne(namespace + "getCmplCnt", chartVo));
			chartVo.setRcvCnt((Integer) sql.selectOne(namespace + "getRcvCnt", chartVo));

			sql.insert(namespace + "addCmplStockHistory", chartVo);
			sql.update(namespace + "minusComplStock", chartVo);
		}

		String str = "";

		try {
			if (list.size() > 0)
				sql.insert(namespace + "addPrdData", list);
			str = "success";
		} catch (Exception e) {
			str = "fail";
		}

		return str;
	}

	@Override
	public String getStockStatus(ChartVo chartVo) throws Exception {
		SqlSession sql = getSqlSession();
		List<ChartVo> dataList = sql.selectList(namespace + "getStockStatus", chartVo);

		List list = new ArrayList<ChartVo>();
		for (int i = 0; i < dataList.size(); i++) {
			Map map = new HashMap();
			map.put("prdNo", URLEncoder.encode(dataList.get(i).getPrdNo(), "utf-8"));
			map.put("rcvCnt", dataList.get(i).getRcvCnt());
			map.put("cnt", dataList.get(i).getCnt());
			map.put("spec", dataList.get(i).getSpec());

			list.add(map);
		}
		;

		Map dataMap = new HashMap();
		dataMap.put("dataList", list);
		ObjectMapper om = new ObjectMapper();
		String str = "";
		str = om.defaultPrettyPrintingWriter().writeValueAsString(dataMap);
		return str;
	}

	@Override
	public String getReleaseInfoHist(ChartVo chartVo) throws Exception {
		SqlSession sql = getSqlSession();
		List<ChartVo> dataList = sql.selectList(namespace + "getReleaseInfoHist", chartVo);

		List list = new ArrayList<ChartVo>();
		for (int i = 0; i < dataList.size(); i++) {
			Map map = new HashMap();
			map.put("prdNo", URLEncoder.encode(dataList.get(i).getPrdNo(), "utf-8"));
			map.put("spec", dataList.get(i).getSpec());
			map.put("lotNo", dataList.get(i).getLotNo());
			map.put("outLot", dataList.get(i).getOutLot());
			map.put("date", dataList.get(i).getDate());
			map.put("outCnt", dataList.get(i).getOutCnt());

			list.add(map);
		}
		;

		Map dataMap = new HashMap();
		dataMap.put("dataList", list);
		ObjectMapper om = new ObjectMapper();
		String str = "";
		str = om.defaultPrettyPrintingWriter().writeValueAsString(dataMap);
		return str;
	}

	@Override
	public String getLotTracer(ChartVo chartVo) throws Exception {
		SqlSession sql = getSqlSession();
		List<ChartVo> dataList = sql.selectList(namespace + "getLotTracer", chartVo);

		List list = new ArrayList<ChartVo>();
		for (int i = 0; i < dataList.size(); i++) {
			Map map = new HashMap();
			map.put("prdNo", URLEncoder.encode(dataList.get(i).getPrdNo(), "utf-8"));
			map.put("oprNo", dataList.get(i).getOprNo());
			map.put("name", URLEncoder.encode(dataList.get(i).getName(), "utf-8"));
			map.put("worker", URLEncoder.encode(dataList.get(i).getWorker(), "utf-8"));
			map.put("lotNo", dataList.get(i).getLotNo());
			map.put("shipId", dataList.get(i).getShipId());
			map.put("shipLotNo", dataList.get(i).getShipLotNo());
			map.put("date", dataList.get(i).getDate());

			list.add(map);
		}
		;

		Map dataMap = new HashMap();
		dataMap.put("dataList", list);
		ObjectMapper om = new ObjectMapper();
		String str = "";
		str = om.defaultPrettyPrintingWriter().writeValueAsString(dataMap);
		return str;
	}

	@Override
	public String getLotNo(ChartVo chartVo) throws Exception {
		SqlSession sql = getSqlSession();
		List<ChartVo> dataList = sql.selectList(namespace + "getLotNo", chartVo);

		List list = new ArrayList<ChartVo>();
		for (int i = 0; i < dataList.size(); i++) {
			Map map = new HashMap();
			map.put("lotNo", dataList.get(i).getLotNo());

			list.add(map);
		}
		;

		Map dataMap = new HashMap();
		dataMap.put("dataList", list);
		ObjectMapper om = new ObjectMapper();
		String str = "";
		str = om.defaultPrettyPrintingWriter().writeValueAsString(dataMap);
		return str;
	};

	@Override
	public String getShipLotNo(ChartVo chartVo) throws Exception {
		SqlSession sql = getSqlSession();
		List<ChartVo> dataList = sql.selectList(namespace + "getShipLotNo", chartVo);

		List list = new ArrayList<ChartVo>();
		for (int i = 0; i < dataList.size(); i++) {
			Map map = new HashMap();
			map.put("lotNo", dataList.get(i).getLotNo());

			list.add(map);
		}
		;

		Map dataMap = new HashMap();
		dataMap.put("dataList", list);
		ObjectMapper om = new ObjectMapper();
		String str = "";
		str = om.defaultPrettyPrintingWriter().writeValueAsString(dataMap);
		return str;
	}

	@Override
	public String chkLogin(ChartVo chartVo) throws Exception {
		SqlSession sql = getSqlSession();
		String str = "";
		try {
			String exist = (String) sql.selectOne(namespace + "chkLogin", chartVo);

			if (exist != "" || exist != null) {
				str = exist;
			} else {
				str = "fail";
			}
		} catch (Exception e) {
			e.printStackTrace();
			str = "fail";
		}
		return str;
	}

	@Override
	public String getOutLotTracer(ChartVo chartVo) throws Exception {
		SqlSession sql = getSqlSession();
		List<ChartVo> dataList = sql.selectList(namespace + "getOutLotTracer", chartVo);

		List list = new ArrayList<ChartVo>();
		for (int i = 0; i < dataList.size(); i++) {
			Map map = new HashMap();
			map.put("prdNo", URLEncoder.encode(dataList.get(i).getPrdNo(), "utf-8"));
			map.put("oprNo", dataList.get(i).getOprNo());
			map.put("name", URLEncoder.encode(dataList.get(i).getName(), "utf-8"));
			map.put("worker", URLEncoder.encode(dataList.get(i).getWorker(), "utf-8"));
			map.put("lotNo", dataList.get(i).getLotNo());
			map.put("shipLotNo", dataList.get(i).getShipLotNo());
			map.put("date", dataList.get(i).getDate());
			map.put("shipId", dataList.get(i).getShipId());

			list.add(map);
		}
		;

		Map dataMap = new HashMap();
		dataMap.put("dataList", list);
		ObjectMapper om = new ObjectMapper();
		String str = "";
		str = om.defaultPrettyPrintingWriter().writeValueAsString(dataMap);
		return str;
	}

	@Override
	public String getOprNoList(ChartVo chartVo) throws Exception {
		SqlSession sql = getSqlSession();
		List<ChartVo> dataList = sql.selectList(namespace + "getOprNoList", chartVo);

		List list = new ArrayList<ChartVo>();
		for (int i = 0; i < dataList.size(); i++) {
			Map map = new HashMap();
			map.put("oprNm", dataList.get(i).getOprNm());

			list.add(map);
		}
		;

		Map dataMap = new HashMap();
		dataMap.put("dataList", list);
		ObjectMapper om = new ObjectMapper();
		String str = "";
		str = om.defaultPrettyPrintingWriter().writeValueAsString(dataMap);
		return str;
	}

	@Override
	public String transferOprCnt(String val) throws Exception {
		SqlSession sql = getSqlSession();

		JSONParser jsonParser = new JSONParser();
		JSONObject jsonObj = (JSONObject) jsonParser.parse(val);
		JSONArray jsonArray = (JSONArray) jsonObj.get("val");

		List<ChartVo> list = new ArrayList<ChartVo>();
		for (int i = 0; i < jsonArray.size(); i++) {
			JSONObject tempObj = (JSONObject) jsonArray.get(i);

			ChartVo chartVo = new ChartVo();
			chartVo.setPrdNo(tempObj.get("prdNo").toString());
			chartVo.setOprNm(tempObj.get("oprNm").toString());
			chartVo.setDate(tempObj.get("date").toString());
			chartVo.setPreOpr(tempObj.get("preOpr").toString());
			chartVo.setSendCnt(Integer.parseInt(tempObj.get("sendCnt").toString()));

			int StockeExist = (Integer) sql.selectOne(namespace + "chkPreOprCnt", chartVo);
			// �씠�쟾 怨듭젙 �닔�웾 -
			if (StockeExist > 0) {
				sql.update(namespace + "minusPreOprCnt", chartVo);
			} else {
				sql.insert(namespace + "addPreOprCnt", chartVo);
			}

			int exist = (Integer) sql.selectOne(namespace + "chkOprCnt", chartVo);
			// 怨듭젙 �닔�웾 ++
			if (exist > 0) {
				sql.update(namespace + "updateOprCnt", chartVo);
			} else {
				sql.insert(namespace + "addOprCnt", chartVo);
			}

			list.add(chartVo);
		}

		String str = "";

		try {

			if (list.size() > 0)
				sql.insert(namespace + "addOutCnt", list);
			str = "success";
		} catch (Exception e) {
			str = "fail";
		}

		return str;
	}

	@Override
	public String getOprStockInfo(ChartVo chartVo) throws Exception {
		SqlSession sql = getSqlSession();
		List<ChartVo> dataList = sql.selectList(namespace + "getOprStockInfo", chartVo);

		List list = new ArrayList<ChartVo>();
		for (int i = 0; i < dataList.size(); i++) {
			Map map = new HashMap();

			map.put("oprNm", dataList.get(i).getOprNm());
			map.put("id", dataList.get(i).getId());
			map.put("prdNo", URLEncoder.encode(dataList.get(i).getPrdNo(), "utf-8"));
			map.put("stockCnt", dataList.get(i).getStockCnt());

			list.add(map);
		}
		;

		Map dataMap = new HashMap();
		dataMap.put("dataList", list);
		ObjectMapper om = new ObjectMapper();
		String str = "";
		str = om.defaultPrettyPrintingWriter().writeValueAsString(dataMap);
		return str;
	}

	@Override
	public String getReOpList(ChartVo chartVo) throws Exception {
		SqlSession sql = getSqlSession();
		List<ChartVo> dataList = sql.selectList(namespace + "getReOpList", chartVo);

		List list = new ArrayList<ChartVo>();
		for (int i = 0; i < dataList.size(); i++) {
			Map map = new HashMap();

			map.put("dvcName", URLEncoder.encode(dataList.get(i).getDvcName(), "utf-8"));
			map.put("date", dataList.get(i).getDate());
			map.put("prgName", dataList.get(i).getPrgName());
			map.put("chkSpdLoad", dataList.get(i).getChkSpdLoad());
			map.put("cylTm", dataList.get(i).getCylTm());
			map.put("cylAvgTm", dataList.get(i).getCylAvgTm());
			map.put("cylSdTm", dataList.get(i).getCylSdTm());
			map.put("totalSpdLoad", dataList.get(i).getTotalSpdLoad());
			map.put("totalAvgSpdLoad", dataList.get(i).getTotalAvgSpdLoad());
			map.put("totalSdSpdLoad", dataList.get(i).getTotalSdSpdLoad());
			map.put("smplCnt", dataList.get(i).getSmplCnt());
			map.put("sDate", dataList.get(i).getSDate());

			list.add(map);
		}
		;

		Map dataMap = new HashMap();
		dataMap.put("dataList", list);
		ObjectMapper om = new ObjectMapper();
		String str = "";
		str = om.defaultPrettyPrintingWriter().writeValueAsString(dataMap);
		return str;
	};

	@Override
	public String drawCircle(ChartVo chartVo) throws Exception {
		SqlSession sql = getSqlSession();
		List<ChartVo> dataList = sql.selectList(namespace + "drawCircle", chartVo);

		List list = new ArrayList<ChartVo>();
		for (int i = 0; i < dataList.size(); i++) {
			Map map = new HashMap();

			map.put("id", dataList.get(i).getId());
			map.put("dvcId", dataList.get(i).getDvcId());
			map.put("dvcName", URLEncoder.encode(dataList.get(i).getName(), "utf-8"));
			map.put("x", dataList.get(i).getX());
			map.put("y", dataList.get(i).getY());
			map.put("w", dataList.get(i).getWidth());
			map.put("fontSize", dataList.get(i).getFontSize());
			map.put("lastChartStatus", dataList.get(i).getLastChartStatus());
			map.put("noTarget", dataList.get(i).getNoTarget());
			map.put("isChg", dataList.get(i).getIsChg());
			map.put("chgTy", dataList.get(i).getChgTy());
			map.put("type", dataList.get(i).getType());

			list.add(map);
		}
		;

		Map dataMap = new HashMap();
		dataMap.put("dataList", list);
		ObjectMapper om = new ObjectMapper();
		String str = "";
		str = om.defaultPrettyPrintingWriter().writeValueAsString(dataMap);
		return str;
	};

	@Override
	public String login(ChartVo chartVo) throws Exception {
		SqlSession sql = getSqlSession();
		String str = "";
		ChartVo accountInfo = new ChartVo();
		try {
			accountInfo = (ChartVo) sql.selectOne(namespace + "login", chartVo);
			if (accountInfo.getId() != null) {
				Map map = new HashMap();

				map.put("id", accountInfo.getId());
				map.put("level", accountInfo.getLv());
				map.put("name", URLEncoder.encode(accountInfo.getName(), "UTF-8"));
				map.put("message", "success");

				ObjectMapper om = new ObjectMapper();
				str = om.defaultPrettyPrintingWriter().writeValueAsString(map);
			}
		} catch (Exception e) {
			e.printStackTrace();
			Map map = new HashMap();
			map.put("message", "failed");
			ObjectMapper om = new ObjectMapper();
			str = om.defaultPrettyPrintingWriter().writeValueAsString(map);

		}

		return str;
	}

	@Override
	public boolean checkId(ChartVo chartVo) throws Exception {
		SqlSession sql = getSqlSession();

		int num = (int) sql.selectOne(namespace + "checkId", chartVo);
		if (num > 0) {
			return true;
		} else {
			return false;
		}

	}

	@Override
	public boolean doSignIn(ChartVo chartVo) throws Exception {
		SqlSession sql = getSqlSession();

		try {
			sql.insert(namespace + "doSignIn", chartVo);
		} catch (Exception e) {
			e.printStackTrace();
			return false;
		}
		return true;
	}

	@Override
	public String getComNameList() throws Exception {

		SqlSession sql = getSqlSession();
		List<ChartVo> dataList = sql.selectList(namespace + "getComNameList");

		List list = new ArrayList<ChartVo>();
		for (int i = 0; i < dataList.size(); i++) {
			Map map = new HashMap();

			map.put("shopId", dataList.get(i).getShopId());
			map.put("name", dataList.get(i).getName());

			list.add(map);
		}
		;

		Map dataMap = new HashMap();
		dataMap.put("dataList", list);
		ObjectMapper om = new ObjectMapper();
		String str = "";
		str = om.defaultPrettyPrintingWriter().writeValueAsString(dataMap);
		return str;
	}

	@Override
	public String setUser(UserVO userVO) throws Exception {
		SqlSession sql = getSqlSession();
		
		String result = "";
		
		int count = (int) sql.selectOne(namespace + "getUserCount", userVO.getId());
		
		System.out.println(userVO.getName());
		
		if(count==0) {
			try {
				sql.insert(namespace + "setUser", userVO);
				result="success";
			}catch(Exception e) {
				e.printStackTrace();
				result="failed";
			}
			
		}else {
			result = "duple";
		}
		
		return result;
	}
	
	@Override
	public String setUsernameAndPassword(UserVO userVO) throws Exception {
		SqlSession sql = getSqlSession();
		
		String result = "";
		
		int count = (int) sql.selectOne(namespace + "getUserCount", userVO.getId());
		
		System.out.println(userVO.getName());
		
		if(count==1) {
			try {
				sql.update(namespace + "setUsernameAndPassword", userVO);
				result="success";
			}catch(Exception e) {
				e.printStackTrace();
				result="failed";
			}
			
		}else {
			result = "not_found";
		}
		
		return result;
	}
	
	
	
	@Override
	public String getUser(UserVO userVO) throws Exception {
		SqlSession sql = getSqlSession();
		UserVO user = (UserVO)sql.selectOne(namespace + "getUser", userVO);

		Map dataMap = new HashMap();
		dataMap.put("id", user.getId());
		System.out.println(user.getName());
		dataMap.put("name", URLEncoder.encode(user.getName(), "UTF-8"));
		ObjectMapper om = new ObjectMapper();
		String str = "";
		str = om.defaultPrettyPrintingWriter().writeValueAsString(dataMap);
		return str;
	}


	@Override
	public String getCheckedList(ChartVo chartVo) throws Exception {
		SqlSession sql = getSqlSession();
		
		List<ChartVo> dataList = sql.selectList(namespace + "getCheckedList", chartVo);
		List list = new ArrayList<ChartVo>();
		for (int i = 0; i < dataList.size(); i++) {
			Map map = new HashMap();
			map.put("dvcId", dataList.get(i).getDvcId());
			map.put("name", dataList.get(i).getName());
			map.put("jig", URLEncoder.encode(dataList.get(i).getJig(), "UTF-8"));
			map.put("checked", dataList.get(i).getChecked());

			list.add(map);
		}
		;

		Map dataMap = new HashMap();
		dataMap.put("dataList", list);
		ObjectMapper om = new ObjectMapper();
		String str = "";
		str = om.defaultPrettyPrintingWriter().writeValueAsString(dataMap);
		return str;

	}

	@Override
	public String pushSave(ChartVo chartVo) throws Exception {
		SqlSession sql = getSqlSession();
		
		List<ChartVo> dataList = new ArrayList<ChartVo>();
		for(int i=0;i<chartVo.getList().length;i++) {
			ChartVo temp = new ChartVo();
			temp.setId(chartVo.getId());
			temp.setShopId(chartVo.getShopId());
			temp.setDvcId(chartVo.getList()[i]);
			temp.setUuid(chartVo.getUuid());
			dataList.add(temp);
		}
		
//		System.out.println("uuid" + chartVo.getUuid());
		
		sql.delete(namespace+"deletePushReg", chartVo);
		sql.insert(namespace+"pushSave", dataList);
		
		
//		if(chartVo.getHoliday().equals("true")) {
//			logger.info("holiday = true");
//		}
//			
//		logger.info("holiday : " + chartVo.getHoliday());
//		logger.info("night   : " + chartVo.getNightTime());
//		logger.info("weekday   : " + chartVo.getWeekday());
		
		sql.delete(namespace+"deleteUuid", chartVo);
		sql.insert(namespace+"alarmSetting", chartVo);
		
		return "success";
	}

	@Override
	public String logout(ChartVo chartVo) throws Exception {
		String result="failed";
		SqlSession sql = getSqlSession();
		try {
			sql.delete(namespace+"logout", chartVo);
			result="success";
		}catch(Exception e) {
			e.printStackTrace();
		}
		return result;
		
	}

	@Override
	public String getStatus(ChartVo chartVo) throws Exception {
		SqlSession sql = getSqlSession();
		List <ChartVo> statusList = sql.selectList(namespace +"getStatus", chartVo);
		
		List list = new ArrayList();
		for(int i = 0; i < statusList.size(); i++){
			Map map = new HashMap();
			map.put("dvcId", statusList.get(i).getDvcId());
			map.put("startDateTime", statusList.get(i).getStartDateTime());
			map.put("endDateTime", statusList.get(i).getEndDateTime());
			map.put("chart_status", statusList.get(i).getChartStatus());
			map.put("spdOverride", statusList.get(i).getSpdOverride());
			map.put("diff", statusList.get(i).getTimeDiff());
			
			list.add(map);
		};
		
		Map statusMap = new HashMap();
		statusMap.put("statusList", list);
		
		ObjectMapper om = new ObjectMapper();
		String str = "";
		str = om.defaultPrettyPrintingWriter().writeValueAsString(statusMap);
		return str;
	}

	@Override
	public ChartVo getAlarmSetting(ChartVo chartVo) throws Exception {
		SqlSession sql = getSqlSession();
		chartVo = (ChartVo) sql.selectOne(namespace +"getAlarmSetting", chartVo);

//		logger.info("chart : " + chartVo);
		
		return chartVo;
	}
	
	@Override
	public String getMachineList(ChartVo chartVo) throws Exception {
		SqlSession sql = getSqlSession();
		List <ChartVo> machineList = sql.selectList(namespace +"getMachineList", chartVo);
		
		List list = new ArrayList();
		for(int i = 0; i < machineList.size(); i++){
			Map map = new HashMap();
			map.put("dvcId", machineList.get(i).getDvcId());
			map.put("name", machineList.get(i).getName());
			map.put("ip", machineList.get(i).getIp());
			
			list.add(map);
		};
		
		Map machineListMap = new HashMap();
		machineListMap.put("machineList", list);
		
		ObjectMapper om = new ObjectMapper();
		String str = "";
		str = om.defaultPrettyPrintingWriter().writeValueAsString(machineListMap);
		return str;
	}
	
	@Override
	public String chkMachineStatus(ChartVo chartVo) throws Exception {
		
		SqlSession sql = getSqlSession();
//		List <ChartVo> statusList = sql.selectList(namespace +"chkMachineStatus", chartVo);
		String status = (String) sql.selectOne(namespace +"chkMachineStatus", chartVo);
		String rare = (String) sql.selectOne(namespace +"chkMachineStatus2", chartVo);
		
//		logger.info("STATUS : " + status);
//		logger.info("RAREDATA : " + rare);
		
		Map map = new HashMap();
		
		if(status.equals("CUT") || rare.equals("CUT")) {
//			logger.info("CUT");
			status = "CUT";
		}
		
		if(status.equals("IN-CYCLE") || rare.equals("IN-CYCLE")) {
//			logger.info("IN-CYCLE");
			status = "IN-CYCLE";
		}
		
		map.put("status", status);
						
		List list = new ArrayList();
		
		list.add(map);
//		for(int i = 0; i < statusList.size(); i++){
//			
//			logger.info(statusList.get(i).getStatus());
//						
//			
//			Map map = new HashMap();
//			map.put("status", statusList.get(i).getStatus());
//			
//			list.add(map);
//		};
		
		Map machineListMap = new HashMap();
		machineListMap.put("status", list);
		
		ObjectMapper om = new ObjectMapper();
		String str = "";
		str = om.defaultPrettyPrintingWriter().writeValueAsString(machineListMap);
		return str;
	}

};